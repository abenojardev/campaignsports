<?php

//session_start(); /* if not already done */
//$post = $_POST;
  
 //Online Payment
  //if ( $post['paymentMethod'] == 'online' ) {
    //if ( isset($post['donationValueCustom']) && $post['donationValueCustom'] != "" ) {
      //$donationValue = $post['donationValueCustom'];
    //} else {
      //$donationValue = $post['donationValue'];
    //}
      
    //$email = (isset($post['intl']) && $post['intl']) ? $post['emailI'] : $post['email'];
    //$city = (isset($post['city']) && $post['city']) ? $post['city'] : $post['cityI'];
    //$state = (isset($post['state']) && $post['state']) ? $post['state'] : $post['stateI'];
    //$zip = (isset($post['zip']) && $post['zip']) ? $post['zip'] : $post['zipI'];

class SoapClientHMAC extends SoapClient {
  public function __doRequest($request, $location, $action, $version, $one_way = NULL) {
  global $context;
  $hmackey = "VQ9uj1iYxhRxsu4Tfq1NpY_CvVtEgzHd"; // Orig: QANi0CiGXt9qShl4LCszrARXX74NtJfp <-- Insert your HMAC key here
  $keyid = "514920"; // <-- Insert the Key ID here Orig: 260051
  $hashtime = date("c");
  $hashstr = "POST\ntext/xml; charset=utf-8\n" . sha1($request) . "\n" . $hashtime . "\n" . parse_url($location,PHP_URL_PATH);
  $authstr = base64_encode(hash_hmac("sha1",$hashstr,$hmackey,TRUE));
  if (version_compare(PHP_VERSION, '5.6.25') == 1) {
    ini_set("user_agent", "PHP-SOAP/" . PHP_VERSION . "\r\nAuthorization: GGE4_API " . $keyid . ":" . $authstr . "\r\nx-gge4-date: " . $hashtime . "\r\nx-gge4-content-sha1: " . sha1($request));
  } else {
    stream_context_set_option($context,array("http" => array("header" => "authorization: GGE4_API " . $keyid . ":" . $authstr . "\r\nx-gge4-date: " . $hashtime . "\r\nx-gge4-content-sha1: " . sha1($request))));
  }
    return parent::__doRequest($request, $location, $action, $version, $one_way);  // Line 49 is the SOAP error on credentials change..mks
  }
  
  public function SoapClientHMAC($wsdl, $options = NULL) {
  global $context;
  $context = stream_context_create();
  $options['stream_context'] = $context;
  return parent::SoapClient($wsdl, $options);
  }
}

$trxnProperties = array(
  "User_Name"=>"",
  "Secure_AuthResult"=>"",
  "Ecommerce_Flag"=>"",
  "XID"=>"",
  "ExactID"=>$_POST["ddlPOS_ExactID"],            //Payment Gateway
  "CAVV"=>"",
  "Password"=>"hrFGdG9O2qdjm1vBZMeez6umZFbRx6uQ",     //Gateway Password Orig: 4cdnoMAU3U67BDn1eth0ymQsXHX1vW6J
  "CAVV_Algorithm"=>"",
  "Transaction_Type"=>$_POST["ddlPOS_Transaction_Type"],//Transaction Code I.E. Purchase="00" Pre-Authorization="01" etc.
  //"Reference_No"=>$_POST["tbPOS_Reference_No"],
 // "Customer_Ref"=>$_POST["tbPOS_Customer_Ref"],
 // "Reference_3"=>$_POST["tbPOS_Reference_3"],
  //"Client_IP"=>"",                              //This value is only used for fraud investigation.
  "Client_Email"=>$_POST["tb_Client_Email"],      //This value is only used for fraud investigation.
  "Language"=>$_POST["ddlPOS_Language"],        //English="en" French="fr"
  "Card_Number"=>$_POST["tbPOS_Card_Number"],       //For Testing, Use Test#s VISA="4111111111111111" MasterCard="5500000000000004" etc.
  "Expiry_Date"=>$_POST["ddlPOS_Expiry_Date_Month"] . $_POST["ddlPOS_Expiry_Date_Year"],//This value should be in the format MM/YY.
  "CardHoldersName"=>$_POST["tbPOS_CardHoldersName"],
  //"Track1"=>"",
 // "Track2"=>"",
  //"Authorization_Num"=>$_POST["tbPOS_Authorization_Num"],
  //"Transaction_Tag"=>$_POST["tbPOS_Transaction_Tag"],
  "DollarAmount"=>$_POST["tbPOS_DollarAmount"],
  "VerificationStr1"=>$_POST["tbPOS_VerificationStr1"],
  "VerificationStr2"=>"",
  //"CVD_Presence_Ind"=>"",
  //"Secure_AuthRequired"=>"",
  "Currency"=>"",
  //"PartialRedemption"=>"",
  
  // Level 2 fields 
  //"ZipCode"=>$_POST["tbPOS_ZipCode"],
  //"Tax1Amount"=>$_POST["tbPOS_Tax1Amount"],
  //"Tax1Number"=>$_POST["tbPOS_Tax1Number"],
  //"Tax2Amount"=>$_POST["tbPOS_Tax2Amount"],
  //"Tax2Number"=>$_POST["tbPOS_Tax2Number"],
  
  //"SurchargeAmount"=>$_POST["tbPOS_SurchargeAmount"], //Used for debit transactions only
 // "PAN"=>$_POST["tbPOS_PAN"]              //Used for debit transactions only
  );

// change url from "https://api.demo.globalgatewaye4.firstdata.com/transaction/v12/wsdl");
$client = new SoapClientHMAC("https://api.globalgatewaye4.firstdata.com/transaction/v12/wsdl"); 
$trxnResult = $client->SendAndCommit($trxnProperties);


if(@$client->fault){
    // there was a fault, inform
    print "<B>FAULT:  Code: {$client->faultcode} <BR />";
    print "String: {$client->faultstring} </B>";
    $trxnResult["CTR"] = "There was an error while processing. No TRANSACTION DATA IN CTR!";
}
//Uncomment the following commented code to display the full results.

echo "<H3><U>Transaction Properties BEFORE Processing</U></H3>";
echo "<TABLE border='0'>\n";
echo " <TR><TD><B>Property</B></TD><TD><B>Value</B></TD></TR>\n";
foreach($trxnProperties as $key=>$value){
    echo " <TR><TD>$key</TD><TD>:$value</TD></TR>\n";
}
echo "</TABLE>\n";

echo "<H3><U>Transaction Properties AFTER Processing</U></H3>";
echo "<TABLE border='0'>\n";
echo " <TR><TD><B>Property</B></TD><TD><B>Value</B></TD></TR>\n";
foreach($trxnResult as $key=>$value){
    $value = nl2br($value);
    echo " <TR><TD valign='top'>$key</TD><TD>:$value</TD></TR>\n";
}
echo "</TABLE>\n";


// kill object
unset($client);
?>

<html>
<head>
<title>VPOS - Sample Code</title>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
</head>

<body>
    <table>
      <tr><td>
      <table cellSpacing="0" cellPadding="0" width="660" align="left" border="0">
<tr>
  <td><font face="verdana,arial,helvetica" size="5"><b>First Data Global Gateway e4 POS</b></font></td>
</tr>
          </table></td></tr>
          <tr><td>
          <table cellSpacing="6" cellPadding="0" width="660" align="left" border="2">
            <tr>
              <td align="left" valign="top">
            <?php 
              foreach($trxnResult as $key=>$value){
                if ($key == "CTR") {
                    $value = nl2br($value);
                  print $value;
                }
              }
            ?></td>
                    <!-- NOTE: chr(10) is the ASCII equivalent of the "Line Feed" character -->
            </tr>
            <tr>
              <td align="center" valign="top"><a href="javascript:history.back();">Perform Another Transaction</a></td>
            </tr>
      </table></td></tr>
    </table>
</body>
</html>
