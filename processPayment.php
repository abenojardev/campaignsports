<?php
	session_start(); /* if not already done */
	$post = $_POST;
	
	// Online Payment
	if ( $post['paymentMethod'] == 'online' ) {
		if ( isset($post['donationValueCustom']) && $post['donationValueCustom'] != "" ) {
			$donationValue = $post['donationValueCustom'];
		} else {
			$donationValue = $post['donationValue'];
		}
			
		$email = (isset($post['intl']) && $post['intl']) ? $post['emailI'] : $post['email'];
		$city = (isset($post['city']) && $post['city']) ? $post['city'] : $post['cityI'];
		$state = (isset($post['state']) && $post['state']) ? $post['state'] : $post['stateI'];
		$zip = (isset($post['zip']) && $post['zip']) ? $post['zip'] : $post['zipI'];
			
		// Build a simple transaction XML string
		$xml ="
		<order>
			
			<orderoptions>
				<ordertype>Sale</ordertype>
			</orderoptions>
			<merchantinfo>
				<configfile>1001268121</configfile> <!-- CHANGE THIS TO YOUR STORE NUMBER -->
			</merchantinfo>
			<creditcard>
				<cardnumber>".$post['num']."</cardnumber>
				<cardexpmonth>".$post['cardExpMo']."</cardexpmonth>
				<cardexpyear>".$post['cardExpYr']."</cardexpyear> 
			</creditcard>
			<payment>
				<chargetotal>".$donationValue."</chargetotal>
			</payment>
			<billing>
				<name>".$post['fname']." ".$post['lname']."</name>
				<address1>".$post['address']."</address1>
				<address2>".$post['address2']."</address2>
				<city>".$city."</city>
				<state>".$state."</state>
				<zip>".$zip."</zip>
			</billing>
		</order>";
		
		$host	= "secure.linkpt.net";
		//$host	= "staging.linkpt.net"; // test host
		$port	= 1129;
		$cert	= "1001268121.pem"; // change this to the name and location of your certificate file
		//$cert	= "1909375010.pem"; // test certificate file
		
		$hoststring = "https://".$host.":".$port."/LSGSXML";
		
		// use PHP built-in curl functions
		$ch = curl_init ();
		curl_setopt ($ch, CURLOPT_URL,$hoststring);
		curl_setopt ($ch, CURLOPT_POST, 1); 
		curl_setopt ($ch, CURLOPT_POSTFIELDS, $xml); // the string we built above
		curl_setopt ($ch, CURLOPT_SSLCERT, $cert);
		curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
		//curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
												
		//  send the string to LSGS
		$result = curl_exec ($ch);
	
		if (strlen($result) < 2)    // no response
			$result = "<r_error>Could not execute curl.</r_error>"; 
		
		// look at the  xml that comes back
		//print ("response:  $result\n\n<br><br>");
		
		// Process the XML from here.... 
		// Or OPTIONALLY - you could convert XML to an array
		preg_match_all ("/<(.*?)>(.*?)\</", $result, $outarr, PREG_SET_ORDER);
		
		$n = 0;
		while (isset($outarr[$n]))
		{
			$retarr[$outarr[$n][1]] = strip_tags($outarr[$n][0]);
			$n++; 
		}
	
		while (list($key, $value) = each($retarr)) {
			$post[$key] = $value;
			//echo "$key = $value <br>";
		}
		
		require_once($_SESSION['relative_path'] . 'inc/donation/class/DonationUser.php');
		$result = DonationUser::makeDonation($post);
	} 	
	// Check Payment
	else if ( $post['paymentMethod'] == 'check' ) {
		require_once($_SESSION['relative_path'] . 'inc/donation/class/DonationUser.php');
		$result = DonationUser::makeDonation($post);
	}
	// PayPal Payment
	else if ( $post['paymentMethod'] == 'paypal' ) {
		require_once($_SESSION['relative_path'] . 'inc/donation/class/DonationUser.php');
		$result = DonationUser::paypalDonation($post);
	}

?>

<html>
<body>
<form id="frmProcess" name="frmProcess" method="post" action="index.php">
	<input type='hidden' name='submitAction' value='donationResult' />
<?php
	foreach($post AS $postValue => $postData) {
		echo "<input type='hidden' name='".$postValue."' value='".$postData."' />";
	}
?>
</form>
</body>
</html>
