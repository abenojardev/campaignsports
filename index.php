<?php
	
	if ($_SERVER['HTTP_HOST'] != 'localhost' && $_SERVER['HTTP_HOST'] != '192.168.0.125') {
		if ($_SERVER['SERVER_PORT'] != 443) {
			$url = "https://". $_SERVER['SERVER_NAME'] . ":443".$_SERVER['REQUEST_URI'];
			header("Location: $url");
		}
	}
	
	session_start(); /* if not already done */
	require_once('inc/common/path/init.php');
	$relPath = $_SESSION['relative_path'];
	
	// Load include files
// 	require_once($relPath . 'inc/team/layout/layout.php');
	require_once($relPath . 'inc/admin/layout/layout.php');
	require_once($relPath . 'inc/common/config.php');
	
	$post = $_POST;
	$get = $_GET;

	// Login / Logout
	if ( ( isset($get['action']) && $get['action'] == 'login' )
		|| ( isset($post['action']) && $post['action'] == 'login' ) ) {
		
		require_once($relPath . 'inc/donation/class/DonationUser.php');
		
		$teamID = $post['teamID'];
		if ( $teamID == "1000" || $teamID == "1002" )
			$loginCheck = DonationUser::loginOld($post);
		else
			$loginCheck = DonationUser::login($post);
		
		if ($loginCheck)
		{
			$error = $loginCheck;
			echo $error;
		}
		else 
		{
			include_once($relPath . 'inc/donation/layout/login.php');
		}
		
	}
	else if ( ( isset($get['action']) && $get['action'] == 'logout' )
		|| ( isset($post['action']) && $post['action'] == 'logout' ) ) {
		require_once($relPath . 'inc/donation/class/DonationUser.php');
		DonationUser::logout();
		include($relPath . 'inc/donation/display/login_page.php');
	}
	
	// Finalize Donation User Login, continue with donation page
	else if ( isset($post["submitAction"]) && ($post["submitAction"]) && ($post["submitAction"] == "donorLogin") ) {
		if ($post['donor_set'] == 'null') {
			unset($_SESSION["donation_user"]);
			include($relPath . 'inc/donation/display/login_page.php');
		} else {
			$_SESSION["donation_user"] = $post['donor_set'];
			
			if ( $_SESSION['current_folder'] != "1000" && $_SESSION['current_folder'] != "1002" )
				$_SESSION["player_id"] = $post['donor_set'];
				
			include_once($relPath . 'inc/donation/display/donation_page.php');
		}
	}
	
	// Admin Manual Donation
	else if ( isset($post["submitAction"]) && ($post["submitAction"]) && ($post["submitAction"] == "adminManualDonation") ) {
		include_once($relPath . 'inc/donation/display/donation_page.php');
	}
	
	else if ( isset($post["submitAction"]) && ($post["submitAction"]) && ($post["submitAction"] == "adminManualCheckDonation") ) {
		include_once($relPath . 'inc/donation/display/donationCheck_page.php');
	}
	
	// Response Page after paypal processing
	else if ( isset($post["custom"]) && ($post["custom"]) && ($post["custom"] == "payPal_return") ) {
		// Return page, no DB insert.  Display only
		if ( isset($_SESSION['current_folder']) && $_SESSION['current_folder'] != "" ) {
			include_once($relPath . 'inc/donation/display/donationResult_page.php');
		// Data sent from PayPal on end of transaction.  NO page displayed.
		} else {
			require_once($_SESSION['relative_path'] . 'inc/donation/class/DonationUser.php');
			DonationUser::paypalDonation($post);
		}
	}
	
	// Response Page after payment processing
	else if ( isset($post["submitAction"]) && ($post["submitAction"]) && ($post["submitAction"] == "donationResult") ) {
		include_once($relPath . 'inc/donation/display/donationResult_page.php');
	}
	
	// Donation User Logged in, continue with donation
	else if ( isset($_SESSION["donation_user"]) && ($_SESSION["donation_user"]) && ($_SESSION["donation_user"] != "") ) {
		include_once($relPath . 'inc/donation/display/donation_page.php');
	}
	
	// Donation User NOT Logged in, show donation login
	else {
		include($relPath . 'inc/donation/display/login_page.php');
	}

	
?>
