<?php
 
// Build a simple transaction XML string
	$xml ="
    <order>
        <merchantinfo>
            <!-- Replace with your STORE NUMBER or STORENAME-->  
            <configfile>100126812</configfile> 
        </merchantinfo>
        <orderoptions>
            <ordertype>Sale</ordertype>
        </orderoptions>
        <payment>
            <chargetotal>".$donationValue."</chargetotal>
        </payment>
        <creditcard>
            <cardnumber>".$post['num']."</cardnumber>
            <cardexpmonth>".$post['cardExpMo']."</cardexpmonth>
            <cardexpyear>".$post['cardExpYr']."</cardexpyear> 
        </creditcard>
        <payment>
            <chargetotal>15.00</chargetotal>
        </payment>
    </order>";
	
	$host	= "staging.linkpt.net";
	$port	= 1129;
	$cert	= "1909375010.pem"; // change this to the name and location of your certificate file
	
	
	$hoststring = "https://".$host.":".$port."/LSGSXML";
	
	// use PHP built-in curl functions
	$ch = curl_init ();
	curl_setopt ($ch, CURLOPT_URL,$hoststring);
	curl_setopt ($ch, CURLOPT_POST, 1); 
	curl_setopt ($ch, CURLOPT_POSTFIELDS, $xml); // the string we built above
	curl_setopt ($ch, CURLOPT_SSLCERT, $cert);
	curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
//	curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
//	curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
	
//	curl_setopt ($ch, CURLOPT_VERBOSE, 1);	// optional - verbose debug output
											// not for production use
											
	//  send the string to LSGS
	$result = curl_exec ($ch);

	if (strlen($result) < 2)    // no response
	{
		$result = "<r_error>Could not execute curl.</r_error>"; 
	}
	
	
	// look at the  xml that comes back
	print ("response:  $result\n\n<br><br>");
	
	// Process the XML from here.... 
	
	
	// Or OPTIONALLY - you could convert XML to an array
	preg_match_all ("/<(.*?)>(.*?)\</", $result, $outarr, PREG_SET_ORDER);
	
	$n = 0;
	while (isset($outarr[$n]))
	{
		$retarr[$outarr[$n][1]] = strip_tags($outarr[$n][0]);
		$n++; 
	}

	while (list($key, $value) = each($retarr))
		echo "$key = $value <br>";
	
?>
