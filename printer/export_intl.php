<?php
    session_start(); /* if not already done */
	
 	$get = $_GET;
	$team = $get['id'];
	
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
// prior to v2 new printer export format 	
//	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', c.tID AS 'teamCode', t.password AS 'password',";
//	$query .= " c.prefix, c.fname, c.lname, c.relationship, c.prefix2, c.fname2, c.lname2, c.relationship2, c.address, c.address2, c.city, c.state, CONCAT('=\"', c.zip, '\"') as zip, c.email, c.ID";
//	$query .= " FROM contacts c";
//	$query .= " INNER JOIN players p ON c.pID = p.ID";
//	$query .= " INNER JOIN teams t ON c.tID = t.ID";
//	$query .= " WHERE c.tID = $team";
	
	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', c.tID AS 'teamCode', t.password AS 'password',";
	$query .= " c.prefix, c.fname, c.lname, c.relationship AS 'INS2', c.fname2, c.relationship2 AS 'INS3', c.address, c.address2, c.city, c.state, CONCAT('=\"', c.zip, '\"') as zip, c.email, c.ID";
	$query .= " FROM contacts c";
	$query .= " INNER JOIN players p ON c.pID = p.ID";
	$query .= " INNER JOIN teams t ON c.tID = t.ID";
	$query .= " WHERE c.tID = $team AND c.intl IS NOT NULL";
	
	$DB = new DB();
	$contacts = $DB->select_custom($query);
 	
	$contents = getExcelData($contacts);
	//print_r($contacts);
	//break;
	
	$filename = "campaign".$team.".xls";

	//prepare to give the user a Save/Open dialog...
	header("Cache-Control: cache, must-revalidate");
	header("Pragma: public");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Transfer-Encoding: binary");
	
	// Setting the cache expiration to 30 seconds ahead of current time.
	// An IE 8 issue when opening the data directly in the browser without first saving it to a file
	$expiredate = time() + 30;
	$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
	header ($expireheader);

	//output the contents
	echo $contents;
	exit;
?>

<?php
	function getExcelData($data){

		$retval = "";
		if (is_array($data)  && !empty($data))
		{
			$row = 0;
			
			foreach($data as $_data){
				
			if (is_array($_data) && !empty($_data))
			{

					if ($row == 0)
					{
						// write the column headers
						//$retval = implode("\t",array_keys($_data));
						//$retval .= 	"\n";
						$retval = "playerName\t";
						$retval .= "prefix\t";
						$retval .= "fname\t";
						$retval .= "lname\t";
						$retval .= "INS2\t";
						$retval .= "INS3\t";
						$retval .= "address\t";
						$retval .= "address2\t";
						$retval .= "city\t";
						$retval .= "state\t";
						$retval .= "zip\t";
						$retval .= "email\t";
						$retval .= "\n";
					}

					//create a line of values for this row...
					//$retval = implode("\t",array_values($_data));
					$retval .= $_data['playerName']."\t";
					$retval .= $_data['prefix']."\t";
					$retval .= $_data['fname']."\t";
					$retval .= $_data['lname']."\t";
					if (($_data['INS2'] == 'Mom') || ($_data['INS2'] == 'Dad')) {
						$retval .= $_data['INS2']."\t";
					} else if (($_data['INS2'] == 'Aunt') || ($_data['INS2'] == 'Uncle') || ($_data['INS2'] == 'Grandma') || ($_data['INS2'] == 'Grandpa')) {
						$retval .= $_data['INS2']." ".$_data['fname']."\t";
					} else {
						$retval .= $_data['INS2']."\t";	
					}
					
					if (($_data['INS3'] == 'Mom') || ($_data['INS3'] == 'Dad')) {
						$retval .= "& ".$_data['INS3']."\t";
					} else if (($_data['INS3'] == 'Aunt') || ($_data['INS3'] == 'Uncle') || ($_data['INS3'] == 'Grandma') || ($_data['INS3'] == 'Grandpa')) {
						$retval .= "& ".$_data['INS3']." ".$_data['fname']."\t";
					} else {
						$retval .= $_data['INS3']."\t";	
					}
					
					$retval .= $_data['address']."\t";
					$retval .= $_data['address2']."\t";
					$retval .= $_data['city']."\t";
					$retval .= $_data['state']."\t";
					$retval .= $_data['zip']."\t";
					$retval .= $_data['email']."\n";

					//$retval .= "\n";
					
					//increment the row so we don't create headers all over again
					$row++;

				}

			}
		}

		return $retval;

	}
?>
