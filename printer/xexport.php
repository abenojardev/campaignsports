<?php
    session_start(); /* if not already done */
	
 	$get = $_GET;
	$team = $get['id'];
	
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
	
	
	
// prior to v2 new printer export format 	
$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', c.tID AS 'teamCode', t.password AS 'password',";
$query .= " c.prefix, c.fname, c.lname, c.relationship, c.prefix2, c.fname2, c.lname2, c.relationship2, c.address, c.address2, c.city, c.state, CONCAT('=\"', c.zip, '\"') as zip, c.email, c.ID";
$query .= " FROM contacts c";
$query .= " INNER JOIN players p ON c.pID = p.ID";
$query .= " INNER JOIN teams t ON c.tID = t.ID";
$query .= " WHERE c.tID = $team";
	
	
	//$query = "SELECT * FROM contacts WHERE tID = $team";

	
	$DB = new DB();
	$contacts = $DB->select_custom($query);

	//$contents = getExcelData($contacts);

	//print_r($contents);
	//break;
	
	$filename = "campaign".$team.".xls";

	//prepare to give the user a Save/Open dialog...
	header("Cache-Control: cache, must-revalidate");
	header("Pragma: public");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Transfer-Encoding: binary");
	
	// Setting the cache expiration to 30 seconds ahead of current time.
	// An IE 8 issue when opening the data directly in the browser without first saving it to a file
	$expiredate = time() + 30;
	$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
	header ($expireheader);

	//output the contents
	$contents = "Player\tTeam\tPWD\tPrefix\tFirst\tLast\tRel1\tPrefix2\tFirst2\tLast2\tRel2\tAddress\tAddress2\tCity\tState\tZip\tEmail\tID,\n";
	
	foreach($contacts AS $ct) {

	$contents.=$ct['playerName']."\t";
	$contents.=$ct['teamCode']."\t";
	$contents.=$ct['password']."\t";
	$contents.=$ct['prefix']."\t";
	$contents.=$ct['fname']."\t";
	$contents.=$ct['lname']."\t";
	$contents.=$ct['relationship']."\t";
	$contents.=$ct['prefix2']."\t";
	$contents.=$ct['fname2']."\t";
	$contents.=$ct['lname2']."\t";
	$contents.=$ct['relationship2']."\t";
	$contents.=$ct['address']."\t";
	$contents.=$ct['address2']."\t";
	$contents.=$ct['city']."\t";
	$contents.=$ct['state']."\t";
	$contents.=$ct['zip']."\t";
	$contents.=$ct['email']."\t";
    $contents.=$ct['ID']."\n";

	}

echo $contents;
		/* free result set */
		$this->db_connect->close();
		
	//echo $contents;
	exit;
?>

<?php
	function getExcelData($data){

		$retval = "";
		if (is_array($data)  && !empty($data))
		{
			$row = 0;

			foreach($data as $_data){
				
			if (is_array($_data) && !empty($_data))
			{

					if ($row == 0)
					{
						// write the column headers
						//$retval = implode("\t",array_keys($_data));
						//$retval .= 	"\n";
					}
					
					//create a line of values for this row...
					$retval .= implode("\t",array_values($_data));
					$retval .= "\n";
					//increment the row so we don't create headers all over again
					$row++;
				}

			}
		}
		return $retval;

	}
?>
