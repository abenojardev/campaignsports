<?php

	if ($_SERVER['HTTP_HOST'] != 'localhost' && $_SERVER['HTTP_HOST'] != '192.168.0.125') {
		if ($_SERVER['SERVER_PORT'] != 443) {
			$url = "https://". $_SERVER['SERVER_NAME'] . ":443".$_SERVER['REQUEST_URI'];
			header("Location: $url");
		}
	}
	
	// Required on EVERY page.  Alter path to make sure points to init file
	require_once('common/path/init.php');
	$relPath = $_SESSION['relative_path'];
	
	// Load include files
	require_once($relPath . 'inc/adminQB/layout/layout.php');
	require_once($relPath . 'inc/common/config.php');
	
	$post = $_POST;
	$get = $_GET;

	// Login / Logout
	if ( isset($get['action']) && $get['action'] == 'login' ) {
		$username = $post['username'];
		$password = $post['password'];
		
		if ($username == 'qbadmin' && $password == 'Y@hoshua7!') {
			include($relPath . 'inc/adminQB/display/qb_page.php');
			$_SESSION["qbAdmin_id"] = "dashboard";
		}
		else {
			unset($_SESSION["qbAdmin_id"]);
			include($relPath . 'inc/adminQB/display/login_page.php');
		}
		
		
	}
	else if ( isset($get['action']) && $get['action'] == 'logout' ) {
		unset($_SESSION["qbAdmin_id"]);
		include($relPath . 'inc/adminQB/display/login_page.php');
	}
	
	
	//QB Admin Logged in, continue with admin options
	else if ( isset($_SESSION["qbAdmin_id"]) && ($_SESSION["qbAdmin_id"]) && ($_SESSION["qbAdmin_id"] != "") ) {
		// Check for GET/POST actions
		if( isset($get["id"]) )
			$_SESSION["case_id"] = $get["id"];
				
		if ( isset($post['submitAction']) ) {
			include($relPath . 'inc/team/submitActions.php');
			include($relPath . 'inc/admin/adminSubmitActions.php');
		}
	}
	
	else {
		//include($relPath . 'inc/adminQB/display/qb_page.php');
		include($relPath . 'inc/adminQB/display/login_page.php');
	}
	
?>