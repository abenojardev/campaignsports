<?php

	$n1 = $n2 = $n3 = $n4 = $n5 = $n6 = $n7 = $n8 = $n9 = $n10 = "aNav";
	
	switch($_SESSION["nav"]) {
		case 'dashboard':
		$n1 = "aNavOn";
		break;
		
		case 'calendar':
		$n2 = "aNavOn";
		break;
		
		case 'reports':
		$n3 = "aNavOn";
		break;
		
		case 'campaigns':
		$n4 = "aNavOn";
		break;
		
		case 'coaches':
		$n5 = "aNavOn";
		break;
		
		case 'donations':
		$n6 = "aNavOn";
		break;

		case 'salesPortal':
		$n7 = "aNavOn";
		break;

		case 'messages':
		$n8 = "aNavOn";
		break;

		case 'admin':
		$n9 = "aNavOn";
		break;
		
		case 'paidInfluencer':
		$n10 = "aNavOn";
		break;

		default:
		$n1 = "aNavOn";
		break;
	}

	echo "
            <div class='adminNav'>
                <a href='index.php?nav=dashboard' class='" . $n1 ."'>Dashboard</a> 
                <a href='index.php?nav=calendar' class='" . $n2 ."'>Calendar</a>
                <a href='index.php?nav=campaigns' class='" . $n4 ."'>Campaigns</a>
                <a href='index.php?nav=coaches' class='" . $n5 ."'>Coaches</a>
                <a href='index.php?nav=donations' class='" . $n6 ."'>Donations</a>
                <a href='index.php?nav=reports' class='" . $n3 ."'>Reports</a>
                <a href='index.php?nav=salesPortal' class='" . $n7 ."'>Sales Portal</a>
                <!--<a href='index.php?nav=paidInfluencer' class='" . $n10 ."'>Paid Influencer</a> -->
                <a href='index.php?nav=messages' class='" . $n8 ."'>Messages</a>
                <a href='index.php?nav=admin' class='" . $n9 ."'>Admin</a>
                <div class='clear'></div>
            </div>
	";
?>