<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$Team = new TeamMain($_SESSION["campaign_team"]);
	$name = $Team->getTeamName();
	$team = $Team->getTeamType();
	$color = $Team->getTeamColors();

	echo "
		<style type='text/css'>
			.teamPrimaryBGColor {
				background-color:#" . $color['bgColor1'] . ";
				
			}
			.teamSecondaryBGColor {
				background-color:#" . $color['bgColor2'] . ";
				
			}
			.teamPrimaryTxtColor {
				color:#" . $color['txtColor'] . ";
			}
			
			/* general link style in team primary color */
			a:link, a:visited {
				color:#" . $color['txtColor'] . ";
				text-decoration: none;
			}
			a:hover, a:active {
				color:#" . $color['txtColor'] . ";
				text-decoration: underline;
			}
			
			/* coaches nav style in team primary color */
			a.cNav:link, a.cNav:visited {
				float:left;
				padding-left:10px;
				padding-right:10px;
				padding-top:5px;
				padding-bottom:7px;
				text-decoration: none;
				background-color:#cfcfcf;
				color:#00000;
				margin-right:1px;
			}
			a.cNav:hover, a.cNav:active {
				float:left;
				padding-left:10px;
				padding-right:10px;
				padding-top:5px;
				padding-bottom:7px;
				color:#ffffff;
				text-decoration: none;
				background-color:#" . $color['bgColor1'] . ";
				margin-right:1px;
			}
			
			a.cNavOn:link, a.cNavOn:visited, a.cNavOn:hover, a.cNavOn:active {
				float:left;
				padding-left:10px;
				padding-right:10px;
				padding-top:5px;
				padding-bottom:7px;
				color:#fff;
				text-decoration: none;
				background-color:#" . $color['bgColor1'] . ";
				margin-right:1px;
			}
			
			/* pagination style */
			.pagin {
				padding: 2px 0;
				margin: 0;
				font-family: 'Verdana', sans-serif;
				font-size: 7pt;
				font-weight: bold;
			}
			.pagin * {
				padding: 2px 6px;
				margin: 0;
			}
			.pagin a {
				border: solid 1px #666666;
				background-color: #EFEFEF;
				color: #666666;
				text-decoration: none;
			}
			.pagin a:visited {
				border: solid 1px #666666;
				background-color: #EFEFEF;
				color: #60606F;
				text-decoration: none;
			}
			.pagin a:hover, .pagin a:active {
				border: solid 1px #" . $color['bgColor1'] . ";
				background-color: white;
				color: #" . $color['bgColor1'] . ";
				text-decoration: none;
			}
			.pagin span {
				cursor: default;
				border: solid 1px #808080;
				background-color: #F0F0F0;
				color: #B0B0B0;
			}
			.pagin span.current {
				border: solid 1px #666666;
				background-color: #666666;
				color: white;
			}			
		</style>
	";
	
	function showteamHeader() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["campaign_team"]);
		$name = $Team->getTeamName();
		$type = $Team->getTeamType();
		
		echo "
            <h1 class='teamPrimaryTxtColor'>
				$name: $type <span style='font-size:0.65em;'>(ID: ".$_SESSION["campaign_team"].")</span>
			</h1>
		";
	}

	function showteamLogo() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
			<div class='suggestionsWrap' style='text-align:center; margin-top:20px;'>
				<p class='teamPrimaryTxtColor'><strong>Current Team Logo</strong></p>
				<div style='text-align:center;'>
					<img src='" . $path . "/$logoCheck' width='130' height='130' />
				</div>
			</div>
		";
	} //showteamLogo()

?>