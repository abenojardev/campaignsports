<?php
/****************************************************************************************
layout.php

Application: Campaign Sports
Trail Associates, May 2011
****************************************************************************************/

function startToMainHeader($data) {
	$css = ($data['css']) ? $data['css'] : "";
	$js = ($data['js']) ? $data['js'] : "";
	$bodyClass = ( isset($data['bodyClass']) && $data['bodyClass'] ) ? $data['bodyClass'] : "bodyBkg";
	
	echo "
		<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
		<html xmlns='http://www.w3.org/1999/xhtml'>
		<head>
			<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
			<meta name='MSSmartTagsPreventParsing' content='TRUE' />
			<meta http-equiv='imagetoolbar' content='no' />
			<title>Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program</title>
			<meta name='resource-type' content='document'>
			<meta http-equiv='pragma' content='no-cache'>
			<meta name='copyright' content='Copyright Campaign Sports - All Rights Reserved.'>
			<meta name='author' content='Campaign Sports - www.campaignsports.com - Manalapan, New Jersey 18301'>
			<meta http-equiv='reply-to' content='info@campaignsports.com'>
			<meta name='language' content='English'>
			<link rev='made' href='mailto:info@trailassociates.com'>
			<meta name='keywords' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>
			
			<meta name='rating' content='General'>
			<meta name='revisit-after' content='14 days'>
			
			<meta name='ROBOTS' content='ALL'>
			<meta name='DC.Description' content='Campaign Sports is fast, easy and stress-free. Just sit back and let our system do the hard work.'>
			<meta name='DC.Language' scheme='RFC1766' content='EN'>
			<meta name='DC.Coverage.PlaceName' content='USA'>
			
			<meta name='description' content='Campaign Sports is fast, easy and stress-free. Just sit back and let our system do the hard work.'>
			<meta name='Subject' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>
			<meta name='Title' content='" . $data['title'] . "'>
			
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/default.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/admin.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/print.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/v2.css' />
			$css
			$js
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/global.js'></script>
			
		</head>
		<body class='" . $bodyClass . "'>
			<div class='pageWrap'>
				<div class='header'>
					<a href='index.php'>
						<img src='" . $_SESSION['relative_path'] . "images/logo_campaign_sports.gif' width='291' height='88' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' />
					</a>
				</div>
			
			
	";
}



function contentWrapToNav($wrap = 'admin') {
	$wrap1 = ($wrap == 'admin') ? 'adminSecondaryBGColor' : 'teamSecondaryBGColor';
	$wrap2 = ($wrap == 'admin') ? 'adminPrimaryBGColor' : 'teamPrimaryBGColor';
	
	echo "
    <div class='pageContentWrap $wrap1'>
    
    	<div class='innerPageContentWrap $wrap2'>
        
        	<div class='pageContent'>
            
            <div class='topContent'>
            	<div class='welcomeBar'>
                    <div class='welcomeBarCol1'><h1>Welcome <span class='adminPrimaryTxtColor'>" . $_SESSION['masterAdmin_name'] . "</span></h1></div>
                    <div class='welcomeBarCol2'>Today's date is: <span class='adminPrimaryTxtColor'>".date('F d, Y')."</span><br /><div class='logout'><a href='index.php?action=logout'>[logout]</a></div></div>
                </div>
                
                <div class='clear'></div>
                <div class='v2Bar' style='color:#FFF; font-weight:bold; text-align:center; padding-top:10px; padding-bottom:10px; background-color:#F00;'>Version 2.0</div>
            </div>
	";
            
    include_once($_SESSION['relative_path'] . 'inc/admin/layout/navMain.php');
}



function contentClosures() {
	echo "
        	<div class='clear'></div>
            </div>
            <!-- /pageContent -->
        
        <div class='clear'></div>
        </div>
        <!-- /innerPageContentWrap -->
    
    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->
	";
}


function closePageWrapToEnd() {
	echo "
			<div class='clear'></div>
			</div>
			<!-- /pageWrap -->
			
			<div class='footer'>
				<div class='footerContent'>
					<p><a href='http://campaignsports.com/' class='foot' target='_blank'>Member Login</a> |  <a href='http://campaignsports.com/speak-with-campaign-sports-representative.php' class='foot' target='_blank'>Contact Us</a> |  <a href='http://campaignsports.com/campaign-sports-FAQs.php' class='foot' target='_blank'>FAQs</a> |  <a href='http://campaignsports.com/campaign-sports-terms-of-use.php' class='foot' target='_blank'>Terms of Use</a> |  <a href='http://campaignsports.com/campaign-sports-privacy-policy.php' class='foot' target='_blank'>Privacy Policy</a> |  <a href='http://campaignsports.com/campaign-sports-site-map.php' class='foot' target='_blank'>Site Map</a></p>
					<p>&copy; Copyright " . date('Y') . " Campaign Sports, LLC. All Rights Reserved.</p>
					<p style='font-size:11px;'>This site designed and maintained by <a href='http://www.trailassociates.com' target='_blank' style='font-size:11px;'>Trail Associates</a>. Hosting for this site provided by <a href='http://www.trailwebservices.com' target='_blank' style='font-size:11px;'>Trail Web Services</a>.</p>
				</div>			
			<div class='clear'></div>   
			</div>
			
        </body>
        </html>
	";
}

function suggestionsWrap() {
	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Here are some suggestions</strong></p>
 					<ul class='suggestList teamPrimaryTxtColor'>
                    <li class='bg1'><span class='txtBlack'>Former coaches</span></li>
                    <li><span class='txtBlack'>Family doctors / Dentists</span></li>
                    <li class='bg1'><span class='txtBlack'>Grandparents</span></li>
                    <li><span class='txtBlack'>Aunts / Uncles / Cousins</span></li>
                    <li class='bg1'><span class='txtBlack'>Close family / friends in the business world</span></li>
                    <li><span class='txtBlack'>Neighbors</span></li>
                    <li class='bg1'><span class='txtBlack'>Work Associates</span></li>
                    </ul>
                </div>
	";
}


?>