<?php
	$link = "index.php?nav=campaigns&action=donations&view";
	$link2 = "index.php?nav=donations";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$donationsList = Admin::getAllDonations();

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Most Recent Contributions</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td width='60px'><strong>Amount</strong></td>
							<td align='center'><strong>Team ID</strong></td>
							<td align='right'><strong>Date</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($donationsList AS $d) {
							$count++;
							if ($count > 20) continue;
							
							if ( isset($d['payment_date']) && $d['payment_date'] != "") {
								$date = date("M j, Y", strtotime($d['payment_date']));
							}
							else {
								$date = date("M j, Y", strtotime($d['donation_date']));
							}
							
							echo "
						<tr class='$classAlternate'>
							<td><a href='$link=" . $d['ID'] . "&team=" . $d['tID'] . "'>$" . $d['donationValue'] . "</a></td>
							<td align='center'>" . $d['tID'] . "</td>
							<td align='right'>" . $date . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>
                </div>
	";
?>