<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=dashboard&team";
		$link2 = "index.php?nav=campaigns&phase=2";
	} 
	else {
		$link = "";
		$link2 = "";
	}

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$topDonations = Admin::getTopDonations();

	echo "
				<h2 class='adminPrimaryTxtColor'>Top Campaigns: Donations Phase</h2>
				
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong>School</strong></td>
							<td><strong>Team</strong></td>
							<td align='center'><strong>Total<br />Donations</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						$contactCount = "";
						foreach($topDonations AS $td) {
							if ($count > 9 || $td['status'] == 1) continue;
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'>" . $td['name'] . "</td>
							<td align='left'><a href='$link=" . $td['tID'] . "'>" . $td['team'] . "</a></td>
							<td align='center'>$" . $td['donationTotal'] . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>
	";
?>