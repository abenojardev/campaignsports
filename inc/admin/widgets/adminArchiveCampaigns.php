<?php

	$phaseDisplay = ( isset($get['phase']) ) ? "&phase=" . $get['phase'] : "";

	$orderDisplayNew = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=DESC" : "&order=ASC";

	$orderDisplayOld = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=ASC" : "&order=DESC";

	

	echo "

		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />

	";

	

	$order = (isset($get['order'])) ? $get['order'] : 'ASC';

	

	$sort = (isset($get['sort'])) ? $get['sort'] : 'school';

	$pageSort = "&sort=$sort";

	switch($sort) {

		case 'school':

		$sortPass['s'] = 'name';

		break;

		case 'team':

		$sortPass['s'] = 'team';

		break;

		case 'id':

		$sortPass['s'] = 'ID';

		break;

		case 'coach':

		$sortPass['s'] = 'coach';

		break;

		case 'status':

		$sortPass['s'] = 'status';

		break;

		case 'contacts':

		$sortPass['s'] = 'contacts';

		break;

		case 'donations':

		$sortPass['s'] = 'donations';

		break;

		default:

		$sortPass['s'] = 'name';

		break;

	}

	$sortPass['o'] = $order;

	

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');

	$phase = (isset($get['phase'])) ? $get['phase'] : '';

	$search = "";

	

	$headerDisplay = "View Archived Campaigns";

	

	if ($phase) {

		if ($phase == 1) {

			$campaigns = Admin::getArchiveCampaignsS1($sortPass);

		} else if ($phase == 2) {

			$campaigns = Admin::getArchiveCampaignsS2($sortPass);

		} else if ($phase == 's') {

			$s = (isset($get['s'])) ? $get['s'] : $post['searchField'];

			$st = (isset($get['st'])) ? $get['st'] : $post['searchType'];

			$pass['searchData'] = $s;

			$pass['searchType'] = $st;

			$pass['sortS'] = $sortPass['s'];

			$pass['sortO'] = $sortPass['o'];

			$search = "&s=$s&st=$st";

			$headerDisplay = "View Archive Campaigns, Search Criteria: '$s'";

			$campaigns = Admin::searchArchiveCampaigns($pass);

		}

	} else {

		$campaigns = Admin::getArchiveCampaigns($sortPass);

	}

	

	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {

		$link = "index.php?nav=campaigns&action=dashboard&team";

		$link2 = "index.php?nav=campaigns&action=account&team";

		$link3 = "index.php?nav=campaigns&action=players&team";

		$link4 = "index.php?nav=campaigns&action=contacts&team";

		$link5 = "index.php?nav=campaigns&action=donations&team";

		$link6 = "index.php?nav=campaigns&action=masterAdmin&team";

		$linkSort = "index.php?nav=campaigns".$phaseDisplay.$orderDisplayNew.$search."&sort=";

	} 

	else {

		$link = "";

		$link2 = "";

	}

	

	$campaignCount = count($campaigns);

?>

<div class='contentFull'>
  <h2 class='adminPrimaryTxtColor'><?php echo $headerDisplay; ?></h2>
  <div class='contentLeft'>
    <div class='contentLeftData' style="border-top:0px;border-bottom:0px;">
      <div style="width:350px;"><?php 

					include($_SESSION['relative_path'] . 'inc/admin/widgets/adminArchiveSearch.php');

					echo "<br>";

					?>
                    </div>
    </div>
    
    <!-- /contentLeftData -->
    
    <div class='clear'></div>
  </div>
  
  <!-- /contentLeft -->
  
  <div class='contentRight'>
    <?php 

					//include($_SESSION['relative_path'] . 'inc/admin/widgets/adminArchiveData.php');

                ?>
    <div class='clear'></div>
  </div>
  
  <!-- /contentRight -->
  
  <div class='clear'></div>
  <div class=''>
    <table width='100%' border='0' cellspacing='0' cellpadding='7'>
      <tr>
        <td><strong><a href='<?php echo $linkSort; ?>school'>School</a>/<a href='<?php echo $linkSort; ?>team&action=archive'>Team</a></strong></td>
        <td align='center' width='100px;'><strong><a href='<?php echo $linkSort; ?>id&action=archive'>Team ID</a>/Pwd</strong></td>
        <td><strong><a href='<?php echo $linkSort; ?>coach&action=archive'>Coach Info</a></strong></td>
        <td align='center'><strong><a href='<?php echo $linkSort; ?>contacts&action=archive'>Contacts</a></strong></td>
        <td align='center'><strong><a href='<?php echo $linkSort; ?>donations&action=archive'>Donations</a></strong></td>
        <td align='center'><strong>Re-UP</strong></td>
        
        <!--<td align='center'><strong>Players</strong></td>

							<td align='center'><strong>Admin</strong></td>--> 
        
      </tr>
      <?php

                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;

                                $tpages = ceil($campaignCount/6);

                                $min = ($page - 1) * 6 + 1;

                                $max = $page * 6;

                                

                                $classAlternate = "bg1";

                                $count = 0;

                                foreach($campaigns AS $c) {

                                    $count++;

                                    if ($count < $min || $count > $max) continue;

									

									$donationColor = ( ($c['brochure_count'] * $c['brochure_cost'] + $c['brochure_charges']) >= $c['donationBillable'] ) ? "red" : "green";

									

									

									

									//$donationTotal = ($c['countTotals'] != 0) ? "$".$c['donationTotal']/$c['countTotals'] : "$0";

									$donationTotal = ($c['donationTotal'] != "") ? "$".$c['donationTotal'] : "$0";

									$imgLink = $_SESSION['relative_path'] . 'images/icon-re-up.png';

									$imgLink2 = $_SESSION['relative_path'] . 'images/icon-re-upNo.png';

									$frmName =  'reUpForm' . $count;

									

									$reUpLink = ($c['reup'] == 1) ? "<img src='$imgLink2' alt='' title='This Campaign has been ReUpped' />" : "<a href='index.php?nav=campaigns&action=reup&id=" . $c['ID'] . "' class='reupCheck'><img src='$imgLink' alt='' /></a>";

									                                    

									echo "

										<tr class='$classAlternate' align='right'>

											<td align='left' class='search'>" . $c['name'] . "

											<br />" . $c['team'] . "</td>

											

											<td align='center' class='search'>" . $c['ID'] . "

											<br />" . $c['password'] . "</td>

											

											<td align='left' class='search'>" . $c['lname'] . ", " . $c['fname'] . "

											<br /><a href='mailto:" . $c['email'] . "'>" . $c['email'] . "</a></td>

											

											<td align='center'>" . $c['countTotals'] . "</td>

											<td align='center'><font color=".$donationColor.">$donationTotal</font></td>

											

											<td align='center'>

												$reUpLink

											</td>

											

										</tr>

									";

                            

                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";

                                }

                            

                            ?>
    </table>
    <?php

                                echo "<div class='pagination'><br/>";

                                $reload = $_SERVER['PHP_SELF'] . "?nav=campaigns&action=archive".$phaseDisplay.$pageSort.$orderDisplayOld.$search;

                                include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');

                                echo paginate($reload, $page, $tpages, 5);

                                echo "</div>";

                            ?>
  </div>
  
  <!-- /contentFullData -->
  
  <div class='clear'></div>
</div>

<!-- /contentFull --> 

<script language="javascript" type="text/javascript">

			/* <![CDATA[ */

				$('.reupCheck').click(function(event) {

					var r=confirm("Are you sure you want to reup this campaign?");

					if (r==false) {

						event.preventDefault();

					}		

				});

			/* ]]> */

			</script>
<?php 

		if ($phase == 's') {

			echo "

				<script type='text/javascript' language='javascript'>

				// <![CDATA[

					$('td.search').highlight('$s');

				// ]]>

				</script>

			";

		}

	?>
