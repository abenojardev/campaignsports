<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=dashboard&team";
		$link = "index.php?nav=campaigns&action=donations&team";
	} 
	else {
		$link = "";
		$link2 = "";
	}
	
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
	";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$campaigns = Admin::getAllCampaignsByDonations();
	$campaignCount = count($campaigns);
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='adminPrimaryTxtColor'>View All Campaigns with Donations</h2>
					
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td align='center' width='50px'><strong>Team ID</strong></td>
							<td><strong>School</strong></td>
							<td><strong>Team</strong></td>
							<td><strong>Coach</strong></td>
							<td align='center' width='70px'><strong># of<br />Donations</strong></td>
							<td align='center' width='70px'><strong>Total<br />Donations</strong></td>
						</tr>
                        
							<?php
                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
                                $tpages = ceil($campaignCount/15);
                                $min = ($page - 1) * 15 + 1;
                                $max = $page * 15;
                                
                                $classAlternate = "bg1";
                                $count = 0;
                                foreach($campaigns AS $c) {
                                    $count++;
                                    if ($count < $min || $count > $max) continue;
									
									$donationColor = ( ($c['brochure_count'] * $c['brochure_cost'] + $c['brochure_charges']) >= $c['donationBillable'] ) ? "red" : "green";
                                    $donationTotal = ($c['donationTotal'] != "") ? "$".$c['donationTotal'] : "$0";
									
									echo "
										<tr class='$classAlternate' align='right'>
											<td align='center'>" . $c['ID'] . "</td>
											<td align='left'>" . $c['name'] . "</td>
											<td align='left'><a href='$link=" . $c['ID'] . "'>" . $c['team'] . "</a></td>
											<td align='left'>" . $c['fname'] . " " . $c['lname'] . "</td>
											<td align='center'>" . $c['count'] . "</td>
											<td align='center'><font color=".$donationColor.">$donationTotal</font></td>
										</tr>
									";
                            
                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
                                }
                            
                            ?>
                    </table>
                                        
                            <?php
                                echo "<div class='pagination'><br/>";
                                $reload = $_SERVER['PHP_SELF'] . "?nav=donations";
                                include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
                                echo paginate($reload, $page, $tpages, 3);
                                echo "</div>";
                            ?>
                        
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
