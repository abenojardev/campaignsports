<?php
	$link = "index.php?nav=campaigns&action=donations&view";
	$link2 = "index.php?nav=donations";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$donationsList = Admin::getAllDonations();

	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	$DB = new DB();
	$donationsList = $DB->select_custom('SELECT * FROM donations where pID="'.$_GET['edit'].'"');
	
	echo "
                <div class='suggestionsWrapx' style='width:100%;'>
                    <p class='teamPrimaryTxtColor'><strong>Donation Breakdown</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='10'>
						<tr> 
							<td width='center'><strong>Date</strong></td>
							<td align='center'><strong>Amount</strong></td>
							<td align='center'><strong>Donor</strong></td>
							<td align='center'><strong>Type</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($donationsList AS $d) {
							$count++;
							if ($count > 20) continue;
							 
							
							echo "
						<tr class='$classAlternate'> 
							<td align='center'>" . $d['donation_date'] . " </td>
							<td align='center'>$ " . $d['donationValue'] . "</td>
							<td align='center'>" . $d['fname'] . " " . $d['lname'] . "</td>
							<td align='center'>" . $d['paymentMethod'] ."</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>       
					</table>
                </div>
	";
?>