<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=dashboard&team";
		$link2 = "index.php?nav=campaigns&action=account&team";
	} 
	else {
		$link = "";
		$link2 = "";
	}
	
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
	";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$coaches = Admin::getAllCoaches();
	$coachCount = count($coaches);
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='adminPrimaryTxtColor'>View All Coaches</h2>
					
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong>Coach</strong></td>
							<td><strong>School/Team</strong></td>
							<td align='center' width='100px;'><strong>Team ID/Pwd</strong></td>
							<td align='center'><strong>Coach Pwd</strong></td>
							<td align='center'><strong>Day Phone</strong></td>
							<td align='center'><strong>Eve Phone</strong></td>
							<td align='center'><strong>Cell Phone</strong></td>
						</tr>
                        
							<?php
                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
                                $tpages = ceil($coachCount/15);
                                $min = ($page - 1) * 15 + 1;
                                $max = $page * 15;
                                
                                $classAlternate = "bg1";
                                $count = 0;
                                foreach($coaches AS $c) {
                                    $count++;
                                    if ($count < $min || $count > $max) continue;
                                    
									echo "
										<tr class='$classAlternate' align='right'>
											<td align='left'><a href='$link2=" . $c['ID'] . "'>" . $c['fname'] . " " . $c['lname'] . "</a>
											<br /><a href='mailto:" . $c['email'] . "'>" . $c['email'] . "</a></td>
											
											<td align='left'>" . $c['name'] . "
											<br /><a href='$link=" . $c['ID'] . "'>" . $c['team'] . "</a></td>
											
											<td align='center'>" . $c['ID'] . "
											<br />" . $c['password'] . "</td>
											
											<td align='center'>" . $c['adminPassword'] . "</td>
											<td align='center'>" . $c['phoneDay'] . "</td>
											<td align='center'>" . $c['phoneEve'] . "</td>
											<td align='center'>" . $c['phoneCell'] . "</td>
										</tr>
									";
                            
                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
                                }
                            
                            ?>
                    </table>
                                        
                            <?php
                                echo "<div class='pagination'><br/>";
                                $reload = $_SERVER['PHP_SELF'] . "?nav=coaches";
                                include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
                                echo paginate($reload, $page, $tpages, 3);
                                echo "</div>";
                            ?>
                        
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
