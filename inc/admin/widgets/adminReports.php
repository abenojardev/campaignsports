<?php
	$phaseDisplay = ( isset($get['phase']) ) ? "&phase=" . $get['phase'] : "";
	$orderDisplayNew = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=DESC" : "&order=ASC";
	$orderDisplayOld = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=ASC" : "&order=DESC";
	
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
	";
	
	$order = (isset($get['order'])) ? $get['order'] : 'ASC';
	
	$sort = (isset($get['sort'])) ? $get['sort'] : 'school';
	$pageSort = "&sort=$sort";
	switch($sort) {
		case 'school':
		$sortPass['s'] = 'name';
		break;
		case 'team':
		$sortPass['s'] = 'team';
		break;
		case 'id':
		$sortPass['s'] = 'ID';
		break;
		case 'donations':
		$sortPass['s'] = 'donationTotal';
		break;
		case 'billable':
		$sortPass['s'] = 'donationBillable';
		break;
		case 'mailing':
		$sortPass['s'] = 'dateSort';
		break;
		case 'number':
		$sortPass['s'] = 'brochure_count';
		break;
		case 'cost':
		$sortPass['s'] = 'brochure_cost';
		break;
		case 'charges':
		$sortPass['s'] = 'brochure_charges';
		break;
		case 'total':
		$sortPass['s'] = 'total';
		break;
		case 'difference':
		$sortPass['s'] = 'difference';
		break;
		default:
		$sortPass['s'] = 'name';
		break;
	}
	$sortPass['o'] = $order;
	
	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$phase = (isset($get['phase'])) ? $get['phase'] : '';
	$search = "";
	
	$headerDisplay = "View Last 60 days of Mailed Campaigns";
	
	if ($phase) {
		if ($phase == 1) {
			$campaigns = Admin::getAllCampaignsS1($sortPass);
		} else if ($phase == 2) {
			$campaigns = Admin::getAllCampaignsS2($sortPass);
		} else if ($phase == 's') {
			$s = (isset($get['s'])) ? $get['s'] : $post['searchField'];
			$st = (isset($get['st'])) ? $get['st'] : $post['searchType'];
			$pass['searchData'] = $s;
			$pass['searchType'] = $st;
			$pass['sortS'] = $sortPass['s'];
			$pass['sortO'] = $sortPass['o'];
			$search = "&s=$s&st=$st";
			$headerDisplay = "View Campaigns, Search Criteria: '$s'";
			$campaigns = Admin::searchCampaigns($pass);
		}
	} else {
		$campaigns = Admin::getAllCampaignsReport($sortPass);
	}
	
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=reports&action=dashboard&team";
		$link2 = "index.php?nav=reports&action=account&team";
		$link3 = "index.php?nav=reports&action=players&team";
		$link4 = "index.php?nav=reports&action=contacts&team";
		$link5 = "index.php?nav=reports&action=donations&team";
		$link6 = "index.php?nav=reports&action=masterAdmin&team";
		$linkSort = "index.php?nav=reports".$phaseDisplay.$orderDisplayNew.$search."&sort=";
	} 
	else {
		$link = "";
		$link2 = "";
	}
	
	$campaignCount = count($campaigns);
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='adminPrimaryTxtColor'><?php echo $headerDisplay; ?></h2>
					
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong><a href='<?php echo $linkSort; ?>school'>School</a>/<a href='<?php echo $linkSort; ?>team'>Team</a></strong></td>
							<td align='center' width='100px;'><strong><a href='<?php echo $linkSort; ?>id'>Team ID</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>donations'>Total<br />Donations</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>billable'>Billable<br />Donations</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>mailing'>Mail<br />Date</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>number'>Number</a>/<a href='<?php echo $linkSort; ?>cost'>Cost<br />per Brochure</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>charges'>Add'l<br />Charges</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>total'>Total<br />Costs</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>difference'>+ / -</a></strong></td>
                            
							<!--<td align='center'><strong>Players</strong></td>
							<td align='center'><strong>Admin</strong></td>-->
						</tr>
                        
							<?php
                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
                                $tpages = ceil($campaignCount/15);
                                $min = ($page - 1) * 15 + 1;
                                $max = $page * 15;
                                
                                $classAlternate = "bg1";
                                $count = 0;
                                foreach($campaigns AS $c) {
                                    $count++;
                                    if ($count < $min || $count > $max) continue;

									$mailDate = ($c['brochure_mailing']) ? date("M j, Y", strtotime($c['brochure_mailing'])) : "";
									
									$totalCosts = $c['brochure_count'] * $c['brochure_cost'] + $c['brochure_charges'];
									$totalCosts = ($totalCosts) ? $totalCosts : "";
									$totalCostDisplay = ($totalCosts) ? "$" . $totalCosts : "";
									$profit = "$" . abs($c['donationBillable'] - $totalCosts);

									$donationColor = ( ($c['brochure_count'] * $c['brochure_cost'] + $c['brochure_charges']) >= $c['donationBillable'] ) ? "red" : "green";
									
									$donationTotal = ($c['donationTotal'] != "") ? "$".$c['donationTotal'] : "$0";
									$donationBillable = ($c['donationBillable'] != "") ? "$".$c['donationBillable'] : "$0";
                                    
									$brochureCount = ($c['brochure_count'] != "") ? $c['brochure_count'] : "";
									$brochureCost = ($c['brochure_cost'] != "") ? "$".$c['brochure_cost'] : "";
									$brochureCharges = ($c['brochure_charges'] != "") ? "$".$c['brochure_charges'] : "";
									
									$difference = ($c['difference'] != "") ? "$".$c['difference'] : "";
									
									echo "
										<tr class='$classAlternate' align='right'>
											<td align='left' class='search'>" . $c['name'] . "
											<br /><a href='$link=" . $c['ID'] . "'>" . $c['team'] . "</a></td>
											
											<td align='center' class='search'>" . $c['ID'] . "</td>
											
											<td align='center'>" . $donationTotal . "</td>
											<td align='center'>" . $donationBillable . "</td>
											<td align='center'>" . $mailDate . "</td>
											<td align='center'>" . $brochureCount . "<br />" . $brochureCost . "</td>
											<td align='center'>" . $brochureCharges . "</td>
											<td align='center'>" . $totalCostDisplay . "</td>
											<td align='center'><font color=".$donationColor.">".$difference."</font></td>
										</tr>
									";
                            
                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
                                }
                            
                            ?>
                    </table>
                                        
                            <?php
                                echo "<div class='pagination'><br/>";
                                $reload = $_SERVER['PHP_SELF'] . "?nav=reports".$phaseDisplay.$pageSort.$orderDisplayOld.$search;
                                include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
                                echo paginate($reload, $page, $tpages, 5);
                                echo "</div>";
                            ?>
                        
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
    <?php 
		if ($phase == 's') {
			echo "
				<script type='text/javascript' language='javascript'>
				// <![CDATA[
					$('td.search').highlight('$s');
				// ]]>
				</script>
			";
		}
	?>
    
    
