<?php
	$phaseDisplay = ( isset($get['phase']) ) ? "&phase=" . $get['phase'] : "";
	$orderDisplayNew = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=DESC" : "&order=ASC";
	$orderDisplayOld = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=ASC" : "&order=DESC";
	
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
	";
	
	$order = (isset($get['order'])) ? $get['order'] : 'ASC';
	
	$sort = (isset($get['sort'])) ? $get['sort'] : 'school';
	$pageSort = "&sort=$sort";
	switch($sort) {
		case 'school':
		$sortPass['s'] = 'name';
		break;
		case 'team':
		$sortPass['s'] = 'team';
		break;
		case 'id':
		$sortPass['s'] = 'ID';
		break;
		case 'coach':
		$sortPass['s'] = 'coach';
		break;
		case 'status':
		$sortPass['s'] = 'status';
		break;
		case 'contacts':
		$sortPass['s'] = 'contacts';
		break;
		case 'donations':
		$sortPass['s'] = 'donations';
		break;
		default:
		$sortPass['s'] = 'name';
		break;
	}
	$sortPass['o'] = $order;
	
	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$phase = (isset($get['phase'])) ? $get['phase'] : '';
	$search = "";
	
	$headerDisplay = "View All Campaigns";
	
	if ($phase) {
		if ($phase == 1) {
			$campaigns = Admin::getAllCampaignsS1($sortPass);
		} else if ($phase == 2) {
			$campaigns = Admin::getAllCampaignsS2($sortPass);
		} else if ($phase == 's') {
			$s = (isset($get['s'])) ? $get['s'] : $post['searchField'];
			$st = (isset($get['st'])) ? $get['st'] : $post['searchType'];
			$pass['searchData'] = $s;
			$pass['searchType'] = $st;
			$pass['sortS'] = $sortPass['s'];
			$pass['sortO'] = $sortPass['o'];
			$search = "&s=$s&st=$st";
			$headerDisplay = "View Campaigns, Search Criteria: '$s'";
			$campaigns = Admin::searchCampaigns($pass);
		}
	} else {
		$campaigns = Admin::getAllCampaigns($sortPass);
	}
	
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=dashboard&team";
		$link2 = "index.php?nav=campaigns&action=account&team";
		$link3 = "index.php?nav=campaigns&action=players&team";
		$link4 = "index.php?nav=campaigns&action=contacts&team";
		$link5 = "index.php?nav=campaigns&action=donations&team";
		$link6 = "index.php?nav=campaigns&action=masterAdmin&team";
		$linkSort = "index.php?nav=campaigns".$phaseDisplay.$orderDisplayNew.$search."&sort=";
	} 
	else {
		$link = "";
		$link2 = "";
	}
	
	$campaignCount = count($campaigns);
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='adminPrimaryTxtColor'><?php echo $headerDisplay; ?></h2>
					
                    <div style="width:350px;">
                    <?php 
echo "<br>";
					include($_SESSION['relative_path'] . 'inc/admin/widgets/adminSearch.php');

					echo "<br>";

					?>
                    </div>
                    
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong><a href='<?php echo $linkSort; ?>school'>School</a>/<a href='<?php echo $linkSort; ?>team'>Team</a></strong></td>
							<td align='center' width='100px;'><strong><a href='<?php echo $linkSort; ?>id'>Team ID</a>/Pwd</strong></td>
							<td><strong><a href='<?php echo $linkSort; ?>coach'>Coach Info</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>status'>Status</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>contacts'>Contacts</a></strong></td>
							<td align='center'><strong><a href='<?php echo $linkSort; ?>donations'>Donations</a></strong></td>
							<!--<td align='center'><strong>Export</strong></td>-->
                            
							<!--<td align='center'><strong>Players</strong></td>
							<td align='center'><strong>Admin</strong></td>-->
						</tr>
                        
							<?php
                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
                                $tpages = ceil($campaignCount/6);
                                $min = ($page - 1) * 6 + 1;
                                $max = $page * 6;
                                
                                $classAlternate = "bg1";
                                $count = 0;
                                foreach($campaigns AS $c) {
                                    $count++;
                                    if ($count < $min || $count > $max) continue;
									
									$donationColor = ( ($c['brochure_count'] * $c['brochure_cost'] + $c['brochure_charges']) >= $c['donationBillable'] ) ? "red" : "green";
									
									
									
									//$donationTotal = ($c['countTotals'] != 0) ? "$".$c['donationTotal']/$c['countTotals'] : "$0";
									$donationTotal = ($c['donationTotal'] != "") ? "$".$c['donationTotal'] : "$0";
                                    
									echo "
										<tr class='$classAlternate' align='right'>
											<td align='left' class='search'>" . $c['name'] . "
											<br /><a href='$link=" . $c['ID'] . "'>" . $c['team'] . "</a></td>
											
											<td align='center' class='search'>" . $c['ID'] . "
											<br />" . $c['password'] . "</td>
											
											<td align='left' class='search'><a href='$link2=" . $c['ID'] . "'>" . $c['lname'] . ", " . $c['fname'] . "</a>
											<br /><a href='mailto:" . $c['email'] . "'>" . $c['email'] . "</a></td>
											
											<td align='center'>" . $c['status'] . "</td>
											
											<td align='center'><a href='$link4=" . $c['ID'] . "'>" . $c['countTotals'] . "</a></td>
											<td align='center'><a href='$link5=" . $c['ID'] . "'><font color=".$donationColor.">$donationTotal</font></a></td>
											<!--<td align='center'><a href='export.php?id=" . $c['ID'] . "'>Contacts</a><br />
											<a href='exportDonations.php?id=" . $c['ID'] . "'>Donations</a></td>
											
											<td align='center'><a href='$link3=" . $c['ID'] . "'>View</a></td>
											<td align='center'><a href='$link6=" . $c['ID'] . "'>View</a></td>-->
										</tr>
									";
                            
                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
                                }
                            
                            ?>
                    </table>
                                        
                            <?php
                                echo "<div class='pagination'><br/>";
                                $reload = $_SERVER['PHP_SELF'] . "?nav=campaigns".$phaseDisplay.$pageSort.$orderDisplayOld.$search;
                                include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
                                echo paginate($reload, $page, $tpages, 5);
                                echo "</div>";
                            ?>
                        
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
    <?php 
		if ($phase == 's') {
			echo "
				<script type='text/javascript' language='javascript'>
				// <![CDATA[
					$('td.search').highlight('$s');
				// ]]>
				</script>
			";
		}
	?>
    
    
