<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=dashboard&team";
		$link2 = "index.php?nav=campaigns&phase=1";
	} 
	else {
		$link = "";
		$link2 = "";
	}

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$topContacts = Admin::getTopContacts();

	echo "
				<h2 class='adminPrimaryTxtColor'>Top Campaigns: Contacts Phase</h2>
				
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong>School</strong></td>
							<td><strong>Team</strong></td>
							<td align='center'><strong># of<br />Contacts</strong></td>
							<td align='center'><strong>Donations</strong></td>
						</tr>
	";
	                    $totalDonations = Admin::getCampaignTotalDonationSingle($tc['tID'])[0]['count'];
                        $classAlternate = "bg1";
						$count = 0;
						$contactCount = "";
						foreach($topContacts AS $tc) {
							if ($count > 9 || $tc['status'] > 1) continue;
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'>" . $tc['name'] . "</td>
							<td align='left'><a href='$link=" . $tc['tID'] . "'>" . str_replace("�","'",$tc['team']) . "</a></td>
							<td align='center'>" . $tc['countTotals'] . "</td>
							<td align='center'> " . $tds = is_null($totalDonations) ?  0 : $totalDonations . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>
	";
?>