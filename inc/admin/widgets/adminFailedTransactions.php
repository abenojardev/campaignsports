<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=dashboard&team";
		$link = "index.php?nav=campaigns&action=donations&team";
	} 
	else {
		$link = "";
		$link2 = "";
	}
	
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
	";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$failed = Admin::getAllFailedDonations();
	$failedCount = count($failed);
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='adminPrimaryTxtColor'>View Failed Transactions</h2>
					
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td align='center' width='50px'><strong>Team ID</strong></td>
							<td><strong>School</strong></td>
							<td><strong>Team</strong></td>
							<td width='100px'><strong>Date</strong></td>
							<td align='left'><strong># of<br />Donor Name</strong></td>
							<td align='left'><strong>Total<br />Error</strong></td>
						</tr>
                        
							<?php
                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
                                $tpages = ceil($failedCount/15);
                                $min = ($page - 1) * 15 + 1;
                                $max = $page * 15;
                                
                                $classAlternate = "bg1";
                                $count = 0;
                                foreach($failed AS $f) {
                                    $count++;
                                    if ($count < $min || $count > $max) continue;
                                    $date = date("M j, Y", strtotime($f['donation_date']));
									
									echo "
										<tr class='$classAlternate' align='right'>
											<td align='center'>" . $f['ID'] . "</td>
											<td align='left'>" . $f['name'] . "</td>
											<td align='left'><a href='$link=" . $f['ID'] . "'>" . $f['team'] . "</a></td>
											<td align='left'>" . $date . "</td>
											<td align='left'>" . $f['fname'] . " " . $f['lname'] . "</td>
											<td align='left'>$" . $f['r_error'] . "</td>
										</tr>
									";
                            
                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
                                }
                            
                            ?>
                    </table>
                                        
                            <?php
                                echo "<div class='pagination'><br/>";
                                $reload = $_SERVER['PHP_SELF'] . "?nav=donations";
                                include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
                                echo paginate($reload, $page, $tpages, 3);
                                echo "</div>";
                            ?>
                        
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
