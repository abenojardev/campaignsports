<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$link = "index.php?nav=campaigns&action=contacts&team";
		$link2 = "index.php?nav=campaigns";
	} 
	else {
		$link = "";
		$link2 = "";
	}
	
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
	";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$campaigns = Admin::getAllCampaignsByContacts();
	$campaignCount = count($campaigns);
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='teamPrimaryTxtColor'>Campaigns for Contact Export</h2>
					
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong>School</strong></td>
							<td><strong>Team</strong></td>
							<td align='center'><strong>Total<br />Contacts</strong></td>
							<td align='center' width='70px'><strong>Export<br />Contacts</strong></td>
						</tr>
                        
							<?php
                                $page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
                                $tpages = ceil($campaignCount/15);
                                $min = ($page - 1) * 15 + 1;
                                $max = $page * 15;
                                
                                $classAlternate = "bg1";
                                $count = 0;
                                foreach($campaigns AS $c) {
									if ($c['status'] > 1) continue;
                                    $count++;
                                    if ($count < $min || $count > $max) continue;
                                    
									echo "
										<tr class='$classAlternate' align='right'>
											<td align='left'><a href='$link=" . $c['tID'] . "'>" . $c['name'] . "</a></td>
											<td align='left'>" . $c['team'] . "</td>
											<td align='center'>" . $c['countTotals'] . "</td>
											<td align='right'><a href='export.php?id=" . $c['tID'] . "' class='adminButton adminPrimaryBGColor' id='frmSubmit'>Export</a></td>

										</tr>
									";
                            
                                    $classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
                                }
                            
                            ?>
                    </table>
                                        
                            <?php
                                echo "<div class='pagination'><br/>";
                                $reload = $_SERVER['PHP_SELF'] . "?nav=export";
                                include_once($_SESSION['relative_path'] . 'inc/team/layout/pagination.php');
                                echo paginate($reload, $page, $tpages, 3);
                                echo "</div>";
                            ?>
                        
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
