<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	contentWrapToNav();
?>    
            <div class="adminSubNav">
                <a href="exportAllDonations.php" class="sNav">Export All</a> &nbsp; | &nbsp;
                <a href="index.php?nav=donations" class="sNav">Campaigns with Donations</a> &nbsp; | &nbsp;
                <a href="index.php?nav=donations&action=failed" class="sNav">Failed Transactions</a>
            <div class="clear"></div>
            </div>
            
			<div class='contentFull'>
                <div class='contentFullData'>
            
            	<?php include($relPath . 'inc/admin/widgets/adminAllCampaignsByD.php'); ?>
            
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->

<?php
	contentClosures();
	closePageWrapToEnd();
?>