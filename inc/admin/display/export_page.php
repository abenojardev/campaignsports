<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	contentWrapToNav();
	
?>    
            <div class="adminSubNav">
                <a href="#" class="sNav"></a>
            <div class="clear"></div>
            </div>
            
			<div class='contentFull'>
                <div class='contentFullData'>
            
            	<?php include($relPath . 'inc/admin/widgets/adminAllCampaignsForE.php'); ?>
            
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->

<?php
	contentClosures();
	closePageWrapToEnd();
?>