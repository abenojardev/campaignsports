<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	contentWrapToNav();
	
 	// Code for deleting entire entry
	//
	$post = $_POST;
	if (isset($post['designerDeleteCheck'])) {
		include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
		$query = array('tbl' => "designers", 
					   'where' => "ID=" . $post['ID']);
		$DB = new DB();
		$result = $DB->delete_records($query);
	} else if (isset($post['printerDeleteCheck'])) {
		include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
		$query = array('tbl' => "printers", 
					   'where' => "ID=" . $post['ID']);
		$DB = new DB();
		$result = $DB->delete_records($query);
	}
	
?>    

    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_SESSION['relative_path']; ?>css/init.css" />    
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_SESSION['relative_path']; ?>admin/xEdit/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.css" />    
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $_SESSION['relative_path']; ?>admin/xEdit/css/jqueryui-editable.css" />    

	<script src="<?php echo $_SESSION['relative_path']; ?>admin/xEdit/jquery-ui-1.9.2.custom/js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="<?php echo $_SESSION['relative_path']; ?>admin/xEdit/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js" type="text/javascript"></script>    
    <script src="<?php echo $_SESSION['relative_path']; ?>admin/xEdit/js/jqueryui-editable-inline.js" type="text/javascript"></script>
    <script type="text/javascript">
		function validateDelete() {
			if (confirm('Are you sure you want to delete? This action is permanent.')) {
				// Save it!
			} else {
				return false;
			}			
		}	
	</script>

            <div class="adminSubNav">
                <a href="index.php?nav=admin&action=donations" class="sNav">Global Donations Export</a> &nbsp; | &nbsp;
                <a href="index.php?nav=admin&action=designer" class="sNav">Designer Info</a> &nbsp;  
                <!--<a href="index.php?nav=admin&action=printer" class="sNav">Printer Info</a>-->
            <div class="clear"></div>
            </div>

			<div class='contentFull'>
                <div class='contentFullData'>
            		<h2 class='adminPrimaryTxtColor' style='text-align:center'>Admin Page</h2>
<?php
	if (!(isset($get['action'])) || (isset($get['action']) && $get['action'] == 'donations')) {
	    
	    
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	$DB = new DB();
	$teams = $DB->select_custom('SELECT * FROM teams');
	$DB = new DB(); 
	$states = $DB->select_custom('SELECT * FROM gen_states_list');
	 
	
?>            

                    <div class='adminBox'>
                    	<div class='adminCol1'>
	                    	<h3>All Campaigns: Donation Export</h3>
                        </div>
                    	<div class='adminCol2'>
	                    	
                        </div>
                    	<div class='adminCol3'>
                            <a href='exportAllDonations.php' class='adminButton adminPrimaryBGColor'>Export All Donations</a>
                        </div>
                        <div class='clear'></div> 
                    </div>
                    <div class='adminBox'>
                    	<div class='adminCol1'>
	                    	<h3>All Campaigns: Sort Date</h3>
                        </div>
                        <form action="exportSortedDonations.php">  
                        	<div class='adminCol2'>
                        	    <input type="hidden" name="type" value="date">
    	                    	<input type="date" name="date_from" required style="width:40%; float:left; border-radius:5px; padding:1%; margin-right:1%;">
    	                    	<span style="width:5%; text-align:center; float:left">to</span>
    	                    	<input type="date" name="date_to" required style="width:40%; float:left; border-radius:5px; padding:1%; margin-left:1%;"> 
                            </div>
                        	<div class='adminCol3' style="width: 16.2%;">
                                <button class='adminButton adminPrimaryBGColor'style="border:none; color:white; padding:5% !important; font-size:1em; width:100%;">Export All Donations</button>
                            </div>
                        </form>
                        <div class='clear'></div> 
                    </div>
                    <div class='adminBox'>
                    	<div class='adminCol1'>
	                    	<h3>Sort By Campaigns</h3>
                        </div>
                        <form action="exportSortedDonations.php">  
                        	<div class='adminCol2'>
                        	    <input type="hidden" name="type" value="campaign">
    	                    	<select name="cID" required style="width:100%; float:left; border-radius:5px; padding:3%;">
    	                    	    <?php foreach($teams as $v){ ?>
    	                    	        <option value="<?php echo $v['ID']; ?>"><?php echo $v['name'].' - '.$v['team']; ?></option>
    	                    	    <?php } ?>
    	                    	</select>
                            </div>
                        	<div class='adminCol3' style="width: 16.2%;">
                                <button class='adminButton adminPrimaryBGColor'style="border:none; color:white; padding:5% !important; font-size:1em; width:100%;">Export All Donations</button>
                            </div>
                        </form>
                        <div class='clear'></div> 
                    </div>
                    <div class='adminBox'>
                    	<div class='adminCol1'>
	                    	<h3>Coach Email List</h3>
                        </div>
                        <form action="exportSortedDonations.php">  
                        	<div class='adminCol2'>
                        	    <input type="hidden" name="type" value="emaillist"> 
    	                    	<select name="state" required style="width:100%; float:left; border-radius:5px; padding:3%;">
    	                    	    <option value="all">all</option>
    	                    	    <?php foreach($states as $v){ ?>
    	                    	        <option value="<?php echo $v['name']; ?>"><?php echo $v['name'].' - '.$v['abbrev']; ?></option>
    	                    	    <?php } ?>
    	                    	</select>
                            </div>
                        	<div class='adminCol3' style="width: 16.2%;">
                                <button class='adminButton adminPrimaryBGColor'style="border:none; color:white; padding:5% !important; font-size:1em; width:100%;">Export All</button>
                            </div>
                        </form>
                        <div class='clear'></div> 
                    </div>
<?php
	} else if (isset($get['action']) && $get['action'] == 'designer') {
		require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
		$designers = Admin::getAllDesigners();
?>            
                    <div class='adminBox'>
                    	<form method='post' name='frmAddDesigner' action='index.php?nav=admin&action=designer'>
                            <div class='adminCol1'>
                                <h3>Add New Contractor: Designer</h3>
                            </div>
                            <div class='adminCol2'>
                                
                            </div>
                            <div class='adminCol3'>
                                <a href='javascript:document.frmAddDesigner.submit();' class='adminButton adminPrimaryBGColor'>Add Designer</a>
                            </div>
                            <div class='clear'></div>
                            
                            <div style='margin-top:10px;'>
                                <input type="text" name="fname" size="20" class='selectFieldSWR' value="First Name"/>
                                <input type="text" name="lname" size="25" class='selectFieldSWR' value="Last Name"/>
                                <input type="text" name="email" size="30" class='selectFieldSWR' value="Email"/>
                                
                                <input type="text" name="username" size="20" class='selectFieldSWR' value="Username"/>
                                <input type="text" name="password" size="20" class='selectFieldSWR' value="Password"/>
                                <input type="text" name="title" size="50" class='selectFieldSWR' value="Title"/>
                                <input type="hidden" name="submitAction" value='adminAddDesigner' />
                            </div>
                    	</form>
                    <div class='clear'></div> 
                    </div>
                    
                    <div class='adminBox'>
<?php
                    $output = "<table width='100%' border='1' cellpadding='5'>";
                    
                    $row = 0;
                    foreach($designers as $designer) {
                        if ($row == 0) {
                            $keyArray = preg_grep("/ID/i",array_keys($designer),PREG_GREP_INVERT);
                            
                            $output .= "<tr><th>Delete</th><th>";
                            $output .= implode("</th><th>",$keyArray);
                            $output .= "</th></tr><tbody>";
                        }
                        
                        $output .= "<tr>";
                
                        $output .= "<td align='center'>
                                        <form name='frm_".$designer['ID']."' method='post' action='' onsubmit='return validateDelete()'>
                                        <input type='hidden' name='designerDeleteCheck' value=1>
                                        <input type='hidden' name='ID' value='".$designer['ID']."'>
                                        <input type='submit' value='Delete'>
                                        </form>
                                    </td>";
                
                        foreach($designer as $key => $value) {
                            if ($key != 'ID') {
                                if ($key == 'xxx')
                                    $output .= "<td>$value</td>";
                                else 
                                    $output .= "<td><a class='xedit' id='".$key."' data-type='text' data-pk='".$designer['ID']."' data-url='".$_SESSION['relative_path']."admin/designerUpdate.php' data-original-title='".$key."'>$value</a></td>";
                            }
                        }
                        $output .= "</tr>";
                            
                        $row++;
                    }
                    
                    $output .= "</tbody></table>";
                    
                    echo $output;
?>
                    <div class='clear'></div> 
                    </div>
                    
<script>
	$('.xedit').each(function() {
		$(this).editable();
	});
</script>
                    
<?php
	} else if (isset($get['action']) && $get['action'] == 'printer') {
		require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
		$printers = Admin::getAllPrinters();
?>            
                    <div class='adminBox'>
                    	<form method='post' name='frmAddPrinter' action='index.php?nav=admin&action=printer'>
                            <div class='adminCol1'>
                                <h3>Add New Contractor: Printer</h3>
                            </div>
                            <div class='adminCol2'>
                                
                            </div>
                            <div class='adminCol3'>
                                <a href='javascript:document.frmAddPrinter.submit();' class='adminButton adminPrimaryBGColor'>Add Printer</a>
                            </div>
                            <div class='clear'></div>
                            
                            <div style='margin-top:10px;'>
                                <input type="text" name="fname" size="20" class='selectFieldSWR' value="First Name"/>
                                <input type="text" name="lname" size="25" class='selectFieldSWR' value="Last Name"/>
                                <input type="text" name="email" size="30" class='selectFieldSWR' value="Email"/>
                                
                                <input type="text" name="username" size="20" class='selectFieldSWR' value="Username"/>
                                <input type="text" name="password" size="20" class='selectFieldSWR' value="Password"/>
                                <input type="hidden" name="submitAction" value='adminAddPrinter' />
                            </div>
                        </form>
                    <div class='clear'></div> 
                    </div>
                    
                    <div class='adminBox'>
<?php
                    $output = "<table width='100%' border='1' cellpadding='5'>";
                    
                    $row = 0;
                    foreach($printers as $printer) {
                        if ($row == 0) {
                            $keyArray = preg_grep("/ID/i",array_keys($printer),PREG_GREP_INVERT);
                            
                            $output .= "<tr><th>Delete</th><th>";
                            $output .= implode("</th><th>",$keyArray);
                            $output .= "</th></tr><tbody>";
                        }
                        
                        $output .= "<tr>";
                
                        $output .= "<td align='center'>
                                        <form name='frm_".$printer['ID']."' method='post' action='' onsubmit='return validateDelete()'>
                                        <input type='hidden' name='printerDeleteCheck' value=1>
                                        <input type='hidden' name='ID' value='".$printer['ID']."'>
                                        <input type='submit' value='Delete'>
                                        </form>
                                    </td>";
                
                        foreach($printer as $key => $value) {
                            if ($key != 'ID') {
                                if ($key == 'xxx')
                                    $output .= "<td>$value</td>";
                                else 
                                    $output .= "<td><a class='xedit' id='".$key."' data-type='text' data-pk='".$printer['ID']."' data-url='".$_SESSION['relative_path']."admin/printerUpdate.php' data-original-title='".$key."'>$value</a></td>";
                            }
                        }
                        $output .= "</tr>";
                            
                        $row++;
                    }
                    
                    $output .= "</tbody></table>";
                    
                    echo $output;
?>
                    <div class='clear'></div> 
                    </div>
                    
<script>
	$('.xedit').each(function() {
		$(this).editable();
	});
</script>
                    
<?php
	}
?>            
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->

<?php
	contentClosures();
	closePageWrapToEnd();
?>