<?php
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/farbtastic.css' />
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pickerAdmin.css' />
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/farbtastic.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jqModal.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/picker.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/formValidate.js'></script>
		
	";
	
	unset($_SESSION['campaign_team']);
	unset($_SESSION['current_folder']);
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	$countries = Common::getCountries();
		
	$c1 = ( isset($color['bgColor1']) ) ? '#'.$color['bgColor1'] : '#f7941e';
	$c2 = ( isset($color['bgColor2']) ) ? '#'.$color['bgColor2'] : '#000';
	$c3 = ( isset($color['txtColor']) ) ? '#'.$color['txtColor'] : '#f7941e';
?>    
			<div class='contentFull'>
            
                <div class=''>
					<h2 class='teamPrimaryTxtColor'>Add a New Campaign</h2>
					<br />
					<form method='post' id='frm' name='frm' action='index.php?nav=campaigns&action=dashboard' enctype='multipart/form-data'>
					<div class='adminAddLeft'>
                        <br />
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>School/Organization:</strong></div>
                            <div class='formElementCol2'><input name='name' type='text' class='textFieldSWR validate' /></div>
                        </div>
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Team Name:</strong></div>
                            <div class='formElementCol2'><input name='team' type='text' class='textFieldSWR validate' /></div>
                        </div>
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Main Password:</strong></div>
                            <div class='formElementCol2'><input name='password' type='text' class='textFieldSWR validate' /></div>
                        </div>
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Main Password (validate):</strong></div>
                            <div class='formElementCol2'><input name='password2' type='text' class='textFieldSWR validate' /></div>
                        </div>
                    	<br /><br /><br />
                    
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Team Primary Color:</strong></div>
                            <div class='formElementCol2'>
                            <input name='bgColor1' id='color1' type='text' class='textFieldSWR' value='<?php echo $c1; ?>' /> 
                            <a href='#' id='pickerBtn1'><img src='<?php echo $_SESSION['relative_path']; ?>images/icon_color_picker.gif' width='20' height='20' class='colorPickerIcon' /></a>
                            </div>
                        <div class='clear'></div>
                        </div>
                        
                        <span class='helpNotes'>This will colorize the page inner outline background color.</span><br /><br />
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Team Secondary Color:</strong></div>
                            <div class='formElementCol2'>
                            <input name='bgColor2' id='color2' type='text' class='textFieldSWR' value='<?php echo $c2; ?>' /> 
                            <a href='#' id='pickerBtn2'><img src='<?php echo $_SESSION['relative_path']; ?>images/icon_color_picker.gif' width='20' height='20' class='colorPickerIcon' /></a>
                            </div>
                        </div>
                        
                        <span class='helpNotes'>This will colorize the page outer outline background color.</span><br /><br />
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Team Primary Text Color:</strong></div>
                            <div class='formElementCol2'>
                            <input name='txtColor' id='color3' type='text' class='textFieldSWR' value='<?php echo $c3; ?>' /> 
                           <a href='#' id='pickerBtn3'> <img src='<?php echo $_SESSION['relative_path']; ?>images/icon_color_picker.gif' width='20' height='20' class='colorPickerIcon' /></a>
                            </div>
                        </div>
                        
                        <span class='helpNotes'>This will colorize the color of all highlight text.</span>
                        <br /><br /><br />
                        
                        <div class='formElement'>
                            <?php //if ( isset($_SESSION['upload_result']) ) echo "<div style='color:red;font-weight:bold;'>" . $_SESSION['upload_result'] . "<br /><br /></div>"; ?>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Upload Your Team Logo:</strong></div>
                            <div class='formElementCol2'>
                            <input type='file' size='40' name='logoUpload' />
                            </div>
                        </div>
                        
                        <span class='helpNotes'>For best results the logo image size should be 130px wide by 130px in height. Image format must be .gif, .jpg, or .png</span>
                        <br /><br /><br />
                        
                        <div class='registerButton'>
                        	<input type="hidden" name="submitAction" value="adminCreateCampaign" />
                            <p><a href="#" class='adminButton adminPrimaryBGColor' id='frmSubmit'>Create New Campaign</a></p>
                        </div>
                    </div>
                    
                    <div class='adminAddRight'>
                        <div class='formElement'>
                            <br /><div class='formElementCol1'><strong>Coach:</strong></div>
                        </div>
                    
                        <!--
                        <div>
                            <br />
                            <input id="intl" class="intl" name="intl" type="checkbox" value=1 /> Outside of the United States or Canada.
                            <br /><br />
                        </div>
                        -->
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>First Name:</strong></div>
                            <div class='formElementCol2'><input name='fname' type='text' class='textFieldSWR validate' /></div>
                        </div>
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Last Name:</strong></div>
                            <div class='formElementCol2'><input name='lname' type='text' class='textFieldSWR validate' /></div>
                        </div>
                        
                        
                        <input type="hidden" name="title" value=""> 
                        <input type="hidden" name="phoneDay" value="">
                        <input type="hidden" name="phoneEve" value=""> 
                        <!--
                        <div class='formElement'>
                            <div class='formElementCol1'><strong>Title:</strong></div>
                            <div class='formElementCol2'><input name='title' type='text' class='textFieldSWR' /></div>
                        </div> -->
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><strong>Address:</strong></div>
                            <div class='formElementCol2'><input name='address' type='text' class='textFieldSWR' /></div>
                        </div>
                        
                        <div class='formElement'>
                            <div class='formElementCol1'>&nbsp;<strong>Address 2:</strong></div>
                            <div class='formElementCol2'><input name='address2' type='text' class='textFieldSWR' /></div>
                        </div>
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><strong>City:</strong></div>
                            <div class='formElementCol2'><input name='city' type='text' class='textFieldSWR' /></div>
                        </div>
                        
                        <div id="domesticAddy">
                          <div class='formElement'>
                                <div class='formElementCol1'><strong>State/Region:</strong></div>
                                <div class='formElementCol2'>
                                <select id='state' name='state' class='selectFieldSWR' size='1'>
                               <option value=''>Please select...</option>
                                <option value=''>&nbsp;</option>
                                <option value=''>---- United States ----</option>
                                    <?php
                                        foreach($states as $state)
                                        {
                                            echo "<option value='".$state['abbrev']."'";
                                            echo ">".$state['name']."</option>";
                                        }
                                    ?>
                                </select>
                                </div>
                          </div>
                          
                          <div class='formElement'>
                                <div class='formElementCol1'><strong>Zip/Postal Code:</strong></div>
                                <div class='formElementCol2'><input name='zip' type='text' class='textFieldSWR' /></div>
                          </div>
                          
                          <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Email:</strong></div>
                                <div class='formElementCol2'><input name='email' type='text' class='textFieldSWR validate' /></div>
                          </div>
                        </div>
                        
                        <div id="intlAddy">
                          <div class='formElement'>
                                <div class='formElementCol1'><strong>Province/Region:</strong></div>
                                <div class='formElementCol2'><input name='stateI' type='text' class='textFieldSWR' /></div>
                          </div>
                          
                          <div class='formElement'>
                                <div class='formElementCol1'><strong>Postal Code:</strong></div>
                                <div class='formElementCol2'><input name='zipI' type='text' class='textFieldSWR' /></div>
                          </div>
                          
                          <div class='formElement'>
                                <div class='formElementCol1'><strong>Country:</strong></div>
                                <div class='formElementCol2'>
                                <select id='country' name='country' class='selectFieldSWR' size='1'>
                                    <option value=''>Please select...</option>
                                    <?php
                                        foreach($countries as $country)
                                        {
                                            //echo "<option value='".$country['abbrev']."'";
                                            //echo ">".$country['name']."</option>";
                                        }
                                    ?>
                                </select>
                                </div>
                          </div>  
                          <div class='formElement'>
                                <div class='formElementCol1'><strong>Email:</strong></div>
                                <div class='formElementCol2'><input name='emailI' type='text' class='textFieldSWR' /></div>
                          </div>
                        <!-- 
                        <div class='formElement'>
                            <div class='formElementCol1'><strong>Daytime Phone:</strong></div>
                            <div class='formElementCol2'><input name='phoneDay' type='text' class='textFieldSWR' /></div>
                        </div>
                        <div class='formElement'>
                            <div class='formElementCol1'><strong>Evening Phone:</strong></div>
                            <div class='formElementCol2'><input name='phoneEve' type='text' class='textFieldSWR' /></div>
                        </div>-->
                        <div class='formElement'>
                            <div class='formElementCol1'><strong>Cell Phone:</strong></div>
                            <div class='formElementCol2'><input name='phoneCell' type='text' class='textFieldSWR' /></div>
                        </div></div>
                        
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Coaches Password:</strong></div>
                            <div class='formElementCol2'><input name='coachPassword' type='text' class='textFieldSWR validate' /></div>
                        </div>
                        <div class='formElement'>
                            <div class='formElementCol1'><span class='alert'>*</span><strong>Coaches Password (validate):</strong></div>
                            <div class='formElementCol2'><input name='coachPassword2' type='text' class='textFieldSWR validate' /></div>
                        
                        </div>
                    
                    </div>
					</form>
                    
	        	<div class='clear'></div> 
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    
            <div id="colorpickerWindow1" class="jqmWindow">
                <div id="colorpicker1"></div>
                <div><a href='#' id='closePickerBtn1'>Click to Close Color Picker</a></div>
            </div>
            <div id="colorpickerWindow2" class="jqmWindow">
                <div id="colorpicker2"></div>
                <div><a href='#' id='closePickerBtn2'>Click to Close Color Picker</a></div>
            </div>
            <div id="colorpickerWindow3" class="jqmWindow">
                <div id="colorpicker3"></div>
                <div><a href='#' id='closePickerBtn3'>Click to Close Color Picker</a></div>
            </div>


<?php
	unset($_SESSION['upload_result']);
?>