<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.form.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/formValidate.js"></script>
		');
	
    startToMainHeader($data);
?>    
    <div class='pageContentWrap adminSecondaryBGColor'>
    
    	<div class='innerPageContentWrap adminPrimaryBGColor'>
        
        	<div class='pageContent'>
            
            <div class='topContent'>
                <div class='welcomeBar'>
                    <div class='welcomeBarCol1'>&nbsp;</div>
                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span></div>
                </div>
                <div class='clear'></div>
                
                <div class='welcomeBarLogin'>
                    <h1>Welcome to the Master Admin Page</h1>
                </div>
            </div>
			<div class="v2Bar" style="color:#FFF; font-weight:bold; text-align:center; padding-top:10px; padding-bottom:10px; background-color:#F00;">Version 2.0</div>
            <div class='contentFull'>

                <div class='loginWrap' id='loginWrap'>
                	<div id='loginDiv'><?php if (isset($error)) echo $error; ?></div>
                    <div id='loginForm'>
                        <form id='frmLogin' name='frmLogin' method='post' action='index.php?action=login'>
                            <p class='teamPrimaryTxtColor'><strong>Username</strong></p>
                            <p><input name='username' type='text' class='textFieldLogin' /></p>
                            
                            <p class='teamPrimaryTxtColor'><strong>Password</strong></p>
                            <p><input name='password' type='text' class='textFieldLogin' /></p>
                            
                            <p><a href='javascript:document.frmLogin.submit();' id='btnLogin' class='adminButton adminPrimaryBGColor'>Login Now</a></p>
                        </form>
                    </div>
                </div>

            <div class='clear'></div>
            </div>
            <!-- /contentFull -->
<div class='clear'></div>
    <div style="display:none;" class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>

<?php
	contentClosures();
	closePageWrapToEnd();
?>