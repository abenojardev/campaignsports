<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	contentWrapToNav();
	
?>    
            <div class="adminSubNav">
                <a href="#" class="sNav"></a>
            <div class="clear"></div>
            </div>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                	<?php 
					include($_SESSION['relative_path'] . 'inc/admin/widgets/adminSearch.php');
					echo "<br>";
					?>
                </div>
                <div class='contentLeftData'>
                	<?php 
					include($_SESSION['relative_path'] . 'inc/admin/widgets/adminTopContacts.php');
					?>
                </div>
                <!--<div class='contentLeftData'>
                	<?php 
					//include($_SESSION['relative_path'] . 'inc/admin/widgets/adminTopDonations.php');
					?>
                </div> -->
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					include($_SESSION['relative_path'] . 'inc/admin/widgets/adminRecentContributions.php');
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->


<?php
	contentClosures();
	closePageWrapToEnd();
?>