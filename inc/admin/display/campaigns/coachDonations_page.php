<?php
	// Add sorting feature to donations 
	$orderDisplayNew = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=DESC" : "&order=ASC";
	$orderDisplayOld = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=ASC" : "&order=DESC";
	
	$order = (isset($get['order'])) ? $get['order'] : 'ASC';
	
	$sort = (isset($get['sort'])) ? $get['sort'] : 'date';
	$pageSort = "&sort=$sort";
	switch($sort) {
		case 'date':
		$sortPass['s'] = 'd.donation_date';
		break;
		case 'amount':
		$sortPass['s'] = 'd.donationValue';
		break;
		case 'donor':
		$sortPass['s'] = 'd.lname';
		break;
		case 'player':
		$sortPass['s'] = 'p.lname';
		break;
		case 'type':
		$sortPass['s'] = 'd.paymentMethod';
		break;
		
		default:
		$sortPass['s'] = 'd.donation_date';
		break;
	}
	$sortPass['o'] = $order;
	$linkSort = "index.php?nav=campaigns&action=donations".$orderDisplayNew."&sort=";
	
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);
	$donationCount = TeamUser::countDonations($_SESSION['current_folder']);
	$donations = TeamUser::getAllDonationsWithPlayerSort($_SESSION['current_folder'],$sortPass);
?>    
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                
                    <?php include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCampaignProgress.php'); ?>
                    <?php include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachDonationUpdate.php'); ?>
                    
                    <h2 class='teamPrimaryTxtColor'>My Team Donations</h2>

					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
                      <tr>
                        <td><strong><a href='<?php echo $linkSort; ?>date'>Date</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>amount'>Amount</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>donor'>Donor</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>player'>Athlete Name</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>type'>Type</a></strong></td>
                        <td align='center'><strong>DEL</strong></td>
                      </tr>
                      
             <?php
			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
				$tpages = ceil($donationCount/15);
				$min = ($page - 1) * 15 + 1;
				$max = $page * 15;
				
				$classAlternate = "bg1";
				$count = 0;
				foreach($donations AS $d) {
					$count++;
					if ($count < $min || $count > $max) continue;
					$date = date("M j, Y", strtotime($d['donation_date']));
					
					echo "
						<tr class='$classAlternate'>
							<td align='left'>" . $date . "</td>
							<td align='left'><a href='index.php?nav=campaigns&action=donations&view=" . $d['ID'] . "'>$" . $d['donationValue'] . "</a></td>
							<td>" . $d['lname'] . ", " . $d['fname'] . " </td>
							<td>" . $d['pLname'] . ", " . $d['pFname'] . " </td>
							<td>" . $d['paymentMethod'] . "</td>
					";
					
					if ($d['paymentMethod'] == 'check') {
							echo "<td align='center'><a href='index.php?nav=campaigns&action=donations&delete=".$d['ID']."' class='delCheck'>X</a></td>";
					} else {
							echo "<td></td>";
					}
					
					echo "
						</tr>
					";
	
					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
				}
	
			?>
                    </table>
                    	
            <?php
                echo "<div class='pagination'><br/>";
				//$reload = $_SERVER['PHP_SELF'] . "?nav=campaigns&action=" . $_SESSION["nav2"];
				$reload = $_SERVER['PHP_SELF'] . "?nav=campaigns&action=" . $_SESSION["nav2"].$pageSort.$orderDisplayOld;
            	include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
				echo paginate($reload, $page, $tpages, 3);
				echo "</div>";
			?>
                    
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
			<script language="javascript" type="text/javascript">
			/* <![CDATA[ */
				$('.delCheck').click(function(event) {
					var r=confirm("Are you sure you want to delete this check? \nThis action is irreversible.");
					if (r==false) {
						event.preventDefault();
					}		
				});
			/* ]]> */
			</script>
            
        	<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/manualCheckDonation.php');
                        //include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachFailedDonations.php');
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/manualCheckDonation.php');
                        //include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachFailedDonations.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->

