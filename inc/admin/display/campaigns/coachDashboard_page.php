<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
?>    
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                	<?php 
						if ( $status == 1 ) { // Phase 1
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionProgress.php');
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionUpdate.php');
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachTopContributors.php');
						} 
						else {
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCampaignProgress.php');
                    		include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachDonationUpdate.php');
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachTopAchievers.php');
						}
					?>
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        //include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/adminCampaignNotes.php');
                    } 
                    else {
                        //include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/adminCampaignNotes.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->

