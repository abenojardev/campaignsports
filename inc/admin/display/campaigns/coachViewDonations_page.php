<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
	$ID = array();
	$ID['ID'] = $_GET['view'];
	$ID['table'] = 'donations';
	$donationData = TeamUser::getDonationData($_GET['view']);
?>    
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                
                    <?php include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCampaignProgress.php'); ?>
                    
                    <h2 class="teamPrimaryTxtColor">Donor Information</h2>

					<div class="registerWrap">
                        <p><strong>Sponsored Athlete:</strong><strong> <a href='index.php?nav=campaigns&action=players&edit=<?php echo $donationData['pID']; ?>'><?php echo $donationData['pFname']." ".$donationData['pLname']; ?></a></strong></p>
                        <br />
                        <p><strong>Payment method:</strong> <?php echo ucfirst($donationData['paymentMethod']); ?> Donation</p>
                        <p><strong>Date:</strong> <?php 
							if ($donationData['payment_date']) {
								echo date("F j, Y", strtotime($donationData['payment_date']));
							} else {
								echo date("F j, Y", strtotime($donationData['donation_date']));
							}
						?> </p>
                        <p><strong>Amount:</strong> <strong class="teamPrimaryTxtColor">$ <?php echo $donationData['donationValue']; ?></strong></p>
                        <p><strong>Card type:</strong> <?php echo $donationData['cardType']; ?></p>
                        <p><strong>Status:</strong> <?php echo $donationData['r_approved']; ?></p>
                        <p><strong>Reference code:</strong><?php echo $donationData['r_ref']; ?> <?php echo $donationData['txn_id']; ?></p>
                        <br />
                        <p><strong>Name:</strong> <?php echo $donationData['fname']." ".$donationData['lname']; ?></p>
                        <p><strong>Address:</strong> <?php echo $donationData['address']; ?></p>
                        <p><strong>Address 2:</strong> <?php echo $donationData['address2']; ?></p>
                        <p><strong>City:</strong> <?php echo $donationData['city']; ?></p>
                        <p><strong>State:</strong> <?php echo $donationData['state']; ?></p>
                        <p><strong>Zip:</strong> <?php echo $donationData['zip']; ?></p>
                        <p><strong>Country:</strong> <?php echo $donationData['country']; ?></p>
                        <p><strong>Phone:</strong> <?php echo $donationData['phone']; ?></p>
                        <p><strong>Email:</strong> <?php echo $donationData['email']; ?></p>

                        <div class="registerButton"><br />
                            <p><a href="javascript:history.back()" class="teamButton teamPrimaryBGColor">Return to Previous Page</a></p>
                        </div>
                    </div>

                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->

