<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$campStatus = TeamUser::getCampaignStatus11($_SESSION['current_folder']);
	$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
	$contactCount = TeamUser::countContacts($_SESSION['campaign_team']);
	$contacts = TeamUser::getAllContactsWithPlayerNameSort($_SESSION['campaign_team']);
?>    
			<?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                
                <?php 
					include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionProgress.php'); 
					include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionUpdate.php');
				?>
                
                <h2 class='teamPrimaryTxtColor'>My Team Contacts</h2>

					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
                      <tr>
                        <td><strong>Contact Name</strong></td>
                        <td><strong>City</strong></td>
                        <td align='center'><strong>State</strong></td>
                        <td><strong>Athlete Name</strong></td>
                        <td align='center'><strong>DEL</strong></td>
                      </tr>
                      
             <?php
			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
				$tpages = ceil($contactCount/15);
				$min = ($page - 1) * 15 + 1;
				$max = $page * 15;
				
				$classAlternate = "bg1";
				$count = 0;
				foreach($contacts AS $c) {
					$count++;
					if ($count < $min || $count > $max) continue;
					if($campStatus==11) {
					echo "
						<tr class='$classAlternate'>
							<td align='left'><a href='index.php?nav=campaigns&action=contacts&edit=" . $c['ID'] . "'>" . $c['lname'] . ", " . $c['fname'] . "</a></td>
							<td>" . $c['city'] . "</td>
							<td align='center'>" . $c['state'] . "</td>
							<td>" . $c['pFname'] . " " . $c['pLname'] . "</td>
							<td align='center'>N/A</td>
						</tr>
					";
					}else {
					echo "
						<tr class='$classAlternate'>
							<td align='left'><a href='index.php?nav=campaigns&action=contacts&edit=" . $c['ID'] . "'>" . $c['lname'] . ", " . $c['fname'] . "</a></td>
							<td>" . $c['city'] . "</td>
							<td align='center'>" . $c['state'] . "</td>
							<td>" . $c['pFname'] . " " . $c['pLname'] . "</td>
							<td align='center'><a href='index.php?nav=campaigns&action=contacts&delete=".$c['ID']."' class='delCheck'>X</a></td>
						</tr>
					";
					}
	
					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
				}
	
			?>
                    </table>
                    	
            <?php
                echo "<div class='pagination'><br/>";
				$reload = $_SERVER['PHP_SELF'] . "?nav=campaigns&action=" . $_SESSION["nav2"];
            	include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
				echo paginate($reload, $page, $tpages, 3);
				echo "</div>";
			?>
                    
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
			<script language="javascript" type="text/javascript">
			/* <![CDATA[ */
				$('.delCheck').click(function(event) {
					var r=confirm("Are you sure you want to delete this contact? \nThis action is irreversible.");
					if (r==false) {
						event.preventDefault();
					}		
				});
			/* ]]> */
			</script>
            
        	<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->

