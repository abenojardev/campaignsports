<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$team_id = $_SESSION['campaign_team'];
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	$name = $TeamMain->getTeamName();
	$type = $TeamMain->getTeamType();
	$password = $TeamMain->getTeamPassword();
	$designerID = $TeamMain->getTeamDesigner();
	$printerID = $TeamMain->getTeamPrinter();
	$baseCost = $TeamMain->getTeamBaseCost();
	$reason = ($TeamMain->getTeamBaseCostReason()) ? $TeamMain->getTeamBaseCostReason() : "";
	$mailDate = ($TeamMain->getTeamMailDate()) ? date("M j, Y", strtotime($TeamMain->getTeamMailDate())) : "";
	$public = $TeamMain->getTeamPublic();
	
	$brochureInfo = $TeamMain->getTeamBrochureInfo();
	$brochureCount = ($brochureInfo{'brochure_count'}) ? $brochureInfo{'brochure_count'} : "";
	$brochureCost = ($brochureInfo{'brochure_cost'}) ? $brochureInfo{'brochure_cost'} : "";
	$brochureCharges = ($brochureInfo{'brochure_charges'}) ? $brochureInfo{'brochure_charges'} : "";
	
	$statusCheck1 = ($status == 1) ? "checked" : "";
	$statusCheck2 = ($status == 2) ? "checked" : "";
	$statusCheck3 = ($status == 3) ? "checked" : "";
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contacts = TeamUser::countContacts($_SESSION['campaign_team']);
	$donations = TeamUser::getDonationTotal($_SESSION['campaign_team']);
	$players = TeamUser::getAllPlayersNameSort($_SESSION['campaign_team']);
	$coach = TeamUser::getAdminData($_SESSION['campaign_team']);
	
	//$contactsUS = TeamUser::countContactsDomestic($_SESSION['campaign_team']);
	//$contactsCA = TeamUser::countContactsCanada($_SESSION['campaign_team']);
	//$contactsIntl = TeamUser::countContactsIntl($_SESSION['campaign_team']);
	
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
    echo "<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/ui-lightness/jquery-ui-1.8.17.custom.css' />
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-ui-1.8.17.custom.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/ckeditor/ckeditor.js'></script>
	";
	
?>    

	<script language="javascript" type="text/javascript">
    <!--//
	

	
	
        function submitform()
        {
			var player = document.frm2.player.value;
			document.frm2.playerID.value=player;
			
            document.frm2.submit();
        }
        function submitform2()
        {
			var player = document.frm2a.player.value;
			document.frm2a.playerID.value=player;
			
            document.frm2a.submit();
        }

		$(function() {
			$("#datepicker").datepicker();
			
			$("a#benchmarks").click(function() {
				$("div#benchmarks").css("display","block");
				$("div#messages").css("display","none");
				$("div#designer").css("display","none");
				$("div#printer").css("display","none");
				$("div#donations").css("display","none");
				$("div#exports").css("display","none");
				$("div#basics").css("display","none");
			});
			$("a#messages").click(function() {
				$("div#benchmarks").css("display","none");
				$("div#messages").css("display","block");
				$("div#designer").css("display","none");
				$("div#printer").css("display","none");
				$("div#donations").css("display","none");
				$("div#exports").css("display","none");
				$("div#basics").css("display","none");
			});
			$("a#designer").click(function() {
				$("div#benchmarks").css("display","none");
				$("div#messages").css("display","none");
				$("div#designer").css("display","block");
				$("div#printer").css("display","none");
				$("div#donations").css("display","none");
				$("div#exports").css("display","none");
				$("div#basics").css("display","none");
			});
			$("a#printer").click(function() {
				$("div#benchmarks").css("display","none");
				$("div#messages").css("display","none");
				$("div#designer").css("display","none");
				$("div#printer").css("display","block");
				$("div#donations").css("display","none");
				$("div#exports").css("display","none");
				$("div#basics").css("display","none");
			});
			$("a#donations").click(function() {
				$("div#benchmarks").css("display","none");
				$("div#messages").css("display","none");
				$("div#designer").css("display","none");
				$("div#printer").css("display","none");
				$("div#donations").css("display","block");
				$("div#exports").css("display","none");
				$("div#basics").css("display","none");
			});
			$("a#exports").click(function() {
				$("div#benchmarks").css("display","none");
				$("div#messages").css("display","none");
				$("div#designer").css("display","none");
				$("div#printer").css("display","none");
				$("div#donations").css("display","none");
				$("div#exports").css("display","block");
				$("div#basics").css("display","none");
			});
			$("a#basics").click(function() {
				$("div#benchmarks").css("display","none");
				$("div#messages").css("display","none");
				$("div#designer").css("display","none");
				$("div#printer").css("display","none");
				$("div#donations").css("display","none");
				$("div#exports").css("display","none");
				$("div#basics").css("display","block");
			});
		});



    //-->
    </script>
    
            <?php
            	if ($get['sub'] || $get['m']) {
					echo "	
						<script language='javascript'>
						<!--//
							$(function() {
								$('div#benchmarks').css('display','none');
								$('div#messages').css('display','block');
							});
						//-->
						</script>
					";
				}
			?>
		
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentFull'>
            
            <div class='adminSubNav'>
                <a href='javascript:();' id='benchmarks' class='sNav'>Benchmarks / Assignments</a>
				&nbsp;|&nbsp;
                <!--<a href='javascript:();' id='messages' class='sNav'>Messages</a>
				&nbsp;|&nbsp;
                <a href='javascript:();' id='designer' class='sNav'>Designer Info</a>
				&nbsp;|&nbsp;
                <a href='javascript:();' id='printer' class='sNav'>Printer Info</a>  
				&nbsp;|&nbsp;
                <a href='javascript:();' id='donations' class='sNav'>Donation Info</a>
				&nbsp;|&nbsp; -->
                <a href='javascript:();' id='exports' class='sNav'>Exports</a>
				&nbsp;|&nbsp;
                <a href='javascript:();' id='basics' class='sNav'>Basics</a>
            <div class='clear'></div>
            </div>
            
                <div class='contentFullData'>
                	<div id='benchmarks' style='display:block;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Specific Tasks - Campaign Benchmarks / Assignments</h2>
                        
					<?php
						require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
						$campaignStatuses = Campaign::getCampaignStatuses($team_id);
						
						
						
                    	
                    	// Update Mailing Costs
						// Up here for stacking order
						if ($campaignStatuses[15] && !$campaignStatuses[18]) {
							require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
							$costs = Printer::getCosts($team_id);
							echo "
	
								<div class='adminBox'>
									<form method='post' name='frmAdminUpdateCosts' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Brochure: Financial Info</h3>
									</div>
									<div class='adminCol2'>
										<input type='text' name='brochure_count' value='$brochureCount' size='5' class='selectFieldSWR' style='text-align:center;' />&nbsp; Number of Brochures
										<input type='text' name='brochure_cost' value='$brochureCost' size='5' class='selectFieldSWR' style='text-align:center;' />&nbsp; Cost per Brochures
										<input type='text' name='brochure_charges' value='$brochureCharges' size='5' class='selectFieldSWR' style='text-align:center;' />&nbsp; Additional Charges<br />
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminUpdateBrochureInfo' />
										<a href='javascript:document.frmAdminUpdateCosts.submit();' class='teamButton teamPrimaryBGColor'>Update Mailing Info</a>
									</div>
									</form>
									<div class='clear'></div> 
								</div>
							";
						}
						
						
                    	// FIRST CHECK - if v1 campaign, allow for archive
						// MUST check first
						if ($team_id <= $cfg_v2cutoffTeam) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmCompleteCampaign' action='index.php?nav=campaigns'>
									<div class='adminCol1'>
										<h3>Archive this campaign</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminArchiveCampaign' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmCompleteCampaign.submit();' class='teamButton teamPrimaryBGColor'>Archive Campaign</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						} else {
						
                    	// Start Campaign
						if (!$campaignStatuses[2]) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmStartCampaign' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Campaign Waiting to be Started</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminStartCampaign' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmStartCampaign.submit();' class='teamButton teamPrimaryBGColor'>Start Campaign</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						} else {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmStartCampaign' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Resend Campaign Start Emails</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminResendStartInfo' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmStartCampaign.submit();' class='teamButton teamPrimaryBGColor'>Resend Emails</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div> 
							";
						}
						
						// Awaiting Contacts
						if ($campaignStatuses[10] && !$campaignStatuses[8] && !$campaignStatuses[11]) {
							echo "
								<div class='adminBox'>
									
									
										<h3>Awaiting Athletes to Add Contacts</h3>
									
									
								<div class='clear'></div> 
								</div>
							";
						}
                    	// Awaiting Team Information
						if ($campaignStatuses[2] && 
									(!$campaignStatuses[3]) || ($campaignStatuses[3] && $campaignStatuses[3]['pending'])) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Team Info Submission</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Approve Team Information
						if ($campaignStatuses[3] && !$campaignStatuses[4]) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmApproveTeamInfo' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Team Info Waiting for Approval</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<a href='index.php?nav=campaigns&action=campaignActivity&subAction=reqInfo' target='_blank'>View Team Info</a>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminApproveTeamInfo' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmApproveTeamInfo.submit();' class='teamButton teamPrimaryBGColor'>Approve Team Info</a>
									</div>
									<div class='clear'></div> 
									</form>
									<br /><br />
									<hr />
									<br /><br />
									<form method='post' name='frmRejectTeamInfo' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2b' style='width:500px;'>
										If for any reason you do not approve of the current Team Information, please use the box below to send a message to the Coach explaining why.
									</div>
									<div class='adminCol3'>
										<p align='right'>
											<input type='hidden' name='tID' value='".$team_id."'>
											
											<input type='hidden' name='recip' value='Coach'>
											<input type='hidden' name='recipID' value='".$coach['ID']."'>
											<input type='hidden' name='sender' value='Admin'>
											<input type='hidden' name='senderID' value='3'>
				
											<input type='hidden' name='benchmark' value='Team Info'>
											<input type='hidden' name='benchmarkID' value='0'>
											
											<input type='hidden' name='submitAction' value='adminRejectTeamInfo'>
											<a href='javascript:document.frmRejectTeamInfo.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
										</p>
									</div>
									<div class='adminCol2b'>
										<br /><br />
										<input class='textField' name='subject' size='66' value='Subject...' />
										<br />
										<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
										<br /><br />
									</div>
									</form>
									
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Sumbit Brochure Copy
						if ($campaignStatuses[4] && !$campaignStatuses[5]) {
							echo "
								<div class='adminBox'>
								
									<form method='post' name='frmUpdateBrochureCopy' action='index.php?nav=campaigns&action=masterAdmin'>
									
									<div>
										Please enter the final written copy for the brochure for this campaign.
									</div>
									<div>
										<br />
										<textarea class='ckeditor' id='finalCopy' name='finalCopy' cols='100' rows='20'></textarea>	
										<br />
									</div>
									<div class='clear'></div>
									
									<div>
										<br />
										<input type='hidden' name='submitAction' value='updateBrochureCopy' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmUpdateBrochureCopy.submit();' class='teamButton teamPrimaryBGColor'>Submit Brochure Copy</a>
									</div>
									</form>
									<script type='text/javascript'>
									//CKEDITOR.replace('finalCopy', { extraPlugins: 'charcount', maxLength: 0, toolbar: 'TinyBare', toolbar_TinyBare: [
									//[ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
									//[ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ],
									//[ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
									//[ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
									//[ 'NumberedList','BulletedList','Outdent','Indent' ],
									//[ 'Source' ],
									//['CharCount']] });
									
									
CKEDITOR.editorConfig = function( config )
{
	//config.extraPlugins = 'charcount';
	config.toolbar = 'MyToolbar';
	config.toolbar_MyToolbar =
	[
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','CharCount' ] }
	];

};								

CKEDITOR.replace( 'finalCopy',
{
	extraPlugins: 'charcount', maxLength: 0, toolbar : 'MyToolbar'
});							
									
									
									
								  </script>
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Awaiting brochure copy approval from coach
						if ($campaignStatuses[5] && $campaignStatuses[5]['pending'] == 0 && !$campaignStatuses[6]) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Brochure Copy Approval</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Fix Brochure Copy
						if ($campaignStatuses[5] && $campaignStatuses[5]['pending'] == 1) {
							require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
							$campaign = Designer::getSingleCampaign($team_id);
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmUpdateBrochureCopy' action='index.php?nav=campaigns&action=masterAdmin'>
									
									<div>
										Please edit the final written copy for the brochure for this campaign.
									</div>
									<div>
										<br />
										<textarea class='ckeditor' name='finalCopy' cols='100' rows='20'>" .$campaign['final_copy']. "</textarea>	
										<br />
									</div>
									<div class='clear'></div>
									
									<div>
										<br />
										<input type='hidden' name='submitAction' value='updateBrochureCopy' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmUpdateBrochureCopy.submit();' class='teamButton teamPrimaryBGColor'>Update Brochure Copy</a>
									</div>
									</form>
									<script type='text/javascript'>
									//CKEDITOR.replace('finalCopy', { extraPlugins: 'charcount', maxLength: 0, toolbar: 'TinyBare', toolbar_TinyBare: [
									//[ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ],
									//[ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ],
									//[ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ],
									//[ 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ],
									//[ 'NumberedList','BulletedList','Outdent','Indent' ],
									//[ 'Source' ],
									//['CharCount']] });
									
									
CKEDITOR.editorConfig = function( config )
{
	//config.extraPlugins = 'charcount';
	config.toolbar = 'MyToolbar';
	config.toolbar_MyToolbar =
	[
	{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
	{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
	{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
	{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
	{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
	{ name: 'insert', items : [ 'Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak','Iframe' ] },
	{ name: 'tools', items : [ 'Maximize', 'ShowBlocks','-','CharCount' ] }
	];

};								

CKEDITOR.replace( 'finalCopy',
{
	extraPlugins: 'charcount', maxLength: 0, toolbar : 'MyToolbar'
});							
									
									
									
								  </script>
								<div class='clear'></div> 
								</div>
							";
						}
						
						// Assign Designer
						if ($campaignStatuses[6] && !$campaignStatuses[7]) {
							require_once($relPath . 'inc/admin/class/Admin.php');
							$designers = Admin::getAllDesigners();
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmAssignDesigner' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Assign/Reassign Designer</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:0px;'>
										<select name='designer'>
											<option></option>
							";
							foreach ($designers AS $id => $data) {
								if($designerID==$data['ID']){
									$checkDesigner = 'selected="selected"';
								} else {
									$checkDesigner = '';
								}
						
								echo "<option value='".$data['ID']."' $checkDesigner>".$data['fname']." ".$data['lname']."</option>";
							}
							echo "
										</select>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='assignDesigner' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmAssignDesigner.submit();' class='teamButton teamPrimaryBGColor'>Assign Designer</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
                    	// Reassign Designer
						if ($campaignStatuses[7] && !$campaignStatuses[16]) {
							require_once($relPath . 'inc/admin/class/Admin.php');
							$designers = Admin::getAllDesigners();
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmAssignDesigner' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Reassign Designer</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:0px;'>
										<select name='designer'>
											<option></option>
							";
							foreach ($designers AS $id => $data) {
								if($designerID==$data['ID']){
									$checkDesigner = 'selected="selected"';
								} else {
									$checkDesigner = '';
								}
						
								echo "<option value='".$data['ID']."' $checkDesigner>".$data['fname']." ".$data['lname']."</option>";
							}
							echo "
										</select>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='assignDesigner' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmAssignDesigner.submit();' class='teamButton teamPrimaryBGColor'>Reassign Designer</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Awaiting Design from Designer
						if (($campaignStatuses[7] && !$campaignStatuses[9]) || 
									($campaignStatuses[9] && $campaignStatuses[9]['pending'] == 2)) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Brochure Design</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}
						// Approve / Reject Brochure Design
						if ($campaignStatuses[9] && $campaignStatuses[9]['pending'] == 1) {
							require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
							$design = Designer::getCurrentDesign($team_id);
							include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
							$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/designer/";
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmApproveBrochure' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Brochure Design Waiting for Approval</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<div style='float:left;'><img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' /></div>
										<div style='float:left;margin-top:10px;'><a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$design['filename']."&p=$dirname". $design['filename'] . "' target='_blank'>Click here to download brochure</a></div>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='tID' value='$team_id' />
										<input type='hidden' name='dID' value='". $design['ID'] . "' />
										<input type='hidden' name='designerID' value='".$designerID."'>
										<input type='hidden' name='submitAction' value='adminApproveBrochure' />
										<a href='javascript:document.frmApproveBrochure.submit();' class='teamButton teamPrimaryBGColor'>Approve Brochure</a>
									</div>
									<div class='clear'></div> 
									</form>
									<br /><br />
									<hr />
									<br /><br />
									<form method='post' name='frmRejectBrochure' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2b' style='width:500px;'>
										If for any reason you do not approve of the current Team Brochure, please use the box below to send a message to the Designer explaining why.
									</div>
									<div class='adminCol3'>
										<p align='right'>
											<input type='hidden' name='tID' value='".$team_id."'>
											<input type='hidden' name='recipID' value='".$designerID."'>
											<input type='hidden' name='dID' value='". $design['ID'] . "'>
											
											<input type='hidden' name='submitAction' value='adminRejectBrochure'>
											<a href='javascript:document.frmRejectBrochure.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
										</p>
									</div>
									<div class='adminCol2b'>
										<br /><br />
										<input class='textField' name='subject' size='66' value='Subject...'/>
										<br />
										<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
										<br /><br />
									</div>
									</form>
									
								<div class='clear'></div> 
								</div>
							";
						}

                    	// Awaiting Approval of Design from Coach
						if (($campaignStatuses[9] && !$campaignStatuses[9]['pending']) && !$campaignStatuses[10]) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Brochure Approval</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}

                    	// Coach Rejected Brochure Design - Back to Designer
						if ($campaignStatuses[9] && $campaignStatuses[9]['pending'] == 3) {
							require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
							$design = Designer::getCurrentDesign($team_id);
							include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
							$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/designer/";
							
							echo "
								<div class='adminBox'>
									<div class='adminCol1'>
										<h3>Brochure Design Back to Designer</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<div style='float:left;'><img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' /></div>
										<div style='float:left;margin-top:10px;'><a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$design['filename']."&p=$dirname". $design['filename'] . "' target='_blank'>Click here to download brochure</a></div>
									</div>
									<div class='adminCol3'>
									</div>
									<div class='clear'></div> 
									<br /><br />
									<hr />
									<br /><br />
									<form method='post' name='frmReturnBrochure' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2b' style='width:500px;'>
										The Coach does not approve of the current Team Brochure, please use the box below to send a message to the Designer explaining why.
									</div>
									<div class='adminCol3'>
										<p align='right'>
											<input type='hidden' name='tID' value='".$team_id."'>
											<input type='hidden' name='recipID' value='".$designerID."'>
											<input type='hidden' name='dID' value='". $design['ID'] . "'>
											
											<input type='hidden' name='submitAction' value='adminRejectBrochure'>
											<a href='javascript:document.frmReturnBrochure.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
										</p>
									</div>
									<div class='adminCol2b'>
										<br /><br />
										<input class='textField' name='subject' size='66' value='Subject...'/>
										<br />
										<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
										<br /><br />
									</div>
									</form>
									
								<div class='clear'></div> 
								</div>
							";
						}
						
						// Approve / Reject Team Page
						if (($campaignStatuses[25] && !$campaignStatuses[25]['pending']) && $campaignStatuses[26] && $campaignStatuses[27]['pending'] == 1) {
							require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
							$design = Designer::getCurrentDesign($team_id);
							include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
							$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/designer/";
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmApproveTeamPage' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Team Page Waiting for Approval</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										
										<div style='float:left;margin-top:10px;'><a href='" . $_SESSION['relative_path'] . "team/" . $team_id . "/share/' target='_blank'>Click here to view team page</a></div>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='tID' value='". $team_id . "' />
										
										<input type='hidden' name='designerID' value='".$designerID."'>
										<input type='hidden' name='submitAction' value='adminApproveTeamPage' />
										<a href='javascript:document.frmApproveTeamPage.submit();' class='teamButton teamPrimaryBGColor'>Approve Team Page</a>
									</div>
									<div class='clear'></div> 
									</form>
									<br /><br />
									<hr />
									<br /><br />
									<form method='post' name='frmRejectTeamPage' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2b' style='width:500px;'>
										If for any reason you do not approve of the current Team Page Image, please use the box below to send a message to the Designer explaining why.
									</div>
									<div class='adminCol3'>
										<p align='right'>
											<input type='hidden' name='tID' value='".$team_id."'>
											<input type='hidden' name='recipID' value='".$designerID."'>
											
											<input type='hidden' name='submitAction' value='adminRejectTeamPage'>
											<a href='javascript:document.frmRejectTeamPage.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
										</p>
									</div>
									<div class='adminCol2b'>
										<br /><br />
										<input class='textField' name='subject' size='66' value='Subject...'/>
										<br />
										<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
										<br /><br />
									</div>
									</form>
									
								<div class='clear'></div> 
								</div>
							";
						}



						
						// Awaiting team page image from Designer
						if ($campaignStatuses[7] && (!$campaignStatuses[25] || $campaignStatuses[25]['pending'] == 1)) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Team Page Image From Designer</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}
						// Awaiting team page copy from admin
						if ($campaignStatuses[6]) {
							echo "
								<div class='adminBox'>
									
								<h3>Team Page Content</h3>
								<br>
								<form method='post' name='frmPublic' action='index.php?nav=campaigns&action=masterAdmin'>
										<div class='formElement'>
											<div class='formElementCol1'><span class='alert'>*</span><strong>Team Public Page Content:</strong></div>
											<div class='formElementCol2'>
											<textarea class='ckeditor' name='publicCopy' cols='92' rows='20'>".$public['publicCopy']."</textarea> 
											</div>
										</div><br />
			<br />
			
										<div class='formElement'>
											<div class='formElementCol1'><span class='alert'>*</span><strong>Team Public Page Mailing Address:</strong></div>
											<div class='formElementCol2'>
											<textarea class='ckeditor' name='publicAddress' cols='92' rows='10'>".$public['publicAddress']."</textarea> 
											</div>
										</div>
					
										<div class='registerButton'>
											<p><a href='javascript:document.frmPublic.submit();' class='teamButton teamPrimaryBGColor'>Update Public Page Content</a></p><br />
										</div>
								<input type='hidden' name='submitAction' value='adminUpdatePublic' />
								</form>
								<div class='clear'></div> 
								</div>
							";
						}
						
						
                    	
						
						// End Contact Collection/Activate Campaign
						if ($campaignStatuses[7] && $campaignStatuses[8] && !$campaignStatuses[11]) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmCloseCollections' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Activate Campaign</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminCloseCollections' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmCloseCollections.submit();' class='teamButton teamPrimaryBGColor'>Activate Campaign</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
						// Re-Open Contact Collections/Deactivate Campaign
						if ($campaignStatuses[11] && !$campaignStatuses[18]) {
							echo "
								 
								<div class='adminBox'>
									<form method='post' name='frmCloseCollections' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>De-activate Campaign</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminReopenCollections' />
										<input type='hidden' name='tID' value='$team_id' />
										<a href='javascript:document.frmCloseCollections.submit();' class='teamButton teamPrimaryBGColor'>De-activate Campaign</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
								
							";
						}
                    	// Scrub Data
//						else if ($campaignStatuses[11] && !$campaignStatuses[12]) {
//							echo "
//								<!--<div class='adminBox'>
//									<div class='adminCol1'>
//										<h3>Scrub Data</h3>
//									</div>
//									<div class='adminCol2'>
//									</div>
//									<div class='adminCol3'>
//										<a href='scrubber.php?id=$team_id' target='_blank' class='teamButton teamPrimaryBGColor'>Scrub Data</a>
//									</div>
//								<div class='clear'></div> 
//								</div>-->
//							";
//						}

						

                    	// Assign Printer
						if ($campaignStatuses[98] && !$campaignStatuses[99]) { 
						//else if ($campaignStatuses[11] && !$campaignStatuses[12])
							require_once($relPath . 'inc/admin/class/Admin.php');
							$printers = Admin::getAllPrinters();
							//$fcontacts = Admin::filterContacts();
							echo "
								<!--<div class='adminBox'>
									<div>
									
									
									

									</div>
								
								
									<div class='adminCol1'>
										<h3>Scrub Data</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<a href='scrubber.php?id=$team_id' target='_blank' class='teamButton teamPrimaryBGColor'>Scrub Data</a>
									</div>
									<div class='clear'></div>
								<div class='clear'></div> 
								</div>-->
							";
							
							require_once($relPath . 'inc/designer/class/Designer.php');
							$finalFiles = Designer::getFinalFiles($team_id);
							if (isset($finalFiles['final'])) {
								echo "
									<div class='adminBox'>
										<form method='post' name='frmAssignPrinter' action='index.php?nav=campaigns&action=masterAdmin'>
										<div class='adminCol1'>
											<h3>Assign Printer</h3>

										</div>
										<div class='adminCol2' style='margin-top:2px;margin-left:0px;'>
											<select name='printer'>
												<option></option>
								";
								
								foreach ($printers AS $id => $data) {
									echo "<option value='".$data['ID']."'>".$data['fname']." ".$data['lname']."</option>";
								}
							
								echo "
											</select>
										</div>
										<div class='adminCol3'>
											<input type='hidden' name='submitAction' value='assignPrinter' />
											<input type='hidden' name='tID' value='$team_id' />
											<a href='javascript:document.frmAssignPrinter.submit();' class='teamButton teamPrimaryBGColor'>Assign Printer</a>
										</div>
										</form>
										<div class='clear'></div>
										<p><span style='color:#ff0000; font-size:11px; font-style:italic;'>IMPORTANT: Do not assign a printer until you have completed the scrubbing of the contact data,<br />
otherwise the printer will recieve the incomplete version.</span></p>
									<div class='clear'></div> 
									</div>
								";
							} else {
								echo "
									<div class='adminBox'>
										<div class='adminCol1a'>
											<h3>Waiting on Final File Upload from Designer</h3>
										</div>
										<div class='clear'></div>
									<div class='clear'></div> 
									</div>
								";
							}
						}

                    	// Awaiting Approval of Contact List from Printer
						if (($campaignStatuses[98] && !$campaignStatuses[99]) || ($campaignStatuses[99] && $campaignStatuses[99]['pending'] == 2)) {
						//else if (($campaignStatuses[12] && !$campaignStatuses[13]) || ($campaignStatuses[13] && $campaignStatuses[13]['pending'] == 2)) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Contact List Approval</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Awaiting Rescrub of Contact List Printer
						if ($campaignStatuses[99] && $campaignStatuses[99]['pending'] == 1) {
							//else if ($campaignStatuses[13] && $campaignStatuses[13]['pending'] == 1) {
							echo "
							<div class='adminBox'>
									<div class='adminCol1'>
										<h3>Re-Scrub Data</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<a href='scrubber.php?id=$team_id' target='_blank' class='teamButton teamPrimaryBGColor'>Scrub Data</a>
									</div>
									<div class='clear'></div>
								<div class='clear'></div> 
								</div>
								
								<div class='adminBox'>
									<form method='post' name='frmRescrub' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Submit Re-Scrubbed Contact List</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='tID' value='$team_id' />
										<input type='hidden' name='printerID' value='$printerID' />
										<input type='hidden' name='submitAction' value='adminResendContactList' />
										<a href='javascript:document.frmRescrub.submit();' class='teamButton teamPrimaryBGColor'>Send New List</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
								
								
							";
						}
						
                    	// Awaiting Contact List Approval from Printer
						if ($campaignStatuses[98] && !$campaignStatuses[99]) {
						//else if ($campaignStatuses[12] && !$campaignStatuses[13]) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Printer to Approve Contact List</h3>

								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Awaiting Proof from Printer
						if (($campaignStatuses[98] && !$campaignStatuses[99]) || ($campaignStatuses[99] && $campaignStatuses[99]['pending'] == 2)) {
						
						//else if (($campaignStatuses[13] && !$campaignStatuses[14]) || ($campaignStatuses[14] && $campaignStatuses[14]['pending'] == 2)) {
							echo "
								<div class='adminBox'>
										<h3>Waiting for Printer Proof</h3>

								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Approve / Reject Printer Proof
						if ($campaignStatuses[99] && $campaignStatuses[99]['pending'] == 1) {
						//else if ($campaignStatuses[14] && $campaignStatuses[14]['pending'] == 1) {
							require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
							$proof = Printer::getCurrentProof($team_id);
							$envelope = Printer::getCurrentEnvelope($team_id);
							include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
							$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/printer/";
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmApproveProof' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Printer Proof Waiting for Approval</h3>
									</div>
									<!--
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<div style='float:left;'><img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' /></div>
										<div style='float:left;margin-top:10px;'><a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$proof['filename']."&p=$dirname". $proof['filename'] . "' target='_blank'>Click here to download proof</a></div>
									</div>
									-->
									<div class='adminCol3'>
										<input type='hidden' name='tID' value='$team_id' />
										<input type='hidden' name='pID' value='". $proof['ID'] . "' />
										<input type='hidden' name='printerID' value='".$printerID."'>
										<input type='hidden' name='submitAction' value='adminApproveProof' />
										<a href='javascript:document.frmApproveProof.submit();' class='teamButton teamPrimaryBGColor'>Approve Proof</a>
									</div>
									<div class='clear'></div> 
							";
							
							if (isset($envelope['filename'])) {
								echo "
									<div class='adminCol1'>
										&nbsp;
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<div style='float:left;'><img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' /></div>
										<div style='float:left;margin-top:10px;'><a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$envelope['filename']."&p=$dirname". $envelope['filename'] . "' target='_blank'>Click here to download envelope</a></div>
									</div>
									<div class='adminCol3'>
									</div>
									<div class='clear'></div> 
								";
							}
									
							echo "
									</form>
									<br /><br />
									<hr />
									<br /><br />
									<form method='post' name='frmRejectProof' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2b' style='width:500px;'>
										If for any reason you do not approve of the current Team Proof, please use the box below to send a message to the Printer explaining why.
									</div>
									<div class='adminCol3'>
										<p align='right'>
											<input type='hidden' name='tID' value='".$team_id."'>
											<input type='hidden' name='recipID' value='".$printerID."'>
											<input type='hidden' name='pID' value='". $proof['ID'] . "'>
											
											<input type='hidden' name='submitAction' value='adminRejectProof'>
											<a href='javascript:document.frmRejectProof.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
										</p>
									</div>
									<div class='adminCol2b'>
										<br /><br />
										<input class='textField' name='subject' size='66' value='Subject...'/>
										<br />
										<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
										<br /><br />
									</div>
									</form>
									
								<div class='clear'></div> 
								</div>
							";
						}

                    	// Awaiting Approval of Proof from Coach
						// *** DISABLED ***
						if (($campaignStatuses[14] && !$campaignStatuses[14]['pending']) && !$campaignStatuses[15]) {
							echo "
								<div class='adminBox'>
									
										<h3>Waiting for Proof Approval</h3>
									
								<div class='clear'></div> 
								</div>
							";
						}
						
                    	// Coach Rejected Proof - Back to Printer
						// *** DISABLED ***
						if ($campaignStatuses[14] && $campaignStatuses[14]['pending'] == 3) {
							require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
							$proof = Printer::getCurrentProof($team_id);
							include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
							$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/printer/";
							
							echo "
								<div class='adminBox'>
									<div class='adminCol1'>
										<h3>Printer Proof Back to Printer</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<div style='float:left;'><img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' /></div>
										<div style='float:left;margin-top:10px;'><a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$proof['filename']."&p=$dirname". $proof['filename'] . "' target='_blank'>Click here to download proof</a></div>
									</div>
									<div class='adminCol3'>
									</div>
									<div class='clear'></div> 
									<br /><br />
									<hr />
									<br /><br />
									<form method='post' name='frmReturnProof' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2b' style='width:500px;'>
										The Coach does not approve of the current Team Proof, please use the box below to send a message to the Printer explaining why.
									</div>
									<div class='adminCol3'>
										<p align='right'>
											<input type='hidden' name='tID' value='".$team_id."'>
											<input type='hidden' name='recipID' value='".$printerID."'>
											<input type='hidden' name='pID' value='". $proof['ID'] . "'>
											
											<input type='hidden' name='submitAction' value='adminRejectProof'>
											<a href='javascript:document.frmReturnProof.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
										</p>
									</div>
									<div class='adminCol2b'>
										<br /><br />
										<input class='textField' name='subject' size='66' value='Subject...'/>
										<br />
										<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
										<br /><br />
									</div>
									</form>
									
								<div class='clear'></div> 
								</div>
							";
						}

                    	// Awaiting Mailing of Brochures
						/*else if ($campaignStatuses[15] && !$campaignStatuses[16]) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmUpdateMailDate' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol2a'>
										<h3>Waiting on Brochure Mailing Date: $mailDate</h3>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminUpdateMailDate' />
										<input type='hidden' name='tID' value='".$team_id."'>
										<a href='javascript:document.frmUpdateMailDate.submit();' class='teamButton teamPrimaryBGColor'>Manually Set Mailing Benchmark</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
						*/
						
                    	// Awaiting Mailing of Brochures
						// *** DISABLED for Printer Setting Date ***
						if ($campaignStatuses[11] && !$campaignStatuses[16]) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmUpdateMailDate' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Brochure: Mailing Date</h3>
									</div>
									<div class='adminCol2'>
										<input type='text' id='datepicker' size='14' value='" . $mailDate . "' name='brochure_mailing' />
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminUpdateMailDate' />
										<input type='hidden' name='tID' value='".$team_id."'>
										<a href='javascript:document.frmUpdateMailDate.submit();' class='teamButton teamPrimaryBGColor'>Update Mailing Date</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
						

                    	// Awaiting closing of campaign
						if ($campaignStatuses[16] && !$campaignStatuses[18]) {
							require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
							$closingData = Admin::getDataForCampaignClosing($team_id);
							
							echo "
								<div class='adminBox'>
									<form method='post' name='frmCompleteCampaign' action='index.php?nav=campaigns&action=masterAdmin'>
									<h3>Finalize and close this campaign:</h3>
										<p>To finalize and close this campaign, please review the final campaign email to the coach, this email is automatically generated and contains the campaign's final statistics. We have provided you this feature allowing you to modify the email as you see fit before it is sent to the coach. When you are ready to close the campaign and send this final campaign status email to the coach, click the 'finalize Campaign' button below.</p>
									
									
										<br />
										<textarea class='ckeditor' name='completeCampaign' cols='100' rows='200' style='height:300px;'>".$closingData['completeCampaign']."</textarea>	
										<br />
									
									<div class='clear'></div>
									
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminCompleteCampaign' />
										<input type='hidden' name='tID' value='$team_id' />
										<input type='hidden' name='completeCampaignSubject' value='".$closingData['completeCampaignSubject']."' />
					<div class='clear'></div>
										<a href='javascript:document.frmCompleteCampaign.submit();' class='teamButton teamPrimaryBGColor'>Finalize & Close This Campaign</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
						
						
                    	// Awaiting archiving of campaign
						if ($campaignStatuses[18]) {
							echo "
								<div class='adminBox'>
									<form method='post' name='frmCompleteCampaign' action='index.php?nav=campaigns'>
									<div class='adminCol1'>
										<h3>Archive this campaign</h3>
									</div>
									<div class='adminCol2'>
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='submitAction' value='adminArchiveCampaign' />
										<input type='hidden' name='tID' value='$team_id' />
										<input type='hidden' name='completeCampaignSubject' value='".$closingData['completeCampaignSubject']."' />
										<a href='javascript:document.frmCompleteCampaign.submit();' class='teamButton teamPrimaryBGColor'>Archive Campaign</a>
									</div>
									</form>
								<div class='clear'></div> 
								</div>
							";
						}
						

                    	// Update International mailers and base cost
						// At bottom for stacking order
/*						if ($campaignStatuses[11] && !$campaignStatuses[18]) {
							require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
							$costs = Printer::getCosts($team_id);
							echo "
								<div class='adminBox'>
									<form method='post' name='frmAdminUpdateCosts' action='index.php?nav=campaigns&action=masterAdmin'>
									<div class='adminCol1'>
										<h3>Update Mailer Info</h3>
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<input class='textField' name='intlMailers' size='5' style='text-align:center;' value='" . $costs['international_mailers'] . "'/> International Mailers
									</div>
									<div class='adminCol3'>
										<input type='hidden' name='tID' value='$team_id' />
										<input type='hidden' name='submitAction' value='adminUpdateCosts' />
										<a href='javascript:document.frmAdminUpdateCosts.submit();' class='teamButton teamPrimaryBGColor'>Update Mailing Info</a>
									</div>
									<div class='clear'></div> 
									
									<div class='adminCol1'>
										&nbsp;
									</div>
									<div class='adminCol2' style='margin-top:2px;margin-left:30px;'>
										<input class='textField' name='baseCosts' size='5' style='text-align:center;' value='$baseCost'/> Base Cost/Brochure
									</div>
									<div class='clear'></div> 

									<div class='adminCol2b' style='width:500px;'>
										Reason for chaging Base Cost
										<textarea class='' name='reason' cols='59' rows='10'>$reason</textarea>	
									</div>
									<div class='clear'></div> 
									
									</form>
								</div>
							";
						}
*/                    
						}
                    ?>
                    </div>    
                    
                	<div id='messages' style='display:none;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Campaign Specific Messages</h2>
                        
					<?php
	
						$viewCheck = ($get['sub']) ? $get['sub'] : "";
						require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Messaging.php');
						
						if (!$viewCheck) {
							$mData['sender'] = "Admin";
							$mData['senderID'] = 3;
							$messages = Messaging::getAllMessages($team_id);
							
							$messagesCount = count($messages);
					
							echo "
								<div class='contentLeftData'>
									<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
									<table width='100%' border='0' cellspacing='0' cellpadding='7'>
										<tr>
											<td>To</td>
											<td>From</td>
											<td>Benchmark</td>
											<td>Subject</td>
											<td align='right'>Date</td>
										</tr>      
							";
							
							$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
							$tpages = ceil($messagesCount/15);
							$min = ($page - 1) * 15 + 1;
							$max = $page * 15;
							
							$classAlternate = "bg1";
							$count = 0;
							
							foreach($messages AS $m) {
								$count++;
								if ($count < $min || $count > $max) continue;
								$date = date("M j, Y g:i A", strtotime($m['date_sent']));
								$unreadFlag = ($m['markAsRead']) ? "<span>" : "<span style='font-weight:bold;'>";
								$replyFlag = ($m['sender'] == 'Admin') ? "&s=y" : "";
								
								echo "
										<tr class='$classAlternate' align='left'>
											<td>$unreadFlag" . $m['recip'] . "</span></td>
											<td>$unreadFlag" . $m['sender'] . "</span></td>
											<td>$unreadFlag" . $m['benchmark'] . "</span></td>
											<td><a href='index.php?nav=campaigns&action=masterAdmin&sub=viewMessage&mID=".$m['ID']."$replyFlag'>$unreadFlag" . $m['subject'] . "</span></a></td>
											<td align='right'>$unreadFlag$date</span></td>
										</tr>
								";
								$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							}
							
							echo "
										<tr>
											<td colspan='2'>&nbsp;</td>
										</tr>      
									</table>
							";
							
							echo "<div class='pagination'><br/>";
							$reload = $_SERVER['PHP_SELF'] . "?nav=campaigns&action=masterAdmin&m=v";
							include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
							echo paginate($reload, $page, $tpages, 5);
							echo "</div>";
							
							echo "
										</div>
										<!-- /contentLeftData -->
							";  
							
						} else {
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/viewMessage.php');
											  
							echo "
								<div class='contentRight'>
							";
							
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/incomingMessages.php');
							
							echo "
								<div class='clear'></div>
								</div>
								<!-- /contentRight -->
							";  
						}
					?>                        
                    </div>
                     
                                      
                 	<div id='benchmarks-old' style='display:none;'>
                       <div class='adminBox'>
                            <form method='post' name='frm' id='frm' action='index.php?nav=campaigns&action=masterAdmin'>
                            <div class='adminCol1'>
                                <h3>Campaign: Status</h3>
                            </div>
                            <div class='adminCol2'>
                                Current Status: 
                                1 <input type="radio" value=1 name="status" <?php echo $statusCheck1; ?> />
                                2 <input type="radio" value=2 name="status" <?php echo $statusCheck2; ?> />
                                Archive <input type="radio" value='a' name="status" />
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminUpdateStatus' />
                                <a href='javascript:document.frm.submit();' id='updateBtn' class='teamButton teamPrimaryBGColor'>Update Status</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                        
                        <script language="javascript" type="text/javascript">
                        /* <![CDATA[ */
                            $('#updateBtn').click(function(event) {
                                if ($("input[@name='status']:checked").val() == 'a') {
                                    var r=confirm("Are you sure you want to archive this campaign?");
                                    if (r==false) {
                                        event.preventDefault();
                                    } else {
                                        $('#frm').attr('action', 'index.php?nav=campaigns');
                                    }
                                }
                            });
                        /* ]]> */
                        </script>
                        
                        <div class='adminBox'>
                            <form method='post' name='frmMailDate' action='index.php?nav=campaigns&action=masterAdmin'>
                            <div class='adminCol1'>
                                <h3>Brochure: Mailing Date</h3>
                            </div>
                            <div class='adminCol2'>
                                <input type="text" id="datepicker" size="14" value="<?php echo $mailDate; ?>" name="brochure_mailing" />
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminUpdateMailDate' />
                                <a href='javascript:document.frmMailDate.submit();' class='teamButton teamPrimaryBGColor'>Update Mailing Date</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                        
                        <div class='adminBox'>
                            <form method='post' name='frmBrochureInfo' action='index.php?nav=campaigns&action=masterAdmin'>
                            <div class='adminCol1'>
                                <h3>Brochure: Financial Info</h3>
                            </div>
                            <div class='adminCol2'>
                                <input type="text" name="brochure_count" value="<?php echo $brochureCount; ?>" size="5" class='selectFieldSWR' />&nbsp; Number of Brochures
                                <input type="text" name="brochure_cost" value="<?php echo $brochureCost; ?>" size="5" class='selectFieldSWR' />&nbsp; Cost per Brochures
                                <input type="text" name="brochure_charges" value="<?php echo $brochureCharges; ?>" size="5" class='selectFieldSWR' />&nbsp; Additional Charges<br />
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminUpdateBrochureInfo' />
                                <a href='javascript:document.frmBrochureInfo.submit();' class='teamButton teamPrimaryBGColor'>Update Financial Info</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                    </div>
                
                	<div id='donations' style='display:none;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Specific Tasks - Donation Info</h2>
                        
                        <div class='adminBox'>
                            <div class='adminCol1'>
                                <h3>Donations: Export</h3>
                            </div>
                            <div class='adminCol2'>
                                Total Donations: <?php echo "$".$donations['donationTotal']; ?>
                            </div>
                            <div class='adminCol3'>
                                <a href='exportDonations.php?id=<?php echo $_SESSION['campaign_team']; ?>' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Export Donations</a>
                            </div>
                        <div class='clear'></div> 
                        </div>
                        
                        <div class='adminBox'>
                            <form method='post' name='frm2' id='frm2' action='<?php echo $_SESSION['relative_path'] . 'index.php'; ?>' target="_blank">
                            <div class='adminCol1'>
                                <h3>Donations: Online Manual</h3>
                            </div>
                            <div class='adminCol2'>
                                <select id='player' name='player' class='selectFieldSWR' size='1'>
                                    <option value=''>Please select...</option>
                                    <?php
                                        foreach($players as $player)
                                        {
                                            echo "<option value=" . $player['ID'];
                                            echo ">" . $player['lname'] . ", " . $player['fname'] . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminManualDonation' />
                                <input type="hidden" name="playerID" value='' />
                                <input type="hidden" name="teamID" value=<?php echo $_SESSION['campaign_team']; ?> />
                                <a href='javascript:submitform();' class='teamButton teamPrimaryBGColor'>Enter Donation</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                        
                        <div class='adminBox'>
                            <form method='post' name='frm2a' id='frm2a' action='<?php echo $_SESSION['relative_path'] . 'index.php'; ?>' target="_blank">
                            <div class='adminCol1'>
                                <h3>Donations: Check Manual</h3>
                            </div>
                            <div class='adminCol2'>
                                <select id='player' name='player' class='selectFieldSWR' size='1'>
                                    <option value=''>Please select...</option>
                                    <?php
                                        foreach($players as $player)
                                        {
                                            echo "<option value=" . $player['ID'];
                                            echo ">" . $player['lname'] . ", " . $player['fname'] . "</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminManualCheckDonation' />
                                <input type="hidden" name="playerID" value='' />
                                <input type="hidden" name="teamID" value=<?php echo $_SESSION['campaign_team']; ?> />
                                <a href='javascript:submitform2();' class='teamButton teamPrimaryBGColor'>Enter Check</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                    </div>
                
                	<div id='basics' style='display:none;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Specific Tasks - Campaign Basics</h2>
                        
                        <div class='adminBox'>
                            <form method='post' name='frm3' id='frm3' action='index.php?nav=campaigns&action=masterAdmin'>
                            <div class='adminCol1'>
                                <h3>Campaign: School Name</h3>
                            </div>
                            <div class='adminCol2'>
                                <input type="text" size="40" value="<?php echo $name; ?>" name="name" />
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminUpdateSchool' />
                                <a href='javascript:document.frm3.submit();' class='teamButton teamPrimaryBGColor'>Update School</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                        
                        <div class='adminBox'>
                            <form method='post' name='frm4' id='frm4' action='index.php?nav=campaigns&action=masterAdmin'>
                            <div class='adminCol1'>
                                <h3>Campaign: Team Name</h3>
                            </div>
                            <div class='adminCol2'>
                                <input type="text" size="40" value="<?php echo $team; ?>" name="team" />
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminUpdateTeam' />
                                <a href='javascript:document.frm4.submit();' class='teamButton teamPrimaryBGColor'>Update Team</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                        
                        <div class='adminBox'>
                            <form method='post' name='frm5' id='frm5' action='index.php?nav=campaigns&action=masterAdmin'>
                            <div class='adminCol1'>
                                <h3>Campaign: Password</h3>
                            </div>
                            <div class='adminCol2'>
                                <input type="text" size="40" value="<?php echo $password; ?>" name="password" />
                            </div>
                            <div class='adminCol3'>
                                <input type="hidden" name="submitAction" value='adminUpdatePassword' />
                                <a href='javascript:document.frm5.submit();' class='teamButton teamPrimaryBGColor'>Update Password</a>
                            </div>
                            </form>
                        <div class='clear'></div> 
                        </div>
                    </div>
                
                	<div id='exports' style='display:none;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Specific Tasks - Exports</h2>
                        
                        <!--<div class='adminBox'>
                            <div class='adminCol1'>
                                <h3>Scrub Data</h3>
                            </div>
                            <div class='adminCol2' style='margin-top:2px;margin-left:0px;'>
                            </div>
                            <div class='adminCol3'>
                                <a href='scrubber.php?id=<?php echo $team_id; ?>' target='_blank' class='teamButton teamPrimaryBGColor'>Scrub Data</a>
                            </div>
                        <div class='clear'></div> 
                        </div> -->
                        
                        <div class='adminBox'>
                            <div class='adminCol1'>
                                <h3>Contact: Export</h3>
                            </div>
                            <div class='adminCol2'>
                                Total Contacts: <?php echo $contacts; ?>
                            </div>
                            <div class='adminCol3'>
                                <a href='export.php?id=<?php echo $_SESSION['campaign_team']; ?>' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Export Contacts</a>
                            </div>
                        <div class='clear'></div> 
                        </div>
                        
                        <div class='adminBox'>
                            <div class='adminCol1'>
                                <h3>Donations: Export</h3>
                            </div>
                            <div class='adminCol2'>
                                Total Donations: <?php echo "$".$donations['donationTotal']; ?>
                            </div>
                            <div class='adminCol3'>
                                <a href='exportDonations.php?id=<?php echo $_SESSION['campaign_team']; ?>' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Export Donations</a>
                            </div>
                        <div class='clear'></div> 
                        </div>
                    </div>
                
                	<div id='designer' style='display:none;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Specific Tasks - Designer Info</h2>
                        <?php
                        	$designerInfo = Campaign::getDesignerInfo($designerID);
						?>
                        <div class='adminBox'>
                            <div class='adminCol1'>
                                <h3>Designer Assigned:</h3>
                            </div>
                            <div class='adminCol2'>
                                <?php echo $designerInfo['fname']." ".$designerInfo['lname']."<br />". $designerInfo['email']; ?>
                            </div>
                            <div class='adminCol3'>
                                
                            </div>
                        <div class='clear'></div> 
                        </div>
                    </div>
                    
                	<div id='printer' style='display:none;'>
                        <h2 class='teamPrimaryTxtColor'>Master Admin Specific Tasks - Printer Info</h2>
                        <?php
                        	$printerInfo = Campaign::getPrinterInfo($printerID);
						?>
                        <div class='adminBox'>
                            <div class='adminCol1'>
                                <h3>Printer Assigned:</h3>
                            </div>
                            <div class='adminCol2'>
                                <?php echo $printerInfo['fname']." ".$printerInfo['lname']."<br />". $printerInfo['email']; ?>
                            </div>
                            <div class='adminCol3'>
                                
                            </div>
                        <div class='clear'></div> 
                        </div>
                    </div>
                    
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->
    <script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>

