<?php
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/farbtastic.css' />
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/picker.css' />
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/farbtastic.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jqModal.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/picker.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/ckeditor/ckeditor.js'></script>
	";

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	$color = $TeamMain->getTeamColors();
	//$public = $TeamMain->getTeamPublic();
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
	$c1 = ( isset($color['bgColor1']) ) ? '#'.$color['bgColor1'] : '#fff';
	$c2 = ( isset($color['bgColor2']) ) ? '#'.$color['bgColor2'] : '#fff';
	$c3 = ( isset($color['txtColor']) ) ? '#'.$color['txtColor'] : '#000';
?>    
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
					<h2 class='teamPrimaryTxtColor'>Team Page Manager</h2>
					
					<div class='registerWrap'>
                        <form method='post' name='frmColors' action='index.php?nav=campaigns&action=pageManager'>
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Team Primary Color:</strong></div>
                                <div class='formElementCol2'>
                                <input name='bgColor1' id='color1' type='text' class='textFieldSWR' value='<?php echo $c1; ?>' /> 
                                <a href='#' id='pickerBtn1'><img src='<?php echo $_SESSION['relative_path']; ?>images/icon_color_picker.gif' width='20' height='20' class='colorPickerIcon' /></a>
                                </div>
                            <div class='clear'></div>
                            </div>
                          
                            <span class='helpNotes'>This will colorize the page inner outline background color.</span><br /><br />
                        
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Team Secondary Color:</strong></div>
                                <div class='formElementCol2'>
                                <input name='bgColor2' id='color2' type='text' class='textFieldSWR' value='<?php echo $c2; ?>' /> 
                                <a href='#' id='pickerBtn2'><img src='<?php echo $_SESSION['relative_path']; ?>images/icon_color_picker.gif' width='20' height='20' class='colorPickerIcon' /></a>
                                </div>
                            </div>
                          
                            <span class='helpNotes'>This will colorize the page outer outline background color.</span><br /><br />
                            
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Team Primary Text Color:</strong></div>
                                <div class='formElementCol2'>
                                <input name='txtColor' id='color3' type='text' class='textFieldSWR' value='<?php echo $c3; ?>' /> 
                               <a href='#' id='pickerBtn3'> <img src='<?php echo $_SESSION['relative_path']; ?>images/icon_color_picker.gif' width='20' height='20' class='colorPickerIcon' /></a>
                                </div>
                            </div>
                            
                            <span class='helpNotes'>This will colorize the color of all highlight text.</span><br />
        
                            <div class='registerButton'>
                                <p><a href='javascript:document.frmColors.submit();' class='teamButton teamPrimaryBGColor'>Update My Team Colors</a></p><br />
                            </div>
                            <input type="hidden" name="submitAction" value="adminUpdateColors" />
                        </form>
                        <br /><hr color="#CCCCCC" style="height:1px;background-color:#cccccc;" /><br />
                        <form method='post' name='frmLogo' action='index.php?nav=campaigns&action=pageManager' enctype='multipart/form-data'>
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Upload Your Team Logo:</strong></div>
                                <div class='formElementCol2'>
                                <input type='file' size='40' name='logoUpload' />
                                </div>
                            </div>
                            
                            <span class='helpNotes'>For best results the logo image size should be 130px wide by 130px in height. Image format must be .gif, .jpg, or .png</span><br /><br />
                         
                            <div class='registerButton'>
                                <p><a href='javascript:document.frmLogo.submit();' class='teamButton teamPrimaryBGColor'>Update My Team Logo</a></p>
                            </div>
                             <input type="hidden" name="submitAction" value="adminUpdateLogo" />
                       </form>
                    
                    <div class='clear'></div>
                    
                    <!-- team public page content -->
                    <!--
                    <br /><hr color="#CCCCCC" style="height:1px;background-color:#cccccc;" /><br />

					
                    
                    
                    <form method='post' name='frmPublic' action='index.php?nav=campaigns&action=pageManager'>
                    		<div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Team Public Page Content:</strong></div>
                                <div class='formElementCol2'>
                                <textarea class='ckeditor' name='publicCopy' cols='92' rows='20'><?php echo $public['publicCopy']; ?></textarea> 
                                </div>
                            </div><br />
<br />

                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Team Public Page Mailing Address:</strong></div>
                                <div class='formElementCol2'>
                                <textarea class='ckeditor' name='publicAddress' cols='92' rows='10'><?php echo $public['publicAddress']; ?></textarea> 
                                </div>
                            </div>
        
                            <div class='registerButton'>
                                <p><a href='javascript:document.frmPublic.submit();' class='teamButton teamPrimaryBGColor'>Update Public Page Content</a></p><br />
                            </div>
                    <input type="hidden" name="submitAction" value="adminUpdatePublic" />
                    </form>
                    -->
                    
                    
                    </div>
                    
                    
                    
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
						showteamLogo();
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
						showteamLogo();
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->
    
            <div id="colorpickerWindow1" class="jqmWindow">
                <div id="colorpicker1"></div>
                <div><a href='#' id='closePickerBtn1'>Click to Close Color Picker</a></div>
            </div>
            <div id="colorpickerWindow2" class="jqmWindow">
                <div id="colorpicker2"></div>
                <div><a href='#' id='closePickerBtn2'>Click to Close Color Picker</a></div>
            </div>
            <div id="colorpickerWindow3" class="jqmWindow">
                <div id="colorpicker3"></div>
                <div><a href='#' id='closePickerBtn3'>Click to Close Color Picker</a></div>
            </div>


<?php
	unset($_SESSION['upload_result']);
?>