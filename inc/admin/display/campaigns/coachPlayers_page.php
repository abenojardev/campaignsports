<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	
	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
	$playerCount = TeamUser::countPlayers($_SESSION['campaign_team']);
	
	if (isset($get['view']) && $get['view'] == 'contacts') {  
	//	$players = TeamUser::getAllPlayersNameSortContactCount($_SESSION['campaign_team']);
		$players = TeamUser::getAllPlayersNameSortDonationTotal($_SESSION['campaign_team']);
	}
	else if (isset($get['view']) && $get['view'] == 'donations') {            
		$players = TeamUser::getAllPlayersNameSortDonationTotal($_SESSION['campaign_team']);
	}
	else {
		//$players = TeamUser::getAllPlayersNameSort($_SESSION['campaign_team']);
		$players = TeamUser::getAllPlayersNameSortDonationTotal($_SESSION['campaign_team']);
	}
?>    
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
            
			<div class='contentFull'>
            
                <div class='contentFullData'>
            <?php
            
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                        echo "<h2 class='teamPrimaryTxtColor'>Athlete Donation Lists</h2>";
                    } else {
            ?>
                <h2 class='teamPrimaryTxtColor'>My Team Athletes</h2>

					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
                      <tr>
                        <td><strong>Name</strong></td>
                        
            <?php 
				$viewDisplay = ( isset($get['view']) ) ? "&view=".$get['view'] : "";
			
            	if (isset($get['view']) && $get['view'] == 'contacts') {  
					echo "
                        <td><strong>Email</strong></td> 
                        <td align='center'><strong>Total<br />Contacts</strong></td>
                      </tr>
					";
				}
				else if (isset($get['view']) && $get['view'] == 'donations') {            
					echo "
                        <td><strong>Email</strong></td> 
                        <td align='center'><strong>Total<br />Donations</strong></td>
                      </tr>
					";
				}
				else {
					echo "
                        <td><strong>Email</strong></td> 
                        <td><strong>Cell</strong></td>  
                        <td align='center'><strong>Total<br />Donations</strong></td>
                        <td align='center'><strong>DEL</strong></td>
                      </tr>
					";
				}
				
			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
				$tpages = ceil($playerCount/15);
				$min = ($page - 1) * 15 + 1;
				$max = $page * 15;
				
				$classAlternate = "bg1";
				$count = 0;
				foreach($players AS $p) {
				     $total = !is_null($p['donationTotal']) ? $p['donationTotal'] : 0;
					$count++;
					if ($count < $min || $count > $max) continue;
					
					echo "
						<tr class='$classAlternate'>
							<td align='left'><a href='index.php?nav=campaigns&action=players&edit=" . $p['ID'] . "'>" . $p['lname'] . ", " . $p['fname'] . "</a></td>
					";
						
					if (isset($get['view']) && $get['view'] == 'contacts') {  
						echo "
							<td>" . $p['email'] . "</td>
							<td>null</td> 
							<td align='center'>$ " . $total . "</td>
						</tr>
						";
					}
					else if (isset($get['view']) && $get['view'] == 'donations') {            
						echo "
							<td>" . $p['email'] . "</td>
							<td>null</td> 
							<td align='center'>$" . $total . "</td>
						</tr>
						";
					}
					else {
						echo "
							<td>" . $p['email'] . "</td> 
							<td>null</td>
							<td align='center'>$ " . $total . "</td>
							<td align='center'><a href='index.php?nav=campaigns&action=players&delete=".$p['ID']."' class='delCheck adminButton adminPrimaryBGColor'>Delete</a></td>
						</tr>
						";
					}
					
					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
				}
	
	
                echo "</table><div class='pagination'><br/>";
				$reload = $_SERVER['PHP_SELF'] . "?nav=campaigns&action=" . $_SESSION["nav2"] . $viewDisplay;
            	include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
				echo paginate($reload, $page, $tpages, 3);
				echo "</div>";
				
				}
			?>
                    
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
			<script language="javascript" type="text/javascript">
			/* <![CDATA[ */
				$('.delCheck').click(function(event) {
					var r=confirm("Are you sure you want to delete this athlete and ALL matching contacts? \nThis action is irreversible.");
					if (r==false) {
						event.preventDefault();
					}		
				});
			/* ]]> */
			</script>
            
        	<!--<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                       // include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                     //   include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        //include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                       // include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>-->
			<!-- /contentRight -->
