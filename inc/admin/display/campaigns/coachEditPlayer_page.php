<?php
	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/formValidateIntl.js'></script>
	";
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['campaign_team']);
	$status = $TeamMain->getTeamStatus();
	
	include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	$countries = Common::getCountries();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
	$ID = array();
	$ID['ID'] = $_GET['edit'];
	$ID['table'] = 'players';
	$contacts = TeamUser::getXyzFromID($ID);
	
	$contactDisplay = array();
	foreach($contacts AS $f => $d) {
		if( isset($d) ) {
			$contactDisplay[$f] = "value='".$d."'";
		} else {
			$contactDisplay[$f] = "";
		}
		if($f == 'state') $contactDisplay[$f] = $d;
		if($f == 'ID') $contactDisplay[$f] = $d;
		
		//echo "f: $f, d: $d<br>";
	}
	
?>    
            <?php showteamHeader(); ?>
			
            <?php include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php'); ?>
             
			<div class='contentFull'>
                <div class='contentFullData'>
					<div class='pageWrap'>
					<?php 
                        include_once($_SESSION['relative_path'] . 'inc/admin/widgets/adminAtheletesContactList.php');
                        //include_once($_SESSION['relative_path'] . 'inc/admin/widgets/adminAtheletesDonationLists.php');
                    ?>
                    <!--<div class="logout"><a href="index.php?nav=campaigns&action=players">[back]</a></div>
                    <h2 class='teamPrimaryTxtColor'>Athlete Info</h2>
                      <form method='post' name='frm' id='frm' action='index.php?nav=campaigns&action=players'>

                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>First Name:</strong></div>
                                <div class='formElementCol2'><input name='fname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['fname']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Last Name:</strong></div>
                                <div class='formElementCol2'><input name='lname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['lname']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Address 1:</strong></div>
                                <div class='formElementCol2'><input name='address' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['address']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'>&nbsp;<strong>Address 2:</strong></div>
                                <div class='formElementCol2'><input name='address2' type='text' class='textFieldSWR' <?php echo $contactDisplay['address2']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>City:</strong></div>
                                <div class='formElementCol2'><input name='city' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['city']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>State:</strong></div>
                                <div class='formElementCol2'>
                                    <select id='state' name='state' class='selectFieldSWR validate' size='1'>
                                      <option value=''>Please select...</option>
                                <option value=''>&nbsp;</option>
                                <option value=''>---- United States ----</option>
                                        <?php
                                            foreach($states as $state)
                                            {
												$selected = "";
												if ($state['abbrev'] == $contactDisplay['state']) $selected = "selected='selected'";
												echo "<option value='".$state['abbrev']."'";
												echo "$selected>".$state['name']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Zip:</strong></div>
                                <div class='formElementCol2'><input name='zip' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['zip']; ?> /></div>
                            </div>
                            
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Email:</strong></div>
                                <div class='formElementCol2'><input name='email' type='text' class='textFieldSWR pemail validate' <?php echo $contactDisplay['email']; ?> /></div>
                            </div>
                            
                            <div class='registerButton'>
                                  <input type="hidden" name="pID" value='<?php echo $contactDisplay['ID']; ?>' />
                                  <input type="hidden" name="submitAction" value='adminUpdatePlayer' />
                                <p><a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update / Continue</a></p>
                            </div>

                      </form>
                    </div>
                    
                </div> -->
                <!-- /contentLeftData -->
            
        	<div class='clear'></div>  
        	</div>
        	</div>
        	</div>
        	<!-- /contentLeft -->
            
        	<!--<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        // include_once($_SESSION['relative_path'] . 'inc/admin/widgets/adminAtheletesContactList.php');
                        // include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        // include_once($_SESSION['relative_path'] . 'inc/admin/widgets/adminAtheletesContactList.php');
                        // include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
            <div class='clear'></div>
           	</div> -->
			<!-- /contentRight -->

