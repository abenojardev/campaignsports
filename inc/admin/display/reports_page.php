<?php
	if ( isset($get['team']) ) {
		$_SESSION['campaign_team'] = $get['team'];
		$_SESSION['current_folder'] = $get['team'];
	}
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/highlight.js"></script>
		');
	
    startToMainHeader($data);
	
	if ( isset($get["action"]) && ($get["action"]) && ($get["action"] != "add") ) {
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
		$wrap = "team";
	}
	else {
		$wrap = "admin";
	}
	contentWrapToNav($wrap);
?>    
            
			<div class='contentFull'>
                <div class='contentFullData'>
                
			<?php
				if ( isset($get["action"]) ) {
					switch($get["action"]) {
						case 'add':
						$_SESSION["nav2"] = "add";
						include($relPath . 'inc/admin/display/campaignsAdd_page.php');
						break;
						
						
						case 'dashboard':
						$_SESSION["nav2"] = "dashboard";
						include($relPath . 'inc/admin/display/campaigns/coachDashboard_page.php');
						break;
						
						case 'account':
						$_SESSION["nav2"] = "account";
						include($relPath . 'inc/admin/display/campaigns/coachAccount_page.php');
						break;
						
						case 'players':
						$_SESSION["nav2"] = "players";
						if ( isset($get["edit"]) )
							include($relPath . 'inc/admin/display/campaigns/coachEditPlayer_page.php');
						else if ( isset($get["delete"]) ) {
							include($relPath . 'inc/team/class/TeamUser.php');
							$del = TeamUser::deleteAdminPlayer($get['delete']);
							include($relPath . 'inc/admin/display/campaigns/coachPlayers_page.php');
						} else 
							include($relPath . 'inc/admin/display/campaigns/coachPlayers_page.php');
						break;
						
						case 'contacts':
						$_SESSION["nav2"] = "contacts";
						if ( isset($get["edit"]) )
							include($relPath . 'inc/admin/display/campaigns/coachEditContact_page.php');
						else if ( isset($get["delete"]) ) {
							include($relPath . 'inc/team/class/Player.php');
							$del = Player::removeContact($get['delete']);
							include($relPath . 'inc/admin/display/campaigns/coachContacts_page.php');
						} else 
							include($relPath . 'inc/admin/display/campaigns/coachContacts_page.php');
						break;
						
						case 'donations':
						$_SESSION["nav2"] = "donations";
						if ( isset($get["view"]) )
							include($relPath . 'inc/admin/display/campaigns/coachViewDonations_page.php');
						else if ( isset($get["delete"]) ) {
							include($relPath . 'inc/admin/class/Admin.php');
							$del = Admin::removeCheck($get['delete']);
							include($relPath . 'inc/admin/display/campaigns/coachDonations_page.php');
						} 
						else 
							include($relPath . 'inc/admin/display/campaigns/coachDonations_page.php');
						break;
						
						case 'pageManager':
						$_SESSION["nav2"] = "pageManager";
						include($relPath . 'inc/admin/display/campaigns/coachManager_page.php');
						break;
		
						case 'masterAdmin':
						$_SESSION["nav2"] = "masterAdmin";
						include($relPath . 'inc/admin/display/campaigns/adminMaster_page.php');
						break;
		
						default:
						$_SESSION["nav2"] = "dashboard";
						include($relPath . 'inc/admin/display/campaigns/coachDashboard_page.php');
						break;
					}
				}
				else {
					include($relPath . 'inc/admin/widgets/adminReports.php');
				}
			?>    
            
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->


<?php
	contentClosures();
	closePageWrapToEnd();
?>