// JavaScript Document

$(document).ready(function() { 

	$('#collectionUpdateBtn').click(function(event) {
		$('#collectionUpdate').show();
		event.preventDefault();
	});
	
	$('#frm').submit(function() { 
		var val = validate();
		if (val)
		{
			return true;
		}
		else
		{
			return false;
		}
	});	
	
	$('#frmSubmit').click(function(event) {
		$('#frm').submit();
		event.preventDefault();
	});
	
	function validate() { 
		var msg = "Please complete the highlighted items.";
		error_check = false; 
		
		var value = $("#target").val();
		if( !IsNumeric(value) ) {
			msg = "Target amount must be numeric.";
			error_check = true;
		}
		
		if( value <= 0 ) {
			msg = "Target amount must be more than 0.";
			error_check = true;
		}
		
		if (error_check) {
			alert(msg);
			return false;
		}
		else
		{
			return true;
		}
	}

});


function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}
