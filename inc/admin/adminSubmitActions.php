<?php
	if ($post['submitAction'] == 'adminCreateCampaign') {
		
		// Step 1 - Create the new campaign to get the ID
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$newID = Campaign::insertNewCampaign($post);
		
		// Step 2 - Set the session vars to the ID
		$_SESSION['campaign_team'] = $newID;
		$_SESSION['current_folder'] = $newID;
		
		// Step 3 - Set Team Colors
		require_once($relPath . 'inc/team/class/TeamMain.php');
		TeamMain::setTeamColors($post);
		
		// Step 4 - Insert Coach's info
		Campaign::insertNewCoach($post);
		
		// Step 5 - Create the Team folders and index files
		Campaign::createCampaignFiles($newID);
		
		// Step 6 - If logo file uploaded, run method
		$files = $_FILES;
		$setTeamLogoResult = TeamMain::setTeamLogo();
		$_SESSION['upload_result'] = $setTeamLogoResult;

		// Step 7 - Add Status 1 to status_teams table
		$data["ID"] = $newID;
		$data["sID"] = 1;
		Campaign::setCampaignStatus($data);
		
	}
	else if ($post['submitAction'] == 'updateNote') {
		require_once($relPath . 'inc/admin/class/Admin.php');
		$note = Admin::setCampaignNotes($post);
	}
	else if ($post['submitAction'] == 'adminAddDesigner') {
		require_once($relPath . 'inc/admin/class/Admin.php');
		$note = Admin::addDesigner($post);
	}
	else if ($post['submitAction'] == 'adminDeleteMessage') {
		require_once($relPath . 'inc/admin/class/Admin.php');
		$note = Admin::deleteMessage($post);
	}
	
	else if ($post['submitAction'] == 'adminAddPrinter') {
		require_once($relPath . 'inc/admin/class/Admin.php');
		$note = Admin::addPrinter($post);
	}
	
	else if ($post['submitAction'] == 'adminStartCampaign') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$data["ID"] = $post['tID'];
		$data["sID"] = 2;
		Campaign::setCampaignStatus($data);
		
		// email to coach
		require_once($relPath . 'inc/common/display/mailText.php');	
		require_once($relPath . 'inc/team/class/TeamUser.php');		
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
		
		require_once($relPath . 'inc/team/class/TeamMain.php');		
		$teamData = new TeamMain($post['tID']);
		$school = $teamData->getTeamName();
		$team = $teamData->getTeamType();
		$teamName = $school . " " . $team;
		$coachPass = $teamData->getTeamAdminPassword();
		$teamPass = $teamData->getTeamPassword();
		$mailVars=array("coachName"=>$coachName,"teamName"=>$teamName,"teamID"=>$post['tID'],"coachPass"=>$coachPass,"teamPass"=>$teamPass);
		$startCampaignForCoachDataReturn = startCampaignForCoach($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $startCampaignForCoachDataReturn['startCampaignForCoachSubject'];
		$mailData['messageHTML'] = $startCampaignForCoachDataReturn['startCampaignForCoach'];
		Mail::send_email($mailData);
		
		// send to CS admin
		$mailData1['to'] = 'info@campaignsports.com';
		$mailData1['from'] = $cfg_adminEmail;
		$mailData1['subject'] = $startCampaignForCoachDataReturn['startCampaignForCoachSubject'];
		$mailData1['messageHTML'] = $startCampaignForCoachDataReturn['startCampaignForCoach'];
		Mail::send_email($mailData1);
		
		
		// email to coach to forward to players
		$startCampaignForPlayersDataReturn = startCampaignForPlayers($mailVars);

		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $startCampaignForPlayersDataReturn['startCampaignForPlayersSubject'];
		$mailData['messageHTML'] = $startCampaignForPlayersDataReturn['startCampaignForPlayers'];
		Mail::send_email($mailData);
		
		$mailData2['to'] = 'info@campaignsports.com';
		$mailData2['from'] = $cfg_adminEmail;
		$mailData2['subject'] = $startCampaignForPlayersDataReturn['startCampaignForPlayersSubject'];
		$mailData2['messageHTML'] = $startCampaignForPlayersDataReturn['startCampaignForPlayers'];
		Mail::send_email($mailData2);

		
	}
	
	else if ($post['submitAction'] == 'adminResendStartInfo') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$data["ID"] = $post['tID'];
		$data["sID"] = 2;
		//Campaign::setCampaignStatus($data);
		
		// email to coach
		require_once($relPath . 'inc/common/display/mailText.php');	
		require_once($relPath . 'inc/team/class/TeamUser.php');		
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
		
		require_once($relPath . 'inc/team/class/TeamMain.php');		
		$teamData = new TeamMain($post['tID']);
		$school = $teamData->getTeamName();
		$team = $teamData->getTeamType();
		$teamName = $school . " " . $team;
		$coachPass = $teamData->getTeamAdminPassword();
		$teamPass = $teamData->getTeamPassword();
		$mailVars=array("coachName"=>$coachName,"teamName"=>$teamName,"teamID"=>$post['tID'],"coachPass"=>$coachPass,"teamPass"=>$teamPass);
		$startCampaignForCoachDataReturn = startCampaignForCoach($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $startCampaignForCoachDataReturn['startCampaignForCoachSubject'];
		$mailData['messageHTML'] = $startCampaignForCoachDataReturn['startCampaignForCoach'];
		Mail::send_email($mailData);
		
		// send to CS admin
		$mailData1['to'] = 'info@campaignsports.com';
		$mailData1['from'] = $cfg_adminEmail;
		$mailData1['subject'] = $startCampaignForCoachDataReturn['startCampaignForCoachSubject'];
		$mailData1['messageHTML'] = $startCampaignForCoachDataReturn['startCampaignForCoach'];
		Mail::send_email($mailData1);
		
		// email to coach to forward to players
		$startCampaignForPlayersDataReturn = startCampaignForPlayers($mailVars);

		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $startCampaignForPlayersDataReturn['startCampaignForPlayersSubject'];
		$mailData['messageHTML'] = $startCampaignForPlayersDataReturn['startCampaignForPlayers'];
		Mail::send_email($mailData);
		
		$mailData2['to'] = 'info@campaignsports.com';
		$mailData2['from'] = $cfg_adminEmail;
		$mailData2['subject'] = $startCampaignForPlayersDataReturn['startCampaignForPlayersSubject'];
		$mailData2['messageHTML'] = $startCampaignForPlayersDataReturn['startCampaignForPlayers'];
		Mail::send_email($mailData2);
		
		
	}
	
	else if ($post['submitAction'] == 'adminRejectTeamInfo') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 25;
		$dataS["pending"] = 2;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Designer";
		$data['recipID'] = $post['recipID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Team Info Changes";
		$data['benchmarkID'] = $post['dID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}
	
	else if ($post['submitAction'] == 'adminApproveTeamInfo') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$data["ID"] = $post['tID'];
		$data["sID"] = 4;
		Campaign::setCampaignStatus($data);
		
		// Send message to admin about info submission update
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Coach";
		$data['recipID'] = $coach['ID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Team Info Submission Approved";
		$data['benchmarkID'] = 0;
		$data['subject'] = "Team Info Submission Approved";
		$data['message'] = "The Campaign Administrator has approved your submitted Team Info.";
		Messaging::addMessage($data);

		// email to coach that team info approved
		require_once($relPath . 'inc/common/display/mailText.php');		
		$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		$teamInfoApprovedDataReturn = teamInfoApproved($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $teamInfoApprovedDataReturn['teamInfoApprovedSubject'];
		$mailData['messageHTML'] = $teamInfoApprovedDataReturn['teamInfoApproved'];
		Mail::send_email($mailData);
	}

	else if ($post['submitAction'] == 'updateBrochureCopy') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 5;
		$dataS["pending"] = 0;
		Campaign::setCampaignStatus($dataS);
		
		require_once($relPath . 'inc/common/class/campaign/Admin.php');
		Admin::updateInitialInfoFinalCopy($post);
		
		// Send message to coach about copy for review
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Coach";
		$data['recipID'] = $coach['ID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Team Info";
		$data['benchmarkID'] = 0;
		$data['subject'] = "Brochure Copy For Review";
		$data['message'] = "The Campaign Administrator has submitted Brochure Copy for you to review.";
		Messaging::addMessage($data);
		
		// Email to coach that brochure copy is ready for review
		require_once($relPath . 'inc/common/display/mailText.php');	
		$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		$toCoachBrochureCopyDataReturn = toCoachBrochureCopy($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $toCoachBrochureCopyDataReturn['toCoachBrochureCopySubject'];
		$mailData['messageHTML'] = $toCoachBrochureCopyDataReturn['toCoachBrochureCopy'];
		Mail::send_email($mailData);
	}

	else if ($post['submitAction'] == 'assignDesigner') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::assignDesigner($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 7;
		Campaign::setCampaignStatus($data);
		
		require_once($relPath . 'inc/designer/class/Designer.php');
		$data2["tID"] = $post['tID'];
		$data2["dID"] = $post['designer'];
		Designer::insertCosts($data2);
		
		// Email to designer
		require_once($relPath . 'inc/common/display/mailText.php');		
		$designer = Campaign::getDesignerInfo($post['designer']);
		$designerName = $designer['fname']." ".$designer['lname'];
		
		require_once($relPath . 'inc/team/class/TeamMain.php');		
		$teamData = new TeamMain($post['tID']);
		$school = $teamData->getTeamName();
		$team = $teamData->getTeamType();
		$teamName = $school . ", " . $team;
		$mailVars=array("designerName"=>$designerName,"school"=>$school,"team"=>$team,"teamID"=>$post['tID']);
		$toDesignerDataReturn = toDesigner($mailVars);
		
		$mailData['to'] = $designer['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $toDesignerDataReturn['toDesignerSubject'];
		$mailData['messageHTML'] = $toDesignerDataReturn['toDesigner'];
		Mail::send_email($mailData);
	}

	else if ($post['submitAction'] == 'adminRejectBrochure') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 9;
		$dataS["pending"] = 2;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Designer";
		$data['recipID'] = $post['recipID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Brochure Design Changes";
		$data['benchmarkID'] = $post['dID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}

	else if ($post['submitAction'] == 'adminApproveBrochure') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 9;
		$dataS["pending"] = 0;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
						
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Coach";
		$data['recipID'] = $coach['ID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Brochure Design";
		$data['benchmarkID'] = $post['dID'];
		$data['subject'] = "New Design For Review";
		$data['message'] = "The Campaign Administrator has approved a Brochure Design for you to review.";
		Messaging::addMessage($data);
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Designer";
		$dataM['recipID'] = $post['designerID'];
		$dataM['sender'] = "Admin";
		$dataM['senderID'] = 3;
		$dataM['benchmark'] = "Brochure Design Approved";
		$dataM['benchmarkID'] = $post['dID'];
		$dataM['subject'] = "Latest Design Approved";
		$dataM['message'] = "The Campaign Administrator has approved your latest Brochure Design.  Awaiting Coach approval or comments.";
		Messaging::addMessage($dataM);
		
		// email to coach that brouchure design is ready to view
		require_once($relPath . 'inc/common/display/mailText.php');		
		$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		$brochureReadyDataReturn = brochureReady($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $brochureReadyDataReturn['brochureReadySubject'];
		$mailData['messageHTML'] = $brochureReadyDataReturn['brochureReady'];
		Mail::send_email($mailData);		
	}
	
	else if ($post['submitAction'] == 'adminRejectTeamPage') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 25;
		$dataS["pending"] = 1;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Designer";
		$data['recipID'] = $post['recipID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Team Page Image";
		$data['benchmarkID'] = 0;
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}
	
	else if ($post['submitAction'] == 'adminApproveTeamPage') {

		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 27;
		$dataS["pending"] = 0;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/team/class/TeamUser.php');
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
						
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Coach";
		$data['recipID'] = $coach['ID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Team Page Generated";
		$data['benchmarkID'] = 0;
		$data['subject'] = "Your Public Custom Team Page Is Live!";
		$data['message'] = "Your public custom team page is ready! You can use this team page to share across your social media networks, email to friends, family, and colleagues to promote and boost the success of your team\'s fundraising campaign.<br><br>This page contains an integrated sharing feature to allow anyone on the page to share your team\'s page on any of their social networks, or even email it to a friend.<br><br>This team page also includes a quick login feature to make it easier for donors to click the support button to donate to your team\'s fundraising campaign.<br><br>Here is the link to your campaign\'s public page:<br>
		https://joinourcampaign.com/v2/team/".$post['tID']."/share/";
		Messaging::addMessage($data);
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Designer";
		$dataM['recipID'] = $post['designerID'];
		$dataM['sender'] = "Admin";
		$dataM['senderID'] = 3;
		$dataM['benchmark'] = "Team Page Generated";
		$dataM['benchmarkID'] = 0;
		$dataM['subject'] = "Team Page Is Approved";
		$dataM['message'] = "The Campaign Administrator has approved the latest Team Page.";
		Messaging::addMessage($dataM);
		
		// email to coach that brouchure design is ready to view
		require_once($relPath . 'inc/common/display/mailText.php');		
		$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		$teamPageReadyDataReturn = teamPageReady($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $teamPageReadyDataReturn['teamPageReadySubject'];
		$mailData['messageHTML'] = $teamPageReadyDataReturn['teamPageReady'];
		Mail::send_email($mailData);		
	}
	
	
	
	else if ($post['submitAction'] == 'adminCloseCollections') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$data["ID"] = $post['tID'];
		$data["sID"] = 11;
		Campaign::setCampaignStatus($data);
		
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$Campaign = new Campaign($post['tID']);
		$Campaign->setOldCampaignStatus(2);
		
		// email to coach that contacts collection has been closed
		//require_once($relPath . 'inc/common/display/mailText.php');		
		//require_once($relPath . 'inc/team/class/TeamUser.php');		
		//$coach = TeamUser::getAdminData($post['tID']);
		//$coachName = $coach['fname']." ".$coach['lname'];
		
		//$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		//$contactCollectionCompletedDataReturn = contactCollectionCompleted($mailVars);
		
		//$mailData['to'] = $coach['email'];
		//$mailData['from'] = $cfg_adminEmail;
		//$mailData['subject'] = $contactCollectionCompletedDataReturn['contactCollectionCompletedSubject'];
		//$mailData['messageHTML'] = $contactCollectionCompletedDataReturn['contactCollectionCompleted'];
		//Mail::send_email($mailData);
	}
	
	else if ($post['submitAction'] == 'adminReopenCollections') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		//$data["ID"] = $post['tID'];
		//$data["sID"] = 11;
		//Campaign::setCampaignStatus($data);
		
		$tID = $post['tID'];
		Campaign::removeCampaignActiveStatus($tID);
		
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$Campaign = new Campaign($post['tID']);
		$Campaign->setOldCampaignStatus(1);
		
		// email to coach that contacts collection has been closed
		//require_once($relPath . 'inc/common/display/mailText.php');		
		//require_once($relPath . 'inc/team/class/TeamUser.php');		
		//$coach = TeamUser::getAdminData($post['tID']);
		//$coachName = $coach['fname']." ".$coach['lname'];
		
		//$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		//$contactCollectionCompletedDataReturn = contactCollectionCompleted($mailVars);
		
		//$mailData['to'] = $coach['email'];
		//$mailData['from'] = $cfg_adminEmail;
		//$mailData['subject'] = $contactCollectionCompletedDataReturn['contactCollectionCompletedSubject'];
		//$mailData['messageHTML'] = $contactCollectionCompletedDataReturn['contactCollectionCompleted'];
		//Mail::send_email($mailData);
	}
	
	else if ($post['submitAction'] == 'assignPrinter') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::assignPrinter($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 12;
		Campaign::setCampaignStatus($data);

		require_once($relPath . 'inc/printer/class/Printer.php');
		$data2["tID"] = $post['tID'];
		$data2["pID"] = $post['printer'];
		Printer::insertCosts($data2);
		
		// Email to printer
		require_once($relPath . 'inc/common/display/mailText.php');		
		$printer = Campaign::getPrinterInfo($post['printer']);
		$printerName = $printer['fname']." ".$printer['lname'];
		
		require_once($relPath . 'inc/team/class/TeamMain.php');		
		$teamData = new TeamMain($post['tID']);
		$school = $teamData->getTeamName();
		$team = $teamData->getTeamType();
		$teamName = $school . ", " . $team;
		$mailVars=array("printerName"=>$printerName,"school"=>$school,"team"=>$team,"teamID"=>$post['tID']);
		$toPrinterDataReturn = toPrinter($mailVars);
		
		$mailData['to'] = $printer['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $toPrinterDataReturn['toPrinterSubject'];
		$mailData['messageHTML'] = $toPrinterDataReturn['toPrinter'];
		Mail::send_email($mailData);
	}

	else if ($post['submitAction'] == 'adminResendContactList') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$data["ID"] = $post['tID'];
		$data["sID"] = 13;
		$data["pending"] = 2;
		Campaign::setCampaignStatus($data);
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Printer";
		$dataM['recipID'] = $post['printerID'];
		$dataM['sender'] = "Admin";
		$dataM['senderID'] = 3;
		$dataM['benchmark'] = "Contact List";
		$dataM['benchmarkID'] = 0;
		$dataM['subject'] = "New List";
		$dataM['message'] = "The Campaign Administartor has uploaded a new copy of the Contact List for you to review.";
		Messaging::addMessage($dataM);
	}

	else if ($post['submitAction'] == 'adminRejectProof') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 14;
		$dataS["pending"] = 2;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Printer";
		$data['recipID'] = $post['recipID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Printer Proof";
		$data['benchmarkID'] = $post['pID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}

	else if ($post['submitAction'] == 'adminApproveProof') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 14;
		$dataS["pending"] = 0;
		Campaign::setCampaignStatus($dataS);
		$dataS["sID"] = 15;
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$coach = TeamUser::getAdminData($post['tID']);
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$data['tID'] = $post['tID'];
		$data['recip'] = "Coach";
		$data['recipID'] = $coach['ID'];
		$data['sender'] = "Admin";
		$data['senderID'] = 3;
		$data['benchmark'] = "Printer Proof";
		$data['benchmarkID'] = $post['pID'];
		$data['subject'] = "New Proof For Review";
		$data['message'] = "The Campaign Administrator has approved a Printer Proof for you to review.";
		Messaging::addMessage($data);
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Printer";
		$dataM['recipID'] = $post['printerID'];
		$dataM['sender'] = "Admin";
		$dataM['senderID'] = 3;
		$dataM['benchmark'] = "Printer Proof";
		$dataM['benchmarkID'] = $post['pID'];
		$dataM['subject'] = "Latest Proof Approved";
		$dataM['message'] = "The Campaign Administrator has approved your latest Proof.  Awaiting Coach approval or comments.";
		Messaging::addMessage($dataM);
		
		// email to coach that printer proof is ready to view
		// ------ REMOVED -- only campaign admin will receive the proofs from the printer
		//require_once($relPath . 'inc/common/display/mailText.php');		
		
		//$mailData['to'] = $coach['email'];
		//$mailData['from'] = $cfg_adminEmail;
		//$mailData['subject'] = $proofReadySubject;
		//$mailData['messageHTML'] = $proofReady;
		//Mail::send_email($mailData);
		
		
		// Email to printer
		require_once($relPath . 'inc/common/display/mailText.php');		
		$printer = Campaign::getPrinterInfo($post['printer']);
		$printerName = $printer['fname']." ".$printer['lname'];
		
		require_once($relPath . 'inc/team/class/TeamMain.php');		
		$teamData = new TeamMain($post['tID']);
		$school = $teamData->getTeamName();
		$team = $teamData->getTeamType();
		$teamName = $school . ", " . $team;
		$mailVars=array("printerName"=>$printerName,"school"=>$school,"team"=>$team,"teamID"=>$post['tID']);
		$toPrinterProofApprovedDataReturn = toPrinterProofApproved($mailVars);
		
		$mailData['to'] = $printer['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $toPrinterProofApprovedDataReturn['toPrinterProofApprovedSubject'];
		$mailData['messageHTML'] = $toPrinterProofApprovedDataReturn['toPrinterProofApproved'];
		Mail::send_email($mailData);
	}

	// ------ REMOVED -- the printer will set mailing date and fire this
	// ------ Now the system will fire this based on estimated date set by printer
	else if ($post['submitAction'] == 'adminUpdateMailDate') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamMailDate($post);
		$date = date("Y-m-d", strtotime($post['brochure_mailing'])); 
		
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 16;
		Campaign::setCampaignStatus($dataS);
		
		// email to coach that brouchures have been mailed
		require_once($relPath . 'inc/common/display/mailText.php');		
		require_once($relPath . 'inc/team/class/TeamUser.php');		
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
		
		$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		$brochuresMailedDataReturn = brochuresMailed($mailVars);

		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $brochuresMailedDataReturn['brochuresMailedSubject'];
		$mailData['messageHTML'] = $brochuresMailedDataReturn['brochuresMailed'];
		Mail::send_email($mailData);
	}


	else if ($post['submitAction'] == 'adminUpdateCosts') {
		require_once($relPath . 'inc/common/class/campaign/Admin.php');
		Admin::updateCosts($post);
	}
	
	
	else if ($post['submitAction'] == 'adminCompleteCampaign') {
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 18;
		Campaign::setCampaignStatus($dataS);
		
		// Update DB with final email
		require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
		$dataPass=array("tID"=>$post['tID'],"emailData"=>$post['completeCampaign']);
		Admin::setClosingEmail($dataPass);
		
		require_once($relPath . 'inc/team/class/TeamUser.php');		
		$coach = TeamUser::getAdminData($post['tID']);
		
		// email to coach that campaign is completed
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $post['completeCampaignSubject'];
		$mailData['messageHTML'] = '
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>
<body>'.stripslashes($post['completeCampaign']).'</body>
</html>';

		Mail::send_email($mailData);
	}

	else if ($post['submitAction'] == 'adminArchiveCampaign') {
		// Archive Campaign
		require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
		$Admin = new Admin($_SESSION['masterAdmin_id']);
		$Admin->archiveCampaign($post['tID']);
		$Admin->deleteCampaignAfterArchive($post['tID']);
	}