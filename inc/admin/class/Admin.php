<?php

/****************************************************************************************

Admin.php

Defines the Admin class.



Application: Campaign Sports

Trail Associates, July 2011
Artisan Digital Studios, April 2017
****************************************************************************************/

include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');

include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');



class Admin {

	protected $xxx;

	private $zzz;


	public function __construct( $ID ) {

		if($ID == 'empty') {		

		} else {

			try {

				$query = array('select' => "*", 

							   'tbl' => "admins", 

							   'where' => "ID=$ID");

				$DB = new DB();

				$result = $DB->select_single($query);

				

				foreach($result as $field => $data) {

					$this->$field = $data;

				}

			} catch ( Exception $e ) {

				throw new Exception( 'Error instantiating Player: ' . $e->getMessage() );

			}

		}

	} // __construct()

	

	

	public function login($post) {

		try {

			if($post['username']!='' && $post['password']!='') {

				$query = array('select' => "*", 

							   'tbl' => "admins", 

							   'where' => "username = '".$post['username']."' AND password = '".$post['password']."'");

				$DB = new DB();

				$result = $DB->select_single($query);

				

				if ($result) {

					$_SESSION["masterAdmin_id"] = $result['ID'];

					$_SESSION["masterAdmin_name"] = $result['name'];

					$loginFailure = NULL;

				}

				else  {

					$loginFailure = 'No matching records were found, please try again.';

				}		

			} 

			else {

				$loginFailure = 'Please enter your username and password';

			} 

			

			return $loginFailure;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; login(): ' . $e->getMessage() );

		}

	} // login()

	

	public function logout() {

		unset($_SESSION["masterAdmin_id"]);

		unset($_SESSION["masterAdmin_name"]);

		unset($_SESSION["campaign_team"]);

	} // logout()





	public function countCampaigns() {

		try {

			$query = array('select' => "COUNT(*) AS count", 

						   'tbl' => "teams");

			$DB = new DB();

			$result = $DB->select_single($query);

			

			return $result['count'];

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; countCampaigns(): ' . $e->getMessage() );

		}

	} // countCampaigns()



	public function searchCampaigns($pass) {

		try {

			if ($pass['sortS'] == 'contacts')

				$sort = "countTotals " . $pass['sortO'];

			else if ($pass['sortS'] == 'donations')

				$sort = "donationTotal " . $pass['sortO'];

			else if ($pass['sortS'] == 'coach')

				$sort = "tc.lname " . $pass['sortO'];

			else

				$sort = "t.".$pass['sortS'] . " " . $pass['sortO'];

			

			$searchData = $pass['searchData'];

			$searchType = $pass['searchType'];

			

			if ($searchType == 'all') {

				$where = "HAVING t.name LIKE '%".$searchData."%' 

				OR t.team LIKE '%".$searchData."%'

				OR t.ID LIKE '%".$searchData."%'

				OR tc.fname LIKE '%".$searchData."%'

				OR tc.lname LIKE '%".$searchData."%'

				OR tc.email LIKE '%".$searchData."%'";

			} else if ($searchType == 'coach') {

				$where = "HAVING tc.fname LIKE '%".$searchData."%' 

				OR tc.lname LIKE '%".$searchData."%'

				OR tc.email LIKE '%".$searchData."%'";

			} else {

				$where = "HAVING t.".$searchType." LIKE '%".$searchData."%'";

			}

			

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM donations WHERE tID = t.ID) AS donationTotal", 

							'sort' => $sort,

							'tbl' => "teams t LEFT JOIN team_contacts tc ON t.ID = tc.tID LEFT JOIN contacts c ON t.ID = c.tID GROUP BY t.ID ".$where);

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; searchCampaigns(): ' . $e->getMessage() );

		}

	} // searchCampaigns()

	

	public function getAllCampaigns($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donations')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'coach')

				$sort = "tc.lname " . $sortPass['o'];

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

				

			//$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID) AS donationTotal", 



			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM donations WHERE tID = t.ID) AS donationTotal, (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges", 

							'sort' => $sort,

							'tbl' => "teams t LEFT JOIN team_contacts tc ON t.ID = tc.tID LEFT JOIN contacts c ON t.ID = c.tID GROUP BY t.ID");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaigns(): ' . $e->getMessage() );

		}

	} // getAllCampaigns()

	

	public function getAllCampaignsS1($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donations')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'coach')

				$sort = "tc.lname " . $sortPass['o'];

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

			

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM donations WHERE tID = t.ID) AS donationTotal, (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges", 

							'sort' => $sort,

							'tbl' => "teams t LEFT JOIN team_contacts tc ON t.ID = tc.tID LEFT JOIN contacts c ON t.ID = c.tID GROUP BY t.ID HAVING t.status = 1");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaignsS1(): ' . $e->getMessage() );

		}

	} // getAllCampaignsS1()

	

	public function getAllCampaignsS2($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donations')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'coach')

				$sort = "tc.lname " . $sortPass['o'];

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

			

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM donations WHERE tID = t.ID) AS donationTotal, (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges", 

							'sort' => $sort,

							'tbl' => "teams t LEFT JOIN team_contacts tc ON t.ID = tc.tID LEFT JOIN contacts c ON t.ID = c.tID GROUP BY t.ID HAVING t.status = 2");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaignsS2(): ' . $e->getMessage() );

		}

	} // getAllCampaignsS2()

	

	

	public function getAllCampaignsForExport() {

		try {

			$query = array('select' => "c.ID, c.tID, c.pID, COUNT(c.ID) AS countTotals, t.name, t.team, t.status, tc.fname, tc.lname", 

							'sort' => "countTotals DESC",

							'tbl' => "teams AS t JOIN team_contacts AS tc ON tc.tID = t.ID JOIN contacts AS c ON c.tID = t.ID GROUP BY c.tID");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaignsForExport(): ' . $e->getMessage() );

		}

	} // getAllCampaignsForExport()

	

	

	public function getCampaignTotalDonationSingle($tID) {
		try {

			$query = array('select' => "t.ID, COUNT(d.donationValue) AS count, ROUND(SUM(d.donationValue),2) AS donationTotal,  (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges, t.name, t.team, t.status, tc.fname, tc.lname", 

							'sort' => "donationTotal DESC",

							'tbl' => "teams AS t JOIN team_contacts AS tc ON t.ID = tc.tID JOIN donations AS d ON t.ID = d.tID WHERE t.ID ='".$tID."'");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaignsByDonations(): ' . $e->getMessage() );

		}
	}
	
	 
	public function getAllCampaignsByDonations() {

		try {

			$query = array('select' => "t.ID, COUNT(d.donationValue) AS count, ROUND(SUM(d.donationValue),2) AS donationTotal,  (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges, t.name, t.team, t.status, tc.fname, tc.lname", 

							'sort' => "donationTotal DESC",

							'tbl' => "teams AS t JOIN team_contacts AS tc ON t.ID = tc.tID JOIN donations AS d ON t.ID = d.tID GROUP BY t.ID");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaignsByDonations(): ' . $e->getMessage() );

		}

	} // getAllCampaignsByDonations()

	
	// function to grab all phase 1 campaigns for deletion
		public function getAllCampaignsDel($sortPass) {
		try {
			if ($sortPass['s'] == 'contacts')
				$sort = "countTotals " . $sortPass['o'];
			else if ($sortPass['s'] == 'donations')
				$sort = "donationTotal " . $sortPass['o'];
			else if ($sortPass['s'] == 'coach')
				$sort = "tc.lname " . $sortPass['o'];
			else
				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals", 
							'sort' => $sort,
							'tbl' => "teams t LEFT JOIN team_contacts tc ON t.ID = tc.tID LEFT JOIN contacts c ON t.ID = c.tID WHERE t.status=1 GROUP BY t.ID");
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllCampaigns(): ' . $e->getMessage() );
		}
	} // getAllCampaignsDel()
	
	public function searchCampaignsDel($pass) {
		try {
			if ($pass['sortS'] == 'contacts')
				$sort = "countTotals " . $pass['sortO'];
			else if ($pass['sortS'] == 'donations')
				$sort = "donationTotal " . $pass['sortO'];
			else if ($pass['sortS'] == 'coach')
				$sort = "tc.lname " . $pass['sortO'];
			else
				$sort = "t.".$pass['sortS'] . " " . $pass['sortO'];
			
			$searchData = $pass['searchData'];
			$searchType = $pass['searchType'];
			
			if ($searchType == 'all') {
				$where = "HAVING t.name LIKE '%".$searchData."%' 
				OR t.team LIKE '%".$searchData."%'
				OR t.ID LIKE '%".$searchData."%'
				OR tc.fname LIKE '%".$searchData."%'
				OR tc.lname LIKE '%".$searchData."%'
				OR tc.email LIKE '%".$searchData."%'";
			} else if ($searchType == 'coach') {
				$where = "HAVING tc.fname LIKE '%".$searchData."%' 
				OR tc.lname LIKE '%".$searchData."%'
				OR tc.email LIKE '%".$searchData."%'";
			} else {
				$where = "HAVING t.".$searchType." LIKE '%".$searchData."%'";
			}
			
			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals", 
							'sort' => $sort,
							'tbl' => "teams t LEFT JOIN team_contacts tc ON t.ID = tc.tID LEFT JOIN contacts c ON t.ID = c.tID WHERE t.status=1 GROUP BY t.ID ".$where);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; searchCampaigns(): ' . $e->getMessage() );
		}
	} // searchCampaigns()
	
	public function removeCampaign($tID) {
		try {
			$query = "
				DELETE FROM contacts WHERE tID = $tID;
			";
			
			$DB = new DB();
			$result = $DB->custom($query);

			$query = "
				DELETE FROM donations WHERE tID = $tID;
			";
			
			$DB = new DB();
			$result = $DB->custom($query);

			$query = "
				DELETE FROM donations_failed WHERE tID = $tID;
			";
			
			$DB = new DB();
			$result = $DB->custom($query);

			$query = "
				DELETE FROM players WHERE tID = $tID;
			";
			
			$DB = new DB();
			$result = $DB->custom($query);

			$query = "
				DELETE FROM teams WHERE ID = $tID;
			";
			
			$DB = new DB();
			$result = $DB->custom($query);

			$query = "
				DELETE FROM team_contacts WHERE tID = $tID;
			";
			
			$DB = new DB();
			$result = $DB->custom($query);
			
			//$query = array('tbl' => "team",
			//			   'where' => "ID=$id");
			//$DB = new DB();
			//$result = $DB->delete_records($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; removeCampaign(): ' . $e->getMessage() );
			return $e->getMessage();
		}
	} // removeContact()
	public function countCoaches() {

		try {

			$query = array('select' => "COUNT(*) AS count", 

						   'tbl' => "team_contacts");

			$DB = new DB();

			$result = $DB->select_single($query);

			

			return $result['count'];

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; countCoaches(): ' . $e->getMessage() );

		}

	} // countCoaches()

	

	public function getAllCoaches() {

		try {

			$query = array('select' => "t.ID, tc.fname, tc.lname, tc.email, tc.phoneDay, tc.phoneEve, tc.phoneCell, t.name, t.team, t.password, t.adminPassword", 

							'sort' => "tc.lname, tc.fname",

							'tbl' => "team_contacts AS tc INNER JOIN teams AS t ON tc.tID = t.ID");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCoaches(): ' . $e->getMessage() );

		}

	} // getAllCoaches()

	

	public function getTopContacts() {

		try {

			$query = array('select' => "c.ID, c.tID, c.pID, COUNT(c.ID) AS countTotals, t.name, t.team, t.status", 

							'sort' => "countTotals DESC",

							'tbl' => "contacts AS c INNER JOIN teams AS t ON c.tID = t.ID GROUP BY c.tID");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getTopContacts(): ' . $e->getMessage() );

		}

	} // getTopContacts()

	

	

	public function getTopDonations() {

		try {

			$query = array('select' => "d.ID, d.tID, ROUND(SUM(d.donationValue),2) AS donationTotal, t.name, t.team, t.status", 

							'sort' => "donationTotal DESC",

							'tbl' => "donations AS d LEFT JOIN teams AS t ON d.tID = t.ID GROUP BY d.tID");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getTopDonations(): ' . $e->getMessage() );

		}

	} // getTopDonations()

	

	public function getAllDonations() {

		try {

			$query = array('select' => "*", 

						   'tbl' => "donations", 

						   'sort' => "donation_date DESC LIMIT 20");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllDonations(): ' . $e->getMessage() );

		}

	} // getAllDonations()



	public function getAllFailedDonations() {

		try {

			$query = array('select' => "df.*, t.ID, t.name, t.team", 

						   'tbl' => "donations_failed df INNER JOIN teams t ON df.tID = t.ID", 

						   'sort' => "donation_date DESC");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllFailedDonations(): ' . $e->getMessage() );

		}

	} // getAllFailedDonations()



	public function getCampaignNotes($tID) {

		try {

			$query = array('select' => "note", 

						   'tbl' => "teams",

						   'where' => "ID = $tID");

			$DB = new DB();

			$result = $DB->select_single($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getCampaignNotes(): ' . $e->getMessage() );

		}

	} // getCampaignNotes()



	public function setCampaignNotes($post) {

		try {

			$set = "note='" . $post['note'] ."'";

			

			$query = array('tbl' => "teams", 

						   'set' => $set, 

						   'where' => "ID=" . $post['tID']);

			$DB = new DB();

			$result = $DB->update_single($query);

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; setCampaignNotes(): ' . $e->getMessage() );

		}

	} // setCampaignNotes()



	public function removeCheck($id) {

		try {

			$query = array('tbl' => "donations",

						   'where' => "ID=$id");

			$DB = new DB();

			$result = $DB->delete_records($query);



		} catch ( Exception $e ) {

			//throw new Exception( 'Error with Method; removeCheck(): ' . $e->getMessage() );

			return $e->getMessage();

		}

	} // removeCheck()





	public function archiveCampaign($tID) {

		try {

			$query = "

				INSERT INTO archive_approvals SELECT * FROM approvals WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_contacts SELECT * FROM contacts WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_designer_costs SELECT * FROM designer_costs WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_designer_submissions SELECT * FROM designer_submissions WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_donations SELECT * FROM donations WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_donations_failed SELECT * FROM donations_failed WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_messages SELECT * FROM messages WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_players SELECT * FROM players WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_printer_costs SELECT * FROM printer_costs WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_printer_submissions SELECT * FROM printer_submissions WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_status_teams SELECT * FROM status_teams WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_teams SELECT * FROM teams WHERE ID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_team_contacts SELECT * FROM team_contacts WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				INSERT INTO archive_team_initial_info SELECT * FROM team_initial_info WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



		} catch ( Exception $e ) {

			//throw new Exception( 'Error with Method; archiveCampaign(): ' . $e->getMessage() );

			return $e->getMessage();

		}

	} // archiveCampaign()





	public function deleteCampaignAfterArchive($tID) {

		try {

			$query = "

				DELETE FROM approvals WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM contacts WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM designer_costs WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM designer_submissions WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM donations WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM donations_failed WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM messages WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM players WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM printer_costs WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM printer_submissions WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM status_teams WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM teams WHERE ID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM team_contacts WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



			$query = "

				DELETE FROM team_initial_info WHERE tID = $tID;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);



		} catch ( Exception $e ) {

			//throw new Exception( 'Error with Method; deleteCampaignAfterArchive(): ' . $e->getMessage() );

			return $e->getMessage();

		}

	} // deleteCampaignAfterArchive()

	

	

	

	public function getAllCampaignsReport($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donationTotal')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'donationBillable')

				$sort = "donationBillable " . $sortPass['o'];

			else if ($sortPass['s'] == 'total')

				$sort = "totalCosts " . $sortPass['o'];

			else if ($sortPass['s'] == 'difference')

				$sort = "difference " . $sortPass['o'];	

			else if ($sortPass['s'] == 'dateSort')

				$sort = "dateSort " . $sortPass['o'];	

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

				

			$query = array('select' => "t.ID, t.name, t.team, t.brochure_mailing, DATE(t.brochure_mailing) as dateSort, (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID) AS donationTotal, (SELECT ROUND(SUM(donationValue),2) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges, (t.brochure_count * t.brochure_cost + t.brochure_charges) AS totalCosts, ((SELECT ifnull(ROUND(SUM(donationValue),2),0) FROM donations WHERE tID = t.ID AND paymentMethod != 'check') - (t.brochure_count * t.brochure_cost + t.brochure_charges)) AS difference", 

							'sort' => $sort,

							'where' => "t.brochure_mailing >= DATE_SUB(CURDATE(), INTERVAL 90 DAY)",

							'tbl' => "teams t");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllCampaignsReport(): ' . $e->getMessage() );

		}

	} // getAllCampaignsReport()


    public function deleteMessage($post) {
        
		try {
			$query = "
				DELETE FROM messages WHERE ID = ".$post['id'].";
			";
			
			$DB = new DB();
			$result = $DB->custom($query);
			 
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; deleteMessage(): ' . $e->getMessage() );
			return $e->getMessage();
		}
    }
    
	public function addDesigner($post) {

		try {

			$fields = "fname";

			$values = "'".$post['fname']."'";

			

			$fields .= ",lname";

			$values .= ",'".$post['lname']."'";



			$fields .= ",email";

			$values .= ",'".$post['email']."'";



			$fields .= ",username";

			$values .= ",'".$post['username']."'";

			

			$fields .= ",password";

			$values .= ",'".$post['password']."'";
			
			$fields .= ",title";

			$values .= ",'".$post['title']."'";

			

			$query = array('tbl' => "designers", 

						   'fields' => $fields, 

						   'values' => $values);

			$DB = new DB();

			$result = $DB->insert_single($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; addDesigner(): ' . $e->getMessage() );

		}

	} // addDesigner()



	public function addPrinter($post) {

		try {

			$fields = "fname";

			$values = "'".$post['fname']."'";

			

			$fields .= ",lname";

			$values .= ",'".$post['lname']."'";



			$fields .= ",email";

			$values .= ",'".$post['email']."'";



			$fields .= ",username";

			$values .= ",'".$post['username']."'";

			

			$fields .= ",password";

			$values .= ",'".$post['password']."'";

			

			$query = array('tbl' => "printers", 

						   'fields' => $fields, 

						   'values' => $values);

			$DB = new DB();

			$result = $DB->insert_single($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; addPrinter(): ' . $e->getMessage() );

		}

	} // addPrinter()



	public function getAllDesigners() {

		try {

			$query = array('select' => "*", 

							'sort' => "lname, fname ASC",

							'tbl' => "designers");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllDesigners(): ' . $e->getMessage() );

		}

	} // getAllDesigners()



	public function getAllPrinters() {

		try {

			$query = array('select' => "*", 

							'sort' => "lname, fname ASC",

							'tbl' => "printers");

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getAllPrinters(): ' . $e->getMessage() );

		}

	} // getAllPrinters()





	public function getDataForCampaignClosing($ID) {

		try {

			require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

			$playerCount = TeamUser::countPlayers($ID);

			$contactCount = TeamUser::countContacts($ID);

			$domContactCount = TeamUser::countContactsDomestic($ID);

			$intlContactCount = $contactCount - $domContactCount;

			

			$donations = TeamUser::getAllDonations($ID);

			

			$creditCards = $checks = $paypal = 0;

			foreach ($donations AS $d) {

				if ($d['paymentMethod'] == 'online') $creditCards++;

				else if ($d['paymentMethod'] == 'check') $checks++;

				else if ($d['paymentMethod'] == 'PayPal') $paypal++;

			}

			
			

			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			
			$brochureInfo = Campaign::getBrochureInfo($ID);

			$campaignStatuses = Campaign::getCampaignStatuses($ID);
			
			$brochureCount = $brochureInfo['brochure_count'];
			
			$brochureCost = $brochureInfo['brochure_cost'];
			
			if($brochureInfo['brochure_charges']!='') {
				$brochureCharges = $brochureInfo['brochure_charges'];
			} else {
				$brochureCharges = '0.00';
			}
			if($brochureInfo['brochure_charges']!='') {
				$campaignCost = $brochureInfo['brochure_count'] * $brochureInfo['brochure_cost'] + $brochureInfo['brochure_charges'];
			} else {
				$campaignCost = $brochureInfo['brochure_count'] * $brochureInfo['brochure_cost'];
			}
			
			require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');		

			$coach = TeamUser::getAdminData($_SESSION['current_folder']);

			$coachName = $coach['fname']." ".$coach['lname'];

			

			$data['teamID'] = $ID;

			$data['campaignStartDate'] = date("Y-m-d", strtotime($campaignStatuses[2]['status_date']));

			$data['playersRegistered'] = $playerCount;

			$data['domesticContacts'] = $domContactCount;

			$data['internationalContacts'] = $intlContactCount;

			$data['brochuresMailed'] = $brochureCount;

			$data['costPerBrochure'] = $brochureCost;
			
			$data['additionalCosts'] = $brochureCharges;

			$data['totalCampaignCost'] = $campaignCost;

			$data['checkDonations'] = $checks;

			$data['creditCardDonations'] = $creditCards;

			$data['paypalDonations'] = $paypal;

			$data['coachName'] = $coachName;

			

			require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');

			$coach = TeamUser::getAdminData($ID);

			

			require_once($_SESSION['relative_path'] . 'inc/common/display/mailText.php');	

			$dataReturn = completeCampaign($data);	

	

			return $dataReturn;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getDataForCampaignClosing(): ' . $e->getMessage() );

		}

	} // getDataForCampaignClosing()



	public function setClosingEmail($data) {

		try {

			$set = "v2_final_email='" . $data['emailData'] ."'";

			

			$query = array('tbl' => "teams", 

						   'set' => $set, 

						   'where' => "ID=" . $data['tID']);

			$DB = new DB();

			$result = $DB->update_single($query);

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; setClosingEmail(): ' . $e->getMessage() );

		}

	} // setClosingEmail()







	public function getArchiveCampaigns($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donations')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'coach')

				$sort = "tc.lname " . $sortPass['o'];

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

				

			//$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT SUM(donationValue) FROM donations WHERE tID = t.ID) AS donationTotal", 



			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, t.reup, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM archive_donations WHERE tID = t.ID) AS donationTotal, (SELECT SUM(donationValue) FROM archive_donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges", 

							'sort' => $sort,

							'tbl' => "archive_teams t LEFT JOIN archive_team_contacts tc ON t.ID = tc.tID LEFT JOIN archive_contacts c ON t.ID = c.tID GROUP BY t.ID");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getArchiveCampaigns(): ' . $e->getMessage() );

		}

	} // getArchiveCampaigns()

	

	public function getArchiveCampaignsS1($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donations')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'coach')

				$sort = "tc.lname " . $sortPass['o'];

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

			

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, t.reup, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM archive_donations WHERE tID = t.ID) AS donationTotal, (SELECT SUM(donationValue) FROM archive_donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges", 

							'sort' => $sort,

							'tbl' => "archive_teams t LEFT JOIN archive_team_contacts tc ON t.ID = tc.tID LEFT JOIN archive_contacts c ON t.ID = c.tID GROUP BY t.ID HAVING t.status = 1");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getArchiveCampaignsS1(): ' . $e->getMessage() );

		}

	} // getArchiveCampaignsS1()

	

	public function getArchiveCampaignsS2($sortPass) {

		try {

			if ($sortPass['s'] == 'contacts')

				$sort = "countTotals " . $sortPass['o'];

			else if ($sortPass['s'] == 'donations')

				$sort = "donationTotal " . $sortPass['o'];

			else if ($sortPass['s'] == 'coach')

				$sort = "tc.lname " . $sortPass['o'];

			else

				$sort = "t.".$sortPass['s'] . " " . $sortPass['o'];

			

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, t.reup, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM archive_donations WHERE tID = t.ID) AS donationTotal, (SELECT SUM(donationValue) FROM archive_donations WHERE tID = t.ID AND paymentMethod != 'check') AS donationBillable, t.brochure_count, t.brochure_cost, t.brochure_charges", 

							'sort' => $sort,

							'tbl' => "archive_teams t LEFT JOIN archive_team_contacts tc ON t.ID = tc.tID LEFT JOIN archive_contacts c ON t.ID = c.tID GROUP BY t.ID HAVING t.status = 2");

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; getArchiveCampaignsS2(): ' . $e->getMessage() );

		}

	} // getArchiveCampaignsS2()



	public function searchArchiveCampaigns($pass) {

		try {

			if ($pass['sortS'] == 'contacts')

				$sort = "countTotals " . $pass['sortO'];

			else if ($pass['sortS'] == 'donations')

				$sort = "donationTotal " . $pass['sortO'];

			else if ($pass['sortS'] == 'coach')

				$sort = "tc.lname " . $pass['sortO'];

			else

				$sort = "t.".$pass['sortS'] . " " . $pass['sortO'];

			

			$searchData = $pass['searchData'];

			$searchType = $pass['searchType'];

			

			if ($searchType == 'all') {

				$where = "HAVING t.name LIKE '%".$searchData."%' 

				OR t.team LIKE '%".$searchData."%'

				OR t.ID LIKE '%".$searchData."%'

				OR tc.fname LIKE '%".$searchData."%'

				OR tc.lname LIKE '%".$searchData."%'

				OR tc.email LIKE '%".$searchData."%'";

			} else if ($searchType == 'coach') {

				$where = "HAVING tc.fname LIKE '%".$searchData."%' 

				OR tc.lname LIKE '%".$searchData."%'

				OR tc.email LIKE '%".$searchData."%'";

			} else {

				$where = "HAVING t.".$searchType." LIKE '%".$searchData."%'";

			}

			

			$query = array('select' => "t.ID, t.password, t.name, t.team, t.status, t.reup, tc.fname, tc.lname, tc.email, COUNT(DISTINCT c.ID) AS countTotals, (SELECT ROUND(SUM(donationValue),2) FROM archive_donations WHERE tID = t.ID) AS donationTotal", 

							'sort' => $sort,

							'tbl' => "archive_teams t LEFT JOIN archive_team_contacts tc ON t.ID = tc.tID LEFT JOIN archive_contacts c ON t.ID = c.tID GROUP BY t.ID ".$where);

			

			$DB = new DB();

			$result = $DB->select_multi($query);

			

			return $result;

			

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; searchArchiveCampaigns(): ' . $e->getMessage() );

		}

	} // searchArchiveCampaigns()

	

	

	public function reUpCampaign($id) {

		try {

			// Insert Team, get New Team ID

			//

			$query = array('select' => "ID, password, adminPassword, name, team, bgColor1, bgColor2, txtColor, active", 

						   'tbl' => "archive_teams",

						   'where' => "ID = $id");

			$DB = new DB();

			$result = $DB->select_single($query);

			//print_r($result);

			$i = 0;

			$fields = $values = "";

			foreach($result AS $field => $value) {

				if ($field == 'ID') continue;

				//echo "<br> $field: $value<br>";

				if ($i == 0) {

					$fields .= $field;

					$values .= "\"".$value."\"";

				} elseif ($field == 'active') {

					$fields .= "," . $field;

					$values .= "," . $value;

				} else {

					$fields .= "," . $field;

					$values .= ",\"" . $value."\"";

				}

				$i = 1;

			}

			$query = array('tbl' => "teams", 

						   'fields' => $fields, 

						   'values' => $values);

			$DB = new DB();

			$newID = $DB->insert_single($query);

			

			// Flag that archive has been reupped already

			//

			$set = "reup=1";

			

			$query = array('tbl' => "archive_teams", 

						   'set' => $set, 

						   'where' => "ID=$id");

			$DB = new DB();

			$result = $DB->update_single($query);

			

			

			// Insert Contacts, with New Team ID

			//

			$query = "

				INSERT INTO contacts(ID, pID, tID, prefix, fname, lname, relationship, multi, prefix2, fname2, lname2, relationship2, company, address, address2, city, state, zip, email, canada, intl, country, register_date)

				SELECT ID, pID, $newID, prefix, fname, lname, relationship, multi, prefix2, fname2, lname2, relationship2, company, address, address2, city, state, zip, email, canada, intl, country, register_date

				FROM archive_contacts

				WHERE tID = $id;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);

			

			// Insert Players, with New Team ID

			//

			$query = "

				INSERT INTO players(ID, tID, fname, lname, address, address2, city, state, zip, email, register_date, password)

				SELECT ID, $newID, fname, lname, address, address2, city, state, zip, email, register_date, password

				FROM archive_players

				WHERE tID = $id;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);

	

			// Insert coach data, with New Team ID

			//

			$query = "

				INSERT INTO team_contacts(ID, tID, fname, lname, title, address, address2, city, state, zip, intl, country, phoneDay, phoneEve, phoneCell, email)

				SELECT ID, $newID, fname, lname, title, address, address2, city, state, zip, intl, country, phoneDay, phoneEve, phoneCell, email

				FROM archive_team_contacts

				WHERE tID = $id;

			";

			

			$DB = new DB();

			$result = $DB->custom($query);

			

			// Set benchmark for "New" created Campaign

			//

			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');

			$data["ID"] = $newID;

			$data["sID"] = 1;

			Campaign::setCampaignStatus($data);

			

			// Create the "New" Team folders and index files

			//

			$data['id'] = $id;

			$data['newID'] = $newID;

			Campaign::createReUpCampaignFiles($data);



			echo "<script language='javascript' type='text/javascript'>location.href='index.php?nav=campaigns&action=dashboard&team=$newID'</script>";

			exit();

	

		} catch ( Exception $e ) {

			throw new Exception( 'Error with Method; reUpCampaign(): ' . $e->getMessage() );

		}

	} // reUpCampaign()

	



}