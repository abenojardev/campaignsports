$(document).ready(function() { 
						   
	$('#frm').submit(function() { 
		var val = validate();
		if (val)
		{
			return true;
		}
		else
		{
			return false;
		}
	});	
	
	$('#frmSubmit').click(function(event) {
		$('#frm').submit();
		event.preventDefault();
	});

	function validate() { 
		var msg = "Please complete the highlighted items.";
		error_check = false; 
	
		$(".validate").each(function (i) {
			if ((this.value == "") || (this.value == "Subject...") || (this.value == "Message...")) {
				error_check = true;
				$(this).addClass("fieldEmpty");
			} else {
				$(this).removeClass("fieldEmpty");
			}
		});
				
		var p1Field = $("input[name='password']");
		var p2Field = $("input[name='password2']");

		if(p1Field.length > 0) {
			var p1 = p1Field.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var p2 = p2Field.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			
			if (p1 != p2) {
				msg = "Your team passwords do not match.";
				error_check = true;
				p1Field.addClass("fieldEmpty");
				p2Field.addClass("fieldEmpty");
			}
		}
		
		var p3Field = $("input[name='coachPassword']");
		var p4Field = $("input[name='coachPassword2']");

		if(p3Field.length > 0) {
			var p3 = p3Field.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			var p4 = p4Field.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			
			if (p3 != p4) {
				msg = "Your coach passwords do not match.";
				error_check = true;
				p3Field.addClass("fieldEmpty");
				p4Field.addClass("fieldEmpty");
			}
		}
		
		if (error_check) {
			alert(msg);
			return false;
		}
		else
		{
			return true;
		}
	}
 
}); 