<?php
	if ( isset($_SESSION["designer_id"]) && ($_SESSION["designer_id"]) && ($_SESSION["designer_id"] != "") ) {
		$team_id = $get['tID'];
		$recip = "Designer";
		$recipID = $_SESSION["designer_id"];
		$link = "index.php?nav=viewCampaign&sNav=prevDesigns&tID=$team_id";
		$label = "Designs";
	} 
	else if ( isset($_SESSION["printer_id"]) && ($_SESSION["printer_id"]) && ($_SESSION["printer_id"] != "") ) {
		$team_id = $get['tID'];
		$recip = "Printer";
		$recipID = $_SESSION["printer_id"];
		$link = "index.php?nav=viewCampaign&sNav=prevProofs&tID=$team_id";
		$label = "Proofs";
	} 
	
	if ($recip == 'Designer') {
		require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
		$submissions = Designer::getSubmittedDesigns($team_id);
	} else if ($recip == 'Printer') {
		require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
		$submissions = Printer::getSubmittedProofs($team_id);
		$brochures = Printer::getSubmittedBrochures($get['tID']);
		$envelopes = Printer::getSubmittedEnvelopes($get['tID']);
	}
	
	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Submitted $label</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
	";
	
						if ($recip == 'Designer') {
							$classAlternate = "bg1";
							$count = 0;
							$dCount = count($submissions);
							foreach($submissions AS $s) {
								if ($s['final'] == 1) { $dCount--; continue; }
								$count++;
								if ($count > 10) continue;
								$date = date("M j, Y", strtotime($s['date_submitted']));
								echo "
									<tr class='$classAlternate'>
										<td>#$dCount - " . $date . "</td>
									</tr>
								";
								$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
								$dCount--;
							}
						} else if ($recip == 'Printer') {
							$classAlternate = "bg1";
							$count = 0;
							$bCount = count($brochures)+1;
							$eCount = count($envelopes)+1;
							foreach($submissions AS $s) {
								if (isset($s['envelope']) && $s['envelope'] == 1) {
									$proofOrEnvelope = "Envelope";
									$eCount--;
									$dCount = $eCount;
								} else {
									$proofOrEnvelope = "Proof";
									$bCount--;
									$dCount = $bCount;
								}
								
								$count++;
								if ($count > 10) continue;
								$date = date("M j, Y", strtotime($s['date_submitted']));
								echo "
									<tr class='$classAlternate'>
										<td>$proofOrEnvelope #$dCount - " . $date . "</td>
									</tr>
								";
								$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							}
						}
							
	echo "
						<tr>
							<td>&nbsp;</td>
						</tr>      
						<tr>
							<td align='right'><a href='$link'>View All</a></td>
						</tr>              
					</table>

                </div>
	";
?>