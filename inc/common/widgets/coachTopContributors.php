<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$link = "index.php?nav=campaigns&action=players&edit";
		$link2 = "index.php?nav=campaigns&action=players&view=contacts";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$link = "index.php?action=players&edit";
		$link2 = "index.php?action=players&view=contacts";
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$topPlayers = TeamUser::getTopPlayers($team_id);
	
	echo "
                <h2 class='teamPrimaryTxtColor'>My Team's Top Contributors</h2>

					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td><strong>Team Athlete's Name</strong></td>
							<td align='center'><strong>Register Date</strong></td>
							<td align='center'><strong>Total Contacts</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($topPlayers AS $tp) {
							if ($count > 4) continue;
							$date = date("M j, Y", strtotime($tp['register_date']));
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'><a href='$link=" . $tp['pID'] . "'>" . $tp['lname'] . ", " . $tp['fname'] . "</a></td>
							<td align='center'>" . $date . "</td>
							<td align='center'>" . $tp['countField'] . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>
	";
?>