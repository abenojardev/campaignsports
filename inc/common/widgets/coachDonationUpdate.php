<?php
	$action = ( isset($_SESSION["nav2"]) ) ? $_SESSION["nav2"] : "dashboard";

	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$link = "index.php?nav=campaigns&action=$action";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$link = "index.php?action=$action";
	}
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$Team = new TeamMain($team_id);
	$goal = $Team->getTeamGoal();
	$colors = $Team->getTeamColors();
	
	$goalPerc = @($donationTotal / $goal);
	$goalBar = round($goalPerc * 460);
	
	$tic1 = $goal / 5;
	$tic2 = $tic1 * 2;
	$tic3 = $tic1 * 3;
	$tic4 = $tic1 * 4;
	
	echo "
					<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/donationUpdate.css' />
					<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
					<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery.form.js'></script>
					<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/donationUpdate.js'></script>
					
					<div class='updateLink'>
						<a href='#' id='donationUpdateBtn'>Click Here to Change Campaign Goal</a>
					</div>
					
					<div class='donationUpdate' id='donationUpdate'>
						<form method='post' name='frm' id='frm' action='$link'>
						<div class='formElement'>
							<div class='formElementCol2'>
								<input type='hidden' name='cID' value='xxx' />
								<input type='hidden' name='submitAction' value='adminUpdateGoal' />
								<a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update My Team's Campaign Goal</a>
								<input name='goal' id='goal' type='text' size='10' class='selectFieldSWR' value='$goal'/>
							</div>
						</div>
						</form>					
                 
				 	</div>
					
	";
?>