<?php
	if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		$team_id = $_SESSION['campaign_team'];
		$recip = "Admin";
		$recipID = 3;
	} 
	else if ( isset($_SESSION["designer_id"]) && ($_SESSION["designer_id"]) && ($_SESSION["designer_id"] != "") ) {
		$team_id = $get['tID'];
		$recip = "Designer";
		$recipID = $_SESSION["designer_id"];
	} 
	else if ( isset($_SESSION["printer_id"]) && ($_SESSION["printer_id"]) && ($_SESSION["printer_id"] != "") ) {
		$team_id = $get['tID'];
		$recip = "Printer";
		$recipID = $_SESSION["printer_id"];
	} 

	require_once($relPath . 'inc/common/class/campaign/Messaging.php');
	
	$mData['recip'] = $recip;
	$mData['recipID'] = $recipID;
	$mData['unreadOnly'] = 1;
	$messages = Messaging::getAllReceivedMessages($mData);

	echo "
                <div class='suggestionsWrap'>
                    <p class=''><strong>Unread Messages</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($messages AS $m) {
							if ($count > 9) continue;
							$unreadFlag = ($m['markAsRead']) ? "<span>" : "<span style='font-weight:bold;'>";
							
							echo "
								<tr class='$classAlternate'>
									<td>$unreadFlag<a href='index.php?nav=messages&sub=viewMessage&mID=" . $m['ID'] . "'>" . $m['benchmark'] . " - " . $m['subject'] . "</a></span></td>
								</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td>&nbsp;</td>
						</tr>      
						<tr>
							<td align='right'><a href='index.php?nav=messages'>View All</a></td>
						</tr>              
					</table>

                </div>
	";
?>