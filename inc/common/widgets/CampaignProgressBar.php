<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$adminCheck = true;
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$adminCheck = false;
	}
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$donation = TeamUser::getDonationTotal($team_id);
	$donationTotal = $donation['donationTotal'];
	$donationTotal = ($donationTotal) ?  $donationTotal : 0;
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$Team = new TeamMain($team_id);
	$goal = $Team->getTeamGoal();
	$colors = $Team->getTeamColors();
	$mailDate = ($TeamMain->getTeamMailDate()) ? date("M j, Y", strtotime($TeamMain->getTeamMailDate())) : "";
	
	$goalPerc = @($donationTotal / $goal);
	$goalBar = round($goalPerc * 460);
	$perBar = $goalPerc * 100;
	
	if ($goalBar > 460) $goalBar = 460;
	
	$tic1 = $goal / 5;
	$tic2 = $tic1 * 2;
	$tic3 = $tic1 * 3;
	$tic4 = $tic1 * 4;
	

	
	if ($adminCheck && $mailDate != NULL) {
		echo "
			<div style='border: 1px solid #000;text-align:center;padding:5px;margin-bottom:20px;'>
			<h2 class='teamPrimaryTxtColor'>Brochure Mailing Date: &nbsp;&nbsp;&nbsp; $mailDate</h2>
			</div>	
		";
	}
	
	echo "
					<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/donationBar.css' />

					<div class='campaignProgress'>
						<br />
						<div class='campaignProgressTitleBar'>
							<div class='donationBarMainContainer2'>
								<div class='donationBarTitleLeft2'>
									<strong>$$donationTotal of $$goal</strong>
								</div>
								<div class='donationBarTitleRight' style='color:#".$colors['txtColor']."'>

								</div>
								<div class='clear'><br /></div>
								
							 
								<div class='donationBarLower2'></div>
								<div class='donationBar2' style='width:".$perBar."%;background-color:#f79646'></div>
								<div class='donationBarTotal2' style='width:".$goalBar."px;'>

								</div>
							</div>
						</div>
						<br />
						<br />
					
	";						
        				include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachAtheleteRegistered.php');
	echo "        				
                    </div>
					
	";
?>



		