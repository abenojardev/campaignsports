<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
	} 
	else {
		$team_id = $_SESSION['current_folder'];
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$playerCount = TeamUser::countPlayers($team_id);
	$contactCount = TeamUser::countContacts($team_id);
	$average = @round($contactCount / $playerCount, 1);

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$target = $Team->getTeamTarget();

	$targetSuccess = $playerCount * $target;

	echo "
					<h2 class='teamPrimaryTxtColor'>My Team's Contact Collection Progress</h2>
					<div class='campaignProgress'>
						<div class='campaignStats'>
							<div class='campaignStatsCol1'>
								<strong>Total Registered Athletes</strong>
								<div class='teamPrimaryTxtColor statsNum'>$playerCount</div>
							</div>
							
							<div class='campaignStatsCol2'>
								<strong>Total Submitted Contacts</strong>
								<div class='teamPrimaryTxtColor statsNum'>$contactCount</div>
							</div>
							
							<div class='campaignStatsCol3'>
								<strong class='teamPrimaryTxtColor'>Average Contacts<br />Per Athlete</strong>
								<div class='teamPrimaryTxtColor statsNum'>$average</div>
							</div>
							<div class='clear'></div>
							
							<p><strong>Note:</strong><br />
							The ideal target # of contacts to be entered by each athlete is <strong class='teamPrimaryTxtColor'>$target</strong><br />
							The ideal target # of contacts for a sucessful campaign is <strong class='teamPrimaryTxtColor'>$targetSuccess</strong>
							</p>
						</div>
					<div class='clear'></div>
					</div>
	";
?>