<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$link = "index.php?nav=campaigns&action=contacts&edit";
		$link2 = "index.php?nav=campaigns&action=contacts";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$link = "index.php?action=contacts&edit";
		$link2 = "index.php?action=contacts";
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contactsList = TeamUser::getAllContactsByDate($team_id);

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Most Recent Contacts Added</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td align='left'><strong>Contact</strong></td>
							<td width='55px' align='center'><strong>Location</strong></td>
							<td align='right'><strong>Date</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($contactsList AS $c) {
							if ($count > 4) continue;
							$date = date("M j, Y", strtotime($c['register_date']));
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'><a href='$link=" . $c['ID'] . "'>" . substr($c['fname'],0,1) . ". " . $c['lname'] . "</a></td>
							<td align='center'>" . $c['state'] . "</td>
							<td align='right'>" . $date . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>

                </div>
	";
?>