<?php
	require_once($relPath . 'inc/common/class/campaign/Messaging.php');
	$message = Messaging::getMessage($get['mID']);
	$team = Messaging::getMessageTeamInfo($message['tID']);
	$date = date("M j, Y", strtotime($message['date_sent']));
	
	switch ($message['recip']) {
		case 'Admin':
		$link = "index.php?nav=campaigns&action=campaignActivity&subAction=messages";
		break;
		
		case 'Coach':
		$link = "index.php?action=campaignActivity&subAction=messages";
		break;
		
		case 'Designer':
		$link = "index.php?nav=viewCampaign&sNav=messages&tID=$team_id";
		break;
		
		case 'Printer':
		$link = "index.php?nav=viewCampaign&sNav=messages&tID=$team_id";
		break;
	}
	
	if ($get['s'] == 'y') {
		// Sent message; do nothing
	} else if ($message['markAsRead'] == 1) {
		// Already marked as read; do nothing
	} else {
		// Owned, unread message; Mark as read
		Messaging::markAsRead($message['ID']);
	}	

	echo "
	<div class='contentLeft'>
		<div class='contentLeftData'>
			<h2 class='teamPrimaryTxtColor'>Incoming Message</h2>
			<div class='clear'></div>
			<div class='contentLeftData' style='font-size:1.2em;font-weight:bold;margin-bottom:15px;margin-top:20px;'>
				Sent: <span style='font-weight:normal;'>$date</span><br><br>
				Team: <span style='font-weight:normal;'><a href='index.php?nav=campaigns&action=dashboard&team=".$message['tID']."$replyFlag'>$unreadFlag" . $message['tID'] . " - ".$team['name'].": ".$team['team']."</span></a><br><br>
			
				From: <span style='font-weight:normal;'>".$message['sender']."</span>
			</div>
			<table cellpadding='0' cellspacing='0' border='0'>
			<tr>
			<td style='font-weight:bold;'>Subject:&nbsp;&nbsp;</td>
			<td><div style='font-size:1.1em;margin-top:5px;margin-bottom:5px;'>
				".$message['subject']."
			</div></td>
			</tr>
			<tr>
			<td style='font-weight:bold;'>Message:&nbsp;&nbsp;</td>
			<td><div style='font-size:1.1em;margin-top:5px;margin-bottom:5px;'>
				".$message['message']."
			</div></td>
			</tr>
			</table>				
	";
			
	if (!$get['s'] && $get['nav'] != 'messages') {			
		echo "
				<br />
				<div class='contentLeftData' style='font-size:1.2em;font-weight:bold;margin-bottom:10px;'>
					Send a Response
				</div>
				<form method='post' name='frmMessageReply' action='$link'>
					<input class='textField' name='subject' size='67' value='Subject...'/>
					<br />
					<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>
					<p align='right'>
						<input type='hidden' name='tID' value='".$team_id."'>
						
						<input type='hidden' name='recip' value='".$message['sender']."'>
						<input type='hidden' name='recipID' value='".$message['senderID']."'>
						<input type='hidden' name='sender' value='".$message['recip']."'>
						<input type='hidden' name='senderID' value='".$message['recipID']."'>
	
						<input type='hidden' name='benchmark' value='".$message['benchmark']."'>
						<input type='hidden' name='benchmarkID' value='".$message['benchmarkID']."'>
						
						<input type='hidden' name='submitAction' value='messageReply'>
						<a href='javascript:document.frmMessageReply.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
					</p>
				</form>
		";
	}
	
	
	echo "
		</div>
		<!-- /contentLeftData -->
				<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	<div class='clear'></div>
	</div>
	<!-- /contentLeft -->
	";

?>