<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$link = "index.php?nav=campaigns&action=players&edit";
		$link2 = "index.php?nav=campaigns&action=players&view=donations";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$link = "index.php?action=players&edit";
		$link2 = "index.php?action=players&view=donations";
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$topPlayers = TeamUser::getTopAchievers($team_id);

	echo "
				<h2 class='teamPrimaryTxtColor'>My Team's Top Achievers</h2>
				
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td><strong>Team Athlete's Name</strong></td>
							<td align='center'><strong></strong></td>
							<td align='center'><strong>Total Donations</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						$contactCount = "";
						foreach($topPlayers AS $tp) {
							if ($count > 4) continue;
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'><a href='$link=" . $tp['pID'] . "'>" . $tp['lname'] . ", " . $tp['fname'] . "</a></td>
							<td align='center'>" . $contactCount . "</td>
							<td align='center'>$" . $tp['donationTotal'] . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>
	";
?>