<?php
	$action = ( isset($_SESSION["nav2"]) ) ? $_SESSION["nav2"] : "dashboard";

	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$link = "index.php?nav=campaigns&action=$action";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$link = "index.php?action=$action";
	}
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$Team = new TeamMain($team_id);
	$goal = $Team->getTeamGoal();
	$target = $Team->getTeamTarget();
	$colors = $Team->getTeamColors();
	
	$goalPerc = @($donationTotal / $goal);
	$goalBar = round($goalPerc * 460);
	
	$tic1 = $goal / 5;
	$tic2 = $tic1 * 2;
	$tic3 = $tic1 * 3;
	$tic4 = $tic1 * 4;
	
	echo "
					<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/donationUpdate.css' />
					<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
					<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery.form.js'></script>
					<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/admin/scripts/collectionUpdate.js'></script>
					
					<div class='updateLink'>
						<a href='#' id='collectionUpdateBtn'>Click Here to Change Ideal Target # per Athlete</a>
					</div>
					
					<div class='collectionUpdate' id='collectionUpdate'>
						<form method='post' name='frm' id='frm' action='$link'>
						<div class='formElement'>
							<div class='formElementCol2'>
								<input type='hidden' name='cID' value='xxx' />
								<input type='hidden' name='submitAction' value='adminUpdateTarget' />
								<a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update Ideal Target # per Athlete</a>
								<input name='target' id='target' type='text' size='10' class='selectFieldSWR' value='$target'/>
							</div>
						</div>
						</form>					
                 
				 	</div>
					
	";
?>