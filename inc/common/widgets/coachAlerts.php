<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
	} 
	else {
		$team_id = $_SESSION['current_folder'];
	}

	require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
	$statusList = Campaign::getStatusList();
	$campaignStatuses = Campaign::getCampaignStatuses($team_id);

	foreach($statusList AS $a => $b) {
		if ($campaignStatuses[2] && !$campaignStatuses[3])
			$printout[0] = "Team Info Submission Due";
		else if ($campaignStatuses[3] && $campaignStatuses[3]['pending'])
			$printout[0] = "Team Info Submission Rejected";
		else if (($campaignStatuses[5] && !$campaignStatuses[5]['pending']) && !$campaignStatuses[6])
			$printout[0] = "Brochure Copy Awaiting Approval";
		else if (($campaignStatuses[9] && !$campaignStatuses[9]['pending']) && !$campaignStatuses[10])
			$printout[0] = "Brochure Awaiting Approval";
		else if (($campaignStatuses[14] && !$campaignStatuses[14]['pending'] && !$campaignStatuses[15]))
			$printout[0] = "Printer Proof Awaiting Approval";
		
		if ($campaignStatuses[2] && !$campaignStatuses[8])
			$printout[1] = "No Athlete Contacts Added";
	}

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>ALERTS</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
	";
                        $classAlternate = "bg1";
						$count = 0;
						if ($printout) {
							foreach($printout AS $p) {
								if ($count > 4) continue;
								echo "
									<tr class='$classAlternate'>
										<td><span style='color:red;font-weight:bold;'>&nbsp;! </span>" . $p . "</td>
									</tr>
								";
								$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
								$count++;
							}
						}
	echo "
					</table>

                </div>
	";
?>