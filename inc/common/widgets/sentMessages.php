<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$sender = "Coach";
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$coach = TeamUser::getAdminData($team_id);
		$senderID = $coach["ID"];
		$link = "index.php?nav=campaigns&action=campaignActivity&subAction=messages&sub=sent";
	} 
	else if ( isset($_SESSION["designer_id"]) && ($_SESSION["designer_id"]) && ($_SESSION["designer_id"] != "") ) {
		$team_id = $get['tID'];
		$sender = "Designer";
		$senderID = $_SESSION["designer_id"];
		$link = "index.php?nav=viewCampaign&sNav=messages&tID=$team_id&sub=sent";
	} 
	else if ( isset($_SESSION["printer_id"]) && ($_SESSION["printer_id"]) && ($_SESSION["printer_id"] != "") ) {
		$team_id = $get['tID'];
		$sender = "Printer";
		$senderID = $_SESSION["printer_id"];
		$link = "index.php?nav=viewCampaign&sNav=messages&tID=$team_id&sub=sent";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$sender = "Coach";
		$senderID = $_SESSION["admin_id"];
		$link = "index.php?action=campaignActivity&subAction=messages&sub=sent";
	}

	require_once($relPath . 'inc/common/class/campaign/Messaging.php');
	
	$mData['tID'] = $team_id;
	$mData['sender'] = $sender;
	$mData['senderID'] = $senderID;
	$messages = Messaging::getSentMessages($mData);

	echo "
                <div class='suggestionsWrap'>
                    <p class=''><strong>Sent Messages</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($messages AS $m) {
							if ($count > 9) continue;
							$unreadFlag = ($m['markAsRead']) ? "<span>" : "<span style='font-weight:bold;'>";
							
							echo "
								<tr class='$classAlternate'>
									<td>$unreadFlag<a href='" . $link . "&sub=view&mID=" . $m['ID'] . "'>" . $m['benchmark'] . " - " . $m['subject'] . "</a></span></td>
								</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td>&nbsp;</td>
						</tr>      
						<tr>
							<td align='right'><a href='$link'>View All</a></td>
						</tr>              
					</table>

                </div>
	";
?>