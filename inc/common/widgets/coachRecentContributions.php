<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$link = "index.php?nav=campaigns&action=donations&view";
		$link2 = "index.php?nav=campaigns&action=donations";
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$link = "index.php?action=donations&view";
		$link2 = "index.php?action=donations";
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$donationsList = TeamUser::getAllDonations($team_id);

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Most Recent Contributions</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td width='60px'><strong>Amount</strong></td>
							<td align='center'><strong>Donor</strong></td>
							<td align='right'><strong>Date</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($donationsList AS $d) {
							if ($count > 4) continue;
							
							if ( isset($d['payment_date']) && $d['payment_date'] != "") {
								$date = date("M j, Y", strtotime($d['payment_date']));
							}
							else {
								$date = date("M j, Y", strtotime($d['donation_date']));
							}
							
							echo "
						<tr class='$classAlternate'>
							<td><a href='$link=" . $d['ID'] . "'>$" . $d['donationValue'] . "</a></td>
							<td align='left'>" . substr($d['fname'],0,1) . ". " . $d['lname'] . "</td>
							<td align='right'>" . $date . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='$link2'>View All</a></td>
						</tr>              
					</table>
                </div>
	";
?>