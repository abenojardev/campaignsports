<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
	} 
	else {
		$team_id = $_SESSION['current_folder'];
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$playerCount = TeamUser::countPlayers($team_id);
	$contactCount = TeamUser::countContacts($team_id);
	$average = @round($contactCount / $playerCount, 1);

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$target = $Team->getTeamTarget();

	$targetSuccess = $playerCount * $target;

	echo "
	    <div class='clear'></div>
		<div class='row' style='color: white;'>
		    <div class='col' style='text-align: end;'><strong>Athletes Registered: $playerCount</strong> </div>
		    <div class='col'><strong>Total Contacts: $contactCount</strong> </div>		
		</div>

	";
?>