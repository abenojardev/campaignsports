<?php
	$team_id = $_SESSION['campaign_team'];
	$link = "index.php?nav=campaigns&action=contacts&edit";
	$link2 = "index.php?nav=campaigns&action=contacts";

	require_once($_SESSION['relative_path'] . 'inc/admin/class/Admin.php');
	$note = Admin::getCampaignNotes($team_id);

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Campaign Notes</strong></p>
                    <form name='noteFrm' id='noteFrm' method='post' action='index.php?nav=campaigns&action=dashboard'>
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td align='right'>
								<textarea class='' name='note' cols='25' rows='10'>";
								if ($note) echo $note['note'];
								echo "</textarea>
							</td>
						</tr>
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='javascript:document.noteFrm.submit();'>Update</a></td>
						</tr>              
					</table>
					<input type='hidden' name='submitAction' value='updateNote'>
					<input type='hidden' name='tID' value='".$team_id."'>
					</form>
                </div>
	";
?>