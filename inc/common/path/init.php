<?php
/****************************************************************************************
init.php

Application: Any, Common File
Trail Associates, February 2011
Artisan Digital Studios, April 2017
****************************************************************************************/

    // Commented out for CSports home directories
	//session_start(); /* if not already done */
	
    /* replace value below with appropriate header to root distance of this include file */
    $header_to_root_distance = 3;
    $header_dir = dirname(__FILE__);
    $root_distance = substr_count($header_dir, DIRECTORY_SEPARATOR) - $header_to_root_distance;
    $includer_distance = substr_count(dirname($_SERVER['SCRIPT_FILENAME']), "/");
    $relative_path = str_repeat('../', $includer_distance - $root_distance);
    $_SESSION['relative_path'] = $relative_path; 
	
	//echo $relative_path;	
	
?>