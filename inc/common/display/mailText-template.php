<?php

$startCampaignForCoachSubject = '
	Campaign Started
';
$startCampaignForCoach = '
	Campaign Started
';


$startCampaignForPlayersSubject = '
	Campaign Started, email to players
';
$startCampaignForPlayers = '
	Campaign Started, email to players
';


$teamInfoApprovedSubject = '
	Your team info has been approved
';
$teamInfoApproved = '
	Your team info has been approved
';


$toCoachBrochureCopySubject = '
	*NEW* Brochure Copy is ready for review
';
$toCoachBrochureCopy = '
	*NEW* Brochure Copy is ready for review
';


$toDesignerSubject = '
	You have a new project in the system
';
$toDesigner = '
	You have a new project in the system
';


$contactCollectionStartedSubject = '
	Contact Collections have started to be entered
';
$contactCollectionStarted = '
	Contact Collections have started to be entered
';


$brochureReadySubject = '
	Brochure is ready for review
';
$brochureReady = '
	Brochure is ready for review
';


$contactCollectionCompletedSubject = '
	Contact Collections are completed
';
$contactCollectionCompleted = '
	Contact Collections are completed
';


$toPrinterSubject = '
	You have a new project in the system
';
$toPrinter = '
	You have a new project in the system
';


$proofReadySubject = '
	Proof is ready for review
';
$proofReady = '
	Proof is ready for review
';


$brochuresMailedSubject = '
	Brochures have been mailed
';
$brochuresMailed = '
	Brochures have been mailed
';


$donationsStartedSubject = '
	Donataions have started to be received
';
$donationsStarted = '
	Donataions have started to be received
';


function completeCampaign($data) {
	$campaignStartDate = $data['campaignStartDate'];
	$playersRegistered = $data['playersRegistered'];
	$domesticContacts = $data['domesticContacts'];
	$internationalContacts = $data['internationalContacts'];
	$totalContacts = $domesticContacts + $internationalContacts;
	$brochuresMailed = $data['brochuresMailed'];
	$costPerBrochure = $data['costPerBrochure'];
	$totalCampaignCost = $data['totalCampaignCost'];
	$checkDonations = $data['checkDonations'];
	$creditCardDonations = $data['creditCardDonations'];
	$paypalDonations = $data['paypalDonations'];
	$totalDonations = $checkDonations + $creditCardDonations + $paypalDonations;
	
	$dataReturn['completeCampaignSubject'] = '
		*NEW* Campaign has been completed
	';
	$dataReturn['completeCampaign'] = '
		*NEW* Campaign has been completed
	';
	
	return $dataReturn;
}






?>