<?php

function startCampaignForCoach($data) {
	$coachName 	= $data['coachName'];
	$teamName 	= $data['teamName'];
	$teamID 	= $data['teamID'];
	$coachPass	= $data['coachPass'];
	
	$dataReturn['startCampaignForCoachSubject'] = '
		Thank you for choosing Campaign Sports!
	';
	$dataReturn['startCampaignForCoach'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table   style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="1" width="30"></td>
              <td><table   style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="20" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                <span>Here is information for accessing your personal Coach\'s Dashboard, where you will be able to manage and monitor your team\'s  fundraising campaign from start to finish.</span> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550">
                
                <strong>Link to your online Dashboard:</strong><br>
                <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                <strong>Your login credentials:</strong><br>
                Username: '.$teamID.'<br>
                Password: '.$coachPass.' <br><hr style="color: #ccc;">
                <h3 style="color: #f88e10; font-size: 20px">For Your Athletes</h3>
                You will receive another email shortly just for your athletes that you will be able to forward to your team with information on how they can log into their online Athlete\'s  Portal.<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                <strong>Best suggestions for your athletes 15+ contacts:</strong><br>
                <span style="text-decoration:underline">Former Coaches,Teachers,Neighbors,Family (Dad & Mom, Grandparents, Uncles and Aunts, Cousins), Family friends in small & big businesses, Family Doctors(s) & Dentist, Attorney etc.</span> <strong>These suggestions will be reiterated on the contacts submission page, quality over quantity.</strong><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                <strong>Follow us on Instagram:</strong> <a href="https://www.instagram.com/campaignsportsllc/">https://www.instagram.com/campaignsportsllc/</a><br />
                <strong>\'Like\' us on Facebook:</strong> <a href="www.facebook.com/campaignsports">www.facebook.com/campaignsports</a><br />
                <strong>Follow us on Twitter:</strong> <a href="www.twitter.com/campaignsports1">www.twitter.com/campaignsports1</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                <br>
                <hr style="color: #ccc;" >
                <h3 style="color: #f88e10;" >Beginning Your Campaign</h3>
               Please log into your Dashboard under the "Campaign Activity" tab and submit your teams information. Please submit the following information about your team:
                <ul>
                  <li>A team photo, group huddle/celebratory photo, and/or 3-4 action shots. All original file sizes in jpeg format.<br>
                    <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                  <li>A high resolution team logo<br>
                    <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                  <li>The best mailing address to send checks, cash, and money orders<br>
                    <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                  <li>Your preferred campaign start date<br>
                    <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                  <li>The number of participating athletes<br>
                    <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                  <li>Your team\'s recent highlights both on the field, in the classroom, and in the community<br>
                    <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                  <li>Your team\'s fundraising goals<br>
                  <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></li>
                </ul>
                <table   style="width: 550px;"  border="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td width="27"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="25"></td>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  width="490"><strong>EXAMPLES</strong><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></td>
                      <td width="25"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="25"></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  ><strong>Team highlights:</strong> Conference Champs, made 2nd round of NCCA\'s  with 3 All-Conference members. Our team GPA last year was a 3.41 and 90% of our student-athletes earned Academic All Conference. Last year we were involved w/ the ALS walk and we hosted a Breast Cancer awareness game that raised money for free mobile mammograms for the community.<br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="8" width="500"></td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  ><strong>Fundraising Goal(s):</strong> $10,000 or litte under 500$ per athlete for preseason trip. Any additional funds will go toward new uniforms and new team winner jackets.</td>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
                <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="25" width="550"><br>
                <strong>Thank you for choosing Campaign Sports!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="1" width="30"></td>
              <td><table   style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"   align="center"><strong style="color: #f88e10;" >Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"   href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a>&nbsp;&nbsp; <a href="https://www.google.com/search?q=Campaign+Sports,+LLC+Gramercy+Square+Drive,+Delray+Beach,+Florida,+United+States&amp;ludocid=15762059684926175680&amp;#lrd=0x88d920209fef7945:0xdabe15c5c087b9c0,1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_google.gif" width="25" height="26" border="0" alt="Campaign Sports on Google"></a>&nbsp;&nbsp; <a href="https://www.instagram.com/campaignsportsllc/"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_instagram.gif" width="25" height="26" border="0" alt="Campaign Sports on Instagram"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"   alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function startCampaignForPlayers($data) {
	$coachName 	= $data['coachName'];
	$teamName 	= $data['teamName'];
	$teamID 	= $data['teamID'];
	$teamPass	= $data['teamPass'];
	
	$dataReturn['startCampaignForPlayersSubject'] = '
		Your Information About '.$teamName.'\'s New Fundraising Campaign
	';
	$dataReturn['startCampaignForPlayers'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Good Morning Everyone,<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
               
                Please click the link below to access your Athlete Dashboard, where you will register and be able to submit an email list of people you know that would br interested in financially supporting the team.<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>Link to your online Dashboard:</strong><br>
                <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>Your temporary login credentials:</strong><br>
                Username: '.$teamID.'<br>
                Password: '.$teamPass.' <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                After you login for the first time, you will be asked to submit your email contact list. To register click the &quot;register&quot; link below the athlete dropdown box and fill out your profile. Once registered, the dropdown box will show your name then prompt you to submit your password upon your return to the site.<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                To ensure the best overall fundraising results, please make to provide <b>at least 15 quality E-Mail contacts</b> to your account of individuals you know personally.<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
               <b>Here are some contact list suggestions that generally yield the best results:</b>
Former Coaches, Teachers, Neighbors, Family (Dad & Mom, Grandparents, Uncles and Aunts, Cousins), Family friends in small & big businesses, Family Doctor(s) & Dentist, Attorney, Pastor, etc.<br />
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
              <strong>Follow us on Instagram:</strong> <a href="https://www.instagram.com/campaignsportsllc/">https://www.instagram.com/campaignsportsllc/</a><br />
                <strong>\'Like\' us on Facebook:</strong> <a href="www.facebook.com/campaignsports">www.facebook.com/campaignsports</a><br />
                <strong>Follow us on Twitter:</strong> <a href="www.twitter.com/campaignsports1">www.twitter.com/campaignsports1</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                         <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a>&nbsp;&nbsp; <a href="https://www.google.com/search?q=Campaign+Sports,+LLC+Gramercy+Square+Drive,+Delray+Beach,+Florida,+United+States&amp;ludocid=15762059684926175680&amp;#lrd=0x88d920209fef7945:0xdabe15c5c087b9c0,1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_google.gif" width="25" height="26" border="0" alt="Campaign Sports on Google"></a>&nbsp;&nbsp; <a href="https://www.instagram.com/campaignsportsllc/"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_instagram.gif" width="25" height="26" border="0" alt="Campaign Sports on Instagram"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function teamInfoApproved($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['teamInfoApprovedSubject'] = '
		Your Team\'s Information Has Been Received And Your Campaign Moves To The Next Step
	';
	$dataReturn['teamInfoApproved'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that we\'ve received the team information you submitted!</strong> Your Campaign Administrator has reviewed all of the team information that was submitted, and it looks like we have everything necessary to move on to the next steps of your team\'s fundraising campaign. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Within the next several days you should receive another email notifying you that the Campaign Sports writers have completed your brochure content, and have submitted it for your review. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Log in to your Coaches Portal to monitor and manage your fundraising campaign: <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                One of the most important steps for a successful campaign is the team members contact collection phase. The overall success of any team fundraising campaign is dependent upon the number and the quality of contacts and their information entered by the team\'s members. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Your Dashboard keeps you completely up to date with the status and progress of this phase. You can monitor and see which athletes have added contacts, the number of contacts, etc. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                In order to make your team\'s campaign perform as well as possible, we ask all the team athletes to include an email address for each of their contacts as well. This will allow us to Pre-Mail and Post-Mail a message to potential donors. We have found the use of emailing conjunction with our printed brochure can make a BIG difference in your overall net results and we include our email blasts for no additional fee. <strong>This is REQUIRED for all International contacts.</strong> To be clear, these individuals email addresses will ONLY be used for promoting your campaign, nothing else. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;" _mce_style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function toCoachBrochureCopy($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['toCoachBrochureCopySubject'] = '
		Your Team\'s Custom Fundraising Brochure Copy Is Ready For Review
	';
	$dataReturn['toCoachBrochureCopy'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform your team\'s custom brochure content is ready for your review and approval!</strong> Campaign Sport\'s writers have submitted the preliminary copy for your team\'s custom fundraising campaign brochure and needs your feedback. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Log in to your Coaches Portal to monitor and manage your fundraising campaign: <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function toDesigner($data) {
	$designerName 	= $data['designerName'];
	$school 		= $data['school'];
	$team 			= $data['team'];
	$teamID			= $data['teamID'];
	
	$dataReturn['toDesignerSubject'] = '
		A New Campaign Is Available In Your Designer Portal
	';
	$dataReturn['toDesigner'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$designerName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that there is a new campaign added to your portal!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The following campaign has been added to your Designer Portal, and requires your attention. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>Campaign Information:</strong><br>
                '.$school.'<br>
                '.$team.'<br>
                '.$teamID.' <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;" href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="55" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function toDesignerBrochureApproved($data) {
	$designerName 	= $data['designerName'];
	$school 		= $data['school'];
	$team 			= $data['team'];
	$teamID			= $data['teamID'];
	
	$dataReturn['toDesignerBrochureApprovedSubject'] = '
		A Brochure Design Has Been Approved And Is Ready For Final Files!
	';
	$dataReturn['toDesignerBrochureApproved'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$designerName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that a brochure design has been approved and is ready for the submission of the final files!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The following campaign is in your Designer Portal, and requires your attention. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>Campaign Information:</strong><br>
                '.$school.'<br>
                '.$team.'<br>
                '.$teamID.' <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;" href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="55" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function contactCollectionStarted($data) {
	$coachName 	= $data['coachName'];
	
	$dataReturn['contactCollectionStartedSubject'] = '
		The Contact Phase Of Your Team\'s Campaign Has Officially Begun
	';
	$dataReturn['contactCollectionStarted'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that the contact collection phase of your team\'s campaign has officially begun!</strong> One of your members has registered, and has submitted the first contact to your fundraising campaign. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Just a reminder, one of the most important steps for a successful campaign is the team members contact collection phase. The overall success of any team fundraising campaign is dependent upon the number and the quality of contacts and their information entered by the team\'s members. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Your Dashboard keeps you completely up to date with the status and progress of this phase. You can monitor and see which athletes have added contacts, the number of contacts, etc. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                In order to make your team\'s campaign perform as well as possible, we ask all the team athletes to include an email address for each of their contacts as well. This will allow us to Pre-Mail and Post-Mail a message to potential donors. We have found the use of emailing conjunction with our printed brochure can make a BIG difference in your overall net results. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>IMPORTANT: Emails are REQUIRED for all International contacts.</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                To be clear, these individuals email addresses will ONLY be used for promoting your campaign, nothing else. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;" href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function brochureReady($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['brochureReadySubject'] = '
		A Custom Brochure Design Has Been Submitted For Your Review
	';
	$dataReturn['brochureReady'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that a brochure design has been submitted for your review and approval!</strong> Our Campaign Sports designers have submitted a custom brochure design for your team\'s campaign. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Log in to your Coaches Portal to monitor and manage your fundraising campaign: <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}
function teamPageReady($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['teamPageReadySubject'] = '
		Your Public Custom Team Page Is Live and Ready to Share!
	';
	$dataReturn['teamPageReady'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                This email is to inform you that your public custom team page is ready! You can use this team page to share across your social media networks, email to friends, family, and colleagues to promote and boost the success of your team\'s fundraising campaign.<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
		This page contains an integrated sharing feature to allow anyone on the page to share your team\'s page on any of their social networks, or even email it to a friend.
		<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
				This team page also includes a quick login feature to make it easier for donors to click the \'Support Our Team\' button to donate to your team\'s fundraising campaign.<br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
				Here is the link to your campaign\'s public page:<br>
				<a href="https://joinourcampaign.com/v2/team/'.$teamID.'/share/" style="color: #115a7f;" target="_blank">https://joinourcampaign.com/v2/team/'.$teamID.'/share/</a> 
                <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. <br></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}

function contactCollectionCompleted($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['contactCollectionCompletedSubject'] = '
		Your Campaign\'s Contact Collection Phase Is Officially Closed
	';
	$dataReturn['contactCollectionCompleted'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" ><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that the contact collection phase of your team\'s fundraising campaign is offically closed!</strong> All contact information has been collected and is being processed for your brochure mailing. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The next phase is the Donation Phase, where you can sit back, relax, and watch the money come rolling in for your fundraiser. You can monitor and track all donations directly on your Coaches Dashboard. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Log in to your Coaches Portal to monitor and manage your fundraising campaign: <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"   align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function toPrinter($data) {
	$designerName 	= $data['designerName'];
	$school 		= $data['school'];
	$team 			= $data['team'];
	$teamID			= $data['teamID'];
	
	$dataReturn['toPrinterSubject'] = '
		A New Campaign Has Been Added To Your Printer Portal
	';
	$dataReturn['toPrinter'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that there is a new campaign added to your portal!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The following campaign has been added to your Printer Portal, and requires your attention. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>Campaign Information:</strong><br>
                '.$school.'<br>
                '.$team.'<br>
                '.$teamID.' <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;" href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="55" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


// ------ REMOVED -- only campaign admin will receive the proofs from the printer
$proofReadySubject = '
	Proof is ready for review
';
$proofReady = '
	Proof is ready for review
';


function toPrinterProofApproved($data) {
	$designerName 	= $data['designerName'];
	$school 		= $data['school'];
	$team 			= $data['team'];
	$teamID			= $data['teamID'];
	
	$dataReturn['toPrinterProofApprovedSubject'] = '
		A Brochure Proof Has Been Approved!
	';
	$dataReturn['toPrinterProofApproved'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that a campaign brochure proof has been approved!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The following campaign is in your Printer Portal, and requires your attention. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>Campaign Information:</strong><br>
                '.$school.'<br>
                '.$team.'<br>
                '.$teamID.' <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;" href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="55" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function brochuresMailed($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['brochuresMailedSubject'] = '
		Your Team\'s Custom Fundraising Brochures Are Scheduled To Be Mailed';
	$dataReturn['brochuresMailed'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" _mce_style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that your team\'s campaign brochures have been scheduled to be mailed on '. $date .'!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Within several business days from the time your team\'s brochure is mailed, your teams contacts will begin to receive their brochures and donations should start rolling in. <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Log in to your Coaches Portal to monitor and manage your fundraising campaign: <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"   align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function donationsStarted($data) {
	$coachName 	= $data['coachName'];
	$teamID 	= $data['teamID'];
	
	$dataReturn['donationsStartedSubject'] = '
		Donations To Your Team\'s Fundraising Campaign Have Begun
	';
	$dataReturn['donationsStarted'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="5" width="550"><br>
                Greetings '.$coachName.', <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>This email is to inform you that the donation phase of your team\'s campaign has officially begun!</strong> One of your team member\'s contacts have submitted their first donation to your fundraising campaign. Congratulations! <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Log in to your Coaches Portal to monitor and manage your fundraising campaign: <a style="color: #115a7f;" href="http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/">http://www.joinourcampaign.com/v2/team/'.$teamID.'/admin/</a> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                <strong>We wish you and your team great success!</strong> <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                Sincerely, <br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="15" width="550"><br>
                The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a><br>
                <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="35" width="550"><br></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;" >&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}


function completeCampaign($data) {
	$coachName 	= $data['coachName'];
	$teamID = $data['teamID'];
	$campaignStartDate = $data['campaignStartDate'];
	$playersRegistered = $data['playersRegistered'];
	$domesticContacts = $data['domesticContacts'];
	$internationalContacts = $data['internationalContacts'];
	$totalContacts = $domesticContacts + $internationalContacts;
	$brochuresMailed = $data['brochuresMailed'];
	$costPerBrochure = $data['costPerBrochure'];
	$additionalCosts = $data['additionalCosts'];
	$totalCampaignCost = number_format($data['totalCampaignCost'], 2);
	$checkDonations = $data['checkDonations'];
	$creditCardDonations = $data['creditCardDonations'];
	$paypalDonations = $data['paypalDonations'];
	$totalDonations = $checkDonations + $creditCardDonations + $paypalDonations;
	$campaignMath = $totalDonations - $totalCampaignCost;
	//$campaignTotal = number_format($campaignMath, 2);
	$campaignTotal = $totalDonations - $totalCampaignCost;
	
	$dataReturn['completeCampaignSubject'] = '
		Your Fundraising Campaign has been completed
	';
	$dataReturn['completeCampaign'] = '
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;">
                <p>Greetings '.$coachName.',</p>
                <p><strong>This email is to inform your fundraising campaign is completed!</strong></p>
               <p>Here are the details of your campaign:</p>
                <p><strong>Start Date:</strong> '.$campaignStartDate.'</p>
                <p><strong style="color: #f88e10;">ATHLETES</strong><br />
                <strong>Total # of Athletes:</strong> '.$playersRegistered.'</p>
                <p><strong style="color: #f88e10;">CONTACTS</strong><br />
                <strong>Domestic Contacts:</strong> '.$domesticContacts.'<br>
                <strong>International Contacts:</strong> '.$internationalContacts.'<br>
                <strong>Total # of Contacts:</strong> '.$totalContacts.'</p>
                <p><strong style="color: #f88e10;">BROCHURES</strong><br />
                <strong># of Brochures Sent:</strong> '.$brochuresMailed.'<br>
                <strong>Cost Per Brochure:</strong> '.$costPerBrochure.'<br>
				<strong>Additional Costs:</strong> '.$additionalCosts.'<br>
                <strong>Total Campaign Cost:</strong> '.$totalCampaignCost.'</p>
                <p><strong style="color: #f88e10;">DONATIONS</strong><br />
                <strong>Check Donations:</strong> '.$checkDonations.'<br>
                <strong>Credit Card Donations:</strong> '.$creditCardDonations.'<br>
                <strong>PayPal Donations:</strong> '.$paypalDonations.'<br>
                <strong>Total Donations:</strong> '.$totalDonations.'</p>
                 <p><strong style="color: #f88e10;">FINAL RESULTS</strong><br />
                <strong>Campaign Total:</strong> '.$campaignTotal.'</p>
<p>Thank you again for using Campaign Sports for your sports fundraiser.</p>
<p>Sincerely,</p>
               <p>The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a></p></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;">&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
	';
	
	$dataReturnXXX['completeCampaign'] = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;">
                <p>Greetings '.$coachName.',</p>
                <p><strong>This email is to inform your fundraising campaign is completed!</strong></p>
               <p>Here are the details of your campaign:</p>
                <p><strong>Start Date:</strong> '.$campaignStartDate.'</p>
                <p><strong style="color: #f88e10;">ATHLETES</strong><br />
                <strong>Total # of Athletes:</strong> '.$playersRegistered.'</p>
                <p><strong style="color: #f88e10;">CONTACTS</strong><br />
                <strong>Domestic Contacts:</strong> '.$domesticContacts.'<br>
                <strong>International Contacts:</strong> '.$internationalContacts.'<br>
                <strong>Total # of Contacts:</strong> '.$totalContacts.'</p>
                <p><strong style="color: #f88e10;">BROCHURES</strong><br />
                <strong># of Brochures Sent:</strong> '.$brochuresMailed.'<br>
                <strong>Cost Per Brochure:</strong> '.$costPerBrochure.'<br>
				<strong>Additional Costs:</strong> '.$additionalCosts.'<br>
                <strong>Total Campaign Cost:</strong> '.$totalCampaignCost.'</p>
                <p><strong style="color: #f88e10;">DONATIONS</strong><br />
                <strong>Check Donations:</strong> '.$checkDonations.'<br>
                <strong>Credit Card Donations:</strong> '.$creditCardDonations.'<br>
                <strong>PayPal Donations:</strong> '.$paypalDonations.'<br>
                <strong>Total Donations:</strong> '.$totalDonations.'</p>
                 <p><strong style="color: #f88e10;">FINAL RESULTS</strong><br />
                <strong>Campaign Total:</strong> '.$campaignTotal.'</p>
<p>Thank you again for using Campaign Sports for your sports fundraiser.</p>
<p>Sincerely,</p>
               <p>The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a></p></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;"  border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;">&copy; '.date("Y").' Campaign Sports - Red Bank, NJ &amp; Delray Beach, FL - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
	';
	
	return $dataReturn;
}






?>