<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$adminCheck = true;
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$adminCheck = false;
	}
	
	// Coach Page
	if (!$adminCheck) {
		$navLink = "action=campaignActivity";
		
		$data = array(
			'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
			'css' => '
				<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
			',
			'js' => '
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
				<script src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.MetaData.js" type="text/javascript" language="javascript"></script>
				<script src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.MultiFile.js" type="text/javascript" language="javascript"></script>
				<script src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.blockUI.js" type="text/javascript" language="javascript"></script>
				
		');

		startToMainHeader($data);
		include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($team_id);
		$status = $TeamMain->getTeamStatus();
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($team_id);
			
		echo "
			<div class='pageContentWrap teamSecondaryBGColor'>
			  <div class='innerPageContentWrap teamPrimaryBGColor'>
				<div class='pageContent'>
		";
		showteamHeader();
		echo "
				  <div class='topContent'>
					<div class='welcomeBar'>
					  <div class='welcomeBarCol1'>
						<h1>Welcome <span class='teamPrimaryTxtColor'>" . $contact['fname']." ".$contact['lname'] ."</span></h1>
					  </div>
					  <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'>
		";
		echo date('F d, Y');
		echo "
					  </span><br />
						<div class='logout'><a href='index.php?action=logout'>[logout]</a></div>
					  </div>
					</div>
					<div class='clear'></div>
					<p>On this page, you are able to manage and update your team's campaign activity. </p>
				  </div>
		";
		include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php');
		
	// CS Admin Page
	} else {
		$navLink = "nav=campaigns&action=campaignActivity&subAction=reqInfo";
		
		echo "
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery.MetaData.js'></script>
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery.MultiFile.js'></script>
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery.blockUI.js'></script>
		";
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['campaign_team']);
		$status = $TeamMain->getTeamStatus();
		
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
		
        showteamHeader();
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php');
		
	}
	
	// Common Display Data
	require_once($relPath . 'inc/common/class/campaign/Coach.php');
	$data = Coach::getInitialInfo($team_id);
	$iu = ($data) ? "coachReqInfoUpdate" : "coachReqInfoInsert";

	include_once($_SESSION['relative_path'] . 'inc/common/display/coachPages/campaignActivity_nav.php');
	caNav($team_id);
	
	if (!$adminCheck) echo "<br />";
	
	//$buttonID = ($adminCheck) ?
		//"adminButton" :
		//"coachButton";
		
	$adminHiddenID = ($adminCheck) ?
		"<input type='hidden' name='coachID' value='".$contact['ID']."'>" :
		"<input type='hidden' name='coachID' value='".$_SESSION['admin_id']."'>";
		
	$buttonID = ($adminCheck) ?
		"coachButton" :
		"coachButton";
		
	echo "
			<script language='javascript'>
			<!--//
				//$(function() {
					//$('a#adminButton').click(function(event) {
						//event.preventDefault();
						//alert('Admins can not alter data here, only coaches.');
					//});
				//});
			//-->
			</script>
	
			  <div class='contentLeft'>
				<div class='contentLeftData'>
					<h2 class='teamPrimaryTxtColor'>Team Information Submission</h2>
					<form method='post' name='frm' id='frm' enctype='multipart/form-data' action='index.php?".$navLink."'>
						This is THE starting point for your fundraising campaign. Please fill out all of the information below about your team, to assist us in creating a campaign to best suit your team's needs.
						<br /><br />
						<strong>If you have any questions please call or email us immediately so we can help.</strong>
						<br /><br />
						<strong style='font-weight:bold; color:#ff0000;'>IMPORTANT: PLEASE NOTE THAT THE ACTUAL COPY IN YOUR BROCHURE CAN NOT EXCEED 36 LINES, INCLUDING BLANK LINES.</strong>
						
						<br /><br />
						<strong>Fundraising Goals</strong>
						<br /><br />
						Please explain your financial goal for this fundraiser.  Specifically, how much you need to raise and the purpose for these funds. 
						<br /><br />
							<textarea class='txtTextArea' name='goals' cols='59' rows='10'>";
							if ($data) echo $data['goals'];
							echo "</textarea>	
						<br /><br />
						<strong>Team Highlights & Additional Information</strong><br /><br />
						Please explain, in just a few sentences, any team's statistics, accomplishments, awards and any extracurricular activities, charities, volunteering, etc

						<br /><br />
							<textarea class='txtTextArea' name='stats' cols='59' rows='10'>";
							if ($data) echo $data['stats'];
							echo "</textarea>	
						<br /><br />
						<strong>Additional Information</strong><br /><br />
						Please provide the best mailing address to send checks, your preferred campaign start date, and the # of participating athletes. 
Also feel free to provide any additional information that would be helpful in creating a successful message.

						<br /><br />
							<textarea class='txtTextArea' name='addInfo' cols='59' rows='10'>";
							if ($data) echo $data['add_info'];
							echo "</textarea>	
						<br /><br />
						<strong>Team Slogan</strong><br /><br />
						Please provide the Team Slogan you would like to appear on the front of the Team's brochure.

						<br /><br />
							<textarea class='txtTextArea' name='slogan' cols='59' rows='2'>";
							if ($data) echo $data['slogan'];
							echo "</textarea>	
						<br /><br />
						<strong>Team Pictures</strong><br /><br />
						Please provide/upload pictures of your team for inclusion in your team's campaign brochure/mailer. Also please include your official picture to use in the brochure along with the team's pictures.
						<br /><br />
						 <strong style='font-weight:bold; color:#ff0000;'>IMPORTANT: PICTURES UPLOADED MUST BE IN HI-RES TO OBTAIN THE BEST RESULTS FOR YOUR BROCHURE. SINGLE ACTION SHOTS SHOULD BE NO LESS THAN 1MB IN SIZE - TEAM SHOTS SHOULD BE NO LESS THAN 3MB.</strong>
						<br /><br />
						Upload pictures: <input type='file' name='multiUpload[]' id='multiUpload' size='40' class='multi' />
						<input type='hidden' name='uploadPath' value='team/" . $team_id . "/teamPhotos'>
						";
						
						if ($iu == "coachReqInfoUpdate") {
							include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
							$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/teamPhotos";
													
							$images = scandir($dirname);
							$ignore = Array(".", "..");
							
							foreach($images as $image){
								$imageSize = filesize($_SESSION['relative_path'] . "team/" . $team_id . "/teamPhotos/".$image);
								$formatBytes = Coach::formatBytes($imageSize);
								if(!in_array($image, $ignore)) {
									echo "
									<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:right;'>
										<input type='checkbox' name='fileDelete[]' value='" . $image . "' />
										<br />
										Remove
										<br />
										Picture
									</div>
									<div class='genFloatL' style='margin-top:15px;'>
										<img src='" . $_SESSION['relative_path'] . "team/" . $team_id . "/teamPhotos/$image' height='150' />
									</div>
									<div class='genFloatL' style='margin-top:15px;margin-left:10px;'>
										File: $image
										<br>Size: ".$formatBytes."
										<br><a href='" . $_SESSION['relative_path'] . "team/" . $team_id . "/teamPhotos/$image' download='$image'>Download</a>
									</div>
									<div class='clear'></div>
									";
								}
							} 
						}
						
						echo "
						<input type='hidden' name='submitAction' value='".$iu."'>
						<!-- <input type='hidden' name='coachID' value='".$_SESSION['admin_id']."'> -->
						".$adminHiddenID."
						<input type='hidden' name='tID' value='".$team_id."'>
						<br /><br />
						<p align='center'>
							<a href='javascript:document.frm.submit();' id='$buttonID' class='teamButton teamPrimaryBGColor'>Submit Team Information</a>
						</p>
					</form>
				</div>
				<!-- /contentLeftData -->
				
				<div class='clear'></div>
			  </div>
			  <!-- /contentLeft -->
			  
			  <div class='contentRight'>
	";
	
	include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachAlerts.php');
	include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
	
	echo "
			<div class='clear'></div>
		  </div>
		  <!-- /contentRight -->
	";  
			  
	if (!$adminCheck) {
		echo "
			  <div class='clear'></div>
			  <div class='playerSSLSeal'><span id='siteseal'><a href='https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R' target='_blank'><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
			  <div class='clear'></div>
			</div>
			<!-- /pageContent -->
			
			<div class='clear'></div>
		  </div>
		  <!-- /innerPageContentWrap -->
		  
		  <div class='clear'></div>
		</div>
		<!-- /pageContentWrap -->
		";

		closePageWrapToEnd();
		
	}


?>