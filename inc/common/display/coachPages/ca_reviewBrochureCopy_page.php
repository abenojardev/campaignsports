<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$adminCheck = true;
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$adminCheck = false;
	}
	
	// Coach Page
	if (!$adminCheck) {
		$navLink = "action=campaignActivity";
		
		$data = array(
			'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
			'css' => '
				<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
			',
			'js' => '
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateIntl.js"></script>
		');

		startToMainHeader($data);
		include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$status = $TeamMain->getTeamStatus();
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['current_folder']);
			
		echo "
			<div class='pageContentWrap teamSecondaryBGColor'>
			  <div class='innerPageContentWrap teamPrimaryBGColor'>
				<div class='pageContent'>
		";
		showteamHeader();
		echo "
				  <div class='topContent'>
					<div class='welcomeBar'>
					  <div class='welcomeBarCol1'>
						<h1>Welcome <span class='teamPrimaryTxtColor'>" . $contact['fname']." ".$contact['lname'] ."</span></h1>
					  </div>
					  <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'>
		";
		echo date('F d, Y');
		echo "
					  </span><br />
						<div class='logout'><a href='index.php?action=logout'>[logout]</a></div>
					  </div>
					</div>
					<div class='clear'></div>
					<p>On this page, you are able to manage and update your team's campaign activity. </p>
				  </div>
		";
		include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php');
		
	// CS Admin Page
	} else {
		$navLink = "nav=campaigns&action=campaignActivity&subAction=brochureCopy";
		
		echo "
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/formValidateAdminAcct.js'></script>
		";
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['campaign_team']);
		$status = $TeamMain->getTeamStatus();
		
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
		
        showteamHeader();
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php');
		
	}
	
	// Common Display Data
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$design = Designer::getCurrentDesign($team_id);
	include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
	$dirname =  $_SESSION['siteRoot'] . "/team/" . $team_id . "/designer/";
	
	include_once($_SESSION['relative_path'] . 'inc/common/display/coachPages/campaignActivity_nav.php');
	caNav($team_id);
	
	if (!$adminCheck) echo "<br />";
	
	$adminHiddenID = ($adminCheck) ?
		"<input type='hidden' name='coachID' value='".$contact['ID']."'>" :
		"<input type='hidden' name='coachID' value='".$_SESSION['admin_id']."'>";
		
	//$buttonID = ($adminCheck) ?
		//"adminButton" :
		//"coachButton";
	//$buttonID2 = ($adminCheck) ?
		//"adminButton2" :
		//"coachButton2";
		
	$buttonID = ($adminCheck) ?
		"coachButton" :
		"coachButton";
	$buttonID2 = ($adminCheck) ?
		"coachButton2" :
		"coachButton2";
		
	require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
	$Campaign = new Campaign($team_id);
	$titleLabel = ($Campaign->benchmarkCheck(6)) ? "Review" : "Approve";

	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($team_id);

	echo "
			<script language='javascript'>
			<!--//
				$(function() {
					$('a#adminButton').click(function(event) {
						event.preventDefault();
						alert('Admins can not alter data here, only coaches.');
					});
					$('a#adminButton2').click(function(event) {
						event.preventDefault();
						alert('Admins can not alter data here, only coaches.');
					});
				});
			//-->
			</script>
	
			  <div class='contentLeft'>
				<div class='contentLeftData'>
					<h2 class='teamPrimaryTxtColor'>$titleLabel Brochure Text</h2>
					<div style='margin-bottom:10px;'>
				<!--		<p>The copy for your team brochure has been created and awaits for your review and approval. Please review the brochure copy, and if you approve of the brochure please sign with your initials in the place provided below. If for any reason you do not approve of the copy, you can send a messsage to your Campaign Adminsitrator to explain changes, corrections, comments, etc.</p>
					--></div>
					<div style='margin-bottom:20px;border:#CCC solid 1px;padding:5px;'>
						" .$campaign['final_copy']. "
					</div>
					<div class='contentLeftData'>
						<strong>Approval</strong>
						<br /><br />
						If you have reviewed your team's campaign brochure copy, and approve this as your final version for the campaign brochure, please confirm by entering your initials in the box below.
						<br /><br />
	";
	$approvalCheck = $Campaign->benchmarkCheck(6);
	
	if (!$approvalCheck) {
		echo "
		        <center>
		        <img src='".$_SESSION['relative_path'] . "images/brochure.jpg' style=''>
		        </center><br><br><br>
						<form method='post' name='frmApproveCopy' action='index.php?".$navLink."'>
							<div class='genFloatL' style='margin-bottom:10px;'>
								<input type='hidden' name='tID' value='".$team_id."'>
								
								<!-- <input type='hidden' name='coachID' value='".$_SESSION["admin_id"]."'> -->
								".$adminHiddenID."
								<input type='hidden' name='submitAction' value='coachApproveBrochureCopy'>
								<input class='textField validate centerText' name='initials' size='6' />
							</div>
							<div class='genFloatL' style='margin-top:5px;margin-left:20px;'>
								<a href='javascript:document.frmApproveCopy.submit();' id='$buttonID' class='teamButton teamPrimaryBGColor'>I Approve</a>
							</div>
							<div class='clear'></div>
						</form>
					</div>
					<form method='post' name='frmRejectBrochureCopy' action='index.php?".$navLink."'>
						<div class='contentLeftData'>
							<strong>Change, Correction and Comments</strong><br /><br />
							If for any reason you do not approve of the current version of your team's brochure, please use the box below to send a message to your Campaign Administrator explaining any changes, correctinos or comments.
							<br /><br />
							<input class='textField' name='subject' size='66' value='Subject...'/>
							<br />
							<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
							<br /><br />
							<p align='right'>
								<input type='hidden' name='tID' value='".$team_id."'>
								<!-- <input type='hidden' name='coachID' value='".$_SESSION["admin_id"]."'> -->
								".$adminHiddenID."
								<input type='hidden' name='submitAction' value='coachRejectBrochureCopy'>
								<a href='javascript:document.frmRejectBrochureCopy.submit();' id='$buttonID2' class='teamButton teamPrimaryBGColor'>Send Message</a>
								
							</p>
						</div>
					</form>
		";
	} else {
		echo "<span style='color:#03C119;'>You approved this brochure copy on: ";
		echo date("F j, Y", strtotime($approvalCheck));
		echo "</span><br /><br /></div>";
	}
					
	echo "
				</div>
				<!-- /contentLeftData -->
				
				<div class='clear'></div>
			  </div>
			  <!-- /contentLeft -->
			  
			  <div class='contentRight'>
	";
	
	include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachAlerts.php');
	include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
	
	echo "
			<div class='clear'></div>
		  </div>
		  <!-- /contentRight -->
	";  
			  
	if (!$adminCheck) {
		echo "
			  <div class='clear'></div>
			  <div class='playerSSLSeal'><span id='siteseal'><a href='https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R' target='_blank'><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
			  <div class='clear'></div>
			</div>
			<!-- /pageContent -->
			<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
			<div class='clear'></div>
		  </div>
		  <!-- /innerPageContentWrap -->
		  
		  <div class='clear'></div>
		</div>
		<!-- /pageContentWrap -->
		";

		closePageWrapToEnd();
		
	}


?>