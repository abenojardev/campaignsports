<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$adminCheck = true;
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$adminCheck = false;
	}
	
	// Coach Page
	if (!$adminCheck) {
		$navLink = "action=campaignActivity&subAction=messages";
		
		$data = array(
			'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
			'css' => '
				<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
			',
			'js' => '
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidate.js"></script>
		');

		startToMainHeader($data);
		include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$status = $TeamMain->getTeamStatus();
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['current_folder']);
			
		echo "
			<div class='pageContentWrap teamSecondaryBGColor'>
			  <div class='innerPageContentWrap teamPrimaryBGColor'>
				<div class='pageContent'>
		";
		showteamHeader();
		echo "
				  <div class='topContent'>
					<div class='welcomeBar'>
					  <div class='welcomeBarCol1'>
						<h1>Welcome <span class='teamPrimaryTxtColor'>" . $contact['fname']." ".$contact['lname'] ."</span></h1>
					  </div>
					  <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'>
		";
		echo date('F d, Y');
		echo "
					  </span><br />
						<div class='logout'><a href='index.php?action=logout'>[logout]</a></div>
					  </div>
					</div>
					<div class='clear'></div>
					<p>On this page, you are able to manage and update your team's campaign activity. </p>
				  </div>
		";
		include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php');
		
	// CS Admin Page
	} else {
		$navLink = "nav=campaigns&action=campaignActivity&subAction=messages";
		
		echo "
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/formValidateAdminAcct.js'></script>
		";
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['campaign_team']);
		$status = $TeamMain->getTeamStatus();
		
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
		
        showteamHeader();
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/adminNav.php');
		
	}

	// Common Display Data
	include_once($_SESSION['relative_path'] . 'inc/common/display/coachPages/campaignActivity_nav.php');
	caNav($team_id);
	
	if (!$adminCheck) echo "<br />";
	
	$sendRecCheck = ($get['sub']) ? $get['sub'] : "";
	
	require_once($relPath . 'inc/common/class/campaign/Messaging.php');
	$mData['tID'] = $team_id;
	
	if (!$sendRecCheck || $sendRecCheck == 'sent') {
		if ($sendRecCheck == 'sent') {
			$mData['sender'] = "Coach";
			$mData['senderID'] = $contact['ID'];
			$messages = Messaging::getSentMessages($mData);
			$headerPrintout = "Sent";
			$sub = "&s=y";
			$pageSub = "&sub=sent";
		} else {
			$mData['recip'] = "Coach";
			$mData['recipID'] = $contact['ID'];
			$messages = Messaging::getReceivedMessages($mData);
			$headerPrintout = "Incoming";
			$sub = "";
			$pageSub = "";
		} 
		
		$messagesCount = count($messages);
		
	$buttonID = ($adminCheck) ?
		"adminButton" :
		"coachButton";
		
		echo "
			<script language='javascript'>
			<!--//
				$(function() {
					$('a#adminButton').click(function(event) {
						event.preventDefault();
						alert('Admins can not alter data here, only coaches.');
					});
				});
			//-->
			</script>
			
		<div class='contentLeft'>
			<div class='contentLeftData'>
				<h2 class='teamPrimaryTxtColor-cancel'>Review $headerPrintout Messages</h2>
			
				<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
				<table width='100%' border='0' cellspacing='0' cellpadding='7'>
		";
		
		$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
		$tpages = ceil($messagesCount/15);
		$min = ($page - 1) * 15 + 1;
		$max = $page * 15;
		
		$classAlternate = "bg1";
		$count = 0;
		
		foreach($messages AS $m) {
			$count++;
			if ($count < $min || $count > $max) continue;
			$date = date("M j, Y g:i A", strtotime($m['date_sent']));
			$unreadFlag = ($m['markAsRead']) ? "<span>" : "<span style='font-weight:bold;'>";
			
			echo "
					<tr class='$classAlternate' align='left'>
						<td><a href='index.php?".$navLink."&sub=view&mID=".$m['ID']."$sub'>$unreadFlag" . $m['benchmark'] . " - " . $m['subject'] . "</span></a></td>
						<td align='right'>$date</td>
					</tr>
			";
			$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
		}
		
		echo "
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>      
				</table>
		";
		
		echo "<div class='pagination'><br/>";
		$reload = $_SERVER['PHP_SELF'] . "?$navLink$pageSub";
		include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
		echo paginate($reload, $page, $tpages, 5);
		echo "</div>";
		
		echo "
					</div>
					<!-- /contentLeftData -->
					
					<div class='clear'></div>
				  </div>
				  <!-- /contentLeft -->
				  
				  <div class='contentRight'>
		";
		
		if ($sendRecCheck) {
			include_once($_SESSION['relative_path'] . 'inc/common/widgets/incomingMessages.php');
		} else {
			include_once($_SESSION['relative_path'] . 'inc/common/widgets/sentMessages.php');
		}
		
		echo "
				<div class='clear'></div>
			  </div>
			  <!-- /contentRight -->
		";  
		
	} else if ($sendRecCheck == 'view') {
		include_once($_SESSION['relative_path'] . 'inc/common/widgets/viewMessage.php');
						  
		echo "
			<div class='contentRight'>
		";
		
		include_once($_SESSION['relative_path'] . 'inc/common/widgets/sentMessages.php');
		
		echo "
			<div class='clear'></div>
			</div>
			<!-- /contentRight -->
		";  
	}
	
	if (!$adminCheck) {
		echo "
			  <div class='clear'></div>
			  <div class='playerSSLSeal'><span id='siteseal'><a href='https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R' target='_blank'><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
			  <div class='clear'></div>
			</div>
			<!-- /pageContent -->
			
			<div class='clear'></div>
		  </div>
		  <!-- /innerPageContentWrap -->
		  
		  <div class='clear'></div>
		</div>
		<!-- /pageContentWrap -->
		";

		closePageWrapToEnd();
		
	}


?>