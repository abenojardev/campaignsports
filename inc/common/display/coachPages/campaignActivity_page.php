<?php
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
		$adminCheck = true;
	} 
	else {
		$team_id = $_SESSION['current_folder'];
		$adminCheck = false;
	}
	
	// Coach Page
	if (!$adminCheck) {
		$navLink = "action=campaignActivity&subAction=";
		
		$data = array(
			'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
			'css' => '
				<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
			',
			'js' => '
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
				<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidate.js"></script>
		');


		
		include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$status = $TeamMain->getTeamStatus();
			
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['current_folder']);
			

		
	// CS Admin Page
	} else {
		$navLink = "nav=campaigns&action=campaignActivity&subAction=";
		
		echo "
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/formValidateAdminAcct.js'></script>
		";
		include_once($_SESSION['relative_path'] . 'inc/admin/layout/campaigns/teamStyles.php');
	
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['campaign_team']);
		$status = $TeamMain->getTeamStatus();
		
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contact = TeamUser::getAdminData($_SESSION['campaign_team']);
		


		
	}
	
	// Common Display Data
	include_once($_SESSION['relative_path'] . 'inc/common/display/coachPages/campaignActivity_nav.php');
	caNav($team_id);
	
	if (!$adminCheck) echo "<br />";
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
	$statusList = Campaign::getStatusList();
	$campaignStatuses = Campaign::getCampaignStatuses($team_id);
	
	$mailDate = ($TeamMain->getTeamMailDate()) ? date("F j, Y", strtotime($TeamMain->getTeamMailDate())) : "";
	
	echo "

					<table width='100%' border='0' cellspacing='0' cellpadding='5' id='table'>
						<!--<tr class='head'>
							<td width='250'><strong>Benchmark</strong></td>
							<td><strong>Date</strong></td>
							<td width='45'><strong>Status</strong></td>
						</tr>-->
	";
	
	foreach($statusList AS $a => $b) {
		if ($campaignStatuses[$a] && !$campaignStatuses[$a]['pending'])
			$s = "<img src='" . $_SESSION['relative_path'] . "images/benchmark_completed.png' alt='' />";
		else if ($campaignStatuses[$a] && $campaignStatuses[$a]['pending'])
			$s = "<img src='" . $_SESSION['relative_path'] . "images/benchmark_pending.png' alt='' />";
		else
			$s = "<img src='" . $_SESSION['relative_path'] . "images/benchmark_no.png' alt='' />";
		
		$d = ($campaignStatuses[$a]) ? date("F j, Y", strtotime($campaignStatuses[$a]['status_date'])) : "N/A";

		if ($campaignStatuses[$a]['sID'] == 16)
			$d = $mailDate;
		
//		if ($campaignStatuses[$a]['sID'] == 3 && $campaignStatuses[$a]['pending'])
//			$bDisplay = "<a href='index.php?".$navLink."reqInfo' class='sNav'>" . $b['benchmark'] . "</a>";
//		else if (($campaignStatuses[$a]['sID'] == 7 && !$campaignStatuses[$a]['pending']) && $campaignStatuses[$a]['sID'] == 8)
//			$bDisplay = "<a href='index.php?".$navLink."brochure' class='sNav'>" . $b['benchmark'] . "</a>";
//		else if ($campaignStatuses[$a]['sID'] == 12 && !$campaignStatuses[$a]['pending'])
//			$bDisplay = "<a href='index.php?".$navLink."proof' class='sNav'>" . $b['benchmark'] . "</a>";
//		else 
		
		
		
		$bDisplay = $b['benchmark'];
		
		echo "
		
		
			<tr>
				<td>" . $bDisplay . " </td>	
				<!-- <td>" . $d . "</td>	-->
				<td align='center'>" . $s . "</td>	
			</tr>
		";
	}
	
						
	echo "
					</table>

	";

			  
	if (!$adminCheck) {
		echo "


		";


		
	}


?> 

<style type="text/css">
	#table tr:nth-child(odd){ 
		background: white;
	} 
	#table tr:nth-child(even){
		background: #dae5f4;
	}
	#table tr:first-child{ 
        border-bottom:5px solid #b8d1f3 !important;
        border-top:5px solid #b8d1f3 !important;
	}
	#table tr:last-child{ 
        border-bottom:5px solid #b8d1f3 !important; 
	}
</style>
 