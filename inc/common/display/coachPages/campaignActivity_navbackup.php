<?php

	function caNav($tID) {
		if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
			 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
			$team_id = $_SESSION['campaign_team'];
			$adminCheck = true;
			$navLink = "nav=campaigns&action=campaignActivity&subAction=";
		} 
		else {
			$team_id = $_SESSION['current_folder'];
			$adminCheck = false;
			$navLink = "action=campaignActivity&subAction=";
		}
		
		require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
		//$statusList = Campaign::getStatusList();
		$campaignStatuses = Campaign::getCampaignStatuses($team_id);
			
		$teamInfoLabel = ($campaignStatuses[4]) ? "Review/Update Team Info" : "Submit Initial Required Team Info";
		$teamInfoNav = ($campaignStatuses[2]) ? "<a href='index.php?".$navLink."reqInfo' class='sNav'>$teamInfoLabel</a>&nbsp;|&nbsp;" : "";
		
		
		$brochureCopyNav = ($campaignStatuses[6]) ? "<a href='index.php?".$navLink."reqInfo' class='sNav'>$copyLabel</a>&nbsp;|&nbsp;" : "";
		
		$brochureCopyLabel = ($campaignStatuses[6]) ? "Review Brochure Text" : "Approve Brochure Text";
		$brochureCopyNav = ($campaignStatuses[5] && !$campaignStatuses[5]['pending']) ? "<a href='index.php?".$navLink."brochureCopy' class='sNav'>$brochureCopyLabel</a>&nbsp;|&nbsp;" : "";
		
		
		$brochureLabel = ($campaignStatuses[10]) ? "Review Brochure Design" : "Approve Brochure Design";
		$brochureNav = ($campaignStatuses[9] && !$campaignStatuses[9]['pending']) ? "<a href='index.php?".$navLink."brochure' class='sNav'>$brochureLabel</a>&nbsp;|&nbsp;" : "";
		
		$proofLabel = ($campaignStatuses[15]) ? "Review Printer Proof" : "Approve Printer Proof";
		$proofNav = ($campaignStatuses[14] && !$campaignStatuses[14]['pending']) ? "<a href='index.php?".$navLink."printer' class='sNav'>$proofLabel</a>&nbsp;|&nbsp;" : "";
		
		echo "
				<div class='navSubNav' style='background-image: url(" . $_SESSION['relative_path'] . "images/coach-activity-arrow.png);background-repeat: no-repeat;background-position: left center; padding-left:60px; height:20px; padding-top:5px; margin-top:0px;'>
					$teamInfoNav
					$brochureCopyNav
					$brochureNav
					<!-- $proofNav -->
					<a href='index.php?".$navLink."messages' class='sNav'>Review Messages</a>
				<div class='clear'></div>
				</div>
		";
	}

?>