<?php

	// Required on EVERY page.  Alter path to make sure points to init file
	require_once('../path/init.php');
	$relPath = $_SESSION['relative_path'];
	
	$get = $_GET;

	require_once($_SESSION['relative_path'] . '/inc/team/class/Player.php');
	$contacts = Player::getAllContacts2($get['player_id']);

?>

<script language="javascript">
<!--//
$(document).ready(function() { 
						   
	$("#contactQuick").change(function(){
		selectValue2 = $(this).val();					   
									   
		if (selectValue2 == 0) {
			$('#dynamic_dropdown2').css("display", "block");
		}
		else {
			$('#dynamic_dropdown2').css("display", "none");
		}
	});

});
//-->
</script>

<select id='contactQuick' name='contactQuick' class='selectFieldSWR' size='1'>
    <option value='initial'>Select Donor</option>
    <option value='0'>- Not On List -</option>
    <?php
        foreach($contacts as $contact)
        {
            echo "<option value=" . $contact['ID'] . ":" . $contact['fname'] . ":" . $contact['lname'];
            echo ">" . $contact['lname'] . ", " . $contact['fname'] . "</option>";
        }
    ?>
</select>
