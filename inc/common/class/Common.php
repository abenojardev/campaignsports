<?php
/****************************************************************************************
Common.php
Defines the Common class.
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Common {

	public function __construct() {
		try {

		} catch ( Exception $e ) {
			throw new Exception( 'Error instantiating Common: ' . $e->getMessage() );
		}
	} // __construct()


	public function getStates() {
		try {
			$query = array('select' => "*", 
						   'tbl' => "gen_states_list", 
						   'where' => "",
						   'sort' => "ID ASC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getStates(): ' . $e->getMessage() );
		}
	} // getStates()


	public function getCountries() {
		try {
			$query = array('select' => "*", 
						   'tbl' => "gen_country_list", 
						   'where' => "",
						   'sort' => "name");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCountries(): ' . $e->getMessage() );
		}
	} // getCountries()


	public function getRelationships() {
		$rel = array(
			array('name' => 'Mom', 'value' => 'Mom'),
			array('name' => 'Dad', 'value' => 'Dad'),
			array('name' => 'Grandma', 'value' => 'Grandma'),
			array('name' => 'Grandpa', 'value' => 'Grandpa'),
			array('name' => 'Aunt', 'value' => 'Aunt'),
			array('name' => 'Uncle', 'value' => 'Uncle')
		);
		
		return $rel;
			
	} // getRelationships()

	// added Ms to getPrefixes 9-19-17
	public function getPrefixes() {
		$rel = array(
			array('name' => 'Mr', 'value' => 'Mr'),
			array('name' => 'Mrs', 'value' => 'Mrs'),
			array('name' => 'Miss', 'value' => 'Miss'),
			array('name' => 'Ms', 'value' => 'Ms'),
			array('name' => 'Dr', 'value' => 'Dr')
		);
		
		return $rel;
			
	} // getPrefixes()


	public function taDonorTesting($post) {

		//$mailData['to'] 		= "mark@whitestardata.com";
		$mailData['to'] 		= "mkents@artisandigitalstudios.com";
		$mailData['subject']	= "Donation Check: Stage ". $post['taStage'] . "; " . $post['paymentMethod'] . "!";
		$mailData['messageHTML']= "POST data:<br>";
		
		foreach($post AS $f => $v) {
			$mailData['messageHTML'] .= $f . ": " . $v . "<br>";
		}
		
		$mail = Mail::send_email($mailData);
		
	} // getRelationships()

}

?>