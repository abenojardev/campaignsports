<?php
/****************************************************************************************
Campaign.php
Defines the Campaign class.

Application: Campaign Sports
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Campaign {
	
	protected $xxx;
	private $teamID;

	public function __construct( $ID = NULL ) {
		if($ID == NULL) {
			
		} else {
			try {
				$this->teamID = $ID;
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Campaign: ' . $e->getMessage() );
			}
		}
	} // __construct()
	
	
	public function insertNewCampaign($post) {
		try {
			$i = 0;
			$fields = $values = "";
			
			$contactArray = array('name', 'team', 'password'); 
			
			foreach($contactArray as $contactData) {
				if ($i == 0) {
					$fields .= $contactData;
					$values .= "'".$post[$contactData]."'";
				} else {
					$fields .= "," . $contactData;
					$values .= ",'" . $post[$contactData]."'";
				}
				$i = 1;

//			echo '<pre>'; print_r($contactArray); echo '</pre>';
//			var_dump($contactArray);
			}
			$fields .= ",active";
			$values .= ",1";
			
			$fields .= ",status";
			$values .= ",1";
						
			$fields .= ",adminPassword";
			$values .= ",'".$post['coachPassword']."'";
			
			$config = new config();
			$bc = $config->baseCost();
			$fields .= ",v2_base_cost_per_brochure";
			$values .= ",".$bc;

			$query = array('tbl' => "teams", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; insertNewCampaign(): ' . $e->getMessage() );
		}
	} // insertNewCampaign()
	
	public function createCampaignFiles($ID) {
		include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
		$path1 =  $_SESSION['siteRoot'] . "/team/" . $ID;
		mkdir($path1);
		
		
		$dest1a = $path1 . "/adminIndex.php";
		$dest1b = $path1 . "/index.php";
		$dest1c = $path1 . "/logo.jpg";
		
		$path1a =  $_SESSION['siteRoot'] . "/team/1001/adminIndex.php";
		$path1b =  $_SESSION['siteRoot'] . "/team/1001/index.php";
		$path1c =  $_SESSION['siteRoot'] . "/team/1001/logo.jpg";
		
		copy($path1a, $dest1a);
		copy($path1b, $dest1b);
		copy($path1c, $dest1c);
		
		//team page files
		$path3 =  $_SESSION['siteRoot'] . "/team/" . $ID . "/share";
		mkdir($path3);
		
		$dest1d = $path3 . "/index.php";
		$dest1e = $path3 . "/sponsor_button.png";
		
		$path1d =  $_SESSION['siteRoot'] . "/team/1001/share/index.php";
		$path1e =  $_SESSION['siteRoot'] . "/team/1001/share/sponsor_button.png";
		
		copy($path1d, $dest1d);
		copy($path1e, $dest1e);


		$path2 =  $_SESSION['siteRoot'] . "/team/" . $ID . "/admin";
		mkdir($path2);
		
		$dest2a = $path2 . "/index.php";
		$path2a =  $_SESSION['siteRoot'] . "/team/1001/admin/index.php";
		copy($path2a, $dest2a);

	} // createCampaignFiles()

	public function insertNewCoach($post) {
		$db = new DB();
		try {
			$i = 0;
			$fields = $values = "";
			
			$contactArray = array('fname', 'lname', 'title', 'address', 'address2', 'city', 'phoneDay', 'phoneEve', 'phoneCell'); 

			foreach($contactArray as $contactData) {
				if ($i == 0) {
					$fields .= $contactData;
					$values .= "'".$post[$contactData]."'";
				} else {
					$fields .= "," . $contactData;
					$values .= ",'" . $post[$contactData]."'";
				}
				$i = 1;
			}
			$fields .= ",tID";
			$values .= "," . $_SESSION['campaign_team'];
			
			if (isset($post['intl']) && $post['intl']) {
				$fields .= ",state";
				$values .= ",'".$post['stateI']."'";
				$fields .= ",zip";
				$values .= ",'".$post['zipI']."'";
				$fields .= ",email";
				$values .= ",'".$post['emailI']."'";
				$fields .= ",country";
				$values .= ",'".$post['country']."'";
				$fields .= ",intl";
				$values .= ",'1'";
			} else {
				$fields .= ",state";
				$values .= ",'".$post['state']."'";
				$fields .= ",zip";
				$values .= ",'".$post['zip']."'";
				$fields .= ",email";
				$values .= ",'".$post['email']."'";
			}
			
			$query = array('tbl' => "team_contacts", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			//return $DB->insert_id;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; insertNewCoach(): ' . $e->getMessage() );
		}
	} // insertNewCoach()

	public static function getSingleCampaignStatus($data) {
		try {
			$tID = $data["tID"];
			$sID = $data["sID"];
			
			$query = array('select' => "*", 
						   'tbl' => "status",
						   'where' => "tID = $tID AND sID = $sID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSingleCampaignStatus(): ' . $e->getMessage() );
		}
	} // getSingleCampaignStatus()

	public function setCampaignStatus($data) {
		try {
			$tID = $data["ID"];
			$sID = $data["sID"];
			$pending = ($data["pending"]) ? $data["pending"] : 0;
			
			$query = array('select' => "ID", 
						   'tbl' => "status_teams",
						   'where' => "tID = $tID AND sID = $sID");
			$DB = new DB();
			$statusCheck = $DB->select_single($query);
			
			if ($statusCheck) {
				$set = "pending=" . $pending . ",status_date=NOW()";
				
				$query = array('tbl' => "status_teams", 
							   'set' => $set, 
							   'where' => "ID=" . $statusCheck['ID']);
				$DB = new DB();
				$result = $DB->update_single($query);
			} else {
				$fields = $values = "";
				
				$fields .= "tID";
				$values .= $tID;
				
				$fields .= ",sID";
				$values .= "," . $sID;
				
				$fields .= ",pending";
				$values .= "," . $pending;
				
				$query = array('tbl' => "status_teams", 
							   'fields' => $fields, 
							   'values' => $values);
				$DB = new DB();
				$result = $DB->insert_single($query);
			}
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setCampaignStatus(): ' . $e->getMessage() );
		}
	} // setCampaignStatus()
	
	public function removeCampaignActiveStatus($tID) {
		try {
			$query = "
				DELETE FROM status_teams WHERE tID = $tID AND sID='11';
			";
			
			$DB = new DB();
			$result = $DB->custom($query);
			
			} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; removeCampaignStatus(): ' . $e->getMessage() );
		}
	} // setCampaignStatus()
			
			
	public function getStatusList() {
		try {
			$query = array('select' => "*", 
						   'tbl' => "status",
						   'sort' => "weightID ASC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getStatusList(): ' . $e->getMessage() );
		}
	} // getStatusList()

	public static function getCampaignStatuses($tID) {
		try {
			$query = array('select' => "s.ID, st.sID, st.status_date, st.pending, s.benchmark", 
						   'tbl' => "status_teams st INNER JOIN status s ON st.sID = s.ID",
						   'where' => "st.tID = $tID",
						   'sort' => "st.sID DESC, st.status_date DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCampaignStatuses(): ' . $e->getMessage() );
		}
	} // getCampaignStatuses()
	
	
	public function assignDesigner($post) {
		try {
			$set = "designerID=" . $post['designer'];
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $post['tID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; assignDesigner(): ' . $e->getMessage() );
		}
	} // assignDesigner()

	public function assignPrinter($post) {
		try {
			$set = "printerID=" . $post['printer'];
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $post['tID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; assignPrinter(): ' . $e->getMessage() );
		}
	} // assignPrinter()

	public function getDesignerInfo($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "designers",
						   'where' => "ID = $ID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getDesignerInfo(): ' . $e->getMessage() );
		}
	} // getDesignerInfo()

	public function getPrinterInfo($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "printers",
						   'where' => "ID = $ID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getPrinterInfo(): ' . $e->getMessage() );
		}
	} // getPrinterInfo()



	public function benchmarkCheck($bID) {
		try {
			//require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			$campaignStatuses = $this->getCampaignStatuses($_SESSION["current_folder"]);
			
			if ($campaignStatuses[$bID]) {
				return $campaignStatuses[$bID]['status_date'];
			} else {
				return false;
			}
			
		} catch ( Exception $e ) {
			//throw new Exception( 'Error with Method; benchmarkCheck(): ' . $e->getMessage() );
			return $e->getMessage();
		}
	} // benchmarkCheck()




	public function setOldCampaignStatus($status) {
		try {
			$set = "status=" . $status;
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $this->teamID);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setOldCampaignStatus(): ' . $e->getMessage() );
		}
	} // setOldCampaignStatus()



	
	public function createReUpCampaignFiles($data) {
		function cpy($source, $dest){
			if(is_dir($source)) {
				$dir_handle=opendir($source);
				while($file=readdir($dir_handle)){
					if($file!="." && $file!=".."){
						if(is_dir($source."/".$file)){
							mkdir($dest."/".$file);
							cpy($source."/".$file, $dest."/".$file);
						} else {
							copy($source."/".$file, $dest."/".$file);
						}
					}
				}
				closedir($dir_handle);
			} else {
				copy($source, $dest);
			}
		}
		
		include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
		$dest =  $_SESSION['siteRoot'] . "/team/" . $data['newID'];
		$source =  $_SESSION['siteRoot'] . "/team/" . $data['id'];
		mkdir($dest);
		cpy($source, $dest);
		
		//team page files
		//$path3 =  $dest . "/share";
		//mkdir($path3);
		
		//$dest1d = $path3 . "/index.php";
		//$dest1e = $path3 . "/sponsor_button.png";
		
		//$path1d =  $_SESSION['siteRoot'] . "/team/1001/share/index.php";
		//$path1e =  $_SESSION['siteRoot'] . "/team/1001/share/sponsor_button.png";
		
		//copy($path1d, $dest1d);
		//copy($path1e, $dest1e);

	} // createReUpCampaignFiles()


	public function getBrochureInfo($f) {
		try {
			$query = array('select' => "brochure_count, brochure_cost, brochure_charges", 
						   'tbl' => "teams",
						   'where' => "ID=" . $f);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getBrochureInfo(): ' . $e->getMessage() );
		}
	} // getBrochureInfo()


}