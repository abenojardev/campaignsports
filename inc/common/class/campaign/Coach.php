<?php
/****************************************************************************************
Coach.php
Defines the Coach class.

Application: Campaign Sports
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Coach {
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Coach: ' . $e->getMessage() );
			}
		}
	} // __construct()
	
	public function formatBytes($size, $precision = 2)
	{
		$base = log($size) / log(1024);
		$suffixes = array('', 'k', 'M', 'G', 'T');   
	
		return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
	}

	public function insertInitialInfo($post) {
		try {
			$fields = "tID";
			$values = $post['tID'];
			$fields .= ",goals";
			$values .= ",'" . $post['goals'] . "'";
			$fields .= ",stats";
			$values .= ",'" . $post['stats'] . "'";
			$fields .= ",add_info";
			$values .= ",'" . $post['addInfo'] . "'";
			$fields .= ",slogan";
			$values .= ",'" . $post['slogan'] . "'";

			$query = array('tbl' => "team_initial_info", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			//return $DB->insert_id;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; insertInitialInfo(): ' . $e->getMessage() );
		}
	} // insertInitialInfo()

	public function updateInitialInfo($post) {
		try {
			$set = "goals='" . $post['goals'] ."',";
			$set .= "stats='" . $post['stats'] ."',";
			$set .= "add_info='" . $post['addInfo'] ."',";
			$set .= "slogan='" . $post['slogan'] ."'";
			
			$query = array('tbl' => "team_initial_info", 
						   'set' => $set, 
						   'where' => "tID=" . $post['tID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateInitialInfo(): ' . $e->getMessage() );
		}
	} // updateInitialInfo()

	public function getInitialInfo($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "team_initial_info", 
						   'where' => "tID=$tID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;

		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getInitialInfo(): ' . $e->getMessage() );
		}
	} // getInitialInfo()

	public function approveBrochureCopy($post) {
		try {
			$fields = "tID";
			$values = $post['tID'];
			$fields .= ",benchmark";
			$values .= ",'Brochure Copy'";
			$fields .= ",bID";
			$values .= ",'" . $post['benchmarkID'] . "'";
			$fields .= ",initialedBy";
			$values .= ",'Coach'";
			$fields .= ",ibID";
			$values .= ",'" . $_SESSION['admin_id'] . "'";
			$fields .= ",initials";
			$values .= ",'" . $post['initials'] . "'";

			$query = array('tbl' => "approvals", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; approveBrochureCopy(): ' . $e->getMessage() );
		}
	} // approveBrochureCopy()

	public function approveBrochure($post) {
		try {
			$fields = "tID";
			$values = $post['tID'];
			$fields .= ",benchmark";
			$values .= ",'Brochure Design'";
			$fields .= ",bID";
			$values .= ",'" . $post['benchmarkID'] . "'";
			$fields .= ",initialedBy";
			$values .= ",'Coach'";
			$fields .= ",ibID";
			$values .= ",'" . $_SESSION['admin_id'] . "'";
			$fields .= ",initials";
			$values .= ",'" . $post['initials'] . "'";

			$query = array('tbl' => "approvals", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; approveBrochure(): ' . $e->getMessage() );
		}
	} // approveBrochure()

	public function approveProof($post) {
		try {
			$fields = "tID";
			$values = $post['tID'];
			$fields .= ",benchmark";
			$values .= ",'Printer Proof'";
			$fields .= ",bID";
			$values .= ",'" . $post['benchmarkID'] . "'";
			$fields .= ",initialedBy";
			$values .= ",'Coach'";
			$fields .= ",ibID";
			$values .= ",'" . $_SESSION['admin_id'] . "'";
			$fields .= ",initials";
			$values .= ",'" . $post['initials'] . "'";

			$query = array('tbl' => "approvals", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; approveProof(): ' . $e->getMessage() );
		}
	} // approveProof()




}