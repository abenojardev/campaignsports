<?php
/****************************************************************************************
Admin.php
Defines the Admin class.

Application: Campaign Sports
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Admin {
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Admin: ' . $e->getMessage() );
			}
		}
	} // __construct()
	
	public function updateInitialInfoFinalCopy($post) {
		try {
			$set = "final_copy='" . $post['finalCopy'] ."'";
			
			$query = array('tbl' => "team_initial_info", 
						   'set' => $set, 
						   'where' => "tID=" . $post['tID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateInitialInfoFinalCopy(): ' . $e->getMessage() );
		}
	} // updateInitialInfoFinalCopy()

	public function updateCosts($post) {
		try {
			$set = "international_mailers=" . $post['intlMailers'];
			
			$query = array('tbl' => "printer_costs", 
						   'set' => $set, 
						   'where' => "tID=" . $post['tID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
			$set2 = "v2_base_cost_per_brochure=" . $post['baseCosts'] . ", v2_base_cost_edit_reason='" . $post['reason'] . "'";
			
			$query2 = array('tbl' => "teams", 
						   'set' => $set2, 
						   'where' => "ID=" . $post['tID']);
			$DB2 = new DB();
			$result2 = $DB2->update_single($query2);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateCosts(): ' . $e->getMessage() );
		}
	} // updateCosts()



}