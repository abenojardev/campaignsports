<?php
/****************************************************************************************
Messaging.php
Defines the Messaging class.

Application: Campaign Sports
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Messaging {
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Messages: ' . $e->getMessage() );
			}
		}
	} // __construct()
	
	public function addMessage($post) {
		try {
			$fields = "tID";
			$values = $post['tID'];
			$fields .= ",recip";
			$values .= ",'" . $post['recip'] . "'";
			$fields .= ",recipID";
			$values .= "," . $post['recipID'];
			$fields .= ",sender";
			$values .= ",'" . $post['sender'] . "'";
			$fields .= ",senderID";
			$values .= "," . $post['senderID'];
			$fields .= ",benchmark";
			$values .= ",'" . $post['benchmark'] . "'";
			$fields .= ",benchmarkID";
			$values .= "," . $post['benchmarkID'];
			$fields .= ",subject";
			$values .= ",'" . $post['subject'] . "'";
			$fields .= ",message";
			$values .= ",'" . $post['message'] . "'";

			$query = array('tbl' => "messages", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			return $DB->insert_id;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; addMessage(): ' . $e->getMessage() );
		}
	} // addMessage()

	public function getMessage($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "messages",
						   'where' => "ID=".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getMessage(): ' . $e->getMessage() );
		}
	} // getMessage()
	
	public function getMessageTeamInfo($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "teams",
						   'where' => "ID=".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getMessage(): ' . $e->getMessage() );
		}
	} // getMessage()


	public function getReceivedMessages($mData) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "messages",
						   'where' => "tID=".$mData['tID']." AND recip='".$mData['recip']."' AND recipID=".$mData['recipID'],
						   'sort' => "date_sent DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getReceivedMessages(): ' . $e->getMessage() );
		}
	} // getReceivedMessages()
	
	public function getReceivedDesignerMessages($mData) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "messages",
						   'where' => "recip='".$mData['recip']."' AND recipID=".$mData['recipID'],
						   'sort' => "date_sent DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getReceivedMessages(): ' . $e->getMessage() );
		}
	} // getReceivedMessages()
	
	public function getSentMessages($mData) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "messages",
						   'where' => "tID=".$mData['tID']." AND sender='".$mData['sender']."' AND senderID=".$mData['senderID'],
						   'sort' => "date_sent DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSentMessages(): ' . $e->getMessage() );
		}
	} // getSentMessages()

	public function getAllReceivedMessages($mData) {
		try {
			$markAsRead = ($mData['unreadOnly']) ? " AND markAsRead=0" : "";
			 
			$query = array('select' => "*", 
						   'tbl' => "messages",
						   'where' => "recip='".$mData['recip']."'$markAsRead AND recipID=".$mData['recipID']." AND date_sent BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()",
						   'sort' => "date_sent DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllReceivedMessages(): ' . $e->getMessage() );
		}
	} // getAllReceivedMessages()

	public function getAllSentMessages($mData) {
		try {
			$markAsRead = ($mData['markAsRead']) ? " AND markAsRead=0" : "";
			
			$query = array('select' => "*", 
						   'tbl' => "messages",
						   'where' => "sender='".$mData['sender']."'$markAsRead AND senderID=".$mData['senderID'],
						   'sort' => "date_sent DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllSentMessages(): ' . $e->getMessage() );
		}
	} // getAllSentMessages()


	public function getAllMessages($tID) {
		try {
			$query = array('select' => "*",
						   'tbl' => "messages",
						   'where' => "tID=".$tID,
						   'sort' => "date_sent DESC");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllMessages(): ' . $e->getMessage() );
		}
	} // getAllMessages()



	public function sendGenericMessageToAdmin($data) {
		try {
			$fields = "tID";
			$values = $data['tID'];
			$fields .= ",recip";
			$values .= ",'Admin'";
			$fields .= ",recipID";
			$values .= "," . 3;
			$fields .= ",sender";
			$values .= ",'" . $data['sender'] . "'";
			$fields .= ",senderID";
			$values .= "," . $data['senderID'];
			$fields .= ",benchmark";
			$values .= ",'" . $data['benchmark'] . "'";
			$fields .= ",benchmarkID";
			$values .= "," . $data['benchmarkID'];
			$fields .= ",subject";
			$values .= ",'" . $data['benchmark'] . " awaiting action.'";
			$fields .= ",message";
			$values .= ",'" . $data['message'] . "'";

			$query = array('tbl' => "messages", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			return $DB->insert_id;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; sendGenericMessageToAdmin(): ' . $e->getMessage() );
		}
	} // sendGenericMessageToAdmin()

	public function markAsRead($ID) {
		try {
			$set = "markAsRead=1";
			
			$query = array('tbl' => "messages", 
						   'set' => $set, 
						   'where' => "ID=" . $ID);
			$DB = new DB();
			$result = $DB->update_single($query);
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; markAsRead(): ' . $e->getMessage() );
		}
	} // markAsRead()
























	public function yyy($post) {
		try {
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; xxx(): ' . $e->getMessage() );
		}
	} // xxx()

	public function zzz($post) {
		try {
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; xxx(): ' . $e->getMessage() );
		}
	} // xxx()








}
?>