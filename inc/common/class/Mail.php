<?php
/****************************************************************************************
Mail.php
Defines the Mail class.

Trail Associates, February 2011
****************************************************************************************/

class Mail {
	
	protected $mailHeaders;
	protected $mailTo;
	protected $mailFrom;
	protected $mailSubject;
	protected $mailMessage;
	
	public function __construct() {
		try {
			$this->db_connect = new mysqli($this->db_server, $this->db_user, $this->db_password, $this->db_name);
			if (!$this->db_connect) throw new DBException($this->db_connect->error);
		} catch ( Exception $e ) {
			throw new Exception( 'Error instantiating DB: ' . $e->getMessage() );
		}
	}
	// __construct()
	
	
	function send_email( $mailData ) {
		
		$to 			= $mailData['to'];
		$from 			= $mailData['from'];
		$subject		= $mailData['subject'];
		$message		= (isset($mailData['message'])) ? $mailData['message'] : "";
		$messageHTML	= (isset($mailData['messageHTML'])) ? $mailData['messageHTML'] : "";
		
		// To send HTML mail, the Content-type header must be set
		$headers  = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n";
		
		// Additional headers
		$headers .= "From: $from" . "\r\n" .
			"Reply-To: $from" . "\r\n" .
			"X-Mailer: PHP/" . phpversion();
			
		// message
		if ($messageHTML) {
			$message_body = $messageHTML;
		} else {
			$message_body = "
				<html>
				<head>
				  <title>$subject</title>
				</head>
				<body>
					$message
				</body>
				</html> 
			";
		}
		
		// Send Mail
		//mail($to, $subject, $message_body, $headers);
		
		if ( mail($to, $subject, $message_body, $headers) )
			return true;
		else
			return false;
		
	} // send_email()
	
};

?>