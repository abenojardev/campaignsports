<?php
/****************************************************************************************
Upload.php
Defines the Common class.

Trail Associates, February 2011
Artisan Digital Studios, February 2017
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Upload {

	public function __construct() {
		try {

		} catch ( Exception $e ) {
			throw new Exception( 'Error instantiating Upload: ' . $e->getMessage() );
		}
	} // __construct()

	public function multiUpload($post) {
		try {
			if ( isset($_FILES['multiUpload']) && $_FILES['multiUpload']['error'][0] != 4 ) {
				include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
				$folderPath =  $_SESSION['siteRoot'] . "/" . $post['uploadPath'];
				mkdir($folderPath);
				
				foreach ($_FILES["multiUpload"]["error"] as $key => $error) {
					if ($error == UPLOAD_ERR_OK) {
						list($file, $extension) = explode('.', $_FILES["multiUpload"]["name"][$key]);
						$file_name = $file . ".$extension";
						$tmp_name = $_FILES["multiUpload"]["tmp_name"][$key];
						move_uploaded_file($tmp_name, "$folderPath/$file_name");
					}
					else {
						// Error checking, how to handle
						//return $error;
					}
				}
			}
			if ( isset($post['fileDelete']) ) {
				include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
				$folderPath =  $_SESSION['siteRoot'] . "/" . $post['uploadPath'];
				
				foreach ($post['fileDelete'] as $arrayLoop => $imageLink) {
					if ( is_file($folderPath . "/" . $imageLink) ) unlink($folderPath . "/" . $imageLink);
				}
			}
			return $file_name;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; multiUpload(): ' . $e->getMessage() );
		}
	} // multiUpload()


}
?>