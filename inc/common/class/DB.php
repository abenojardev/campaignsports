<?php
/****************************************************************************************
DB.php
Defines the DB class.

Trail Associates, February 2011
Artisan Digital Studios, February 2017
*****************************************************************************************
v.1.0 04-02-2011 mhs
- Started Version Tracking 
- Added date_default_timezone_set   ** May move to cfg file **

v.1.1 05-31-2011 mhs
- Added Method: select_custom - Use, self explainatory 

v.1.11 06-10-2011 mhs
- Altered Method: select_multi - Added ORDER BY functionality 

v.1.12 07-30-2011 mhs
- Altered Method: select_single - Added Where/Sort pass-thru functionality
- Moved date_default_timezone_set to DBvars.php
- Moved DB parameters ($db_server, $db_user, $db_password, $db_name) to DBvars.php
  Reason: Changes made to DB.php don't need to change from dev to live environments.

v.1.13 10-18-2011 mhs
- Added Method: delete_records - Use, self explainatory 

****************************************************************************************/

include_once($_SESSION['relative_path'] . 'inc/common/classBase/DBvars.php');

class DB extends DBvars {
		
	public $db_connect;
	
	public function __construct() {
		try {
			$this->db_connect = new mysqli($this->db_server, $this->db_user, $this->db_password, $this->db_name);
			if (!$this->db_connect) throw new DBException($this->db_connect->error);
		} catch ( Exception $e ) {
			throw new Exception( 'Error instantiating DB: ' . $e->getMessage() );
		}
	}
	// __construct()
	
	public function insert_table($query) {
		$query_string = "CREATE TABLE " . $query['tbl'] . " (" . $query['fields'] . ")";
		//echo $query_string;
		$result = $this->db_connect->query($query_string);
		if (!$result) throw new DBException($this->db_connect->error);
		else {
			/* free result set */
			$this->db_connect->close();
			return TRUE;
		}
	}
	// insert_table()
	
	public function insert_single($query) {
		$query_string = "INSERT INTO " . $query['tbl'] . " (" . $query['fields'] . ") VALUES (" . $query['values'] . ")";
		//echo $query_string;
		$result = $this->db_connect->query($query_string);
		if (!$result) throw new DBException($this->db_connect->error);
		
		$return_id = $this->db_connect->insert_id;
		
		/* free result set */
		$this->db_connect->close();
		
		return $return_id;
	}
	// insert_single()
	
	public function select_single($query) {
		$sort = ( isset($query['sort']) && $query['sort'] != "" ) ? "ORDER BY " . $query['sort'] : "";
		$where = ( isset($query['where']) && $query['where'] != "" ) ? "WHERE " . $query['where'] : "";

		$query_string = "SELECT " . $query['select'] . " FROM " . $query['tbl'] . " $where $sort";
		//echo $query_string;
		$result = $this->db_connect->query($query_string);
		if ($result->num_rows == 0)
			return 0;
		
		$result_set = $result->fetch_assoc();
		$result_array = array();
		foreach($result_set as $field => $data) {
			$result_array[$field] = $data;
			//echo "Field: $field, Data: $data<br>";
		}

		/* free result set */
		$result->close();
		
		return $result_array;
	}
	// select_single()
	
	public function select_multi($query) {
		$sort = ( isset($query['sort']) && $query['sort'] != "" ) ? "ORDER BY " . $query['sort'] : "";
		$where = ( isset($query['where']) && $query['where'] != "" ) ? "WHERE " . $query['where'] : "";
		
		$query_string = "SELECT " . $query['select'] . " FROM " . $query['tbl'] . " $where $sort";
		//echo $query_string;
		
		$result = $this->db_connect->query($query_string);
		if (count($result) == 0)
			return 0;

		$result_set = array();
		while ( $row = $result->fetch_array(MYSQLI_ASSOC) ) {
			$result_set[$row['ID']] = array();
			foreach($row as $field => $data) {
				$result_set[$row['ID']][$field] = $data;
				//echo "Field: $field, Data: $data<br>";
			}
			
		} // end result loop
			
		/* free result set */
		$result->close();
		
		return $result_set;
	}
	// select_multi()
	
	public function select_custom($query) {
		$query_string = $query;
		
		$result = $this->db_connect->query($query_string);
		if (count($result) == 0)
			return 0;

		$result_set = array();
		while ( $row = $result->fetch_array(MYSQLI_ASSOC) ) {
			$result_set[$row['ID']] = array();
			foreach($row as $field => $data) {
				$result_set[$row['ID']][$field] = $data;
				//echo "Field: $field, Data: $data<br>";
			}
			
		} // end result loop
			
		/* free result set */
		$this->db_connect->close();
		
		return $result_set;
	}
	// select_custom()
	

	public function update_single($query) {
		$query_string = "UPDATE " . $query['tbl'] . " SET " . $query['set'] . " WHERE " . $query['where'];
		//echo $query_string;
		$result = $this->db_connect->query($query_string);
		if (!$result) throw new DBException($this->db_connect->error);
		
		/* free result set */
		$this->db_connect->close();
	}
	// update_single()
	

	public function delete_records($query) {
		$query_string = "DELETE FROM " . $query['tbl'] . " WHERE " . $query['where'];
		//echo $query_string;
		$result = $this->db_connect->query($query_string);
		if (!$result) throw new DBException($this->db_connect->error);
		
		/* free result set */
		$this->db_connect->close();
		
		return $result;
	}
	// delete_records()


	public function custom($query) {
		$query_string = $query;
		//echo $query_string;
		
		$result = $this->db_connect->query($query_string);
		if (count($result) == 0)
			return 0;
	
		/* free result set */
		$this->db_connect->close();
		
		return $result;
	}
	// custom()
	



	// Takes the given string and trims excess whitespace from the ends, strips out HTML tags, and escapes DB special characters like ';'
	//
	function clean_string($input) {
		$input = trim($input);
		$input = strip_tags($input);
		
		$input = $this->db_connect->escape_string($input);
	
		return $input;
	} // clean_string()

};



// A specialized Exception that accepts $db->error, sets a generic message, and logs a detailed error.
//
class DBException extends Exception {
	protected $dbError;	// DB error returned from mysqli

	// Redefine the exception with one mandatory parameter: the output of $db->error.
	// @todo We should almost certainly email admin (not necessarily cell phone) on DB errors.
	//
    public function __construct($db_error) {
		$this->dbError = $db_error;
		if ( strpos($this->file, '\\')) {
			$script = substr($this->file, strrpos($this->file, '\\') + 1);
		} elseif (strpos($this->file, '/')) {
			$script = substr($this->file, strrpos($this->file, '/') + 1);
		} else {
			$script = $this->file;
		}
		$backtrace = $this->getTrace();
		$function = $backtrace[0]['function'];
		try {
			//logEvent('error', "$script::$function(): line $this->line: DB Error: $db_error IP: ".$_SERVER['REMOTE_ADDR']);
		} catch (Exception $e) { /* Nothing else to do */ }
		
        parent::__construct('DB Error - see log for details', 0); // make sure everything is assigned properly
    }
	// __construct()
	
	public function getDBError() { return $this->dbError; }
};



?>