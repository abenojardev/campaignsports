<?php

	if ($_SERVER['HTTP_HOST'] != 'localhost' && $_SERVER['HTTP_HOST'] != '192.168.0.125') {
		if ($_SERVER['SERVER_PORT'] != 443) {
			$url = "https://". $_SERVER['SERVER_NAME'] . ":443".$_SERVER['REQUEST_URI'];
			header("Location: $url");
		}
	}
	
	// Required on EVERY page.  Alter path to make sure points to init file
	require_once('common/path/init.php');
	$relPath = $_SESSION['relative_path'];
	
	// Load include files
	require_once($relPath . 'inc/designer/layout/layout.php');
	require_once($relPath . 'inc/common/config.php');
	
	$post = $_POST;
	$get = $_GET;

	// Login / Logout
	if ( isset($get['action']) && $get['action'] == 'login' ) {
		require_once($relPath . 'inc/designer/class/Designer.php');
		
		$loginCheck = Designer::login($post);
		if ($loginCheck) {
			$error = $loginCheck;
			include($relPath . 'inc/designer/display/login_page.php');
		}
		else {
			$_SESSION["nav"] = "camapigns";
			include($relPath . 'inc/designer/display/dashboardMain_page.php');
		}
		
	}
	else if ( isset($get['action']) && $get['action'] == 'logout' ) {
		unset($_SESSION["designer_id"]);
		include($relPath . 'inc/designer/display/login_page.php');
	}
	
	
	//Designer Logged in, continue with admin options
	else if ( isset($_SESSION["designer_id"]) && ($_SESSION["designer_id"]) && ($_SESSION["designer_id"] != "") ) {
		// Check for GET/POST actions
		if ( isset($post['submitAction']) ) {
			include($relPath . 'inc/designer/submitActions.php');
		}
		
		if ( isset($get["nav"]) ) {
			switch($get["nav"]) {
				case 'campaigns':
				$_SESSION["nav"] = "campaigns";
				include($relPath . 'inc/designer/display/dashboardMain_page.php');
				break;
				
				case 'viewCampaign':
				$_SESSION["nav"] = "viewCampaign";
				include($relPath . 'inc/designer/display/campaignView_page.php');
				break;
		
				case 'messages':
				$_SESSION["nav"] = "messages";
				include($relPath . 'inc/designer/display/messages_page.php');
				break;
		
				default:
				$_SESSION["nav"] = "campaigns";
				include($relPath . 'inc/designer/display/dashboardMain_page.php');
				break;
			}
		}
		else {
			$_SESSION["nav"] = "campaigns";
			include($relPath . 'inc/designer/display/dashboardMain_page.php');
		}
		
	}
	
	else {
		include($relPath . 'inc/designer/display/login_page.php');
	}
	
?>