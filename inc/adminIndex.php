<?php

	if ($_SERVER['HTTP_HOST'] != 'localhost' && $_SERVER['HTTP_HOST'] != '192.168.0.125') {
		if ($_SERVER['SERVER_PORT'] != 443) {
			$url = "https://". $_SERVER['SERVER_NAME'] . ":443".$_SERVER['REQUEST_URI'];
			header("Location: $url");
		}
	}
	
	// Required on EVERY page.  Alter path to make sure points to init file
	require_once('common/path/init.php');
	$relPath = $_SESSION['relative_path'];
	
	// Load include files
	require_once($relPath . 'inc/admin/layout/layout.php');
	require_once($relPath . 'inc/common/config.php');
	
	$post = $_POST;
	$get = $_GET;

	// Login / Logout
	if ( isset($get['action']) && $get['action'] == 'login' ) {
		require_once($relPath . 'inc/admin/class/Admin.php');
		
		$loginCheck = Admin::login($post);
		if ($loginCheck) {
			$error = $loginCheck;
			include($relPath . 'inc/admin/display/login_page.php');
		}
		else {
			$_SESSION["nav"] = "dashboard";
			include($relPath . 'inc/admin/display/dashboardMain_page.php');
		}
		
		
	}
	else if ( isset($get['action']) && $get['action'] == 'logout' ) {
		require_once($relPath . 'inc/admin/class/Admin.php');
		Admin::logout();
		include($relPath . 'inc/admin/display/login_page.php');
	}
	
	
	// Admin Logged in, continue with admin options
	else if ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) {
		// Check for GET/POST actions
		if( isset($get["id"]) )
			$_SESSION["case_id"] = $get["id"];
				
		if ( isset($post['submitAction']) ) {
			include($relPath . 'inc/team/submitActions.php');
			include($relPath . 'inc/admin/adminSubmitActions.php');
		}
		
		if ( isset($get["nav"]) ) {
			switch($get["nav"]) {
				case 'dashboard':
				$_SESSION["nav"] = "dashboard";
				include($relPath . 'inc/admin/display/dashboardMain_page.php');
				break;
				
				case 'paidInfluencer':
				$_SESSION["nav"] = "paidInfluencer";
				include($relPath . 'inc/admin/display/paidInfluencer_page.php');
				break;
				
				case 'calendar':
				$_SESSION["nav"] = "calendar";
				include($relPath . 'inc/admin/display/calendar_page.php');
				break;
				
				case 'reports':
				$_SESSION["nav"] = "reports";
				include($relPath . 'inc/admin/display/reports_page.php');
				break;
				
				case 'campaigns':
				$_SESSION["nav"] = "campaigns";
				include($relPath . 'inc/admin/display/campaigns_page.php');
				break;
				
				case 'coaches':
				$_SESSION["nav"] = "coaches";
				include($relPath . 'inc/admin/display/coaches_page.php');
				break;
				
				case 'donations':
				$_SESSION["nav"] = "donations";
				if ( isset($get["action"]) && $get["action"] == 'failed' )
					include($relPath . 'inc/admin/display/donationsFailed_page.php');
				else 
					include($relPath . 'inc/admin/display/donations_page.php');
				break;
				
				case 'salesPortal':
				$_SESSION["nav"] = "salesPortal";
				include($relPath . 'inc/admin/display/salesPortal_page.php');
				break;
				
				case 'account':
				$_SESSION["nav"] = "account";
				include($relPath . 'inc/admin/display/account_page.php');
				break;
				
				case 'messages':
				$_SESSION["nav"] = "messages";
				include($relPath . 'inc/admin/display/messages_page.php');
				break;
				
				case 'admin':
				$_SESSION["nav"] = "admin";
				include($relPath . 'inc/admin/display/admin_page.php');
				break;

				default:
				$_SESSION["nav"] = "dashboard";
				include($relPath . 'inc/admin/display/dashboardMain_page.php');
				break;
			}
		}
		else {
			$_SESSION["nav"] = "dashboard";
			include($relPath . 'inc/admin/display/dashboardMain_page.php');
		}

	}
	else {
		include($relPath . 'inc/admin/display/login_page.php');
	}
	
?>