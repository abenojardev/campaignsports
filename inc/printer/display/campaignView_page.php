<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js">');
	
    startToMainHeader($data);
	contentWrapToNav();
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
	$campaignStatuses = Campaign::getCampaignStatuses($get['tID']);
	
?>    
            <div class='adminSubNav'>
                <a href='index.php?nav=viewCampaign&sNav=reviewCampaign&tID=<?php echo $get['tID']; ?>' class='sNav'>Review Campaign Info</a>
				&nbsp;|&nbsp;
                
<?php
	if ($campaignStatuses[12] && !$campaignStatuses[15]) {
		echo "
                <a href='index.php?nav=viewCampaign&sNav=submitProof&tID=" . $get['tID'] . "' class='sNav'>Confirm Submission of Brochure/Envelope Proofs</a>
				&nbsp;|&nbsp;
                <!-- <a href='index.php?nav=viewCampaign&sNav=submitEnvelope&tID=" . $get['tID'] . "' class='sNav'>Submit Envelope Proof</a>
				&nbsp;|&nbsp; -->
		";				
	}
/*	else if ($campaignStatuses[15] && !$campaignStatuses[16]) {
		echo "
                <a href='index.php?nav=viewCampaign&sNav=setMailingDate&tID=" . $get['tID'] . "' class='sNav'>Set Mailing Date</a>
				&nbsp;|&nbsp;
		";				
	}
	else if ($campaignStatuses[16]) {
		echo "
                <a href='index.php?nav=viewCampaign&sNav=checkMailingDate&tID=" . $get['tID'] . "' class='sNav'>Check Mailing Date</a>
				&nbsp;|&nbsp;
		";				
	}
*/?>    

                <!-- New "Mailing date" change -->
                <a href='index.php?nav=viewCampaign&sNav=setMailingDate&tID=<?php echo $get['tID']; ?>' class='sNav'>Set Mailing Date</a>
				&nbsp;|&nbsp;
                <!-- End New "Mailing date" change -->
                
                <a href='index.php?nav=viewCampaign&sNav=prevProofs&tID=<?php echo $get['tID']; ?>' class='sNav'>Previous Proofs</a>
				&nbsp;|&nbsp;
<!--                <a href='index.php?nav=viewCampaign&sNav=mailingCosts&tID=<?php echo $get['tID']; ?>' class='sNav'>Print/Mailing Costs</a>
 				&nbsp;|&nbsp;
-->                <a href='index.php?nav=viewCampaign&sNav=messages&tID=<?php echo $get['tID']; ?>' class='sNav'>Messages</a>
            <div class='clear'></div>
            </div>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                	<?php 
					switch($get['sNav']) {
						case 'reviewCampaign':
							include($_SESSION['relative_path'] . 'inc/printer/layout/campaignReview.php');
						break;
						
						case 'submitProof':
							include($_SESSION['relative_path'] . 'inc/printer/layout/submitProof.php');
						break;
						case 'submitEnvelope':
							include($_SESSION['relative_path'] . 'inc/printer/layout/submitEnvelope.php');
						break;
						
						case 'setMailingDate':
							include($_SESSION['relative_path'] . 'inc/printer/layout/setMailingDate.php');
						break;
						case 'checkMailingDate':
							include($_SESSION['relative_path'] . 'inc/printer/layout/checkMailingDate.php');
						break;
						
						case 'prevProofs':
							include($_SESSION['relative_path'] . 'inc/printer/layout/prevProofs.php');
						break;
						case 'viewProof':
							include($_SESSION['relative_path'] . 'inc/printer/layout/viewProof.php');
						break;
						case 'mailingCosts':
							include($_SESSION['relative_path'] . 'inc/printer/layout/proofCosts.php');
						break;
						case 'messages':
							include($_SESSION['relative_path'] . 'inc/printer/layout/messages.php');
						break;
						default:
							include($_SESSION['relative_path'] . 'inc/printer/layout/campaignReview.php');
						break;
						
					}
					?>
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					switch($get['sNav']) {
						case 'reviewCampaign':
							include($_SESSION['relative_path'] . 'inc/common/widgets/submissions.php');
						break;



						case 'submitProof':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/submissions.php');
						break;
						case 'submitEnvelope':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/submissions.php');
						break;
						
						case 'prevProofs':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'viewProof':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'mailingCosts':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'messages':
							if ($get['sub'] && $get['sub'] == 'sent')
								include_once($_SESSION['relative_path'] . 'inc/common/widgets/incomingMessages.php');
							else
								include_once($_SESSION['relative_path'] . 'inc/common/widgets/sentMessages.php');
						break;
						default:
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
					}
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->


<?php
	contentClosures();
	closePageWrapToEnd();
?>