<?php
	// Printer Submit Actions File
	
	if ($post['submitAction'] == 'rejectContactList') {
		require_once($relPath . 'inc/printer/class/Printer.php');
		//Printer::approveContactList($post);
		
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 13;
		$dataS["pending"] = 1;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($dataS);
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Printer";
		$dataM['senderID'] = $_SESSION["printer_id"];
		$dataM['benchmark'] = "Contact List";
		$dataM['benchmarkID'] = 0;
		$dataM['subject'] = $post['subject'];
		$dataM['message'] = $post['message'];
		Messaging::addMessage($dataM);
	}
	
	else if ($post['submitAction'] == 'approveContactList') {
		require_once($relPath . 'inc/printer/class/Printer.php');
		Printer::approveContactList($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 13;
		$data["pending"] = 0;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($data);
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Printer";
		$dataM['senderID'] = $_SESSION["printer_id"];
		$dataM['benchmark'] = "Contact List";
		$dataM['benchmarkID'] = 0;
		$dataM['subject'] = "List Approved";
		$dataM['message'] = "Scrubbed Contact List has been approved by printer.";
		Messaging::addMessage($dataM);
	}
	
	else if ($post['submitAction'] == 'submitProof') {
		
		// Step 1 - Check Curent design name
		//require_once($relPath . 'inc/printer/class/Printer.php');
		//$currentProof = Printer::getCurrentProof($post['tID']);

		// Step 2 - Upload file
		//require_once($relPath . 'inc/common/class/files/Upload.php');
		//$filename = Upload::multiUpload($post);
		
		// Step 3 - Update Status 
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 14;
		$dataS["pending"] = 1;
		Campaign::setCampaignStatus($dataS);
		
		// Step 4 - Update submissions table if new revision
		//if ($currentProof['filename'] != $filename) {
			require_once($relPath . 'inc/printer/class/Printer.php');
			$newID = Printer::submitProof($post);
			
			$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "New Proof in System";
			$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The printer has uploaded a proof revision for review.";
		//} else {
			//$newID = $currentProof['ID'];
			//$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "Proof Revision Uploaded";
			//$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The printer has uploaded an update to a current proof revision for review.";
		//}
		
		// Step 5 - Send mail to admin 
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Printer";
		$dataM['senderID'] = $post['pID'];
		$dataM['benchmark'] = "Brochure Design";
		$dataM['benchmarkID'] = $newID;
		Messaging::addMessage($dataM);
	}
	
	else if ($post['submitAction'] == 'submitEnvelope') {
		
		// Step 1 - Check Curent design name
		require_once($relPath . 'inc/printer/class/Printer.php');
		$currentProof = Printer::getCurrentProof($post['tID']);

		// Step 2 - Upload file
		require_once($relPath . 'inc/common/class/files/Upload.php');
		$filename = Upload::multiUpload($post);
		
		// Step 3 - Update Status 
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 14;
		$dataS["pending"] = 1;
		Campaign::setCampaignStatus($dataS);
		
		// Step 4 - Update submissions table if new revision
		if ($currentProof['filename'] != $filename) {
			require_once($relPath . 'inc/printer/class/Printer.php');
			$newID = Printer::submitProof($post);
			
			$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "New Proof in System";
			$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The printer has uploaded a proof revision for review.";
		} else {
			$newID = $currentProof['ID'];
			$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "Proof Revision Uploaded";
			$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The printer has uploaded an update to a current proof revision for review.";
		}
		
		// Step 5 - Send mail to admin 
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Printer";
		$dataM['senderID'] = $post['pID'];
		$dataM['benchmark'] = "Brochure Design";
		$dataM['benchmarkID'] = $newID;
		Messaging::addMessage($dataM);
	}
	
	else if ($post['submitAction'] == 'setMailingDate') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($post['tID']);
		$TeamMain->setPrinterTeamMailDate($post);
		
		$date = date("Y-m-d", strtotime($post['brochure_mailing'])); 
	// ------ REMOVED -- the system will set on this estimated mailing date and fire this

		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 16;
		Campaign::setCampaignStatus($dataS);
		
		// Send message to admin 
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "Brochures Mailed";
		$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The printer has mailed the brochures.";
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Printer";
		$dataM['senderID'] = $post['pID'];
		$dataM['benchmark'] = "Mailing Date";
		$dataM['benchmarkID'] = 0;
		Messaging::addMessage($dataM);
		
		// email to coach that brouchures have been mailed
		require_once($relPath . 'inc/common/display/mailText.php');		
		require_once($relPath . 'inc/team/class/TeamUser.php');		
		$coach = TeamUser::getAdminData($post['tID']);
		$coachName = $coach['fname']." ".$coach['lname'];
		
		$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
		$brochuresMailedDataReturn = brochuresMailed($mailVars);
		
		$mailData['to'] = $coach['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $brochuresMailedDataReturn['brochuresMailedSubject'];
		$mailData['messageHTML'] = $brochuresMailedDataReturn['brochuresMailed'];
		Mail::send_email($mailData);
	}
	
	
	else if ($post['submitAction'] == 'updateCosts') {
		require_once($relPath . 'inc/printer/class/Printer.php');
		Printer::updateCosts($post);
	}
	


	else if ($post['submitAction'] == 'messageReply') {
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = $post['recip'];
		$data['recipID'] = $post['recipID'];
		$data['sender'] = $post['sender'];
		$data['senderID'] = $post['senderID'];
		$data['benchmark'] = $post['benchmark'];
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}
?>

