<?php
/****************************************************************************************
Printer.php
Defines the Printer class.

Application: Campaign Sports
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Printer {
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Printer: ' . $e->getMessage() );
			}
		}
	} // __construct()

	public function login($post) {
		try {
			if($post['username']!='' && $post['password']!='') {
				$query = array('select' => "*", 
							   'tbl' => "printers", 
							   'where' => "username = '".$post['username']."' AND password = '".$post['password']."'");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result) {
					$_SESSION["printer_id"] = $result['ID'];
					$_SESSION["printer_name"] = $result['fname'] . " " . $result['lname'];
					$loginFailure = NULL;
				}
				else  {
					$loginFailure = 'No matching records were found, please try again.';
				}		
			} 
			else {
				$loginFailure = 'Please enter your username and password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; login(): ' . $e->getMessage() );
		}
	} // login()
	
	public function logout() {
		unset($_SESSION["printer_id"]);
		unset($_SESSION["printer_name"]);
		unset($_SESSION["campaign_team"]);
	} // logout()


	public function searchCampaigns($pass) {
		try {
			$searchData = $pass['searchData'];
			$searchType = $pass['searchType'];
			
			if ($searchType == 'all') {
				$where = "HAVING (t.name LIKE '%".$searchData."%' 
				OR t.team LIKE '%".$searchData."%'
				OR t.ID LIKE '%".$searchData."%')
				AND t.printerID = ".$_SESSION["printer_id"];
			} else if ($searchType == 'name') {
				$where = "HAVING t.name LIKE '%".$searchData."%' 
				AND t.printerID = ".$_SESSION["printer_id"];
			} else if ($searchType == 'team') {
				$where = "HAVING t.team LIKE '%".$searchData."%' 
				AND t.printerID = ".$_SESSION["printer_id"];
			} else if ($searchType == 'ID') {
				$where = "HAVING t.ID LIKE '%".$searchData."%' 
				AND t.printerID = ".$_SESSION["printer_id"];
			} else {
				$where = "HAVING t.printerID = ".$_SESSION["printer_id"];
			}
			
			$query = array('select' => "t.ID, t.name, t.team, t.status, t.printerID", 
							'sort' => "t.ID desc",
							'tbl' => "teams t GROUP BY t.ID ".$where);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; searchCampaigns(): ' . $e->getMessage() );
		}
	} // searchCampaigns()

	public function getSingleCampaign($tID) {
		try {
			$query = array('select' => "t.*, ti.goals, ti.stats, ti.add_info, tc.fname, tc.lname, tc.email, tc.phoneDay", 
						   'tbl' => "teams t LEFT JOIN team_initial_info ti ON t.ID = ti.tID LEFT JOIN team_contacts tc ON t.ID = tc.tID",
						   'where' => "t.ID = $tID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSingleCampaign(): ' . $e->getMessage() );
		}
	} // getSingleCampaign()

	public function submitProof($data) {
		try {
			$fields = "tID";
			$values = $data["tID"];
			
			$fields .= ",pID";
			$values .= "," . $_SESSION["printer_id"];
			
			if (isset($data["envelope"])) {
				$fields .= ",envelope";
				$values .= ",1";
			}
			
			$fields .= ",filename";
			$values .= ",'" . $_FILES["multiUpload"]["name"][0] . "'";
			
			$query = array('tbl' => "printer_submissions", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; submitProof(): ' . $e->getMessage() );
		}
	} // submitProof()

	public function getSubmittedProofs($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_submissions",
						   'sort' => " date_submitted DESC",
						   'where' => "tID = $tID AND pID = " . $_SESSION["printer_id"]);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSubmittedProofs(): ' . $e->getMessage() );
		}
	} // getSubmittedProofs()

	public function getSubmittedBrochures($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_submissions",
						   'sort' => " date_submitted DESC",
						   'where' => "tID = $tID AND envelope = 0 AND pID = " . $_SESSION["printer_id"]);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSubmittedBrochures(): ' . $e->getMessage() );
		}
	} // getSubmittedBrochures()

	public function getSubmittedEnvelopes($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_submissions",
						   'sort' => " date_submitted DESC",
						   'where' => "tID = $tID AND envelope = 1 AND pID = " . $_SESSION["printer_id"]);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSubmittedEnvelopes(): ' . $e->getMessage() );
		}
	} // getSubmittedEnvelopes()

	public function getCurrentProof($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_submissions",
						   'sort' => " date_submitted DESC LIMIT 1",
						   'where' => "tID = $tID AND envelope = 0 AND pID = (SELECT printerID FROM teams WHERE ID = $tID)");
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCurrentProof(): ' . $e->getMessage() );
		}
	} // getCurrentProof()

	public function getCurrentEnvelope($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_submissions",
						   'sort' => " date_submitted DESC LIMIT 1",
						   'where' => "tID = $tID AND envelope = 1 AND pID = (SELECT printerID FROM teams WHERE ID = $tID)");
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCurrentEnvelope(): ' . $e->getMessage() );
		}
	} // getCurrentEnvelope()

	public function getProof($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_submissions",
						   'where' => "ID = $ID AND pID = " . $_SESSION["printer_id"]);
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getProof(): ' . $e->getMessage() );
		}
	} // getProof()

	public function insertCosts($data) {
		try {
			$fields = "tID";
			$values = $data["tID"];
			
			$fields .= ",pID";
			$values .= "," . $data["pID"];
			
			$query = array('tbl' => "printer_costs", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; insertCosts(): ' . $e->getMessage() );
		}
	} // insertCosts()

	public function updateCosts($post) {
		try {
			//$set = "cost_per_brochure=" . $post['costPerBrochure'] . ",
			$set = "
			domestic_mailers=" . $post['domesticMailers'] .",domestic_rate=" . $post['domesticRate'] . ",printing_costs=" . $post['printingCosts'] .
				",international_mailers=" . $post['internationalMailers'] .",international_rate=" . $post['internationalRate'] .
				",adjustments=" . $post['adjustments'] .",addidtional_details='" . $post['addidtionalDetails'] ."'   ";
			
			$query = array('tbl' => "printer_costs", 
						   'set' => $set, 
						   'where' => "tID=" . $post['tID'] . " AND pID=" . $post['pID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateCosts(): ' . $e->getMessage() );
		}
	} // updateCosts()

	public function getCosts($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " printer_costs",
						   'where' => "tID = $tID");
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCosts(): ' . $e->getMessage() );
		}
	} // getCosts()

	public function approveContactList($post) {
		try {
			$fields = "tID";
			$values = $post['tID'];
			$fields .= ",benchmark";
			$values .= ",'Contact List'";
			$fields .= ",bID";
			$values .= ",0";
			$fields .= ",initialedBy";
			$values .= ",'Printer'";
			$fields .= ",ibID";
			$values .= ",'" . $_SESSION['printer_id'] . "'";
			$fields .= ",initials";
			$values .= ",'" . $post['initials'] . "'";

			$query = array('tbl' => " approvals", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			//return $DB->insert_id;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; approveContactList(): ' . $e->getMessage() );
		}
	} // approveContactList()

	public function benchmarkCheckConatactList($tID) {
		try {
			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			$campaignStatuses = Campaign::getCampaignStatuses($tID);
			
			if ($campaignStatuses[13] && !$campaignStatuses[13]['pending']) {
				return $campaignStatuses[13]['status_date'];
			} else {
				return false;
			}
			
		} catch ( Exception $e ) {
			//throw new Exception( 'Error with Method; benchmarkCheckConatactList(): ' . $e->getMessage() );
			return $e->getMessage();
		}
	} // benchmarkCheckConatactList()


}
?>