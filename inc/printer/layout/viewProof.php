<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	$proof = Printer::getProof($get['id']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');
	
	$datePrint = date("F j, Y", strtotime($proof['date_submitted']));
	
	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
			<div>
			<h2 style='color:#f7941e;'>Previous Proof #" .$get['n']. "</h2>
			Submitted: $datePrint
			</div>
			<div class='genFloatL' style='margin-top:20px;margin-bottom:20px;'>
				<img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' />
			</div>
			<div class='genFloatL' style='margin-top:30px;margin-left:6px;'>
				<h4 class='teamPrimaryTxtColor'>
					<a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$proof['filename']."&p=$dirname". $proof['filename'] . "' target='_blank'>Click here to download and review this proof</a>
				</h4>
			</div>
			<div class='clear'></div>
		</div>	
		<div class='contentLeftData'>
			<strong>Associated Messages</strong>
			<br /><br /><br /><br /><br /><br />
			
			
		</div>	
		<div class='contentLeftData'>
			<strong>Send Message</strong><br /><br />
			You may utilize this area to send a message regarding this particular proof to this campaign's administrator.
			<br /><br />
			<input class='textField' name='' size='66' value='Subject...'/>
			<br />
			<textarea class='txtTextArea' name='goals' cols='55' rows='10'>Message...</textarea>
		</div>
		<div class='genFloatR' style='margin-top:19px;'>
			<a href='javascript:document.frmApprove.submit();' class='teamButton adminPrimaryBGColor'>Send Message</a>
		</div>
		<div class='clear'></div>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	";
	
			
	
?>