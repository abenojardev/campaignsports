<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	$proofs = Printer::getSubmittedProofs($get['tID']);
	$brochures = Printer::getSubmittedBrochures($get['tID']);
	$envelopes = Printer::getSubmittedEnvelopes($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
			<h2 style='color:#f7941e;'>Proof Submissions/Confirmations</h2>
	";
	
	$bCount = count($brochures)+1;
	$eCount = count($envelopes)+1;
	foreach($proofs AS $id => $data) {
		if (isset($data['envelope']) && $data['envelope'] == 1) {
			$proofOrEnvelope = "Envelope";
			$eCount--;
			$dCount = $eCount;
		} else {
			$proofOrEnvelope = "Proof";
			$bCount--;
			$dCount = $bCount;
		}
		
		$datePrint = date("F j, Y", strtotime($data['date_submitted']));
		echo "<div style='width:470px;background-color:#EEE;padding:10px;margin-top:1px;'>
		<!--
		<a href='index.php?nav=viewCampaign&sNav=viewProof&tID=".$data['tID']."&n=$dCount&id=$id' class='sNav'>
			$proofOrEnvelope #$dCount
		</a>
		-->
	
			Proof Submission #$dCount
		</a> - Submitted:&nbsp; $datePrint</div>";
	}
			
	echo "
		</div>	
	";
	
?>