<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	echo "
		<div class=''>
			<h2 style='color:#f7941e;'>Campaign Messages</h2>
			<br />
		</div>	
	";
	
	$sendRecCheck = ($get['sub']) ? $get['sub'] : "";
	
	require_once($relPath . 'inc/common/class/campaign/Messaging.php');
	$mData['tID'] = $get['tID'];
	
	if (!$sendRecCheck || $sendRecCheck == 'sent') {
		if ($sendRecCheck == 'sent') {
			$mData['sender'] = "Printer";
			$mData['senderID'] = $_SESSION["printer_id"];
			$messages = Messaging::getSentMessages($mData);
			$headerPrintout = "Sent";
			$sub = "&s=y";
		} else {
			$mData['recip'] = "Printer";
			$mData['recipID'] = $_SESSION["printer_id"];
			$messages = Messaging::getReceivedMessages($mData);
			$headerPrintout = "Incoming";
			$sub = "";
		} 
		
		$messagesCount = count($messages);
		
		echo "
			<div class='contentLeftData'>
				<h2 class='teamPrimaryTxtColor-cancel'>Review $headerPrintout Messages</h2>
			
				<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
				<table width='100%' border='0' cellspacing='0' cellpadding='7'>
		";
		
		$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
		$tpages = ceil($messagesCount/15);
		$min = ($page - 1) * 15 + 1;
		$max = $page * 15;
		
		$classAlternate = "bg1";
		$count = 0;
		
		foreach($messages AS $m) {
			$count++;
			if ($count < $min || $count > $max) continue;
			$date = date("M j, Y g:i A", strtotime($m['date_sent']));
			$unreadFlag = ($m['markAsRead']) ? "<span>" : "<span style='font-weight:bold;'>";
			
			echo "
					<tr class='$classAlternate' align='left'>
						<td><a href='index.php?nav=viewCampaign&sNav=messages&tID=".$get['tID']."&sub=view&mID=".$m['ID']."$sub'>$unreadFlag" . $m['benchmark'] . " - " . $m['subject'] . "</span></a></td>
						<td align='right'>$date</td>
					</tr>
			";
			$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
		}
		
		echo "
					<tr>
						<td colspan='2'>&nbsp;</td>
					</tr>      
				</table>
		";
		
		echo "<div class='pagination'><br/>";
		$reload = $_SERVER['PHP_SELF'] . "?nav=viewCampaign&sNav=messages&tID=".$get['tID'];
		include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
		echo paginate($reload, $page, $tpages, 5);
		echo "</div>";
		
		echo "
					</div>
					<!-- /contentLeftData -->
		";  
		
	} else if ($sendRecCheck == 'view') {
		include_once($_SESSION['relative_path'] . 'inc/common/widgets/viewMessage.php');
	}
		
	
?>