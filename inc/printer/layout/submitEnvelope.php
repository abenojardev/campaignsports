<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<form method='post' name='frm' id='frm' enctype='multipart/form-data' action='index.php?nav=viewCampaign&sNav=prevProofs&tID=".$get['tID']."'>
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Submit Envelope Proof</h2>
				<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:left;'>
					<input type='file' name='multiUpload[]' id='multiUpload' size='40' />
					<input type='hidden' name='uploadPath' value='team/" . $campaign['ID'] . "/printer'>
				</div>
				<div class='genFloatR' style='margin-top:19px;'>
					<input type='hidden' name='submitAction' value='submitEnvelope'>
					<input type='hidden' name='envelope' value=1>
					<input type='hidden' name='tID' value='".$get['tID']."'>
					<input type='hidden' name='pID' value='".$_SESSION['printer_id']."'>
					<a href='javascript:document.frm.submit();' class='teamButton adminPrimaryBGColor'>Upload</a>
				</div>
				<div class='clear'></div>
				<div>
					<br />
					NOTE: Please make sure there are no spaces in the filename.
					<br /><br />
					Envelope proofs are not \"required\" by the system.  Uploading an envelope proof will not fire the same event as uploading a brochure proof.
					<br /><br />
					Keep file names the same for each revision as this will just overwrite the current file.  Major changes require a new revision and a new file name.
				</div>
				<br /><br />
			</div>
			
			<div class='contentLeftData'>
				<strong>Send Accompanying Message to Campaign Admin</strong><br /><br />
				<input class='textField' name='subject' size='66' value='Subject...'/>
				<br />
				<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>
			</div>	
			<!--<div class='genFloatR' style='margin-top:19px;'>
				<a href='javascript:document.frmApprove.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
			</div>-->
			<div class='clear'></div>
		</form>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	";
	
?>