<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	$mailDate = ($campaign['brochure_mailing']) ? date("M j, Y", strtotime($campaign['brochure_mailing'])) : "";

	echo "
   		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/ui-lightness/jquery-ui-1.8.17.custom.css' />
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-ui-1.8.17.custom.min.js'></script>
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/ckeditor/ckeditor.js'></script>
	
		<script language='javascript' type='text/javascript'>
		<!--//
			$(function() {
				$('#datepicker').datepicker();
			});

		//-->
		</script>
	
	
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<form method='post' name='frm' id='frm' enctype='multipart/form-data' action='index.php?nav=viewCampaign&sNav=setMailingDate&tID=".$get['tID']."'>
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Set Mailing Date</h2>
				<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:left;'>
					<input type='text' id='datepicker' size='14' value='" . $mailDate . "' name='brochure_mailing' />
				</div>
				<div class='genFloatR' style='margin-top:19px;'>
					<input type='hidden' name='submitAction' value='setMailingDate'>
					<input type='hidden' name='tID' value='".$get['tID']."'>
					<input type='hidden' name='pID' value='".$_SESSION['printer_id']."'>
					<a href='javascript:document.frm.submit();' class='teamButton adminPrimaryBGColor'>Set Mailing Date</a>
				</div>
				<div class='clear'></div>
				<div>
					<br />
					<!-- NOTE: Anything to say here or remove note completely? -->
				</div>
				<br /><br />
			</div>
			
			<div class='contentLeftData'>
				<strong>Send Accompanying Message to Campaign Admin</strong><br /><br />
				<input class='textField' name='subject' size='66' value='Subject...'/>
				<br />
				<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>
			</div>	
			<!--<div class='genFloatR' style='margin-top:19px;'>
				<a href='javascript:document.frmApprove.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
			</div>-->
			<div class='clear'></div>
		</form>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	";
	
?>