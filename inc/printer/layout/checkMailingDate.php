<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	$mailDate = ($campaign['brochure_mailing']) ? date("M j, Y", strtotime($campaign['brochure_mailing'])) : "";

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['brochure_mailing']. ": " .$campaign['brochure_mailing']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
			<h2 style='color:#f7941e'>Check Mailing Date</h2>
			<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:left;'>
				You mailed the brochures on " . $mailDate . ".
			</div>
			<div class='genFloatR' style='margin-top:19px;'>
			</div>
			<div class='clear'></div>
			<div>
				<br />
			</div>
			<br /><br />
		</div>
		<div class='clear'></div>
	";
	
?>