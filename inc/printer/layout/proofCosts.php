<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	$costs = Printer::getCosts($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	$costPerBrochure = $costs['cost_per_brochure'];
	$domMailers = $costs['domestic_mailers'];
	$domRate = $costs['domestic_rate'];
	$printingCosts = $costs['printing_costs'];
	$intMailers = $costs['international_mailers'];
	$intRate = $costs['international_rate'];
	$adjustments = $costs['adjustments'];
	$addidtionalDetails = $costs['addidtional_details'];
	$totalMailers = $domMailers;
	$totalCosts = ($printingCosts) + ($domMailers * $domRate) + $adjustments;

	echo "
		<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/validate.css' />
		<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
		<script>
			$(document).ready(function() {
				$('#frm').submit(function() { 
					var val = validate();
					if (val)
					{
						return true;
					}
					else
					{
						return false;
					}
				});	
				
				$('#frmSubmit').click(function() {
					$('#frm').submit();
				});
			
				function validate() { 
					var msg = 'Numbers only.';
					error_check = false; 
				
					$('.validate').each(function (i) {
						var re = /\d+(\.\d+)?$/;
						if (re.test(this.value)) {
							$(this).removeClass('fieldEmpty');
						} else {
							error_check = true;
							$(this).addClass('fieldEmpty');
						}
					});
					
					if (error_check) {
						alert(msg);
						return false;
					}
					else
					{
						return true;
					}
				}
			});
		</script>
	
	
	
	
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<form method='post' name='frm' id='frm' action='index.php?nav=viewCampaign&sNav=mailingCosts&tID=" .$campaign['ID']. "'>
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Print and Mailing Costs</h2>
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong>Printing Costs</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField validate' name='printingCosts' size='10' value='$printingCosts' />
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong>Adjustments</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField validate' name='adjustments' size='10' value='$adjustments' />
				</div>
				<div class='clear'></div>
				
				<!--
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong>Cost per Brochure</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField' name='costPerBrochure' size='10' value='$costPerBrochure' />
				</div>
				<div class='clear'></div>
				-->
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong># of Domestic Mailers</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField validate' name='domesticMailers' size='10' value='$domMailers' />
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong>Domestic Rate</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField validate' name='domesticRate' size='10' value='$domRate' />
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong># of International Mailers</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField' name='internationalMailers' size='10' value='$intMailers' />
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='width:170px;margin-top:10px;margin-right:10px;'>
					<strong>International Rate</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField' name='internationalRate' size='10' value='$intRate' />
				</div>
				<div class='clear'></div>
				
				<div style='margin-top:10px;'>
					<strong>Total Mailers = $domMailers</strong>
				</div>
				<div style='margin-top:10px;'>
					<strong>Total Costs = $$totalCosts</strong>
				</div>
				
				<br /><br />
			</div>
			
			<div class='contentLeftData'>
				<br />
				<strong>Additional Details and Information Concerning Printing/Mailing Costs</strong><br /><br />
				<textarea class='' name='addidtionalDetails' cols='59' rows='10'>$addidtionalDetails</textarea>
			</div>	
			<div class='genFloatR' style='margin-top:19px;'>
				<input type='hidden' name='submitAction' value='updateCosts'>
				<input type='hidden' name='tID' value='".$get['tID']."'>
				<input type='hidden' name='pID' value='".$_SESSION['printer_id']."'>
				<a href='#' id='frmSubmit' class='teamButton adminPrimaryBGColor'>Update Costs</a>
			</div>
			<div class='clear'></div>
		</form>
	";
	
?>