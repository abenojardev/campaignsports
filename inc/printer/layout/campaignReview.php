<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$campaign = Printer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contacts = TeamUser::countContacts($get['tID']);
	$contactsCan = TeamUser::countContactsCanada($get['tID']);
	$contactsDom = TeamUser::countContactsDomestic($get['tID']);
	$contactsIntl = $contacts - $contactsDom;
	
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$finalFiles = Designer::getFinalFiles($get['tID']);
	
	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
		<strong>Coach Information</strong><br />
		" .$campaign['fname']. " " .$campaign['lname']. " - " .$campaign['email']. " - P:" .$campaign['phoneDay']. "
		<br /><br />
		</div>
	";
			
	if ($finalFiles['filename']) {
		include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
		$dirname =  $_SESSION['siteRoot'] . "/team/" . $get['tID'] . "/designer/";
		
		echo "
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Campaign Files</h2>
				<br />
				<div class='genFloatL' style='margin-bottom:20px;'>
					<img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' />
				</div>
				<div class='genFloatL' style='margin-top:10px;margin-left:6px;'>
					<h4 class='teamPrimaryTxtColor'><a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$finalFiles['filename']."&p=$dirname". $finalFiles['filename'] . "' target='_blank'>Click here to download the designer files</a></h4>
				</div>
				<div class='clear'></div>
			</div>	
		";
	}
		
	echo "
		<div class='contentLeftData'>
			<h2 style='color:#f7941e'>Campaign Information</h2>
			<br />
			<strong>Number of Contacts:</strong> $contacts
			<br />
			Domestic Contacts: $contactsDom
			<br />
			Canadian Contacts: $contactsCan
			<br />
			International Contacts: $contactsIntl
			<br />
			
			<div class='genFloatL' style='margin-top:10px;margin-bottom:10px;'>
				<img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' />
			</div>
			<div class='genFloatL' style='margin-top:20px;margin-left:6px;'>
				<h4 class='teamPrimaryTxtColor'>
					<a href='export.php?id=".$get['tID']."'>Click here to download DOMESTIC Contacts</a>
				</h4>
			</div>
			<div class='clear'></div>
			<div class='genFloatL' style='margin-top:10px;margin-bottom:10px;'>
				<img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' />
			</div>
			<div class='genFloatL' style='margin-top:20px;margin-left:6px;'>
				<h4 class='teamPrimaryTxtColor'>
					<a href='export_ca.php?id=".$get['tID']."'>Click here to download CANADA Contacts</a>
				</h4>
			</div>
			<div class='clear'></div>
			<div class='genFloatL' style='margin-top:10px;margin-bottom:10px;'>
				<img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' />
			</div>
			<div class='genFloatL' style='margin-top:20px;margin-left:6px;'>
				<h4 class='teamPrimaryTxtColor'>
					<a href='export_intl.php?id=".$get['tID']."'>Click here to download INTERNATIONAL Contacts</a>
				</h4>
			</div>
			<div class='clear'></div>
		</div>	
		
		<div class='contentLeftData'>
			<strong>Approval</strong>
			<br /><br />
			Please download the latest contact list for this campaign. Once you have reviewed the contact data, please confirm by entering your initials in the box below. If for any reason you can not confirm reciept, you can use the form below to send a message to this campaign's Campaign Administrator.
			<br /><br />
	";
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	$approvalCheck = Printer::benchmarkCheckConatactList($get['tID']);
	
	if (!$approvalCheck) {
		echo "
			<form method='post' name='frmApprove' action='index.php?nav=viewCampaign&sNav=reviewCampaign&tID=".$get['tID']."'>
				<div class='genFloatL' style='margin-bottom:10px;'><input class='textField validate centerText' name='initials' size='6' /></div>
				<div class='genFloatL' style='margin-top:5px;margin-left:20px;'>
					<input type='hidden' name='tID' value='".$get['tID']."'>
					<input type='hidden' name='submitAction' value='approveContactList'>
					<a href='javascript:document.frmApprove.submit();' class='teamButton adminPrimaryBGColor'>I Approve</a>
				</div>
				<div class='clear'></div>
			</form>
		";
	} else {
		echo "<span style='color:#03c603'>";
		echo "You approved the contact list on: ";
		echo date("F j, Y", strtotime($approvalCheck));
		echo "</span>";
		echo "<br /><br />";
	}
					
	echo "
		
		<form method='post' name='frmReject' action='index.php?nav=viewCampaign&sNav=reviewCampaign&tID=".$get['tID']."'>
			<input type='hidden' name='submitAction' value='messageBrochure'>
			<input type='hidden' name='tID' value='".$team_id."'>
			<div class='contentLeftData'>
				<strong>Change, Correction and Comments</strong><br /><br />
				If for any reason you do not approve of the current version of this campaign's contact list, please use the box below to send a message to your Campaign Administrator explaining any changes, corrections or comments.
				<br /><br />
				<input class='textField' name='subject' size='66' value='Subject...'/>
				<br />
				<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>	
				<br /><br />
				<p align='right'>
					<input type='hidden' name='tID' value='".$get['tID']."'>
					<input type='hidden' name='submitAction' value='rejectContactList'>
					<a href='javascript:document.frmReject.submit();' class='teamButton adminPrimaryBGColor'>Send Message</a>
				</p>
			</div>
		</form>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	<div class='clear'></div>
	</div>
	";
	
?>