<?php
	require_once($_SESSION['relative_path'] . 'inc/printer/class/Printer.php');
	
	$s = (isset($get['s'])) ? $get['s'] : $post['searchField'];
	$st = (isset($get['st'])) ? $get['st'] : $post['searchType'];
	$pass['searchData'] = ($s) ? $s : "";
	$pass['searchType'] = ($st) ? $st : "";
	$search = ($s) ? "&s=$s&st=$st" : "";
	
	switch($st) {
		case 'all':
		$highlightSearch = "$('td.search').highlight('$s');";
		break;	
		case 'ID':
		$highlightSearch = "$('td.sID').highlight('$s');";
		break;	
		case 'name':
		$highlightSearch = "$('td.sName').highlight('$s');";
		break;	
		case 'team':
		$highlightSearch = "$('td.sTeam').highlight('$s');";
		break;	
	}

	$campaigns = Printer::searchCampaigns($pass);
	$campaignCount = count($campaigns);

	echo "
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
				<h2 class='adminPrimaryTxtColor'>Campaigns</h2>
				
					<table width='100%' border='0' cellspacing='0' cellpadding='7'>
						<tr>
							<td width='44'><strong>ID</strong></td>
							<td><strong>School</strong></td>
							<td><strong>Team</strong></td>
						</tr>
	";
						$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
						$tpages = ceil($campaignCount/15);
						$min = ($page - 1) * 15 + 1;
						$max = $page * 15;
						
						$classAlternate = "bg1";
						$count = 0;
						
						
						foreach($campaigns AS $c) {
							$count++;
							if ($count < $min || $count > $max) continue;
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left' class='search sID'>" . $c['ID'] . "</td>
							<td align='left' class='search sName'>" . $c['name'] . "</td>
							<td align='left' class='search sTeam'><a href='index.php?nav=viewCampaign&tID=" . $c['ID'] . "'>" . $c['team'] . "</a></td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
						}
	echo "
						<tr>
							<td colspan='2'>&nbsp;</td>
						</tr>      
					</table>
	";
				if ($s) {
					echo "
						<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/jquery-1.5.1.min.js'></script>
						<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/common/scripts/highlight.js'></script>
						<script type='text/javascript' language='javascript'>
						// <![CDATA[
							$highlightSearch
						// ]]>
						</script>
					";
				}
	
				echo "<div class='pagination'><br/>";
				$reload = $_SERVER['PHP_SELF'] . "?nav=campaigns".$search;
				include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
				echo paginate($reload, $page, $tpages, 5);
				echo "</div>";
	
?>