<?php
/****************************************************************************************
DonationUser.php
Defines the DonationUser class.

Application: Campaign Sports
Artisan Digital Studios, November 2017
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class DonationUser {
	
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				$query = array('select' => "*", 
							   'tbl' => "contacts", 
							   'where' => "ID=$ID");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				foreach($result as $field => $data) {
					$this->$field = $data;
				}
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating DonationUser: ' . $e->getMessage() );
			}
		}
	} // __construct()
	
	
	public function loginOld($post) {
		try {
			if($post['teamID']!='' && $post['playerID']!='') 
			{
				$teamID = $post['teamID'];
				$playerID = substr($post['playerID'], 6);
				
				$query = array('select' => "ID", 
							   'tbl' => "players", 
							   'where' => "ID = $playerID AND tID = $teamID");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result)
				{	
					$_SESSION["player_id"] = $playerID;
					$_SESSION['current_folder'] = $teamID;
					include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
					$loginFailure = NULL;
				}
				else
				{
					$loginFailure = 'There was no athlete matched to that team, please check your mailer and try again.';
				}		
			} 
			else 
			{
				$loginFailure = 'Please enter your Team Code and Team Password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; loginOld(): ' . $e->getMessage() );
		}
	} // loginOld()

	public function login($post) {
		try {
			if($post['teamID']!='' && $post['playerID']!='') 
			{
				$teamID = $post['teamID'];
				$playerID = $post['playerID'];
				
				$query = array('select' => "ID", 
							   'tbl' => "teams", 
							   'where' => "ID = $teamID AND password = '$playerID'");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result)
				{	
					//$_SESSION["player_id"] = $playerID;
					$_SESSION['current_folder'] = $teamID;
					include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
					$loginFailure = NULL;
				}
				else
				{
					$loginFailure = 'There was no athlete matched to that team, please check your mailer and try again.';
				}		
			} 
			else 
			{
				$loginFailure = 'Please enter your Team Code and Team Password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; login(): ' . $e->getMessage() );
		}
	} // login()


	public function logout() {
		unset($_SESSION["player_id"]);
		unset($_SESSION["current_folder"]);
		unset($_SESSION["donation_user"]);
	} // logout()
	
	
	public function makeDonation($post) {
		try {
			// Insert record
			$i = 0;
			$fields = $values = "";
			
			foreach($post as $f => $v) {
				if ($f == 'donationValue' || $f == 'donationValueCustom') continue;
				if ($f == 'cardType' || $f == 'num') continue;
				if ($f == 'cardExpMo' || $f == 'cardExpYr') continue;
				if ($f == 'p1' || $f == 'p2' || $f == 'p3') continue;
				if ($f == 'city' || $f == 'state' || $f == 'zip') continue;
				if ($f == 'cityI' || $f == 'stateI' || $f == 'zipI') continue;
				if ($f == 'email' || $f == 'emailI') continue;
				if ($f == 'phoneI' || $f == 'country' || $f == 'intl') continue;
				if ($i == 0) {
					$fields .= $f;
					$values .= "'".$v."'";
				} else {
					$fields .= "," . $f;
					$values .= ",'" . $v."'";
				}
				$i = 1;
				
			}

			if ( isset($post['r_error']) && $post['r_error'] != "" ) {
				$table = "donations_failed";
			} else {
				$table = "donations";
			}
			
			if ( isset($post['donationValueCustom']) && $post['donationValueCustom'] != "" ) {
				$fields .= ",donationValue";
				$values .= ",'".$post['donationValueCustom']."'";
			} else {
				$fields .= ",donationValue";
				$values .= ",'".$post['donationValue']."'";
			}
			
			// make sure clean phone goes through if empty ... no dashes
			if (($post['p1']!='') && ($post['p2']!='') &&  ($post['p3']!='')) {
				$postPhoneValue = $post['p1']."-".$post['p2']."-".$post['p3'];
			} else {
				$postPhoneValue = '';
			}
			if (isset($post['intl']) && $post['intl']) {
				$fields .= ",phone";
				$values .= ",'".$post['phoneI']."'";
				$fields .= ",city";
				$values .= ",'".$post['cityI']."'";
				$fields .= ",state";
				$values .= ",'".$post['stateI']."'";
				$fields .= ",zip";
				$values .= ",'".$post['zipI']."'";
				$fields .= ",email";
				$values .= ",'".$post['emailI']."'";
				$fields .= ",country";
				$values .= ",'".$post['country']."'";
				$fields .= ",intl";
				$values .= ",'".$post['intl']."'";
			} else {
				$fields .= ",phone";
				$values .= ",'".$post['p1']."-".$post['p2']."-".$post['p3']."'";
				$fields .= ",city";
				$values .= ",'".$post['city']."'";
				$fields .= ",state";
				$values .= ",'".$post['state']."'";
				$fields .= ",zip";
				$values .= ",'".$post['zip']."'";
				$fields .= ",email";
				$values .= ",'".$post['email']."'";
			}
			
			$teamID = $_SESSION['current_folder'];
			if ( $teamID == "1000" || $teamID == "1002" ) {
				$fields .= ",cID";
				$values .= "," . $_SESSION["donation_user"];
				
				$fields .= ",pID";
				$values .= "," . $_SESSION["player_id"];
			}
			else {
				$fields .= ",pID";
				$values .= "," . $_SESSION["donation_user"];
			}
			
			$fields .= ",tID";
			$values .= "," . $_SESSION["current_folder"];
			
			$query = array('tbl' => $table, 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			// Successful insert
			if ($result)
			{
				require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
				$campaignStatuses = Campaign::getCampaignStatuses($_SESSION["current_folder"]);
				
				if (!$campaignStatuses[17]) {
					$data["ID"] = $_SESSION["current_folder"];
					$data["sID"] = 17;
					Campaign::setCampaignStatus($data);
					
					// email to coach that contact collection has started
					require_once($relPath . 'inc/common/display/mailText.php');		
					require_once($relPath . 'inc/team/class/TeamUser.php');		
					$coach = getAdminData($_SESSION['current_folder']);
					$coachName = $coach['fname']." ".$coach['lname'];
					
					$mailVars=array("coachName"=>$coachName,"teamID"=>$post['tID']);
					$donationsStartedDataReturn = donationsStarted($mailVars);
					
					$mailData['to'] = $coach['email'];
					$mailData['from'] = $cfg_adminEmail;
					$mailData['subject'] = $donationsStartedDataReturn['donationsStartedSubject'];
					$mailData['messageHTML'] = $donationsStartedDataReturn['donationsStarted'];
					Mail::send_email($mailData);
				}
			}
			else
			{
				throw new Exception('Could not complete your request, please contact the administrator.');
			}		
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; makeDonation(): ' . $e->getMessage() );
		}
	} // makeDonation()
	
	
	public function paypalDonation($post) {
		try {
			// Check for duplicate entry
			$query = array('select' => "*", 
						   'tbl' => "donations", 
						   'where' => "txn_id='".$post["txn_id"]."'");
			$DB = new DB();
			$dupeCheck = $DB->select_single($query);
			
			if (!$dupeCheck) {
				// Insert record
				$fields = $values = "";
				
				$itemData = explode("-", $post['item_number']);
				
				$fields .= "tID";
				$values .= $itemData[0];
				
				$teamID = $itemData[0];
				if ( $teamID == "1000" || $teamID == "1002" ) {
					$fields .= ",cID";
					$values .= "," . $_SESSION["donation_user"];
					
					$fields .= ",pID";
					$values .= "," . $_SESSION["player_id"];
				}
				else {
					$fields .= ",pID";
					$values .= "," . $itemData[1];
				}
				
				$fields .= ",fname";
				$values .= ",'" . $post["first_name"]."'";
				$fields .= ",lname";
				$values .= ",'" . $post["last_name"]."'";
				$fields .= ",address";
				$values .= ",'" . $post["address_street"]."'";
				$fields .= ",city";
				$values .= ",'" . $post["address_city"]."'";
				$fields .= ",state";
				$values .= ",'" . $post["address_state"]."'";
				$fields .= ",zip";
				$values .= ",'" . $post["address_zip"]."'";
				$fields .= ",country";
				$values .= ",'" . $post["address_country"]."'";
	
				$fields .= ",email";
				$values .= ",'" . $post["payer_email"]."'";
	
				$fields .= ",donationValue";
				$values .= "," . number_format($post["mc_gross"]);
				$fields .= ",paymentMethod";
				$values .= ", 'PayPal'";
				// reverted to 'payment date' to conform to field name in sent txn array
				// 'donation date
				$fields .= ",payment_date";
				$values .= ",'" . $post["donation_date"]."'";
				$fields .= ",txn_id";
				$values .= ",'" . $post["txn_id"]."'";
				
				$query = array('tbl' => "donations", 
							   'fields' => $fields, 
							   'values' => $values);
				$DB = new DB();
				$result = $DB->insert_single($query);
				
				// Successful insert
				if ($result)
				{
					require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
					$campaignStatuses = Campaign::getCampaignStatuses($_SESSION["current_folder"]);
					
					if (!$campaignStatuses[17]) {
						$data["ID"] = $_SESSION["current_folder"];
						$data["sID"] = 17;
						Campaign::setCampaignStatus($data);
						
						// email to coach that contact collection has started
						require_once($relPath . 'inc/common/display/mailText.php');		
						require_once($relPath . 'inc/team/class/TeamUser.php');		
						$coach = TeamUser::getAdminData($_SESSION['current_folder']);
						
						$mailData['to'] = $coach['email'];
						$mailData['from'] = $cfg_adminEmail;
						$mailData['subject'] = $donationsStartedSubject;
						$mailData['messageHTML'] = $donationsStarted;
						Mail::send_email($mailData);
					}
				}
				else
				{
					throw new Exception('Could not complete your request, please contact the administrator.');
				}		
			}
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; paypalDonation(): ' . $e->getMessage() );
		}
	} // paypalDonation()
	
	
	public function manualQuickCheckDonation($post) {
		try {
			// Insert record
			$fields = $values = "";
			
			$contactData = explode(":", $post['contactInfo']);
			
			$fields .= "tID";
			$values .= $post['teamID'];
			
			$fields .= ",pID";
			$values .= "," . $post['playerID'];
			
			$fields .= ",cID";
			$values .= "," . $contactData[0];
			if ($contactData[0] == '0') {
				$fields .= ",fname";
				$values .= ",'" . $post['fname']."'";
				$fields .= ",lname";
				$values .= ",'" . $post['lname']."'";
			} else {
				$fields .= ",fname";
				$values .= ",'" . $contactData[1]."'";
				$fields .= ",lname";
				$values .= ",'" . $contactData[2]."'";
			}
			
			$fields .= ",donationValue";
			$values .= "," . $post["donation"];
			$fields .= ",paymentMethod";
			$values .= ", 'check'";
			
			$query = array('tbl' => "donations", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			// Successful insert
			if ($result)
			{
				
			}
			else
			{
				throw new Exception('Could not complete your request, please contact the administrator.');
			}		
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; manualQuickCheckDonation(): ' . $e->getMessage() );
		}
	} // manualQuickCheckDonation()
	
	
}