<?php
	$teamID = $_POST['teamID'];

	if ($teamID == "1000" || $teamID == "1002") {
		require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');
		$Player = new Player($_SESSION['player_id']);
		$playerName = $Player->getName();
		$namePrint = "
					<span style='font-weight:bold;' class='teamPrimaryTxtColor'>$playerName</span>?
		";	
		$issuePrint = "
				<p>If this is the wrong athlete, please click no and double check the information you typed with the flyer you recieved.</p>
				<p>If this is still the wrong athlete, please contact an administrator.</p>
		";	
		$successPrint = "
				<p>Great, please select your name from the list so we can post your donation to the correct athlete.</p>
		";	
		
		$contacts = $Player->getAllContacts();
		$selectText = "Select your name";
	}
	else {	
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($teamID);
		$teamName = $TeamMain->getTeamName();
		$teamType = $TeamMain->getTeamType();
		$namePrint = "
					<span style='font-weight:bold;' class='teamPrimaryTxtColor'>$teamName</span><br>
					<span style='font-weight:bold;' class='teamPrimaryTxtColor'>$teamType</span>?
		";	
		$issuePrint = "
				<p>If this is the wrong team, please click no and double check the information you typed with the flyer you recieved.</p>
				<p>If this is still the wrong team, please contact an administrator.</p>
		";	
		$successPrint = "
				<p>Great, please select the athlete's name from the list so we can post your donation to the correct athlete.</p>
		";	
		
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
		$contacts = TeamUser::getAllPlayersNameSort($teamID);
		$selectText = "Select the athlete's name";
	}
	
	
	echo "
		<script>
			$(document).ready(function() { 
				$('#btnPlayerYes').click(function(event) {
					$('#contactList').show();
					$('#playerButtonsDiv').hide();
					$('#loginDiv').show();
					$('#noteOne').hide();
					event.preventDefault();
				});
				
				$('#btnPlayerNo').click(function(event) {
					$('#loginDiv').hide();
					$('#loginForm').show();	
					event.preventDefault();
				});
			});
		</script>
	
		<div id='donationMain'>
			<p style='font-weight:bold;'>Welcome</p>
			<div style='float:left;'>
				Are you here to make a donation for<br>
				$namePrint
			</div>
			<div style='float:left;margin-left:15px;margin-right:5px;'>
				<a href='#' id='btnPlayerYes' class='teamButton teamPrimaryBGColor'>Yes</a>
			</div>
			<div id='playerButtonsDiv' style='float:left;'>
				<a href='#' id='btnPlayerNo' class='teamButton teamPrimaryBGColor'>No</a>
			</div>
			<div class='clear'></div> 
			
			<div id='noteOne'>
			<br />
				$issuePrint
			</div>
			
			<div id='contactList'>
				<br />
				$successPrint
				<br />
				<form name='frmSel' action='index.php' method='post'>
					<select name='donor_set' style='text-transform:capitalize;'>
						<option value='null' style='text-transform:capitalize;'>$selectText</option>
			
	";
				foreach($contacts AS $contact) {
					$cID = $contact['ID'];
					$fname = $contact['fname'];
					$lname = $contact['lname'];
					echo "<option value='$cID' style='text-transform:capitalize;'>$lname, $fname </option>";
				}
	echo "
					</select>
					<input type='hidden' name='submitAction' value='donorLogin' />
					&nbsp; <a href='javascript:document.frmSel.submit()' class='teamButton teamPrimaryBGColor'>Select</a>
				</form>
			</div>
		</div>
	";
	echo "
		<script type='text/javascript'> 
			hideLoginForm();
		</script>
	";


?>