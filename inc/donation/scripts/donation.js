// JavaScript Document

$(document).ready(function() { 

	$(".paymentMethod").change(function(){
		if ($("input[@name='paymentMethod']:checked").val() == 'online') {
			$('#paymentDiv').show();
			$('#paypal1').show();
			$('#paypal2').show();
			$('#paypal3').show();
			$('#paypalFrm').hide();
		}
		if ($("input[@name='paymentMethod']:checked").val() == 'check') {
			$('#paymentDiv').hide();
			$('#paypal1').show();
			$('#paypal2').show();
			$('#paypal3').show();
			$('#paypalFrm').hide();
		}
		if ($("input[@name='paymentMethod']:checked").val() == 'paypal') {
			$('#paymentDiv').hide();
			$('#paypal1').hide();
			$('#paypal2').hide();
			$('#paypal3').hide();
			$('#paypalFrm').show();
		}
	});

	$(".location").change(function(){
		if ($("input[@name='location']:checked").val() == 'domestic') {
			$('#paymentDiv').show();
		}
		if ($("input[@name='location']:checked").val() == 'intl') {
			$('#paymentDiv').hide();
		}
	});

	$(".intl").change(function(){
		var checked = $(this).attr('checked');
		if (checked) {
			$('#intlAddy').show();
			$('#domesticAddy').hide();
			$('#intlAddy2').show();
			$('#domesticAddy2').hide();
			$('#intlAddy3').show();
		}
		else {
			$('#domesticAddy').show();
			$('#intlAddy').hide();
			$('#domesticAddy2').show();
			$('#intlAddy2').hide();
			$('#intlAddy3').hide();
		}
	});


	$('#frmSubmit').click(function(event) {
		$('#frm').submit();
		event.preventDefault();
	});
	
	
    var options = { 
        target:        '#paymentPopup',   // target element(s) to be updated with server response 
        beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse  // post-submit callback 
    }; 
 
    // bind form using 'ajaxForm' 
    $('#frm').ajaxForm(options); 
 
	// pre-submit callback 
	function showRequest(formData, jqForm, options) { 
		var val = validate();
		//var val = true;
		if (val)
		{
			$('#paymentPopup').jqmShow();
			return true;
		}
		else
		{
			return false;
		}
	} 
 
	// post-submit callback 
	function showResponse(responseText, statusText, xhr, $form)  { 
		$('#paymentPopup').jqmHide();
		$('#frmProcess').submit();
	} 	
	


//------------------------------------------------------------------------------
// Begin Modal Functions

	$('#paymentPopup').jqm({
		//trigger: 'a.frmSubmit',
		modal: true, /* FORCE FOCUS */
		overlay: 50,
		onShow: function(h) {
			//timelinePopupDataFill(h);
			//$('div#timelineData').text("");
			/* callback executed when a trigger click. Show window */
			h.w.fadeIn();
		},
		onHide: function(h) {
			h.w.fadeOut("slow",function() { if(h.o) h.o.remove(); });
		}
	});
	
	
// End Modal Functions
//------------------------------------------------------------------------------

	function validate() { 
		var msg = "Please complete the highlighted items.";
		error_check = false; 
				
		if (!checkRadio("frm","paymentMethod")) {
			error_check = true;
			$("#paymentRow1").addClass("fieldEmpty");
		}
		else 
			$("#paymentRow1").removeClass("fieldEmpty");
		
		if ($("input[name='paymentMethod']:checked").val() == 'online') {
			$(".validate2").each(function (i) {
				if (this.value == "") {
					msg = "Please complete the highlighted items.";
					error_check = true;
					$(this).addClass("fieldEmpty");
				} else {
					$(this).removeClass("fieldEmpty");
				}
			});
		}
		
		if (!checkRadio("frm","donationValue")) {
			error_check = true;
			$("#paypal1").addClass("fieldEmpty");
		}
		else 
			$("#paypal1").removeClass("fieldEmpty");
		
		if ($("input[name='paymentMethod']:checked").val() == 'paypal') {
		
			if (!checkRadio("frm","donationValueP")) {
				error_check = true;
				$("#paypal1a").addClass("fieldEmpty");
			}
			else 
				$("#paypal1a").removeClass("fieldEmpty");
		}
		
		if ($("input[name='donationValue']:checked").val() == 'custom') {
			var customField = $("input[name='donationValueCustom']");
			
			var value = customField.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			if( !IsNumeric(value) ) {
				msg = "Donation amount must be numeric.";
				error_check = true;
				customField.addClass("fieldEmpty");
			}
			
			if (customField.val() == '') {
				msg = "Please enter a donation amount.";
				error_check = true;
				customField.addClass("fieldEmpty");
			}
		}
		
		if ($("input[name='donationValueP']:checked").val() == 'custom') {
			var customField = $("input[name='donationValueCustomP']");
			
			var value = customField.val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
			if( !IsNumeric(value) ) {
				msg = "Donation amount must be numeric.";
				error_check = true;
				customField.addClass("fieldEmpty");
			}
			
			if (customField.val() == '') {
				msg = "Please enter a donation amount.";
				error_check = true;
				customField.addClass("fieldEmpty");
			}
		}
		
		
		var checked = $("#intl").attr('checked');
		if (checked) {
			$(".validate3").each(function (i) {
				if (this.value == "") {
					msg = "Please complete the highlighted items.";
					error_check = true;
					$(this).addClass("fieldEmpty");
				} else {
					$(this).removeClass("fieldEmpty");
				}
			});
			$(".validate4").each(function (i) {
				$(this).removeClass("fieldEmpty");
			});
			$(".validate5").each(function (i) {
				$(this).removeClass("fieldEmpty");
			});
		}
		else {
			$(".validate4").each(function (i) {
				if (this.value == "") {
					msg = "Please complete the highlighted items.";
					error_check = true;
					$(this).addClass("fieldEmpty");
				} else {
					$(this).removeClass("fieldEmpty");
				}
			});
			$(".validate3").each(function (i) {
				$(this).removeClass("fieldEmpty");
			});
			
//			var p1 = $("input[name='p1']");
//			var p2 = $("input[name='p2']");
//			var p3 = $("input[name='p3']");
//			var p1v = p1.val();
//			var p2v = p2.val();
//			var p3v = p3.val();
//			
//			if( !IsNumeric(p1v) ) {
//				msg = "Phone number must be numeric.";
//				error_check = true;
//				p1.addClass("fieldEmpty");
//			} else 
//				p1.removeClass("fieldEmpty");
//				
//			if( !IsNumeric(p2v) ) {
//				msg = "Phone number must be numeric.";
//				error_check = true;
//				p2.addClass("fieldEmpty");
//			} else 
//				p2.removeClass("fieldEmpty");
//				
//			if( !IsNumeric(p3v) ) {
//				msg = "Phone number must be numeric.";
//				error_check = true;
//				p3.addClass("fieldEmpty");
//			} else 
//				p3.removeClass("fieldEmpty");
		}
		
		
		$(".validate").each(function (i) {
			if (this.value == "") {
				msg = "Please complete the highlighted items.";
				error_check = true;
				$(this).addClass("fieldEmpty");
			} else {
				$(this).removeClass("fieldEmpty");
			}
		});
			
		if (error_check) {
			alert(msg);
			return false;
		}
		else
		{
			return true;
		}
	}

});

function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}

function checkRadio (frmName, rbGroupName) {
	var radios = document[frmName].elements[rbGroupName];
	for (var i=0; i <radios.length; i++) {
		if (radios[i].checked) {
			return true;
		}
	}
	return false;
}

