<?php
	$data = array(
		'bodyClass' => 'bodyBkgLogin',
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/login.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.form.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/donation/scripts/login.js"></script>
		');
	
    startToMainHeader($data);
?>    
<style>
    .masthead{ 
    }
    .bodyBkgLogin{
        background:none;
    }
</style>




<div class="container-fluid">
    <div class='loginWrap' id='loginWrap'>
                    
        <div class='welcomeBarLogin'>
            <br>
            <center><h1 class="logintitle dview" style="color: #275a7e; ">Welcome to Team Login Page</h1></center>
            <h1 class="logintitle mview" style="color: #275a7e; width: 340px; ">DONOR</h1>
        </div>   
        
                <div id='loginDiv'><?php if (isset($error)) echo $error; ?></div>
                <div id='loginForm'>
                    <!--
                    <form id="frmLogin" name="frm" action="index.php?action=login" method="post"> -->
                    <form id="frmLogin" name="frm" method="post">

                        <p><input id="teamID" name="teamID" type="text" class="loginTextField" value="<?php if(isset($_GET['tid'])){echo $_GET['tid'];}?>" placeholder='Team ID#' /></p>

                        <p><input id="playerID" name="playerID" type="text" class="loginTextField" value="<?php if(isset($_GET['pid'])){echo $_GET['pid'];}?>" placeholder='Password' /></p>
                        <br />
                        <p><a href="#" id="btnLogin" class="" style="
    background: #fda527;
    padding: 15px 154px;
    color: white;
    font-size: large;
">Log-in</a></p>
                    </form>
                    <br>
                    <br>
                    <br>
                    <br>                    
                    <center><p style="
    font-size: 14px;
    font-weight: bold;
    color: #c1ced8;
">PLEASE CLICK <u>HERE</u> IF YOU DON'T HAVE YOUR TEAM ID/PASSWORD</p></center>
                </div>


    </div>
</div>    

            <div class='mobi-help'>
                <br>
                <br>

                <hr>
               <p style="
    text-align: center;
">Need help? <a href="tel:8775111555"><i class="fas fa-phone"></i></a> <a href="mailto:info@campaignsports.com"><i class="fas fa-envelope"></i></a> <a href="tel:8775111555"><i class="fas fa-mobile-alt"></i></a></p>
       
            </div>


            <div class='teamFooterADonor iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class=' . $n1 .'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=addContact' class=' . $n2 .'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>SELECT ATHLETE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorList' class=' . $n5 . '><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONATE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class=' . $n2 . '><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
	
			
<script>
$(document).ready(function() { 

	var queryString = new Array();
	if (queryString.length == 0) {
            if (window.location.search.split('?').length > 1) {
                var params = window.location.search.split('?')[1].split('&');
                for (var i = 0; i < params.length; i++) {
                    var key = params[i].split('=')[0];
                    var value = decodeURIComponent(params[i].split('=')[1]);
                    queryString[key] = value;
                	}
            	}
    }
	var teamCheck = queryString["tid"];
	var playerCheck = queryString["pid"];
	//alert(teamCheck);
	if((teamCheck!='') && (playerCheck!='')) {
	
	//$('#teamID').val(queryString["tid"]);
	//$('#playerID').val(queryString["pid"]);
	$('#frmLogin').attr("action", "index.php?action=login");
	$('#loginDiv').show();
	$('#frmLogin').submit();
	event.preventDefault();
	}
	});
	</script>



<?php
	closePageWrapToEnd();
?>