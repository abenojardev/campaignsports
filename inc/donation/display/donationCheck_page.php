<?php

	if ( isset($post['submitAction']) && $post['submitAction'] == 'adminManualCheckDonation') {

		$_SESSION['current_folder'] = $post['teamID'];

		$_SESSION['player_id'] = $post['playerID'];

		$_SESSION['donation_user'] = $post['playerID'];

	}



	$data = array(

		'bodyClass' => 'bodyBkgLogin',

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '

			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validateDonation.css" />

			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/donation.css" />

		',

		'js' => '

			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>

			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jqModal.js"></script>

			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.form.js"></script>

			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/donation/scripts/donationCheck.js"></script>

		');

	

    startToMainHeader($data);

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');



	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');

	$states = Common::getStates();

	$countries = Common::getCountries();



	$teamID = $_SESSION['current_folder'];

	if ( $teamID == "1000" || $teamID == "1002" ) {

		include_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');

		$Player = new Player($_SESSION['player_id']);

		$player_id = $_SESSION['player_id'];

		$playerName = $Player->getName();

		$contactInfo = $Player->getContact($_SESSION['donation_user']);

		if($contactInfo['intl']) {

			$intlCheck = "checked='checked'";

			$domStyle = "style='display:none;'";

			$intlStyle = "";

		}

		else {

			$intlCheck = "";

			$intlStyle = "style='display:none;'";

			$domStyle = "";

		}

	}

	else {

		include_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');

		$Player = new Player($_SESSION['donation_user']);

		$playerName = $Player->getName();

		$player_id = $_SESSION['donation_user'];



		$intlCheck = "";

		$intlStyle = "style='display:none;'";

		$domStyle = "";

	}



	include_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

	$TeamMain = new TeamMain($teamID);

	$teamName = $TeamMain->getTeamName() . ", " .$TeamMain->getTeamType();



	

	

?>

<script language="javascript">

<!--//

	function submitform()

	{

		var AmountValue = '';

		for (j = 0; j < 6; j++){

			if (document.forms['paypal_frm'].donationValueP[j].checked == true){

				//alert('t1:' + document.forms['paypal_frm'].donationValueP[j].value);

				AmountValue = document.forms['paypal_frm'].donationValueP[j].value;

				

				if(AmountValue=='custom') {

					AmountValue=document.paypal_frm.donationValueCustomP.value;

					document.paypal_frm.amount.value=AmountValue;

				}

				else

				{

					AmountValue=document.paypal_frm.donationValueP[j].value;

					document.paypal_frm.amount.value=AmountValue;	

				}

			}

		}

		//alert('t2:' + AmountValue);

		document.paypal_frm.submit();

	}

//-->

</script>



<div class="pageContentWrap teamSecondaryBGColor">

  <div class="innerPageContentWrap teamPrimaryBGColor">

    <div class="pageContent">

      <?php showteamHeader(); ?>

      

      

      <div class="topContent">

      

        <div class="welcomeBar">

            <div class="welcomeBarCol1">&nbsp;</div>

            <div class="welcomeBarCol2">Today's date is: <span class="teamPrimaryTxtColor"><?php echo date('F d, Y'); ?></span> <br />

            <div class="logout"><a href="index.php?action=logout">[logout]</a></div>

            </div>

        </div>

        

          <div class="clear"></div>

          

          <div class="welcomeBarLogin">

            <h1><span class="teamPrimaryTxtColor"><?php echo $playerName ?></span> thanks you for your support!</h1>

          </div>

          

          <div class="clear"></div>

        </div><!-- /topContent-->

                    

        <div class="contentFull">

        <div class="donateWrap">

          

          <form method="post" id="frm" name="frm" action="processPayment.php">

          

          <div class="paymentWrap">

          

              <div class="paymentRow paymentRowRadio" id="paymentRow1">

                <div class="paymentRowCol1"><strong class="teamPrimaryTxtColor">Payment Method:</strong></div>

                <div class="paymentRowCol2">

                  <input class="paymentMethod" name="paymentMethod" type="radio" value="check" checked="checked"/>

                  Pay by check </div>

                <div class="clear"></div>

              </div><!-- / paymentRow -->

              

                <div class="paymentRow paymentRowRadio" id="paypal1">

                  <div class="paymentRowCol1"><strong class="teamPrimaryTxtColor">Select Gift Amount:</strong></div>

                  <div class="paymentRowCol2">

                    <input class="donationValue" name="donationValue" type="radio" value="25" />

                    $25.00 <br />

                    <input class="donationValue" name="donationValue" type="radio" value="50" />

                    $50.00 <br />

                    <input class="donationValue" name="donationValue" type="radio" value="100" />

                    $100.00 <br />

                    <input class="donationValue" name="donationValue" type="radio" value="250" />

                    $250.00 <br />

                    <input class="donationValue" name="donationValue" type="radio" value="500" />

                    $500.00 <br />

                    <input class="donationValue" name="donationValue" type="radio" value="custom" />

                    Enter an amount <br />

                    &nbsp;&nbsp;&nbsp; $

                    <input name="donationValueCustom" type="text" />

                  </div>

                  <div class="clear"></div>

                </div><!-- / paymentRow -->

                

              

        <div class="payByCheckBox">

          <p><strong>Please note:</strong> If you would like to make a donation by check, please send to:</p>

          <p><?php echo $teamName ?> <br />

            c/o Campaign Sports<br />

            PO Box 527<br />

            Tennent, NJ 07763</p>

          <p>Please include the athlete and team name on the check.</p>

        </div><!-- / payByCheckBox -->

        

        

        <div class="clear"></div>

      </div><!-- /paymentWrap -->

      

      <div class="clear"></div>

      <div class="paymentRow" id="paypal2">

        <p><strong class="teamPrimaryTxtColor">Billing Information:</strong></p>

        <table width="100%" border="0" cellspacing="0" cellpadding="5">

          <tr>

            <td colspan='4'><input id="intl" class="intl" name="intl" type="checkbox" value=1 <?php echo $intlCheck ?> />

              International</td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td width="85px"><strong>First Name:</strong></td>

            <td width="140px"><input name="fname" class='' type="text" value="<?php if (isset($contactInfo['fname'])) echo $contactInfo['fname'] ?>" /></td>

            <td width="125px"><strong>Last Name:</strong></td>

            <td width="160px"><input name="lname" class='' type="text" value="<?php if (isset($contactInfo['lname'])) echo $contactInfo['lname'] ?>" /></td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td><strong>Address 1:</strong></td>

            <td><input name="address" class='' type="text" value="<?php if (isset($contactInfo['address'])) echo $contactInfo['address'] ?>" /></td>

            <td><strong>Address 2:</strong></td>

            <td><input name="address2" type="text" value="<?php if (isset($contactInfo['address2'])) echo $contactInfo['address2'] ?>" /></td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

          </tr>

          <tr id="domesticAddy" <?php echo $domStyle ?> >

            <td><strong>City:</strong></td>

            <td><input name="city" class='' type="text" value="<?php if (isset($contactInfo['city'])) echo $contactInfo['city'] ?>" /></td>

            <td><strong>State:</strong></td>

            <td><select id='state' name='state' class='' size='1'>

                <option value=''>Please select...</option>

                <?php

                                            foreach($states as $state)

                                            {

                                                echo "<option value='".$state['abbrev']."'";

                                                echo ">".$state['name']."</option>";

                                            }

                                        ?>

              </select></td>

            <td><strong>Zip:</strong></td>

            <td><input name="zip" class='' type="text" size="10" value="<?php if (isset($contactInfo['zip'])) echo $contactInfo['zip'] ?>" /></td>

          </tr>

          <tr id="intlAddy" <?php echo $intlStyle ?> >

            <td><strong>City:</strong></td>

            <td><input name="cityI" class='' type="text" value="<?php if (isset($contactInfo['city'])) echo $contactInfo['city'] ?>" /></td>

            <td><strong>Province/Region:</strong></td>

            <td><input name="stateI" class='' type="text" value="<?php if (isset($contactInfo['state'])) echo $contactInfo['state'] ?>" /></td>

            <td><strong>Postal Code:</strong></td>

            <td><input name="zipI" class='' type="text" size="10" value="<?php if (isset($contactInfo['zip'])) echo $contactInfo['zip'] ?>" /></td>

          </tr>

          <tr id="intlAddy3" <?php echo $intlStyle ?> >

            <td><strong>Country:</strong></td>

            <td colspan="3"><select id='country' name='country' class='' size='1'>

                <option value=''>Please select...</option>

                <?php

                                            foreach($countries as $country)

                                            {

                                                echo "<option value='".$country['abbrev']."'";

                                                echo ">".$country['name']."</option>";

                                            }

                                        ?>

              </select></td>

          </tr>

          <tr id="domesticAddy2" <?php echo $domStyle ?> >

            <td><strong>Email:</strong></td>

            <td><input name="email" type="text" value="<?php if (isset($contactInfo['email'])) echo $contactInfo['email'] ?>" /></td>

            <td><strong>Phone:</strong></td>

            <td><input name="p1" type="text" size="3" maxlength="3" />

              -

              <input name="p2" class='validate5' type="text" size="3" maxlength="3" />

              -

              <input name="p3" class='validate5' type="text" size="4" maxlength="4" /></td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

          </tr>

          <tr id="intlAddy2" <?php echo $intlStyle ?> >

            <td><strong>Email:</strong></td>

            <td><input name="emailI" type="text" value="<?php if (isset($contactInfo['email'])) echo $contactInfo['email'] ?>" /></td>

            <td><strong>Phone:</strong></td>

            <td><input name="phoneI" type="text" /></td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

          </tr>

          <tr>

            <td colspan='4'><span class="alert">*</span> Please include an email if you would like to receive an email receipt.</td>

            <td>&nbsp;</td>

            <td>&nbsp;</td>

          </tr>

        </table>

      </div><!-- /paymentRow -->

      

      <div class="registerButton" id="paypal3"><br />

        <br />

        <p><a href="#" id="frmSubmit" class="teamButton teamPrimaryBGColor">Donate Now</a></p>

      </div>

      

      </form>

      

      <div class="clear"></div>

    </div>

    <!-- /donateWrap -->

    

    <div class="clear"></div>

  </div>

  <!-- /contentFull -->

  
<div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
  <div class="clear"></div>

</div>

<!-- /pageContent -->



<div class="clear"></div>

</div>

<!-- /innerPageContentWrap -->



<div class="clear"></div>

</div>

<!-- /pageContentWrap -->



<div class="jqmWindow" id="paymentPopup">

  <div class="preloader"> Please wait: System Processing... <br />

    <br />

    <img src='<?php echo $_SESSION['relative_path'] ?>images/ajax-loader.gif' width='128' height='15' border='0' alt='' /> </div>

</div>

<?php

	closePageWrapToEnd();

?>

