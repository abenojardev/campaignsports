<?php

	$data = array(

		'bodyClass' => '',

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '

		',

		'js' => '

		');



    startToMainHeader($data);

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	

	$teamID = $_SESSION['current_folder'];

	if ( $teamID == "1000" || $teamID == "1002" ) {

		include_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');

		$Player = new Player($_SESSION['player_id']);

		$playerName = $Player->getName();

	}

	else {

		include_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');

		$Player = new Player($_SESSION['donation_user']);

		$playerName = $Player->getName();

	}

	

	include_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

	$TeamMain = new TeamMain($teamID);

	$teamName = $TeamMain->getTeamName() . ", " .$TeamMain->getTeamType();

	

?>    

    

    <div class="pageContentWrap teamSecondaryBGColor">

    

    	<div class="innerPageContentWrap teamPrimaryBGColor">

        

        	<div class="pageContent">

            

            <?php showteamHeader(); ?>

			

            <div class="topContent">

            	<div class="welcomeBar">

                    <div class="welcomeBarCol1">&nbsp;</div>

                    <div class="welcomeBarCol2">Today's date is: <span class="teamPrimaryTxtColor"><?php echo date('F d, Y'); ?>
 </span></div>

                </div>

                

                <div class="clear"></div>

                

                <div class="welcomeBarLogin">

                    <h1><span class="teamPrimaryTxtColor"><?php echo $playerName ?></span> thanks you for your support!</h1>

                    <h2>Donation Confirmation Page</h2>

                </div>

                

                <div class="clear"></div>

                <br /><br />

                

                <?php

                

					//include_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');

					//$post['taStage'] = "pre result";

					//Common::taDonorTesting($post);

				

                    // Online Payment

                    if ( $post['paymentMethod'] == 'online' ) {

                       if ( $post['r_error'] != '' ) {

						   echo "

						   <p><strong class='alert'>ERROR: There was an issue with your credit card transaction.</strong></p>

						   <p><strong>System Message:</strong> " . $post['r_error'] . "</p>

						   <p>&nbsp;</p>

						   <p><a href='index.php'>Click here to return and try again</a></p>

						   <p>&nbsp;</p>

						   <p>&nbsp;</p>

						   ";



					   }

					   else if ( $post['r_approved'] == 'APPROVED' ) {

							if($post['donationValue']!='custom') {

							$donValue = $post['donationValue'];

							} else {

								$donValue = $post['donationValueCustom'];

							}

							

							$email = (isset($post['intl']) && $post['intl']) ? $post['emailI'] : $post['email'];

							$city = (isset($post['city']) && $post['city']) ? $post['city'] : $post['cityI'];

							$state = (isset($post['state']) && $post['state']) ? $post['state'] : $post['stateI'];

							$zip = (isset($post['zip']) && $post['zip']) ? $post['zip'] : $post['zipI'];

							$country = (isset($post['country']) && $post['country']) ? "<br />Country: " . $post['country'] : "";

						   

							echo "

								<p><strong class='green'>SUCCESS: Thank you for your donation!</strong></p>

								<p><strong>System Message:</strong> " . $post['r_approved'] . "</p>

								<p><strong>Your Reference Number is:</strong> <span class='green'>" . $post['r_ref'] . "</span></p>

								

								<p style='text-transform:capitalize'>Name: ".$post['fname']." ".$post['lname']."<br />

								Address: ".$post['address']."<br /> 

								Address2: ".$post['address2']."<br /> 

								City: ".$city."<br /> 

								State/Province/Region: ".$state."<br /> 

								Zip/Postal Code: ".$zip."

								$country</p> 

								<p style='text-transform:capitalize'>Card Type: ".$post['cardType']."<br />

								Transaction Status: ".$post['r_approved']."<br />

								Reference Number: ".$post['r_ref']."

								</p>

								<p>Amount: $".$donValue."</p>

								

								<p>&nbsp;</p>

								<p><em>A receipt of this transaction will be sent to your email.</em></p>

								<p>&nbsp;</p>
								
								<p><a href='index.php?action=logout'>PLEASE CLICK HERE TO CONTINUE</a></p>
<p>&nbsp;</p>
								<SCRIPT LANGUAGE='JavaScript'> 

								// This script was supplied free by Hypergurl // http://www.hypergurl.com <!-- 

								hide script and begin if (window.print) { document.write('<form>Click Here 

								To ' + '<input type=button name=print value=\"Print\" ' + 'onClick=\"javascript:window.print()\"> 

								This Page!</form>'); } // End hide --> </script>							

							";

							GLOBAL $cfg_adminEmail;
							if ( isset($email) && $email != "" ) {
								$mailData['to'] = $email;
								$mailData['subject'] = "Thank you for supporting ". $name . " " . $team . "!";
								$mailData['from'] = $cfg_adminEmail;
								$mailData['messageHTML'] =
									"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta name='viewport' content='width=device-width; initial-scale=1.0;'>
<style type='text/css'>
* {
	-webkit-font-smoothing: antialiased;
	line-height: 100%;
}
div, span, p, a, li, td, strong {
	-webkit-text-size-adjust: none;
}
</style>
<title>Campaign Sports - Where You Always Finish First!</title>
</head>

<body>
<table border='0' cellpadding='0' cellspacing='0' width='550' style='border:1px solid #ccc;'>
  <tr>
    <td align='left' valign='top'><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='1' height='25' border='0'><br>
      <table width='550' border='0' cellspacing='0' cellpadding='0'>
        <tr>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
          <td><table width='550' border='0' cellspacing='0' cellpadding='0'>
              <tr>
                <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/campaign-sports.jpg' width='550' height='108' /></td>
              </tr>
            </table></td>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
        </tr>
        <tr>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
          <td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:0.75em; color:#666; line-height:1.25em;'><br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='20' border='0'><br>
            Greetings <span style='text-transform:uppercase;'>".$post['fname']."</span>, <br><br>
            <strong>Thank you for supporting ". $name . " " . $team . "!. </strong> <br><br>
            We want to sincerely <span style='text-decoration:underline'>THANK YOU</span> for your donation of $".$donValue.". Every dollar gets us that much closer to accomplishing our goals this year. <br><br>
            <strong>We couldn't do it without you! </strong> <br><br>
            Our team would greatly appreciate it if you could take a few minutes to show your support for <strong>Campaign Sports</strong> by liking them on <a href='https://www.facebook.com/CampaignSports' style='color:#115a7f;' target='_blank'>Facebook</a>, following them on <a href='https://twitter.com/intent/user?screen_name=CampaignSports1' style='color:#115a7f;' target='_blank'>Twitter</a>, and following them on <a href='https://www.linkedin.com/company/campaign-sports-llc' style='color:#115a7f;' target='_blank'>LinkedIn</a>. (They designed, printed and mailed our message and were vital in helping us make this fundraiser a SUCCESS!) <br><br>
            Thank you! <br><br>
            Sincerely, <br><br>
            ". $name . " " . $team . "<br />
            P.S. You can find your donation receipt information below. <br><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='35' border='0'><br>
            <strong>Follow Campaign Sports Online</strong> <br><br>
                  <a href='http://www.CampaignSports.com' style='color:#115a7f;' target='_blank'>www.CampaignSports.com</a> <br><br>
                  <a href='https://www.facebook.com/CampaignSports'><img src='https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif' width='25' height='26' border='0' alt='Campaign Sports on Facebook'></a>&nbsp;&nbsp; <a href='https://www.twitter.com/CampaignSports1'><img src='https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif' width='25' height='26' border='0' alt='Campaign Sports on Twitter'></a>&nbsp;&nbsp; <a href='https://www.linkedin.com/company/campaign-sports-llc'><img src='https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif' width='25' height='26' border='0' alt='Campaign Sports on Twitter'></a>
                  
<br><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='20' border='0'><br>                  
            <hr style='color:#CCC;'>
            <br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='10' border='0'><br>
            <h3 style='margin-top:0px;'>Your Receipt</h3>
            Please keep this receipt for your records.<br><br>
            <strong>Name:</strong> ".$post['fname']." ".$post['lname']." <br><br>
            <strong>Address 1:</strong> ".$post['address']." <br><br>
            <strong>Address2:</strong> ".$post['address2']." <br><br>
            <strong>City:</strong> ".$city." <br><br>
            <strong>State/Province/Region:</strong> ".$state." <br><br>
            <strong>Zip/Postal Code:</strong> ".$zip." $country<br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='35' border='0'><br>
            <strong>Card Type:</strong> ".$post['cardType']." <br><br>
            <strong>Transaction Status:</strong> ".$post['r_approved']."<br><br>
            <strong>Reference Number:</strong> ".$post['r_ref']."<br><br>
            <strong>Donation Amount:</strong> $".$donValue." <br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='30' border='0'><br>
            <hr style='color:#CCC;'>
            </td>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
        </tr>
        <tr>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
          <td><table width='550' border='0' cellspacing='0' cellpadding='0'>

              <tr>
                <td align='center' style='font-family:Arial, Helvetica, sans-serif; font-size:0.75em; color:#666; line-height:1.25em;'>&copy; 2018 Campaign Sports - Manalapan, New Jersey and Delray Beach, Florida - All rights reserved. </td>
              </tr>
            </table></td>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
        </tr>
      </table>
      <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='30' border='0'><br></td>
  </tr>
</table>
</body>
</html>";

								$mail = Mail::send_email($mailData);
							}
							
							$mailData2['to'] = $cfg_adminEmail;
							$mailData2['subject'] = "Online payment received for: ". $name . " " . $team . "!";
							
							$mailData2['from'] = $cfg_adminEmail;
							$mailData2['messageHTML'] =
								"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<meta name='viewport' content='width=device-width; initial-scale=1.0;'>
<style type='text/css'>
* {
	-webkit-font-smoothing: antialiased;
	line-height: 100%;
}
div, span, p, a, li, td, strong {
	-webkit-text-size-adjust: none;
}
</style>
<title>Campaign Sports - Where You Always Finish First!</title>
</head>

<body>
<table border='0' cellpadding='0' cellspacing='0' width='550' style='border:1px solid #ccc;'>
  <tr>
    <td align='left' valign='top'><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='1' height='25' border='0'><br>
      <table width='550' border='0' cellspacing='0' cellpadding='0'>
        <tr>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
          <td><table width='550' border='0' cellspacing='0' cellpadding='0'>
              <tr>
                <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/campaign-sports.jpg' width='550' height='108' /></td>
              </tr>
            </table></td>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
        </tr>
        <tr>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
          <td valign='top' style='font-family:Arial, Helvetica, sans-serif; font-size:0.75em; color:#666; line-height:1.25em;'><br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='20' border='0'><br>
            Greetings <span style='text-transform:uppercase;'>".$post['fname']."</span>, <br><br>
            <strong>Thank you for supporting ". $name . " " . $team . "!. </strong> <br><br>
            We want to sincerely <span style='text-decoration:underline'>THANK YOU</span> for your donation of $".$donValue.". Every dollar gets us that much closer to accomplishing our goals this year. <br><br>
            <strong>We couldn't do it without you! </strong> <br><br>
            Our team would greatly appreciate it if you could take a few minutes to show your support for <strong>Campaign Sports</strong> by liking them on <a href='https://www.facebook.com/CampaignSports' style='color:#115a7f;' target='_blank'>Facebook</a>, following them on <a href='https://twitter.com/intent/user?screen_name=CampaignSports1' style='color:#115a7f;' target='_blank'>Twitter</a>, and following them on <a href='https://www.linkedin.com/company/campaign-sports-llc' style='color:#115a7f;' target='_blank'>LinkedIn</a>. (They designed, printed and mailed our message and were vital in helping us make this fundraiser a SUCCESS!) <br><br>
            Thank you! <br><br>
            Sincerely, <br><br>
            ". $name . " " . $team . "<br />

            P.S. You can find your donation receipt information below. <br><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='35' border='0'><br>
            <strong>Follow Campaign Sports Online</strong> <br><br>
                  <a href='http://www.CampaignSports.com' style='color:#115a7f;' target='_blank'>www.CampaignSports.com</a> <br><br>
                  <a href='https://www.facebook.com/CampaignSports'><img src='https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif' width='25' height='26' border='0' alt='Campaign Sports on Facebook'></a>&nbsp;&nbsp; <a href='https://www.twitter.com/CampaignSports1'><img src='https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif' width='25' height='26' border='0' alt='Campaign Sports on Twitter'></a>&nbsp;&nbsp; <a href='https://www.linkedin.com/company/campaign-sports-llc'><img src='https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif' width='25' height='26' border='0' alt='Campaign Sports on Twitter'></a>
                  
<br><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='20' border='0'><br>                  
            <hr style='color:#CCC;'>
            <br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='10' border='0'><br>
            <h3 style='margin-top:0px;'>Your Receipt</h3>
            Please keep this receipt for your records.<br><br>
            <strong>Name:</strong> ".$post['fname']." ".$post['lname']." <br><br>
            <strong>Address 1:</strong> ".$post['address']." <br><br>
            <strong>Address2:</strong> ".$post['address2']." <br><br>
            <strong>City:</strong> ".$city." <br><br>
            <strong>State/Province/Region:</strong> ".$state." <br><br>
            <strong>Zip/Postal Code:</strong> ".$zip." $country<br>
            <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='35' border='0'><br>
            <strong>Card Type:</strong> ".$post['cardType']." <br><br>
            <strong>Transaction Status:</strong> ".$post['r_approved']."<br><br>
            <strong>Reference Number:</strong> ".$post['r_ref']."<br><br>
            <strong>Donation Amount:</strong> $".$donValue." <br><br>
             <hr style='color:#CCC;'>
            <p>As no goods or services were provided to you in return for your charitable contribution, its entire amount is tax-deductible to the full extent otherwise allowed by law. This letter will serve as your receipt.</p>
            <hr style='color:#CCC;'>
            </td>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
        </tr>
        <tr>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
          <td><table width='550' border='0' cellspacing='0' cellpadding='0'>

              <tr>
                <td align='center' style='font-family:Arial, Helvetica, sans-serif; font-size:0.75em; color:#666; line-height:1.25em;'>&copy; 2018 Campaign Sports - Manalapan, New Jersey and Delray Beach, Florida - All rights reserved. </td>
              </tr>
            </table></td>
          <td><img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='30' height='1' border='0'></td>
        </tr>
      </table>
      <img src='https://www.joinourcampaign.com/system_emails/donor/images/trans.gif' alt='' width='550' height='30' border='0'><br></td>
  </tr>
</table>
</body>
</html>";

							$mail2 = Mail::send_email($mailData2);

					   }

                    }            

                    // Check Payment

                    else if ( $post['paymentMethod'] == 'check' ) {

                        echo "

							<p><strong class='green'>Thank you for your support and your commitment to send a donation by check!</strong></p> 

							

							<p><strong>You can mail your check to:</strong><br />

							" . $teamName . "<br />

							c/o Campaign Sports<br />

							PO Box 527<br />

							Tennent, New Jersey, 07763</p>

							<p><em class='alert'>Please be sure to include the name of the team, and the name of the athlete you are supporting.</em></p>

							<p>&nbsp;</p>

							<p><strong>If you have any other questions, please feel free to contact us at:</strong><br />

							<strong>Toll Free:</strong> 877.511.1555<br />

							<strong>Phone:</strong> 732.474.1555<br />

							<strong>Email:</strong> info@campaignsports.com<br />

							</p>

							<p>&nbsp;</p>

						";

						

						if($post['donationValue']!='custom') {

						$donValue = $post['donationValue'];

						} else {

							$donValue = $post['donationValueCustom'];

						}

						

						$email = (isset($post['intl']) && $post['intl']) ? $post['emailI'] : $post['email'];

						$city = (isset($post['city']) && $post['city']) ? $post['city'] : $post['cityI'];

						$state = (isset($post['state']) && $post['state']) ? $post['state'] : $post['stateI'];

						$zip = (isset($post['zip']) && $post['zip']) ? $post['zip'] : $post['zipI'];

						$country = (isset($post['country']) && $post['country']) ? "<br />Country: " . $post['country'] : "";

						if ( isset($email) && $email != "" ) {

							GLOBAL $cfg_adminEmail;

							$mailData['to'] 		= $email;

							$mailData['from']		= $cfg_adminEmail;

							$mailData['subject']	= "Thank you for supporting ". $name . " " . $team . "!";

							$mailData['messageHTML']	= 

								"<p>Thank you for supporting ". $name . " " . $team . "!</p>

								<p>Please keep this receipt for your records.</p>

								<p>Name: ".$post['fname']." ".$post['lname']."<br />

								Address: ".$post['address']."<br /> 

								Address2: ".$post['address2']."<br /> 

								City: ".$city."<br /> 

								State/Province/Region: ".$state."<br /> 

								Zip/Postal Code: ".$zip."

								$country</p> 

								<p><strong>You can mail your check to:</strong><br />

								" . $teamName . "<br />

								c/o Campaign Sports<br />

								PO Box 527<br />

								Tennent, New Jersey, 07763</p>

								<p><em class='alert'>Please be sure to include the name of the team, and the name of the athlete you are supporting.</em></p>

								</p> 

								<p>Amount: $".$donValue."</p>

							";

							$mail = Mail::send_email($mailData);

						}

					}

                    // Check Payment

                    else if ( $post['custom'] == 'payPal_return' ) {

						//require_once($_SESSION['relative_path'] . 'inc/donation/class/DonationUser.php');

						//$result = DonationUser::paypalDonation($post);

						

                        echo "

							<p><strong class='green'>SUCCESS: Thank you for your PayPal donation!</strong></p>

							<p><strong>System Message:</strong> " . $post['payment_status'] . "</p>

						

							<p>Name: ".$post['first_name']." ".$post['last_name']."<br />

							Address: ".$post['address_street']."<br /> 

							Address2: ".$post['address2']."<br /> 

							City: ".$post['address_city']."<br /> 

							State/Province/Region: ".$post['address_state']."<br /> 

							Zip/Postal Code: ".$post['address_zip']."<br /> 

							Country: ".$post['address_country']."

							</p> 

							<p>PayPal email: ".$post['payer_email']."<br />

							Payment Status: ".$post['payment_status']."<br />

							Reference Number: ".$post['txn_id']."

							</p>

							<p>Amount: $".$post['mc_gross']."</p>



							<p><em>A receipt of this transaction will be sent to your email by PayPal.</em></p>

								
								<p><a href='index.php?action=logout'>PLEASE CLICK HERE TO CONTINUE</a></p>
<p>&nbsp;</p>

						";

						

                    }

					

					//include_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');

					//$post['taStage'] = "post result";

					//Common::taDonorTesting($post);

					

                ?>

            

            </div>

<div class='clear'></div>
    <div class="playerSSLSeal" style="padding-top:10px;"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>

        	<div class="clear"></div>

            </div>

            <!-- /pageContent -->

        

        <div class="clear"></div>

        </div>

        <!-- /innerPageContentWrap -->

    

    <div class="clear"></div>

    </div>

	<!-- /pageContentWrap -->

    



<?php

	closePageWrapToEnd();

?>