<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	$costs = Designer::getCosts($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	$designHours = $costs['design_hours'];
	$designRate = $costs['design_rate'];
	$adjustments = $costs['adjustments'];
	$addidtionalDetails = $costs['addidtional_details'];
	$total = $designHours * $designRate + $adjustments;

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<form method='post' name='frm' id='frm' action='index.php?nav=viewCampaign&sNav=designCosts&tID=" .$campaign['ID']. "'>
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Design Costs</h2>
				
				<div class='genFloatL' style='width:100px;margin-top:10px;margin-right:10px;'>
					<strong># of Hours</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField' name='designHours' size='10' value='$designHours' />
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='width:100px;margin-top:10px;margin-right:10px;'>
					<strong>Hourly Rate</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField' name='designRate' size='10' value='$designRate' />
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='width:100px;margin-top:10px;margin-right:10px;'>
					<strong>Adjustments</strong>
				</div>
				<div class='genFloatL' style='margin-top:5px;'>
					<input class='textField' name='adjustments' size='10' value='$adjustments' />
				</div>
				<div class='clear'></div>
				
				<div style='margin-top:10px;margin-right:10px;'>
					<strong>Total Costs = $$total</strong>
				</div>
				
				<br /><br />
			</div>
			
			<div class='contentLeftData'>
				<br />
				<strong>Additional Details and Information Concerning Design Costs</strong><br /><br />
				<textarea class='' name='addidtionalDetails' cols='59' rows='10'>$addidtionalDetails</textarea>
			</div>	
			<div class='genFloatR' style='margin-top:19px;'>
				<input type='hidden' name='submitAction' value='updateCosts'>
				<input type='hidden' name='tID' value='".$get['tID']."'>
				<input type='hidden' name='dID' value='".$_SESSION['designer_id']."'>
				<a href='javascript:document.frm.submit();' class='teamButton adminPrimaryBGColor'>Update Costs</a>
			</div>
			<div class='clear'></div>
		</form>
	";
	
?>