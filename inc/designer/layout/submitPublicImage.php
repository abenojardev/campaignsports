<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');


$filename = $_SESSION['relative_path'] . "team/" . $campaign['ID'] . "/share/bkg-team.png";

if (file_exists($filename)) {
    $fileImg = "<div><img src='".$_SESSION['relative_path'] . "team/" . $campaign['ID'] . "/share/bkg-team.png' width='100%' />
	<p><a href='".$_SESSION['relative_path'] . "team/" . $campaign['ID'] . "/share/' target='_blank'>Click here to see the image in the team's public page.</a></p></div><div class='clear'></div>";
} else {
    $fileImg = "";
}

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<form method='post' name='frm' id='frm' enctype='multipart/form-data' action='index.php?nav=viewCampaign&sNav=submitPublicImage&tID=".$get['tID']."'>
		".$fileImg."
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Submit Public Image</h2>
				<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:left;'>
					<input type='file' name='multiUpload[]' id='multiUpload' size='40' />
					<input type='hidden' name='uploadPath' value='team/" . $campaign['ID'] . "/share'>
				</div>
				<div class='genFloatR' style='margin-top:19px;'>
					<input type='hidden' name='submitAction' value='submitPublicImage'>
					<input type='hidden' name='tID' value='".$get['tID']."'>
					<input type='hidden' name='dID' value='".$_SESSION['designer_id']."'>
					<a href='javascript:document.frm.submit();' class='teamButton adminPrimaryBGColor'>Upload</a>
				</div>
				<div class='clear'></div>
				<div style='color:#ff0000'>
					<br />
					NOTE: Make sure that the file uploaded is a PNG file, and make sure it is named <strong>bkg-team.png</strong>
				</div>
				<br /><br />
			</div>
			<!--
			<div class='contentLeftData'>
				<strong>Send Accompanying Message to Campaign Admin</strong><br /><br />
				<input class='textField' name='subject' size='66' value='Subject...'/>
				<br />
				<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>
			</div>	-->
			<!--<div class='genFloatR' style='margin-top:19px;'>
				<a href='javascript:document.frmApprove.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
			</div>-->
			<div class='clear'></div>
		</form>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	";
	
?>