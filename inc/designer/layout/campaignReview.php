<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
	$dirname =  $_SESSION['siteRoot'] . "/team/" . $campaign['ID'] . "/";
									
	$logoName = getTeamLogoName($campaign['ID']);
	$phoneDisplay = (isset($campaign['phoneDay']) && $campaign['phoneDay'] != "") ? "P: " .$campaign['phoneDay'] : ""; 
	$emailDisplay = (isset($campaign['email']) && $campaign['email'] != "") ? "" .$campaign['email'] : ""; 

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
		<strong>Coach Information</strong><br />
		" .$campaign['fname']. " " .$campaign['lname']." - ".$campaign['title']."<br />" .$campaign['address']."<br />";
		
	if($campaign['address2']!='') {	
	echo $campaign['address2']."<br />";
	}
	echo $campaign['city'].", ".$campaign['state']." ".$campaign['zip']."<br />
		".$emailDisplay."<br />".$phoneDisplay. "
		<br /><br />
		</div>	
		<div class='contentLeftData'>
			<strong>Team ID</strong><br />
			" .$campaign['ID']. "<br /><br />
			<strong>Team Password</strong><br />
			" .$campaign['password']. "<br /><br />
		</div>
		<div class='contentLeftData'>
			<strong>Team Highlights</strong><br /><br />
			" .$campaign['goals']. "<br /><br />
		</div>	
		<div class='contentLeftData'>
			<strong>Fundraising Goals</strong><br /><br />
			" .$campaign['stats']. "<br /><br />
		</div>	
		<div class='contentLeftData'>
			<strong>Additional Information</strong><br /><br />
			" .$campaign['add_info']. "<br /><br />
		</div>	
		<div class='contentLeftData'>
			<strong>Team Slogan</strong><br /><br />
			" .$campaign['slogan']. "<br /><br />
		</div>		
		<div class='contentLeftData'>
			<strong>Team Logo &amp; Colors</strong><br /><br />
			<div class='genFloatL' style='margin-right:40px;'>
				<a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$logoName."&p=$dirname".$logoName;
	
	echo "' target='_blank'>";
		
	showteamLogo($campaign['ID']);
			
	echo "
				</a>
			</div>
			<div class='genFloatL' style='text-align:center;'>
				<div><strong>Color #1</strong> (#" .$campaign['bgColor1']. ")</div>
				<div style='margin-top:5px;margin-bottom:5px;height:25px;width:300px;background-color:#" .$campaign['bgColor1']. "'>&nbsp;</div>
				<div><strong>Color #2</strong> (#" .$campaign['bgColor2']. ")</div>
				<div style='margin-top:5px;margin-bottom:5px;height:25px;width:300px;background-color:#" .$campaign['bgColor2']. "'>&nbsp;</div>
				<div><strong>Text Color</strong> (#" .$campaign['txtColor']. ")</div>
				<div style='margin-top:5px;margin-bottom:5px;height:25px;width:300px;background-color:#" .$campaign['txtColor']. "'>&nbsp;</div>
			</div>
			<div class='clear'></div>
			
			<br /><br />
		</div>	
		<div class='contentLeftData'>
			<strong>Team Pictures</strong><br /><br />
			Click on the images below to dowload<br /><br />
	";	
									
			$images = scandir($dirname."teamPhotos");
			$ignore = Array(".", "..");
			foreach($images as $image){
				if(!in_array($image, $ignore)) {
					echo "
						<div class='genFloatL' style='margin-top:15px;margin-right:15px;border:#000 solid 1px;'>
							<a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$image."&p=" . $dirname . "teamPhotos" . $image . "' target='_blank'>
								<img src='" . $_SESSION['relative_path'] . "team/" . $campaign['ID'] . "/teamPhotos/$image' height='150' />
							</a>
						</div>
					";
				}
			}
			
	echo "
		<div class='clear'></div>
		</div>	
	";
	
?>