<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	$design = Designer::getDesign($get['d']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');
	
	$datePrint = date("F j, Y", strtotime($design['date_submitted']));
	
	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
			<div>
			<h2 style='color:#f7941e;'>Previous Brochure Design #" .$get['n']. "</h2>
			Submitted: $datePrint
			</div>
			<div class='genFloatL' style='margin-top:20px;margin-bottom:20px;'>
				<img src='" . $_SESSION['relative_path'] . "images/pdf.jpg' alt='' />
			</div>
			<div class='genFloatL' style='margin-top:30px;margin-left:6px;'>
				<h4 class='teamPrimaryTxtColor'>
					<!--
					<a href='" . $_SESSION['relative_path'] . "inc/common/functions/download.php?f=".$design['filename']."&p=$dirname". $design['filename'] . "' target='_blank'>Click here to download and review this brochure</a>
					-->
					<a href='" . $_SESSION['relative_path'] . "team/" . $campaign['ID'] . "/designer/".$design['filename']."' download='".$design['filename']."'>Click here to download and review this brochure</a>
				</h4>
			</div>
			<div class='clear'></div>
		</div>	
		<div class='contentLeftData'>
			<strong>Associated Messages</strong>
			<br /><br /><br /><br /><br /><br />
			
			
		</div>	
		<div class='contentLeftData'>
			<strong>Send Message</strong><br /><br />
			You may utilize this area to send a message regarding this particular design to this campaign's administrator.
			<br /><br />
			<input class='textField' name='' size='66' value='Subject...'/>
			<br />
			<textarea class='textArea' name='goals' cols='55' rows='10'>Message...</textarea>
		</div>
		<div class='genFloatR' style='margin-top:19px;'>
			<a href='javascript:document.frmApprove.submit();' class='teamButton adminPrimaryBGColor'>Send Message</a>
		</div>
		<div class='clear'></div>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	<div class='clear'></div>
	</div>
	";
	
			
	
?>