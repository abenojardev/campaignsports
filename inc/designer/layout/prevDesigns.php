<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	$designs = Designer::getSubmittedDesigns($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
			<h2 style='color:#f7941e;'>Previous Brochure Designs</h2>
	";
	
	$dCount = count($designs);
	foreach($designs AS $id => $data) {
		if ($data['final'] == 1) { $dCount--; continue; }
		$datePrint = date("F j, Y", strtotime($data['date_submitted']));
		echo "<div style='width:470px;background-color:#EEE;padding:10px;margin-top:1px;'>
		<a href='index.php?nav=viewCampaign&sNav=viewDesign&tID=".$data['tID']."&n=$dCount&d=$id' class='sNav'>
			Design #$dCount
		</a> - Submitted:&nbsp; $datePrint - ".$data['filename']."</div>";
		$dCount--;
	}
			
	echo "
		</div>	
	";
	
?>