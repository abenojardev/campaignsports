<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');

	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<div class='contentLeftData'>
			<h2 style='color:#f7941e;'>Written Copy for Campaign</h2>
			<br />
			Below you will find the final approved copy to be used in the design of the brochure for this campaign.
			<br /><br />
			<hr style='width:100%; height:1px; border:0px; border-bottom:1px solid #cccccc'> 
			" .$campaign['final_copy']. "<br /><br />
		</div>	
	";
	
?>