<?php
	require_once($_SESSION['relative_path'] . 'inc/designer/class/Designer.php');
	$campaign = Designer::getSingleCampaign($get['tID']);
	require_once($_SESSION['relative_path'] . 'inc/common/functions/teamStyles.php');
	
	$finalFiles = Designer::getFinalFiles($get['tID']);
	$mailDate = ($finalFiles['date_submitted']) ? date("M j, Y", strtotime($finalFiles['date_submitted'])) : "";
	
	if ( isset($finalFiles['filename'])) {
	$finalFileMessage = 'You uploaded files on '.$mailDate.'<br />Filename: ' . $finalFiles['filename'];
	} else {
		$finalFileMessage = '';
	}
	echo "
		<h1 class='adminPrimaryTxtColor'>" .$campaign['name']. ": " .$campaign['team']. " (ID:" .$campaign['ID']. ")</h1>
		<form method='post' name='frm' id='frm' enctype='multipart/form-data' action='index.php?nav=viewCampaign&sNav=uploadFinalFiles&tID=".$get['tID']."'>
			<div class='contentLeftData'>
				<h2 style='color:#f7941e'>Upload Final Files for Printer</h2>
				<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:left; color:#03c603;'>
					" . $finalFileMessage . "
				</div>
				<div class='clear'></div>
				
				<div class='genFloatL' style='margin-top:15px;margin-right:15px;text-align:left;'>
					<input type='file' name='multiUpload[]' id='multiUpload' size='40' />
					<input type='hidden' name='uploadPath' value='team/" . $campaign['ID'] . "/designer'>
				</div>
				<div class='genFloatR' style='margin-top:19px;'>
					<input type='hidden' name='submitAction' value='uploadFinalFiles'>
					<input type='hidden' name='tID' value='".$get['tID']."'>
					<input type='hidden' name='dID' value='".$_SESSION['designer_id']."'>
					<a href='javascript:document.frm.submit();' class='teamButton adminPrimaryBGColor'>Upload</a>
				</div>
				<div class='clear'></div>
				<div style='color:#ff0000;'>
					<br />
					**IMPORTANT UPDATE: Please do not use any apostrophes or special characters in the filenames.<br>
					NOTE: Please make sure there are no spaces in the filename.
					<br /><br />
					If you need to upload file again, please use same file name so old file is overwritten.  We do not require revisions for this upload in the system.
				</div>
				<br /><br />
			</div>
			
			<!--<div class='contentLeftData'>
				<strong>Send Accompanying Message to Campaign Admin</strong><br /><br />
				<input class='textField' name='subject' size='66' value='Subject...'/>
				<br />
				<textarea class='txtTextArea' name='message' cols='59' rows='10'>Message...</textarea>
			</div>	
			<div class='genFloatR' style='margin-top:19px;'>
				<a href='javascript:document.frmApprove.submit();' class='teamButton teamPrimaryBGColor'>Send Message</a>
			</div>-->
			<div class='clear'></div>
		</form>
		<script>
	$('input[name=subject]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
  });
  $('textarea[name=message]').each(function(){
    $(this).data('value', $(this).val()).focus(function(){
      if ($(this).val()==$(this).data('value')) $(this).val('');
    }).blur(function(){
      if ($.trim($(this).val())=='') $(this).val($(this).data('value'));
    });
	});
	</script>
	";
	
?>