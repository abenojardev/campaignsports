<?php
/****************************************************************************************
Designer.php
Defines the Designer class.

Application: Campaign Sports
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Designer {
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Designer: ' . $e->getMessage() );
			}
		}
	} // __construct()

	public function login($post) {
		try {
			if($post['username']!='' && $post['password']!='') {
				$query = array('select' => "*", 
							   'tbl' => "designers", 
							   'where' => "username = '".$post['username']."' AND password = '".$post['password']."'");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result) {
					$_SESSION["designer_id"] = $result['ID'];
					$_SESSION["designer_name"] = $result['fname'] . " " . $result['lname'];
					$loginFailure = NULL;
				}
				else  {
					$loginFailure = 'No matching records were found, please try again.';
				}		
			} 
			else {
				$loginFailure = 'Please enter your username and password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; login(): ' . $e->getMessage() );
		}
	} // login()
	
	public function logout() {
		unset($_SESSION["designer_id"]);
		unset($_SESSION["designer_name"]);
		unset($_SESSION["campaign_team"]);
	} // logout()


	public function searchCampaigns($pass) {
		try {
			$searchData = $pass['searchData'];
			$searchType = $pass['searchType'];
			
			if ($searchType == 'all') {
				$where = "HAVING (t.name LIKE '%".$searchData."%' 
				OR t.team LIKE '%".$searchData."%'
				OR t.ID LIKE '%".$searchData."%')
				AND t.designerID = ".$_SESSION["designer_id"];
			} else if ($searchType == 'name') {
				$where = "HAVING t.name LIKE '%".$searchData."%' 
				AND t.designerID = ".$_SESSION["designer_id"];
			} else if ($searchType == 'team') {
				$where = "HAVING t.team LIKE '%".$searchData."%' 
				AND t.designerID = ".$_SESSION["designer_id"];
			} else if ($searchType == 'ID') {
				$where = "HAVING t.ID LIKE '%".$searchData."%' 
				AND t.designerID = ".$_SESSION["designer_id"];
			} else {
				$where = "HAVING t.designerID = ".$_SESSION["designer_id"];
			}
			
			$query = array('select' => "t.ID, t.name, t.team, t.status, t.designerID", 
							'sort' => "t.ID desc",
							'tbl' => "teams t GROUP BY t.ID ".$where);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; searchCampaigns(): ' . $e->getMessage() );
		}
	} // searchCampaigns()

	public function getSingleCampaign($tID) {
		try {
			$query = array('select' => "t.*, ti.goals, ti.stats, ti.add_info, ti.slogan, ti.final_copy, tc.fname, tc.lname, tc.title, tc.address, tc.address2,  tc.city, tc.state, tc.zip, tc.email, tc.phoneDay", 
						   'tbl' => "teams t LEFT JOIN team_initial_info ti ON t.ID = ti.tID LEFT JOIN team_contacts tc ON t.ID = tc.tID",
						   'where' => "t.ID = $tID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSingleCampaign(): ' . $e->getMessage() );
		}
	} // getSingleCampaign()

	public function submitDesign($data) {
		try {
			$fields = "tID";
			$values = $data["tID"];
			
			$fields .= ",dID";
			$values .= "," . $_SESSION["designer_id"];
			
			$fields .= ",filename";
			$values .= ",'" . $_FILES["multiUpload"]["name"][0] . "'";
			
			$query = array('tbl' => "designer_submissions", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; submitDesign(): ' . $e->getMessage() );
		}
	} // submitDesign()

	public function getSubmittedDesigns($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " designer_submissions",
						   'sort' => " date_submitted DESC",
						   'where' => "tID = $tID AND dID = " . $_SESSION["designer_id"]);
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getSubmittedDesigns(): ' . $e->getMessage() );
		}
	} // getSubmittedDesigns()

	public function getCurrentDesign($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " designer_submissions",
						   'sort' => " date_submitted DESC LIMIT 1",
						   'where' => "tID = $tID AND final = 0 AND dID = (SELECT designerID FROM teams WHERE ID = $tID)");
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCurrentDesign(): ' . $e->getMessage() );
		}
	} // getCurrentDesign()

	public function getDesign($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " designer_submissions",
						   'where' => "ID = $ID AND dID = " . $_SESSION["designer_id"]);
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getDesign(): ' . $e->getMessage() );
		}
	} // getDesign()

	public function insertCosts($data) {
		try {
			$fields = "tID";
			$values = $data["tID"];
			
			$fields .= ",dID";
			$values .= "," . $data["dID"];
			
			$query = array('tbl' => "designer_costs", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; insertCosts(): ' . $e->getMessage() );
		}
	} // insertCosts()

	public function updateCosts($post) {
		try {
			$set = "design_hours=" . $post['designHours'] .",design_rate=" . $post['designRate'] .",adjustments=" . $post['adjustments'] .",addidtional_details='" . $post['addidtionalDetails'] ."'   ";
			
			$query = array('tbl' => "designer_costs", 
						   'set' => $set, 
						   'where' => "tID=" . $post['tID'] . " AND dID=" . $post['dID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateCosts(): ' . $e->getMessage() );
		}
	} // updateCosts()

	public function getCosts($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " designer_costs",
						   'where' => "tID = $tID AND dID = " . $_SESSION["designer_id"]);
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCosts(): ' . $e->getMessage() );
		}
	} // getCosts()

	public function submitFinalFiles($data) {
		try {
			$fields = "tID";
			$values = $data["tID"];
			
			$fields .= ",dID";
			$values .= "," . $_SESSION["designer_id"];
			
			$fields .= ",final";
			$values .= ",1";
			
			$fields .= ",filename";
			$values .= ",'" . $_FILES["multiUpload"]["name"][0] . "'";
			
			$query = array('tbl' => "designer_submissions", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; submitFinalFiles(): ' . $e->getMessage() );
		}
	} // submitFinalFiles()

	public function getFinalFiles($tID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => " designer_submissions",
						   'sort' => " date_submitted DESC LIMIT 1",
						   'where' => "tID = $tID AND final = 1 AND dID = (SELECT designerID FROM teams WHERE ID = $tID)");
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getFinalFiles(): ' . $e->getMessage() );
		}
	} // getFinalFiles()


}
?>