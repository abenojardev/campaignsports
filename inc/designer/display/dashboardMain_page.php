<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js">');
	
    startToMainHeader($data);
	contentWrapToNav();
	
?>    
            <div class="adminSubNav">
                <a href="#" class="sNav"></a>
            <div class="clear"></div>
            </div>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                	<?php 
					include($_SESSION['relative_path'] . 'inc/designer/layout/search.php');
					echo "<br>";
					?>
                </div>
                <div class='contentLeftData'>
                	<?php 
					include($_SESSION['relative_path'] . 'inc/designer/layout/campaignsList.php');
					?>
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					include($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->


<?php
	contentClosures();
	closePageWrapToEnd();
?>