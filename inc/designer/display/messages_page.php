<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js">');
	
    startToMainHeader($data);
	contentWrapToNav();
	
?>    
            <div class="adminSubNav">
                <a href="index.php?nav=messages" class="sNav">Unread Messages</a> &nbsp; | &nbsp;
                <a href="index.php?nav=messages&action=incoming" class="sNav">Inbox</a> &nbsp; | &nbsp;
                <a href="index.php?nav=messages&action=sent" class="sNav">Sent</a> &nbsp; | &nbsp;
            <div class="clear"></div>
            </div>
            
			<div class='contentFull'>
                <div class='contentFullData'>
<?php
            
						$viewCheck = ($get['sub']) ? $get['sub'] : "";
						require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Messaging.php');
						
						if (!$viewCheck) {
							$mData['sender'] = $mData['recip'] = "Designer";
							$mData['senderID'] = $mData['recipID'] = $_SESSION["designer_id"];
							
							if ($get['action'] && $get['action'] == 'sent') {
								$messages = Messaging::getAllSentMessages($mData);
								$label = "Sent";
							} else if ($get['action'] && $get['action'] == 'incoming') {
								$messages = Messaging::getAllReceivedMessages($mData);
								$label = "Incoming";
							} else {
								$mData['unreadOnly'] = 1;
								$messages = Messaging::getAllReceivedMessages($mData);
								$label = "Unread";
							}
							
							$messagesCount = count($messages);
					
							echo "
								<h2 class='adminPrimaryTxtColor' style='text-align:center'>Systemwide Messages: $label</h2>
								<div class='contentLeftData'>
									<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/pagination.css' />
									<table width='100%' border='0' cellspacing='0' cellpadding='7'>
										<tr>
											<td>Team ID</td>
											<td>To</td>
											<td>From</td>
											<td>Benchmark</td>
											<td>Subject</td>
											<td align='right'>Date</td>
										</tr>      
							";
							
							$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;
							$tpages = ceil($messagesCount/15);
							$min = ($page - 1) * 15 + 1;
							$max = $page * 15;
							
							$classAlternate = "bg1";
							$count = 0;
							
							foreach($messages AS $m) {
								$count++;
								if ($count < $min || $count > $max) continue;
								$date = date("M j, Y g:i A", strtotime($m['date_sent']));
								$unreadFlag = ($m['markAsRead']) ? "<span>" : "<span style='font-weight:bold;'>";
								$replyFlag = ($m['sender'] == 'Designer') ? "&s=y" : "";
								
								echo "
										<tr class='$classAlternate' align='left'>
											<td><a href='index.php?nav=viewCampaign&tID=".$m['tID']."$replyFlag'>$unreadFlag" . $m['tID'] . "</span></a></td>
											<td>$unreadFlag" . $m['recip'] . "</span></td>
											<td>$unreadFlag" . $m['sender'] . "</span></td>
											<td>$unreadFlag" . $m['benchmark'] . "</span></td>
											<td><a href='index.php?nav=messages&sub=viewMessage&mID=".$m['ID']."$replyFlag'>$unreadFlag" . $m['subject'] . "</span></a></td>
											<td align='right'>$unreadFlag$date</span></td>
										</tr>
								";
								$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							}
							
							echo "
										<tr>
											<td colspan='2'>&nbsp;</td>
										</tr>      
									</table>
							";
							
							echo "<div class='pagination'><br/>";
							$reload = $_SERVER['PHP_SELF'] . "?index.php?nav=messages";
							include_once($_SESSION['relative_path'] . 'inc/common/functions/pagination.php');
							echo paginate($reload, $page, $tpages, 5);
							echo "</div>";
							
							echo "
										</div>
										<!-- /contentLeftData -->
							";  
							
						} else {
							echo "
								<h2 class='adminPrimaryTxtColor' style='text-align:center'>View Systemwide Message</h2>
							";
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/viewMessage.php');
											  
							echo "
								<div class='contentRight'>
							";
							
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/topLevelUnreadMessages.php');
							
							echo "
								<div class='clear'></div>
								</div>
								<!-- /contentRight -->
							";  
						}
?>
                </div>
                <!-- /contentFullData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentFull -->

<?php
	contentClosures();
	closePageWrapToEnd();
?>