<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js">');
	
    startToMainHeader($data);
	contentWrapToNav();
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
	$campaignStatuses = Campaign::getCampaignStatuses($get['tID']);

?>    
            <div class='adminSubNav'>
                <a href='index.php?nav=viewCampaign&sNav=reviewTeam&tID=<?php echo $get['tID']; ?>' class='sNav'>Team Info</a>
				&nbsp;|&nbsp;
                <a href='index.php?nav=viewCampaign&sNav=writtenCopy&tID=<?php echo $get['tID']; ?>' class='sNav'>Brochure Copy</a>
				&nbsp;|&nbsp;
                
<?php
	if ($campaignStatuses[7] && !$campaignStatuses[10]) {
		echo "
                <a href='index.php?nav=viewCampaign&sNav=submitDesign&tID=" . $get['tID'] . "' class='sNav'>Submit Brochure Design</a>
				&nbsp;|&nbsp;
		";				
	}
	else if ($campaignStatuses[99]) {
		echo "
                <a href='index.php?nav=viewCampaign&sNav=uploadFinalFiles&tID=" . $get['tID'] . "' class='sNav'>Upload Final Files</a>
				&nbsp;|&nbsp;
		";				
	}
?>    

                <a href='index.php?nav=viewCampaign&sNav=prevDesigns&tID=<?php echo $get['tID']; ?>' class='sNav'>Previous Designs</a>
                &nbsp;|&nbsp;
                <a href='index.php?nav=viewCampaign&sNav=submitPublicImage&tID=<?php echo $get['tID']; ?>' class='sNav'>Submit Public Image</a>
				&nbsp;|&nbsp;
                <a href='index.php?nav=viewCampaign&sNav=designCosts&tID=<?php echo $get['tID']; ?>' class='sNav'>Costs</a>
				&nbsp;|&nbsp;
                <a href='index.php?nav=viewCampaign&sNav=messages&tID=<?php echo $get['tID']; ?>' class='sNav'>Messages</a>
            <div class='clear'></div>
            </div>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                	<?php 
					switch($get['sNav']) {
						case 'reviewTeam':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/campaignReview.php');
						break;
						case 'writtenCopy':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/writtenCopy.php');
						break;
						
						case 'submitDesign':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/submitDesign.php');
						break;
						
						case 'submitPublicImage':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/submitPublicImage.php');
						break;
						
						case 'uploadFinalFiles':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/uploadFinalFiles.php');
						break;
						
						case 'prevDesigns':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/prevDesigns.php');
						break;
						case 'viewDesign':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/viewDesign.php');
						break;
						case 'designCosts':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/designCosts.php');
						break;
						case 'messages':
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/messages.php');
						break;
						default:
							include_once($_SESSION['relative_path'] . 'inc/designer/layout/campaignReview.php');
						break;
						
					}
					?>
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					switch($get['sNav']) {
						case 'reviewTeam':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/submissions.php');
						break;
						case 'writtenCopy':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/submissions.php');
						break;
						case 'submitDesign':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/submissions.php');
						break;
						case 'prevDesigns':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'submitPublicImage':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'viewDesign':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'designCosts':
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
						case 'messages':
							if (($get['sub'] && $get['sub'] == 'sent') || ($get['s'] && $get['s'] != 'y'))
								include_once($_SESSION['relative_path'] . 'inc/common/widgets/incomingMessages.php');
							else
								include_once($_SESSION['relative_path'] . 'inc/common/widgets/sentMessages.php');
						break;
						default:
							include_once($_SESSION['relative_path'] . 'inc/common/widgets/latestMessages.php');
						break;
					}
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->


<?php
	contentClosures();
	closePageWrapToEnd();
?>