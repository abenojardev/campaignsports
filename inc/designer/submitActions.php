<?php
	// Designer Submit Actions File
	
	if ($post['submitAction'] == 'submitDesign') {
		
		// Step 1 - Check Current design name
		require_once($relPath . 'inc/designer/class/Designer.php');
		$currentDesign = Designer::getCurrentDesign($post['tID']);

		// Step 2 - Upload file
		require_once($relPath . 'inc/common/class/files/Upload.php');
		$filename = Upload::multiUpload($post);
		
		// Step 3 - Update Status 
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 9;
		$dataS["pending"] = 1;
		Campaign::setCampaignStatus($dataS);
		
		// Step 4 - Update submissions table if new revision
		if ($currentDesign['filename'] != $filename) {
			require_once($relPath . 'inc/designer/class/Designer.php');
			$newID = Designer::submitDesign($post);
			
			$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "New Design in System";
			$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The designer has uploaded a design revision for review.";
		} else {
			$newID = $currentDesign['ID'];
			$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "Design Revision Uploaded";
			$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The designer has uploaded an update to a current design revision for review.";
		}
		
		// Step 5 - Send mail to admin 
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Designer";
		$dataM['senderID'] = $post['dID'];
		$dataM['benchmark'] = "Brochure Design";
		$dataM['benchmarkID'] = $newID;
		Messaging::addMessage($dataM);
	}
	
	if ($post['submitAction'] == 'submitPublicImage') {
		
		// Step 1 - Check Curent design name
		//require_once($relPath . 'inc/designer/class/Designer.php');
		//$currentDesign = Designer::getCurrentDesign($post['tID']);

		// Step 2 - Upload file
		require_once($relPath . 'inc/common/class/files/Upload.php');
		$filename = Upload::multiUpload($post);
		
		// Step 3 - Update Status 
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 25;
		$dataS["pending"] = 0;
		Campaign::setCampaignStatus($dataS);
		$dataS2["ID"] = $post['tID'];
		$dataS2["sID"] = 27;
		$dataS2["pending"] = 1;
		Campaign::setCampaignStatus($dataS2);
		
		// Step 4 - Update submissions table if new revision
		//if ($currentDesign['filename'] != $filename) {
			//require_once($relPath . 'inc/designer/class/Designer.php');
			//$newID = Designer::submitPublicImage($post);
			
			//$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "New Public Page Image in System";
			//$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The designer has uploaded a public page image for review.";
		//} else {
			//$newID = $currentDesign['ID'];
			$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "Public Page Image Uploaded";
			$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The designer has uploaded a public page image for review.";
		//}
		
		// Step 5 - Send mail to admin 
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Designer";
		$dataM['senderID'] = $post['dID'];
		$dataM['benchmark'] = "Public Page Image";
		$dataM['benchmarkID'] = 0;
		Messaging::addMessage($dataM);
	}
	
	else if ($post['submitAction'] == 'uploadFinalFiles') {
		require_once($relPath . 'inc/common/class/files/Upload.php');
		$filename = Upload::multiUpload($post);
		
		require_once($relPath . 'inc/designer/class/Designer.php');
		$newID = Designer::submitFinalFiles($post);
		
		// Send message to admin 
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($post['tID']);
		$printerID = $TeamMain->getTeamPrinter();
		
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		$dataM['subject'] = ($post['subject'] && $post['subject'] != "Subject...") ? $post['subject'] : "Files Uploaded";
		$dataM['message'] = ($post['message'] && $post['message'] != "Message...") ? $post['message'] : "The designer has uploaded the final files for the printer.";
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Admin";
		$dataM['recipID'] = 3;
		$dataM['sender'] = "Designer";
		$dataM['senderID'] = $post['dID'];
		$dataM['benchmark'] = "Final Files";
		$dataM['benchmarkID'] = 0;
		Messaging::addMessage($dataM);

		// Send message to printer 
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Printer";
		$dataM['recipID'] = $printerID;
		$dataM['sender'] = "Designer";
		$dataM['senderID'] = $post['dID'];
		$dataM['benchmark'] = "Final Files";
		$dataM['benchmarkID'] = 0;
		Messaging::addMessage($dataM);
		
	}
	
	else if ($post['submitAction'] == 'updateCosts') {
		require_once($relPath . 'inc/designer/class/Designer.php');
		Designer::updateCosts($post);
	}
	

	else if ($post['submitAction'] == 'messageReply') {
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = $post['recip'];
		$data['recipID'] = $post['recipID'];
		$data['sender'] = $post['sender'];
		$data['senderID'] = $post['senderID'];
		$data['benchmark'] = $post['benchmark'];
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}
?>

