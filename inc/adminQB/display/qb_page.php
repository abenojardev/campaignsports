<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link type="text/css" href="' . $_SESSION['relative_path'] . 'inc/adminQB/scripts/css/ui-lightness/jquery-ui-1.8.21.custom.css" rel="stylesheet" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/adminQB/scripts/jquery-1.7.2.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/adminQB/scripts/jquery-ui-1.8.21.custom.min.js"></script>
		');
	
    startToMainHeader($data);
?>
	<script type="text/javascript">
        $(function(){

            // Datepicker
            $('#datepicker').datepicker({
                inline: true,
				dateFormat: "yy-mm-dd '00:00:00'"
            });
            $('#datepicker2').datepicker({
                inline: true,
				dateFormat: "yy-mm-dd '23:59:59'"
            });

            //hover states on the static widgets
            $('#dialog_link, ul#icons li').hover(
                function() { $(this).addClass('ui-state-hover'); },
                function() { $(this).removeClass('ui-state-hover'); }
            );

        });
    </script>

    <div class='pageContentWrap adminSecondaryBGColor'>
    
    	<div class='innerPageContentWrap adminPrimaryBGColor'>
        
        	<div class='pageContent'>
            
            <div class='topContent'>
                <div class='welcomeBar'>
                    <div class='welcomeBarCol1'>&nbsp;</div>
                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span>
                    <br /><div class='logout'><a href='index.php?action=logout'>[logout]</a></div></div>
                </div>
                <div class='clear'></div>
                
                <div class='welcomeBarLogin'>
                    <h1>Select Your Date Range</h1>
                </div>
            </div>
<div class="v2Bar" style="color:#FFF; font-weight:bold; text-align:center; padding-top:10px; padding-bottom:10px; background-color:#F00;">Version 2.0</div>
            <div class='contentFull'>

                <div class='loginWrap' id='loginWrap'>
                	<div id='exportDiv'><?php if (isset($error)) echo $error; ?></div>
                    <div id='exportForm'>
                        <form id='frmExport' name='frmExport' method='post' action='exportQB.php'>
                            <p class='teamPrimaryTxtColor'><strong>Enter Starting Date</strong></p>
                            <p><input name='qbDate1' type='text' class='textFieldLogin' id="datepicker" /></p>

                            <p class='teamPrimaryTxtColor'><strong>Enter Ending Date</strong></p>
                            <p><input name='qbDate2' type='text' class='textFieldLogin' id="datepicker2" /></p>
                        
                            <p><a href='javascript:document.frmExport.submit();' id='btnExport' class='adminButton adminPrimaryBGColor'>Export</a></p>
                        </form>
                    </div>
                </div>

            <div class='clear'></div>
            </div>
            <!-- /contentFull -->

<?php
	contentClosures();
	closePageWrapToEnd();
?>