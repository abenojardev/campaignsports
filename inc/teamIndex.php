<?php

	if ($_SERVER['HTTP_HOST'] != 'localhost') {
		if ($_SERVER['SERVER_PORT'] != 443) {
			$url = "https://". $_SERVER['SERVER_NAME'] . ":443".$_SERVER['REQUEST_URI'];
			header("Location: $url");
		}
	}
	
	// Required on EVERY page.  Alter path to make sure points to init file
	require_once('common/path/init.php');
	$relPath = $_SESSION['relative_path'];
	
	// Load include files
	require_once($relPath . 'inc/team/layout/layout.php');
	require_once($relPath . 'inc/common/config.php');
	
	$post = $_POST;
	$get = $_GET;

	// Register
	if ( isset($post['action']) && $post['action'] == 'playerRegister' ) {
		require_once($relPath . 'inc/team/class/Player.php');
		$Player = new Player('empty');
		$error = $Player->register($post);
	}

	// Login / Logout
	if ( isset($get['action']) && $get['action'] == 'login' ) {
		require_once($relPath . 'inc/team/class/Player.php');
		require_once($relPath . 'inc/team/class/TeamUser.php');
		// Player Login
		if ( $_SESSION["team_user"] == "player" ) {
			if ( (isset($_SESSION["team_id"]) && ($_SESSION["team_id"]) && ($_SESSION["team_id"] != "") )
			&& ( $_SESSION['current_folder'] > $cfg_playerPassCutoffTeam ) ) {
			//if ( isset($post['submitAction']) && $post['submitAction'] == 'playerSelect' ) {
				$loginCheck = Player::loginLevel2($post);
				if ($loginCheck) {
					$error = $loginCheck;
					echo $error;
				}
				else {
					echo "
						<script>
						   parent.changeURL('index.php' );
						</script>

					";
				}
			}
			else if ( isset($_SESSION["team_id"]) && ($_SESSION["team_id"]) && ($_SESSION["team_id"] != "") ) {
				$pID = $post['player'];
				if ($pID != '' || $pID == NULL) 
					$_SESSION['player_id'] = $pID;
				
				include($relPath . 'inc/team/display/playerDashboard_page.php');
			}
			else {
				$loginCheck = TeamUser::playerLogin($post);
				if ($loginCheck) {
					$error = $loginCheck;
					echo $error;
				}
				else {
					include_once($relPath . 'inc/team/layout/login.php');
				}
			}
			
			
		// Admin Login
		} else if ( $_SESSION["team_user"] == "admin" ) {
			$loginCheck = TeamUser::adminLogin($post);
			if ($loginCheck) {
				$error = $loginCheck;
				include($relPath . 'inc/team/display/teamLogin_page.php');
			}
			else {
				$_SESSION["nav"] = "dashboard";
				include($relPath . 'inc/team/display/adminDashboard_page.php');
			}
		}
		
		
	}
	else if ( isset($get['action']) && $get['action'] == 'logout' ) {
		require_once($relPath . 'inc/team/class/TeamUser.php');
		if ( $_SESSION["team_user"] == "player" ) {
			TeamUser::playerLogout();
			include($relPath . 'inc/team/display/teamLogin_page.php');
		} else if ( $_SESSION["team_user"] == "admin" ) {
			TeamUser::adminLogout();
			include($relPath . 'inc/team/display/teamLogin_page.php');
		}
	}
	else if ( isset($get['action']) && $get['action'] == 'pswdReset' ) {
		if ( $_SESSION["team_user"] == "player" ) {
			require_once($relPath . 'inc/team/class/Player.php');
			$Player = new Player($post["player_id"]);
			$Player->forgotPassword();
			echo "An email has been sent with your password.";
		} 
		else if ( $_SESSION["team_user"] == "admin" ) {
			TeamUser::adminLogout();
			include($relPath . 'inc/team/display/teamLogin_page.php');
		}
	}
	
	// Team User Logged in, continue with team options
	else if ( isset($_SESSION["team_id"]) && ($_SESSION["team_id"]) && ($_SESSION["team_id"] != "") ) {
		// Check for GET/POST actions
		if( isset($get["id"]) )
			$_SESSION["case_id"] = $get["id"];
			
		if ( $_SESSION["team_id"] != $_SESSION["current_folder"] ) {
			$_SESSION["current_folder"] = $_SESSION["team_id"];

			if ( isset($_SESSION["admin_id"]) && ($_SESSION["admin_id"]) && ($_SESSION["admin_id"] != "") )
				$chgPath = '/admin';
			else
				$chgPath = '';

			$url = $relPath . 'team/'.$_SESSION["team_id"].'';
			header("Location: $url");
		}
			
		if ( isset($post['submitAction']) )
			include($relPath . 'inc/team/submitActions.php');
		
		// Contact Admin Logged in, continue with admin options
		if ( isset($_SESSION["admin_id"]) && ($_SESSION["admin_id"]) && ($_SESSION["admin_id"] != "") ) {
			if ( isset($get["action"]) ) {
				switch($get["action"]) {
					case 'dashboard':
					$_SESSION["nav"] = "dashboardx";
					include($relPath . 'inc/team/display/adminDashboard_page.php');
					break;
					
					case 'account':
					$_SESSION["nav"] = "account";
					include($relPath . 'inc/team/display/adminAccount_page.php');
					break;
					
					case 'c_checklist':
					$_SESSION["nav"] = "c_checklist";
					include($relPath . 'inc/team/display/adminTaskList_page.php');
					break;		
					
					case 'checks':
					$_SESSION["nav"] = "checks";
					include($relPath . 'inc/team/display/adminChecks_page.php');
					break;						

					case 'brochure':
					$_SESSION["nav"] = "brochure";
					include($relPath . 'inc/team/display/adminBrochure_page.php');
					break;	
					
					case 'players':
					$_SESSION["nav"] = "players";
					if ( isset($get["edit"]) )
						include($relPath . 'inc/team/display/adminEditPlayer_page.php');
					else if ( isset($get["delete"]) ) {
						include($relPath . 'inc/team/class/TeamUser.php');
						$del = TeamUser::deleteAdminPlayer($get['delete']);
						include($relPath . 'inc/team/display/adminPlayers_page.php');
					} else 
						include($relPath . 'inc/team/display/adminPlayers_page.php');
					break;
					
					case 'contacts':
					$_SESSION["nav"] = "contacts";
					if ( isset($get["edit"]) )
						include($relPath . 'inc/team/display/adminEditContact_page.php');
					else if ( isset($get["delete"]) ) {
						include($relPath . 'inc/team/class/Player.php');
						$del = Player::removeContact($get['delete']);
						include($relPath . 'inc/team/display/adminContacts_page.php');
					} else 
						include($relPath . 'inc/team/display/adminContacts_page.php');
					break;
					
					case 'donations':
					$_SESSION["nav"] = "donations";
					if ( isset($get["view"]) )
						include($relPath . 'inc/team/display/adminViewDonations_page.php');
					else 
						include($relPath . 'inc/team/display/adminDonations_page.php');
					break;
					
					case 'pageManager':
					$_SESSION["nav"] = "pageManager";
					include($relPath . 'inc/team/display/adminManager_page.php');
					break;
	
					case 'campaignActivity':
					$_SESSION["nav"] = "campaignActivity";
					if ( isset($get["subAction"]) ) {
						switch ($get["subAction"]) {
							case 'reqInfo':
								include($relPath . 'inc/common/display/coachPages/ca_requiredInfo_page.php');
							break;
							case 'brochureCopy':
								include($relPath . 'inc/common/display/coachPages/ca_reviewBrochureCopy_page.php');
							break;
							case 'brochure':
								include($relPath . 'inc/common/display/coachPages/ca_reviewBrochure_page.php');
							break;
							case 'printer':
								include($relPath . 'inc/common/display/coachPages/ca_reviewPrinter_page.php');
							break;
							case 'messages':
								include($relPath . 'inc/common/display/coachPages/ca_messages_page.php');
							break;
							default:
								include($relPath . 'inc/common/display/coachPages/campaignActivity_page.php');
							break;
						}
					} else 
						include($relPath . 'inc/common/display/coachPages/campaignActivity_page.php');
					break;
	
					default:
					$_SESSION["nav"] = "dashboard";
					include($relPath . 'inc/team/display/adminDashboard_page.php');
					break;
				}
			}
			else {
				$_SESSION["nav"] = "dashboard";
				include($relPath . 'inc/team/display/adminDashboard_page.php');
			}
			
		}

		// Player User Logged in, continue with player options
		else if ( isset($_SESSION["player_id"]) && ($_SESSION["player_id"]) && ($_SESSION["player_id"] != "") ) {

			if ( isset($get["action"]) ) {
				switch($get["action"]) {
					case 'addContact':
					include($relPath . 'inc/team/display/playerAddContact_page.php');
					break;
					
					case 'dashboard':
					include($relPath . 'inc/team/display/playerDashboard_page.php');
					break;					
					
					case 'editContact':
					include($relPath . 'inc/team/display/playerEditContact_page.php');
					break;
	
					case 'deleteContact':
					include($relPath . 'inc/team/class/Player.php');
					$del = Player::removeContact($get['delete']);
					include($relPath . 'inc/team/display/playerDashboard_page.php');
					break;
	
					case 'donorList':
					include($relPath . 'inc/team/display/playerDonorlist_page.php');
					break;	

					case 'share':
					include($relPath . 'inc/team/display/playerShare_page.php');
					break;						
	
					default:
					include($relPath . 'inc/team/display/playerDashboard_page.php');
					break;
				}
			}
			else {
				include($relPath . 'inc/team/display/playerDashboard_page.php');
			}
			
		}
		// Team User Logged in, not an admin and player has not been selescted or registered
		else {
			include($relPath . 'inc/team/display/playerRegister_page.php');
		}

	}
	else {
		include($relPath . 'inc/team/display/teamLogin_page.php');
	}
	
?>