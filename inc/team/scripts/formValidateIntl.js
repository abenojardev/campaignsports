$(document).ready(function() { 
						   
	$('#frm').submit(function() { 
		var val = validate();
		if (val)
		{
			return true;
		}
		else
		{
			return false;
		}
	});	
	
	$('#frmSubmit').click(function(event) {
		$('#frm').submit();
		event.preventDefault();
	});	
	
	$(".intl").change(function(){
		var checked = $(this).attr('checked');
		if (checked) {
			$('#intlAddy').show();
			$('#domesticAddy').hide();
		}
		else {
			$('#domesticAddy').show();
			$('#intlAddy').hide();
		}
	});

	function validate() { 
		var msg = "Please complete the highlighted items.";
		var msg2 = "Please enter a valid email address.";
		error_check = false; 

		var checked = $("#intl").attr('checked');
		if (checked) {
			
			
			
			$(".validate3").each(function (i) {
				if (this.value == "") {
					msg = "Please complete the highlighted items.";
					error_check = true;
					$(this).addClass("fieldEmpty");
				} else {
					$(this).removeClass("fieldEmpty");
				}
			});
			$(".validate4").each(function (i) {
				$(this).removeClass("fieldEmpty");
			});
			var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			var demail=$(".validate3").attr("value");
			if(!regex.test(demail)){
				alert("Please enter a valid email address and complete any highlighted items.");
				$(".validate3").addClass("fieldEmpty");
  				return false;
			}
		}
		else {
			$(".validate4").each(function (i) {
				if (this.value == "") {
					msg = "Please complete the highlighted items.";
					error_check = true;
					$(this).addClass("fieldEmpty");
				} else {
					$(this).removeClass("fieldEmpty");
				}
			});
			$(".validate3").each(function (i) {
				$(this).removeClass("fieldEmpty");
			});
		}
		
		
		$(".validate").each(function (i) {

			
				
			
			if ((this.value == "") || (this.value == "Subject...") || (this.value == "Message...")) {
				error_check = true;
				$(this).addClass("fieldEmpty");
			} else {
				$(this).removeClass("fieldEmpty");
			}
			$(".pemail").each(function() {
				
				var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				var demail=$(".pemail").attr("value");
				if(!regex.test(demail)){
					msg = "Please enter a valid email address and complete any highlighted items."
					//alert("You must enter a valid e-mail address");
					error_check = true;
					$(".pemail").addClass("fieldEmpty");
					return false;
				} else {
					$(".pemail").removeClass("fieldEmpty");
				}
			});
		});
		
		if (error_check) {
			alert(msg);
			return false;
		}
		else
		{
			return true;
		}

	}

 
}); 
 
