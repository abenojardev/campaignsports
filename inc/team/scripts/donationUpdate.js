// JavaScript Document

$(document).ready(function() { 

	$('#donationUpdateBtn').click(function(event) {
		$('#donationUpdate').show();
		event.preventDefault();
	});
	
	$('#frm').submit(function() { 
		var val = validate();
		if (val)
		{
			return true;
		}
		else
		{
			return false;
		}
	});	
	
	$('#frmSubmit').click(function(event) {
		$('#frm').submit();
		event.preventDefault();
	});
	
	function validate() { 
		var msg = "Please complete the highlighted items.";
		error_check = false; 
		
		var value = $("#goal").val();
		if( !IsNumeric(value) ) {
			msg = "Goal amount must be numeric.";
			error_check = true;
		}
		
		if( value <= 0 ) {
			msg = "Goal amount must be more than 0.";
			error_check = true;
		}
		
		if (error_check) {
			alert(msg);
			return false;
		}
		else
		{
			return true;
		}
	}

});


function IsNumeric(input) {
    return (input - 0) == input && input.length > 0;
}
