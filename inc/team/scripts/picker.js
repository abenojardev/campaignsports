// JavaScript Document

$(document).ready(function() { 
	// Set input colors for picker
	c1 = $('#color1').val();
	c2 = $('#color2').val();
	c3 = $('#color3').val();
	
	$('#color1').css('background-color', c1);
	$('#color2').css('background-color', c2);
	$('#color3').css('background-color', c3);
	$('#color1').css('color', '#fff');
	$('#color2').css('color', '#fff');
	$('#color3').css('color', '#fff');
					   
	// Picker buttons
	$('#pickerBtn1').click(function(event) {
		$('#colorpickerWindow1').jqmShow();
		$('#colorpicker1').farbtastic('#color1');
		event.preventDefault();
	});
	$('#pickerBtn2').click(function(event) {
		$('#colorpickerWindow2').jqmShow();
		$('#colorpicker2').farbtastic('#color2');
		event.preventDefault();
	});
	$('#pickerBtn3').click(function(event) {
		$('#colorpickerWindow3').jqmShow();
		$('#colorpicker3').farbtastic('#color3');
		event.preventDefault();
	});
	
	$('#closePickerBtn1').click(function(event) {
		$('#colorpickerWindow1').jqmHide();
		event.preventDefault();
	});
	$('#closePickerBtn2').click(function(event) {
		$('#colorpickerWindow2').jqmHide();
		event.preventDefault();
	});
	$('#closePickerBtn3').click(function(event) {
		$('#colorpickerWindow3').jqmHide();
		event.preventDefault();
	});
	
	
	
//	$('#demo').hide();
//	var f = $.farbtastic('#colorpicker');
//	var p = $('#colorpicker').css('opacity', 1);
//	var selected;
//	$('.textFieldSWR').each(function () { 
//		f.linkTo(this); $(this).css('opacity', 0.75); }).focus(function() {
//			if (selected) {
//				$(selected).css('opacity', 0.75).removeClass('colorwell-selected');
//			}
//			f.linkTo(this);
//			p.css('opacity', 1);
//			$(selected = this).css('opacity', 1).addClass('colorwell-selected');
//	});
	
	
	
	
//------------------------------------------------------------------------------
// Begin Modal Functions

	$('#colorpickerWindow1').jqm({
		modal: true, /* FORCE FOCUS */
		overlay: 0,
		onShow: function(h) {
			h.w.fadeIn();
		},
		onHide: function(h) {
			h.w.fadeOut("slow",function() { if(h.o) h.o.remove(); });
		}
	});
	$('#colorpickerWindow2').jqm({
		modal: true, /* FORCE FOCUS */
		overlay: 0,
		onShow: function(h) {
			h.w.fadeIn();
		},
		onHide: function(h) {
			h.w.fadeOut("slow",function() { if(h.o) h.o.remove(); });
		}
	});
	$('#colorpickerWindow3').jqm({
		modal: true, /* FORCE FOCUS */
		overlay: 0,
		onShow: function(h) {
			h.w.fadeIn();
		},
		onHide: function(h) {
			h.w.fadeOut("slow",function() { if(h.o) h.o.remove(); });
		}
	});
	
	
// End Modal Functions
//------------------------------------------------------------------------------

});
