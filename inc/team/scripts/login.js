// JavaScript Document

$(document).ready(function() { 

	$('#btnLogin').click(function(event) {
		$('#frmLogin').submit();
		event.preventDefault();
	});
	
    var options = { 
        target:        '#loginDiv'   // target element(s) to be updated with server response 
    };
 
    // bind form using 'ajaxForm' 
    $('#frmLogin').ajaxForm(options);

});

function hideLoginForm() {
	$('#loginForm').hide();
}
