<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');
	$Player = new Player('empty');
	$players = $Player->getAllPlayers();

	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);
	
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
	} 
	else {
		$team_id = $_SESSION['current_folder'];
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$playerCount = TeamUser::countPlayers($team_id);
	$contactCount = TeamUser::countContacts($team_id);
	$average = @round($contactCount / $playerCount, 1);

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$target = $Team->getTeamTarget();

	$targetSuccess = $playerCount * $target;
	
?>    

        	<div class="" style="display: flex;flex-direction: column;max-width: 600px; margin: auto;">

            <?php showteamHeaderAthlete(); ?>
             	    
                <div class='athlete-selectuser'>    	    
                    <div class='athlete-selectuser-form'>                   
                        <?php
                        	require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');
                        	$Player = new Player('empty');
                        	$players = $Player->getAllPlayers();
                        	
                        	if ($_SESSION['current_folder'] > $cfg_playerPassCutoffTeam) {
                        		$text1 = "
                        		<p>If you are <strong>already registered</strong>, please select<br>your name and enter your personal password.</p>
                        		";
                        		$pwBox = "<br /><br />Password: <input type='text' name='password' />";
                        		$selText = "Login";
                        		$button = "<a href='#' id='btnLogin2' class='teamButton teamPrimaryBGColor'>$selText</a>";
                        		$pwForm = "
                        			<form name='passReset' id='passReset' action='index.php?action=pswdReset' method='post'>
                        				<input type='hidden' name='player_id' value='' />
                        				If you have <strong>forgotten your password</strong>, <br />select your name and <a href='#' id='btnPswd'>Click Here</a>.
                        			</form>
                        		";
                        		$newJS = "
                        			$('#btnLogin2').click(function(event) {
                        				$('#frmSel').submit();
                        				event.preventDefault();
                        			});
                        			
                        			var options = { 
                        				target:        '#login2Div'   // target element(s) to be updated with server response 
                        			};
                        		 
                        			// bind form using 'ajaxForm' 
                        			$('#frmSel').ajaxForm(options);
                        			
                        			
                        			$('#btnPswd').click(function(event) {
                        				var pVal = $('#player').val();
                        				if (pVal == 'null') {
                        					alert('Please select your name first.');
                        				}
                        				else {
                        					document.passReset.player_id.value=pVal;
                        					$('#passReset').submit();
                        				}
                        				event.preventDefault();
                        			});
                        			
                        			var options = { 
                        				target:        '#login2Div'   // target element(s) to be updated with server response 
                        			};
                        		 
                        			// bind form using 'ajaxForm' 
                        			$('#passReset').ajaxForm(options);
                        			
                        			
                        			function changeURL( url ) {
                        				document.location = url;
                        			}
                        		";
                        		
                        	}
                        	else {
                        		$text1 = "
                        		<p>If you are already registered, <br>
                        		please select your name below.</p>
                        		";
                        		$pwBox = "";
                        		$selText = "Select";
                        		$button = "<a href='javascript:document.frmSel.submit()' class='teamButton teamPrimaryBGColor'>$selText</a>";
                        		$pwForm = "";
                        		$newJS = "";
                        	}
                        	
                        	
                        	echo "
                        		<div id='login2Div'></div>
                        		
                        		$text1
                        		<form name='frmSel' id='frmSel' action='index.php?action=login' method='post'>
                        		<select name='player' id='player'>
                        			<option value='null'>Select your name</option>
                        	";
                        				foreach($players AS $player) {
                        					$pID = $player['ID'];
                        					$fname = $player['fname'];
                        					$lname = $player['lname'];
                        					echo "<option value='$pID'>$fname $lname</option>";
                        				}
                        	echo "
                        		</select>
                        		<input type='hidden' name='submitAction' value='playerSelect' />
                        		&nbsp; 
                        		$button
                        		$pwBox
                        		</form><br />
                        		$pwForm
                        
                        		<p> If you <strong>have not registered yet</strong>, <br>
                        		please <a href='index.php'>Click Here</a> to get started.</p>
                        	";
                        	
                        	echo "
                        		<script type='text/javascript'> 
                        			hideLoginForm();
                        			$newJS
                        		</script>
                        	";
                        
                        
                        ?>
                    </div>                          
                </div>    
            </div>   



    <div class='clear'></div>

	<!-- /pageContentWrap -->


<?php
	closePageWrapToEndAthlete();
?>
