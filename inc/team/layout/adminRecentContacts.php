<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contactsList = TeamUser::getAllContacts($_SESSION['team_id']);

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Most Recent Contacts Added</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td align='left'><strong>Contact</strong></td>
							<td width='55px' align='center'><strong>Location</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($contactsList AS $c) {
							if ($count > 4) continue;
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'><a href='index.php?action=contacts&edit=" . $c['ID'] . "'>" . substr($c['fname'],0,1) . ". " . $c['lname'] . "</a></td>
							<td align='center'>" . $c['state'] . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='index.php?action=contacts'>View All</a></td>
						</tr>              
					</table>

                </div>
	";
?>