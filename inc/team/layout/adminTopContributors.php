<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$topPlayers = TeamUser::getTopPlayers($_SESSION['team_id']);
	
	echo "
                <h2 class='teamPrimaryTxtColor'>My Team's Top Contributors</h2>

					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td><strong>Team Athlete's Name</strong></td>
							<td align='center'><strong>Register Date</strong></td>
							<td align='center'><strong>Total Contacts</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($topPlayers AS $tp) {
							if ($count > 4) continue;
							$date = date("F j, Y", strtotime($tp['register_date']));
							
							echo "
						<tr class='$classAlternate' align='right'>
							<td align='left'><a href='index.php?action=players&edit=" . $tp['pID'] . "'>" . $tp['lname'] . ", " . $tp['fname'] . "</a></td>
							<td align='center'>" . $date . "</td>
							<td align='center'>" . $tp['countField'] . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='index.php?action=players'>View All</a></td>
						</tr>              
					</table>
	";
?>