<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$Team = new TeamMain($_SESSION["current_folder"]);
	$name = $Team->getTeamName();
	$team = $Team->getTeamType();
	$color = $Team->getTeamColors();

	echo "
		<style type='text/css'>
			.teamPrimaryBGColor {
				background-color:#" . $color['bgColor1'] . ";
				
			}
			.teamSecondaryBGColor {
				background-color:#" . $color['bgColor2'] . ";
				
			}
			.teamPrimaryTxtColor {
				color:#" . $color['txtColor'] . ";
			}
			
			/* general link style in team primary color */
			a:link, a:visited {
				color:#" . $color['txtColor'] . ";
				text-decoration: none;
			}
			a:hover, a:active {
				color:#" . $color['txtColor'] . ";
				text-decoration: underline;
			}
			
			/* coaches nav style in team primary color */
			a.cNav:link, a.cNav:visited {
				float:left;
				padding-left:10px;
				padding-right:10px;
				padding-top:5px;
				padding-bottom:7px;
				text-decoration: none;
				background-color:#cfcfcf;
				color:#00000;
				margin-right:1px;
			}
			a.cNav:hover, a.cNav:active {
				float:left;
				padding-left:10px;
				padding-right:10px;
				padding-top:5px;
				padding-bottom:7px;
				color:#ffffff;
				text-decoration: none;
				background-color:#" . $color['bgColor1'] . ";
				margin-right:1px;
			}
			
			a.cNavOn:link, a.cNavOn:visited, a.cNavOn:hover, a.cNavOn:active {
				float:left;
				padding-left:10px;
				padding-right:10px;
				padding-top:5px;
				padding-bottom:7px;
				color:#fff;
				text-decoration: none;
				background-color:#" . $color['bgColor1'] . ";
				margin-right:1px;
			}
			
			/* pagination style */
			.pagin {
				padding: 2px 0;
				margin: 0;
				font-family: 'Verdana', sans-serif;
				font-size: 7pt;
				font-weight: bold;
			}
			.pagin * {
				padding: 2px 6px;
				margin: 0;
			}
			.pagin a {
				border: solid 1px #666666;
				background-color: #EFEFEF;
				color: #666666;
				text-decoration: none;
			}
			.pagin a:visited {
				border: solid 1px #666666;
				background-color: #EFEFEF;
				color: #60606F;
				text-decoration: none;
			}
			.pagin a:hover, .pagin a:active {
				border: solid 1px #" . $color['bgColor1'] . ";
				background-color: white;
				color: #" . $color['bgColor1'] . ";
				text-decoration: none;
			}
			.pagin span {
				cursor: default;
				border: solid 1px #808080;
				background-color: #F0F0F0;
				color: #B0B0B0;
			}
			.pagin span.current {
				border: solid 1px #666666;
				background-color: #666666;
				color: white;
			}			
		</style>
	";
	
	function showteamHeader() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamHeader' align='center'>
                <table border='0' cellspacing='0' cellpadding='5'>
                  <tr>
                    <td><div class='teamLogo'><img src='" . $path . "/$logoCheck' width='130' height='130' /></div></td>
                    <td valign='middle' class='teamTitle teamPrimaryTxtColor'>$name</td>
                  </tr>
                </table>
			</div>
		";
	}
	function showteamPublicHeader() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		$team = $Team->getTeamType();
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamHeader' align='center'>
                <table border='0' cellspacing='0' cellpadding='5'>
                  <tr>
                    <td><div class='teamLogo'><img src='" . $path . "/$logoCheck' width='130' height='130' /></div></td>
                    <td valign='middle' class='teamTitle teamPrimaryTxtColor'>$name: $team</td>
                  </tr>
                </table>
			</div>
		";
	}
	function showteamLogo() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
			<div class='suggestionsWrap' style='text-align:center; margin-top:20px;'>
				<p class='teamPrimaryTxtColor'><strong>Current Team Logo</strong></p>
				<div style='text-align:center;'>
					<img src='" . $path . "/$logoCheck' width='130' height='130' />
				</div>
			</div>
		";
	} //showteamLogo()
	
	function showteamLogo2() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
			<div class='' style='text-align:center;'>
				<div style='text-align:center;'>
					<img src='" . $path . "/$logoCheck' width='150' height='150' />
				</div>
			</div>
		";
	} //showteamLogo()	
	
	function showteamName() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		
		echo "
                <center><h1> <span class='teamPrimaryTxtColor'>$name</span> Dashboard</h1></center>
		
		";
	}
	

	function showteamHeaderCoach() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamHeader teamHeaderCoach iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><img src='https://csjoc.com/v5/images/coaches/home-selected-icon.png' class='fa-7x'><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=account' class='" . $n2 ."'><img src='https://csjoc.com/v5/images/coaches/profile-selected-icon.png' class='fa-7x'><br><center><h1>PROFILE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=c_checklist' class='" . $n5 ."'><img src='https://csjoc.com/v5/images/coaches/checklist-selected-icon.png' class='fa-7x'><br><center><h1>TASK  LIST</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=brochure' class='" . $n2 ."'><img src='https://csjoc.com/v5/images/coaches/brochure-selected-icon.png' class='fa-7x'><br><center><h1>BROCHURE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=checks' class='" . $n5 ."'><img src='https://csjoc.com/v5/images/coaches/checks-selected-icon.png' class='fa-7x'><br><center><h1>CHECKS</h1></center></a></div>                    
                </div>
			</div>
		";
	}

	function showteamHeaderAthlete() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamHeader teamHeaderAthlete iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=addContact' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>PROFILE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorList' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONOR  LIST</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
		";
	}


	function showteamFooterCoach() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamFooterCoach iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/home-selected-icon.png' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=account' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/profile-selected-icon.png' class='fa-7x'></div><br><center><h1>PROFILE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=c_checklist' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/checklist-selected-icon.png' class='fa-7x'></div><br><center><h1>TASK  LIST</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=brochure' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/brochure-selected-icon.png' class='fa-7x'></div><br><center><h1>BROCHURE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=checks' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/checks-selected-icon.png' class='fa-7x'></div><br><center><h1>CHECKS</h1></center></a></div>                    
                </div>
			</div>
		";
	}


	function showteamFooterAthlete() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamFooterAthlete iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=addContact' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>PROFILE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorList' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONOR  LIST</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
		";
	}
	
	
	
	
	
	
	function showteamHeaderDonor() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamHeader teamHeaderAthlete iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=addContact' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>SELECT ATHLETE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorList' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONATE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
		";
	}
	
	
	
	function showteamFooterDonor() {
		require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
		$Team = new TeamMain($_SESSION["current_folder"]);
		$name = $Team->getTeamName();
		
		$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
		$logoCheck = ( is_file($path . "/logo.gif") ) ? "logo.gif" : "";
		$logoCheck = ( is_file($path . "/logo.jpg") ) ? "logo.jpg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.jpeg") ) ? "logo.jpeg" : $logoCheck;
		$logoCheck = ( is_file($path . "/logo.png") ) ? "logo.png" : $logoCheck;
		
		echo "
            <div class='teamFooterAthlete iconnav' align='center'>
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=addContact' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>SELECT ATHLETE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorList' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONATE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
		";
	}
		
	
	

?>