<?php

/****************************************************************************************

layout.php

Application: Campaign Sports

Artisan Digital Studios, October, 2016

****************************************************************************************/



function startToMainHeader($data) {

	$css = ($data['css']) ? $data['css'] : "";

	$js = ($data['js']) ? $data['js'] : "";

	$bodyClass = ( isset($data['bodyClass']) && $data['bodyClass'] ) ? $data['bodyClass'] : "bodyBkg";


	echo "

    <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>

		<head>

			<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
            <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
			<meta name='MSSmartTagsPreventParsing' content='TRUE' />

			<meta http-equiv='imagetoolbar' content='no' />

			<title>Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program</title>

			<meta name='resource-type' content='document'>

			<meta http-equiv='pragma' content='no-cache'>

			<meta name='copyright' content='Copyright Campaign Sports - All Rights Reserved.'>

			<meta name='author' content='Campaign Sports - www.campaignsports.com - Manalapan, New Jersey 18301'>

			<meta http-equiv='reply-to' content='info@campaignsports.com'>

			<meta name='language' content='English'>

			<link rev='made' href='mailto:mkents@campaignsports.com'>

			<meta name='keywords' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>


			<meta name='rating' content='General'>

			<meta name='revisit-after' content='14 days'>


			<meta name='ROBOTS' content='ALL'>

			<meta name='DC.Description' content='Campaign Sports is fast, easy and stress-free. Just sit back and let our systems do the hard work.'>

			<meta name='DC.Language' scheme='RFC1766' content='EN'>

			<meta name='DC.Coverage.PlaceName' content='USA'>



			<meta name='description' content='Campaign Sports is fast, easy and stress-free. Just sit back and let our system do the hard work.'>

			<meta name='Subject' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>

			<meta name='Title' content='" . $data['title'] . "'>
			
            <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>			

			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/bootstrap431/css/bootstrap-grid.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/bootstrap431/css/bootstrap-reboot.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/bootstrap431/css/bootstrap.css' />			

			
			<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.1/css/all.css' integrity='sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf' crossorigin='anonymous'>			

			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/style.css' />			
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/default.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/admin.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/print.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/v2.css' />

			$css
			$js
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/global.js'></script>

		</head>

        
		<body class='" . $bodyClass . "'>

			<div class='header'>
				<div class='wrapper'>
                    <div class='headerdesktop'>			
    					<div class='headerLeft'>
    						<ul class='contactInfo'>
    							<li><i class='fas fa-phone'></i> 877.511.1555</li>
    							<li><i class='fas fa-envelope'></i> info@campaignsports.com</li>
    						</ul>
    					</div>
    					<div class='headerMiddle'>
    						<a href='index.php'>
    						<img src='" . $_SESSION['relative_path'] . "images/logo.png' width='' height='' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' />
    						</a>
    					</div>
    					<div class='headerRight'>
    						<ul class='socialIcons'>
    							<li><a href=''><i class='fab fa-facebook-f'></i></a></li>
    							<li><a href=''><i class='fab fa-twitter'></i></a></li>
    							<li><a href=''><i class='fab fa-youtube'></i></a></li>
    							<li><a href=''><i class='fab fa-instagram'></i></a></li>
    							<li><a href=''><i class='fab fa-linkedin-in'></i></a></li>
    							<li><a href=''><i class='fab fa-google-plus-g'></i></a></li>
    						</ul>
    					</div>
                    </div>						
                    <div class='headermobile'>			
                        <nav class='navbar navbar-expand-lg navbar-dark'>
                        	<a class='navbar-brand' href='#'><img src='" . $_SESSION['relative_path'] . "images/logo.png' width='' height='' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' /></a>
                        	<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#navbarSupportedContent' aria-controls='navbarSupportedContent' aria-expanded='false' aria-label='Toggle navigation'>
                        	  <span class='navbar-toggler-icon'></span>
                        	</button>
                          
                        	<div class='collapse navbar-collapse' id='navbarSupportedContent'>
                        	  <ul class='socialIcons'>
                        			<li><i class='fas fa-phone'></i> 877.511.1555</li>
                        			<li><i class='fas fa-envelope'></i> info@campaignsports.com</li>	  
                        			<li><a href=''><i class='fab fa-facebook-f'></i></a></li>
                        			<li><a href=''><i class='fab fa-twitter'></i></a></li>
                        			<li><a href=''><i class='fab fa-youtube'></i></a></li>
                        			<li><a href=''><i class='fab fa-instagram'></i></a></li>
                        			<li><a href=''><i class='fab fa-linkedin-in'></i></a></li>
                        			<li><a href=''><i class='fab fa-google-plus-g'></i></a></li>
                        	  </ul>
                        	</div>
                        </nav>
                    </div>


				</div>
			</div>		
            <div class='pageWrap'>
	";

}

function startToMainPublicHeader($data) {

	$css = ($data['css']) ? $data['css'] : "";

	$js = ($data['js']) ? $data['js'] : "";

	$bodyClass = ( isset($data['bodyClass']) && $data['bodyClass'] ) ? $data['bodyClass'] : "bodyBkg";


	echo "

		<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>

		<html xmlns='http://www.w3.org/1999/xhtml'>

		<head>

			<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
            <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
			<meta name='MSSmartTagsPreventParsing' content='TRUE' />

			<meta http-equiv='imagetoolbar' content='no' />

			<title>" . $data['title'] . "</title>

			<meta name='resource-type' content='document'>

			<meta http-equiv='pragma' content='no-cache'>

			<meta name='copyright' content='Copyright Campaign Sports - All Rights Reserved.'>

			<meta name='author' content='Campaign Sports - www.campaignsports.com - Manalapan, New Jersey 07726'>

			<meta http-equiv='reply-to' content='info@campaignsports.com'>

			<meta name='language' content='English'>

			<link rev='made' href='mailto:mkents@campaignsports.com'>

			<meta name='keywords' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>

			

			<meta name='rating' content='General'>

			<meta name='revisit-after' content='14 days'>

			

			<meta name='ROBOTS' content='ALL'>

			<meta name='DC.Description' content='" . $data['desc'] . "'>

			<meta name='DC.Language' scheme='RFC1766' content='EN'>

			<meta name='DC.Coverage.PlaceName' content='USA'>

			

			<meta name='description' content='" . $data['desc'] . "'>

			<meta name='Subject' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>

			<meta name='Title' content='" . $data['title'] . "'>
			<meta property='og:image' content='https://www.joinourcampaign.com/v2/team/" . $data['team'] . "/logo.jpg'/>
			<meta property='og:title' content='" . $data['title'] . "'/>
			<meta property='og:url' content='https://www.joinourcampaign.com/v2/team/" . $data['team'] . "/share/index.php'/>
			<meta property='og:site_name' content='" . $data['pgname'] . "'/>
			
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/default.css' />

			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/print.css' />

			$css

			$js

			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/global.js'></script>


		</head>

		<body class='" . $bodyClass . "'>

			<div class='pageWrap'>

				<div class='header'>

					<a href='index.php'>

						<img src='" . $_SESSION['relative_path'] . "images/logo_campaign_sports.gif' width='291' height='88' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' />

					</a>

				</div>

			

			

	";

}


function closePageWrapToEnd() {
	echo "

			<div class='clear'></div>
			</div>
			<!-- /pageWrap -->

            <div class='teamFooterCoach iconnav' align='center'>
            <br>
            <br>            
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/home-selected-icon.png' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=account' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/profile-selected-icon.png' class='fa-7x'></div><br><center><h1>PROFILE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=c_checklist' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/checklist-selected-icon.png' class='fa-7x'></div><br><center><h1>TASK  LIST</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=brochure' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/brochure-selected-icon.png' class='fa-7x'></div><br><center><h1>BROCHURE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=checks' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v5/images/coaches/checks-selected-icon.png' class='fa-7x'></div><br><center><h1>CHECKS</h1></center></a></div>                    
                </div>
			</div>
		
			<div class='footer'>
				<div class='footerContentTop'>
					<div class='wrapper'>
					
<div class='container'>				

                    <div class='row'>
                        <div class='col footerContentTopleft'>
							<img src='" . $_SESSION['relative_path'] . "images/logo.png' width='' height='' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' />

								<ul class='socialIcons'>
									<li><a href=''><i class='fab fa-facebook-f'></i></a></li>
									<li><a href=''><i class='fab fa-twitter'></i></a></li>
									<li><a href=''><i class='fab fa-youtube'></i></a></li>
									<li><a href=''><i class='fab fa-instagram'></i></a></li>
									<li><a href=''><i class='fab fa-linkedin-in'></i></a></li>
									<li><a href=''><i class='fab fa-google-plus-g'></i></a></li>
								</ul>
                        </div>
                        <div class='col footerContentTopRight'>
							<h4>Information</h4>
							<ul>
								<li><span><i class='fas fa-map-marker-alt'></i></span><span>Delray Beach, FL<br />Red Bank, NJ</span></li>
								<li><span><i class='fas fa-phone'></i></span><span>Toll Free: <a href='tel:8775111555'>877.511.1555</a></span></li>
								<li><span><i class='fas fa-envelope'></i></span><span>Email: <a href='mailto:info@campaignsports.com'>info@campaignsports.com</a></span></li>
							</ul>
                        </div>
                    </div>	

</div>
			

				</div>
				<div class='footerContentBottom'>
					<div class='footerContentBottomWrap'>
						<p>Copyright &copy  Campaign Sports 2018</p>
						<ul class='a' style='color:white !important;'>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Privacy Policy</a></li>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Terms and Conditions</a></li>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Site Map</a></li>
						</ul>
					</div>
				</div>
				
				<style>
				    .a a:link, .a a:visited{
				        color:white !important;
				    }
				</style>

				<div class='footerContent'>
					<p><a href='http://campaignsports.com/' class='foot' target='_blank'>Member Login</a> |  <a href='http://campaignsports.com/speak-with-campaign-sports-representative.php' class='foot' target='_blank'>Contact Us</a> |  <a href='http://campaignsports.com/campaign-sports-FAQs.php' class='foot' target='_blank'>FAQs</a> |  <a href='http://campaignsports.com/campaign-sports-terms-of-use.php' class='foot' target='_blank'>Terms of Use</a> |  <a href='http://campaignsports.com/campaign-sports-privacy-policy.php' class='foot' target='_blank'>Privacy Policy</a> |  <a href='http://campaignsports.com/campaign-sports-site-map.php' class='foot' target='_blank'>Site Map</a></p>
					<p>&copy; Copyright " . date('Y') . " Campaign Sports, LLC. All Rights Reserved.</p>
					<p style='font-size:11px;'>This site designed and maintained by <a href='http://www.trailassociates.com' target='_blank' style='font-size:11px;'>Trail Associates</a>. Hosting for this site provided by <a href='http://www.trailwebservices.com' target='_blank' style='font-size:11px;'>Trail Web Services</a>.</p>
				</div>			
			<div class='clear'></div>   
			</div>
		</div>	

        <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

        </body>
        </html>
	";
}


function closePageWrapToEndAthlete() {
	echo "

			<div class='clear'></div>
			</div>
			<!-- /pageWrap -->

            <div class='teamFooterAthlete iconnav' align='center'>
            <br>
            <br>            
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=account' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>PROFILE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorlist' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONOR  LIST</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
		
			<div class='footer'>
				<div class='footerContentTop'>
					<div class='wrapper'>
					
<div class='container'>				

                    <div class='row'>
                        <div class='col footerContentTopleft'>
							<img src='" . $_SESSION['relative_path'] . "images/logo.png' width='' height='' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' />

								<ul class='socialIcons'>
									<li><a href=''><i class='fab fa-facebook-f'></i></a></li>
									<li><a href=''><i class='fab fa-twitter'></i></a></li>
									<li><a href=''><i class='fab fa-youtube'></i></a></li>
									<li><a href=''><i class='fab fa-instagram'></i></a></li>
									<li><a href=''><i class='fab fa-linkedin-in'></i></a></li>
									<li><a href=''><i class='fab fa-google-plus-g'></i></a></li>
								</ul>
                        </div>
                        <div class='col footerContentTopRight'>
							<h4>Information</h4>
							<ul>
								<li><span><i class='fas fa-map-marker-alt'></i></span><span>Delray Beach, FL<br />Red Bank, NJ</span></li>
								<li><span><i class='fas fa-phone'></i></span><span>Toll Free: <a href='tel:8775111555'>877.511.1555</a></span></li>
								<li><span><i class='fas fa-envelope'></i></span><span>Email: <a href='mailto:info@campaignsports.com'>info@campaignsports.com</a></span></li>
							</ul>
                        </div>
                    </div>	

</div>
			

				</div>
				<div class='footerContentBottom'>
					<div class='footerContentBottomWrap'>
						<p>Copyright &copy  Campaign Sports 2018</p>
						<ul class='a' style='color:white !important;'>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Privacy Policy</a></li>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Terms and Conditions</a></li>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Site Map</a></li>
						</ul>
					</div>
				</div>
				
				<style>
				    .a a:link, .a a:visited{
				        color:white !important;
				    }
				</style>

				<div class='footerContent'>
					<p><a href='http://campaignsports.com/' class='foot' target='_blank'>Member Login</a> |  <a href='http://campaignsports.com/speak-with-campaign-sports-representative.php' class='foot' target='_blank'>Contact Us</a> |  <a href='http://campaignsports.com/campaign-sports-FAQs.php' class='foot' target='_blank'>FAQs</a> |  <a href='http://campaignsports.com/campaign-sports-terms-of-use.php' class='foot' target='_blank'>Terms of Use</a> |  <a href='http://campaignsports.com/campaign-sports-privacy-policy.php' class='foot' target='_blank'>Privacy Policy</a> |  <a href='http://campaignsports.com/campaign-sports-site-map.php' class='foot' target='_blank'>Site Map</a></p>
					<p>&copy; Copyright " . date('Y') . " Campaign Sports, LLC. All Rights Reserved.</p>
					<p style='font-size:11px;'>This site designed and maintained by <a href='http://www.trailassociates.com' target='_blank' style='font-size:11px;'>Trail Associates</a>. Hosting for this site provided by <a href='http://www.trailwebservices.com' target='_blank' style='font-size:11px;'>Trail Web Services</a>.</p>
				</div>			
			<div class='clear'></div>   
			</div>
		</div>	

        <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

        </body>
        </html>
	";
}





function closePageWrapToEndDonor() {
	echo "

			<div class='clear'></div>
			</div>
			<!-- /pageWrap -->

            <div class='teamFooterAthlete iconnav' align='center'>
            <br>
            <br>            
                <div class='row'>
                    <div class='col5'><a href='index.php?action=dashboard' class='" . $n1 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-home.jpg' class='fa-7x'></div><br><center><h1>HOME</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=addContact' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-profile.jpg' class='fa-7x'></div><br><center><h1>SELECT ATHLETE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=donorList' class='" . $n5 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-donorlist.jpg' class='fa-7x'></div><br><center><h1>DONATE</h1></center></a></div>
                    <div class='col5'><a href='index.php?action=share' class='" . $n2 ."'><div class='fa-7-xwrapper'><img src='https://csjoc.com/v6/images/athlete-share.jpg' class='fa-7x'></div><br><center><h1>SHARE</h1></center></a></div>                  
                </div>
			</div>
		
			<div class='footer'>
				<div class='footerContentTop'>
					<div class='wrapper'>
					
<div class='container'>				

                    <div class='row'>
                        <div class='col footerContentTopleft'>
							<img src='" . $_SESSION['relative_path'] . "images/logo.png' width='' height='' border='0' alt='Campaign Sports - Sports Team Program Fundraising' class='logo' />

								<ul class='socialIcons'>
									<li><a href=''><i class='fab fa-facebook-f'></i></a></li>
									<li><a href=''><i class='fab fa-twitter'></i></a></li>
									<li><a href=''><i class='fab fa-youtube'></i></a></li>
									<li><a href=''><i class='fab fa-instagram'></i></a></li>
									<li><a href=''><i class='fab fa-linkedin-in'></i></a></li>
									<li><a href=''><i class='fab fa-google-plus-g'></i></a></li>
								</ul>
                        </div>
                        <div class='col footerContentTopRight'>
							<h4>Information</h4>
							<ul>
								<li><span><i class='fas fa-map-marker-alt'></i></span><span>Delray Beach, FL<br />Red Bank, NJ</span></li>
								<li><span><i class='fas fa-phone'></i></span><span>Toll Free: <a href='tel:8775111555'>877.511.1555</a></span></li>
								<li><span><i class='fas fa-envelope'></i></span><span>Email: <a href='mailto:info@campaignsports.com'>info@campaignsports.com</a></span></li>
							</ul>
                        </div>
                    </div>	

</div>
			

				</div>
				<div class='footerContentBottom'>
					<div class='footerContentBottomWrap'>
						<p>Copyright &copy  Campaign Sports 2018</p>
						<ul class='a' style='color:white !important;'>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Privacy Policy</a></li>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Terms and Conditions</a></li>
							<li style='color:white !important;'><a href='' style='color:white:!important;'>Site Map</a></li>
						</ul>
					</div>
				</div>
				
				<style>
				    .a a:link, .a a:visited{
				        color:white !important;
				    }
				</style>

				<div class='footerContent'>
					<p><a href='http://campaignsports.com/' class='foot' target='_blank'>Member Login</a> |  <a href='http://campaignsports.com/speak-with-campaign-sports-representative.php' class='foot' target='_blank'>Contact Us</a> |  <a href='http://campaignsports.com/campaign-sports-FAQs.php' class='foot' target='_blank'>FAQs</a> |  <a href='http://campaignsports.com/campaign-sports-terms-of-use.php' class='foot' target='_blank'>Terms of Use</a> |  <a href='http://campaignsports.com/campaign-sports-privacy-policy.php' class='foot' target='_blank'>Privacy Policy</a> |  <a href='http://campaignsports.com/campaign-sports-site-map.php' class='foot' target='_blank'>Site Map</a></p>
					<p>&copy; Copyright " . date('Y') . " Campaign Sports, LLC. All Rights Reserved.</p>
					<p style='font-size:11px;'>This site designed and maintained by <a href='http://www.trailassociates.com' target='_blank' style='font-size:11px;'>Trail Associates</a>. Hosting for this site provided by <a href='http://www.trailwebservices.com' target='_blank' style='font-size:11px;'>Trail Web Services</a>.</p>
				</div>			
			<div class='clear'></div>   
			</div>
		</div>	

        <script src='https://code.jquery.com/jquery-3.3.1.slim.min.js' integrity='sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo' crossorigin='anonymous'></script>
        <script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js' integrity='sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1' crossorigin='anonymous'></script>
        <script src='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js' integrity='sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM' crossorigin='anonymous'></script>

        </body>
        </html>
	";
}



function suggestionsWrap() {

	echo "

                <div class='suggestionsWrap'>

                    <p class='teamPrimaryTxtColor'><strong>Here are some suggestions</strong></p>

 					<ul class='suggestList teamPrimaryTxtColor'>

                    <li class='bg1'><span class='txtBlack'>Former coaches</span></li>

                    <li><span class='txtBlack'>Family doctors / Dentists</span></li>

                    <li class='bg1'><span class='txtBlack'>Grandparents</span></li>

                    <li><span class='txtBlack'>Aunts / Uncles / Cousins</span></li>

                    <li class='bg1'><span class='txtBlack'>Close family / friends in the business world</span></li>

                    <li><span class='txtBlack'>Neighbors</span></li>

                    <li class='bg1'><span class='txtBlack'>Work Associates</span></li>

                    </ul>

                </div>

	";

}









?>