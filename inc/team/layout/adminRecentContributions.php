<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$donationsList = TeamUser::getAllDonations($_SESSION['team_id']);

	echo "
                <div class='suggestionsWrap'>
                    <p class='teamPrimaryTxtColor'><strong>Most Recent Contributions</strong></p>
                    
					<table width='100%' border='0' cellspacing='0' cellpadding='3'>
						<tr>
							<td width='60px'><strong>Amount</strong></td>
							<td align='center'><strong>Donor</strong></td>
							<td width='55px' align='center'><strong>Location</strong></td>
						</tr>
	";
                        $classAlternate = "bg1";
						$count = 0;
						foreach($donationsList AS $d) {
							if ($count > 4) continue;
							echo "
						<tr class='$classAlternate'>
							<td><a href='index.php?action=donations&view=" . $d['ID'] . "'>$" . $d['donationValue'] . "</a></td>
							<td align='left'>" . substr($d['fname'],0,1) . ". " . $d['lname'] . "</td>
							<td align='center'>" . $d['state'] . "</td>
						</tr>
							";
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";
							$count++;
						}
	echo "
						<tr>
							<td colspan='3'>&nbsp;</td>
						</tr>      
						<tr>
							<td colspan='3' align='right'><a href='index.php?action=donations'>View All</a></td>
						</tr>              
					</table>
                </div>
	";
?>