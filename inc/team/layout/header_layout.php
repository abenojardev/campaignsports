<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>

		<head>

			<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
            <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />
			<meta name='MSSmartTagsPreventParsing' content='TRUE' />

			<meta http-equiv='imagetoolbar' content='no' />

			<title>Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program</title>

			<meta name='resource-type' content='document'>

			<meta http-equiv='pragma' content='no-cache'>

			<meta name='copyright' content='Copyright Campaign Sports - All Rights Reserved.'>

			<meta name='author' content='Campaign Sports - www.campaignsports.com - Manalapan, New Jersey 18301'>

			<meta http-equiv='reply-to' content='info@campaignsports.com'>

			<meta name='language' content='English'>

			<link rev='made' href='mailto:mkents@campaignsports.com'>

			<meta name='keywords' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>


			<meta name='rating' content='General'>

			<meta name='revisit-after' content='14 days'>


			<meta name='ROBOTS' content='ALL'>

			<meta name='DC.Description' content='Campaign Sports is fast, easy and stress-free. Just sit back and let our systems do the hard work.'>

			<meta name='DC.Language' scheme='RFC1766' content='EN'>

			<meta name='DC.Coverage.PlaceName' content='USA'>



			<meta name='description' content='Campaign Sports is fast, easy and stress-free. Just sit back and let our system do the hard work.'>

			<meta name='Subject' content='sports fundraising, sports team fundraising, sports team sponsorship, Fundraising success, sponsorship brochure, brochure, sports team fundraising profits'>

			<meta name='Title' content='" . $data['title'] . "'>
			
            <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css' integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T' crossorigin='anonymous'>			

			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/bootstrap431/css/bootstrap-grid.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/bootstrap431/css/bootstrap-reboot.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/bootstrap431/css/bootstrap.css' />			

			
			<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.8.1/css/all.css' integrity='sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf' crossorigin='anonymous'>			

			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/style.css' />			
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/default.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/admin.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/print.css' />
			<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/v2.css' />

			$css
			$js
			<script type='text/javascript' src='" . $_SESSION['relative_path'] . "inc/team/scripts/global.js'></script>

		</head>
