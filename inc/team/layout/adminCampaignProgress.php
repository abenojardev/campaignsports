<?php
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$donation = TeamUser::getDonationTotal($_SESSION['current_folder']);
	$donationTotal = $donation['donationTotal'];
	$donationTotal = ($donationTotal) ?  $donationTotal : 0;
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$Team = new TeamMain($_SESSION["current_folder"]);
	$goal = $Team->getTeamGoal();
	$colors = $Team->getTeamColors();
	
	$goalPerc = $donationTotal / $goal;
	$goalBar = round($goalPerc * 460);
	
	if ($goalBar > 460) $goalBar = 460;
	
	$tic1 = $goal / 5;
	$tic2 = $tic1 * 2;
	$tic3 = $tic1 * 3;
	$tic4 = $tic1 * 4;
	
	echo "
					<link rel='stylesheet' type='text/css' href='" . $_SESSION['relative_path'] . "css/donationBar.css' />
					<h2 class='teamPrimaryTxtColor'>My Team's Campaign Progress</h2>
					<div class='campaignProgress'>
						<br />
						<div class='campaignProgressTitleBar'>
							<div class='donationBarMainContainer'>
								<div class='donationBarTitleLeft'>
									<strong>Donations</strong>
								</div>
								<div class='donationBarTitleRight' style='color:#".$colors['txtColor']."'>
									<strong>Our Goal</strong>
								</div>
								<div class='clear'><br /></div>
								
								<div class='donationBarContainer'>
									<div class='ticDiv ticDiv1'>&nbsp;</div>
									<div class='ticDiv'>&nbsp;</div>
									<div class='ticDiv'>&nbsp;</div>
									<div class='ticDiv'>&nbsp;</div>
									<div class='ticDiv'>&nbsp;</div>
								</div>
								<div class='donationBarContainer2'>
									<div class='ticDivValue ticDivValue1'>0</div>
									<div class='ticDivValue ticDivValue2'>$tic1</div>
									<div class='ticDivValue ticDivValue3'>$tic2</div>
									<div class='ticDivValue ticDivValue4'>$tic3</div>
									<div class='ticDivValue ticDivValue5'>$tic4</div>
									<div class='ticDivValue ticDivValue6'>$goal</div>
								</div>
							 
								<div class='donationBarLower'></div>
								<div class='donationBar' style='width:".$goalBar."px;background-color:#".$colors['bgColor1']."'></div>
								<div class='donationBarTotal' style='width:".$goalBar."px;'>
									<div class='donationBarTotalValue'>
										$donationTotal
									</div>
								</div>
							</div>
						</div>
                    </div>
					
	";
?>