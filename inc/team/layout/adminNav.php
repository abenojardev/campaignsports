<?php

	$n1 = $n2 = $n3 = $n4 = $n5 = $n6 = $n7 = "cNav";
	
	switch($_SESSION["nav"]) {
		case 'dashboard':
		$n1 = "cNavOn";
		break;
		
		case 'account':
		$n2 = "cNavOn";
		break;
		
		case 'players':
		$n3 = "cNavOn";
		break;
		
		case 'contacts':
		$n4 = "cNavOn";
		break;
		
		case 'donations':
		$n5 = "cNavOn";
		break;
		
		case 'pageManager':
		$n6 = "cNavOn";
		break;

		case 'campaignActivity':
		$n7 = "cNavOn";
		break;

		default:
		$n1 = "cNavOn";
		break;
	}

	echo "
            <div class='coachNav'>
                <a href='index.php?action=dashboard' class='" . $n1 ."'>Dashboard</a> 
				<a href='index.php?action=campaignActivity' class='" . $n7 ."'>Campaign Activity</a>
                <a href='index.php?action=account' class='" . $n2 ."'>My Account</a>
                <a href='index.php?action=players' class='" . $n3 ."'>Team Athletes</a>
                <a href='index.php?action=contacts' class='" . $n4 ."'>Team Contacts</a>
                <a href='index.php?action=donations' class='" . $n5 ."'>Team Donations</a>
                <a href='index.php?action=pageManager' class='" . $n6 ."'>Manage Team Page</a>
                
            <div class='clear'></div>
            </div>
	";
?>