<?php

	$data = array(

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '',

		'js' => '');

	

    startToMainHeader($data);

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

	$TeamMain = new TeamMain($_SESSION['current_folder']);

	$status = $TeamMain->getTeamStatus();



	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
$campStatus = TeamUser::getCampaignStatus11($_SESSION['current_folder']);
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);

	$contactCount = TeamUser::countContacts($_SESSION['current_folder']);

	$contacts = TeamUser::getAllContactsWithPlayerNameSort($_SESSION['current_folder']);

?>    



    <div class='pageContentWrap teamSecondaryBGColor'>

    

    	<div class='innerPageContentWrap teamPrimaryBGColor'>

        

        	<div class='pageContent'>

            

            <?php showteamHeader(); ?>

			

            <div class='topContent'>

            	<div class='welcomeBar'>

                    <div class="welcomeBarCol1"><h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $contact['fname']." " .$contact['lname']; ?></span></h1></div>

                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>

                </div>

                

                <div class='clear'></div>

                

                <p>On this page, you are able to manage and update all of your team�s contacts that have been entered into the Contact Collection system. This also provides the ability to monitor the number of contacts each of your athletes have entered during the Contact Collection process of the campaign.

 </p>

                

            </div>

            

            <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php'); ?>

            

			<div class='contentLeft'>

            

                <div class='contentLeftData'>

                

                <?php 
					include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionProgress.php'); 
					include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionUpdate.php');
				?>


                <h2 class='teamPrimaryTxtColor'>My Team Contacts</h2>



					<table width='100%' border='0' cellspacing='0' cellpadding='7'>

                      <tr>

                        <td><strong>Contact Name</strong></td>

                        <td><strong>City</strong></td>

                        <td align='center'><strong>State</strong></td>

                        <td><strong>Athlete Name</strong></td>

                        <td align='center'><strong>DEL</strong></td>

                      </tr>

                      

             <?php

			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;

				$tpages = ceil($contactCount/15);

				$min = ($page - 1) * 15 + 1;

				$max = $page * 15;

				

				$classAlternate = "bg1";

				$count = 0;

				foreach($contacts AS $c) {

					$count++;

					if ($count < $min || $count > $max) continue;

					
if($campStatus==11) {
					echo "

						<tr class='$classAlternate'>

							<td align='left'><a href='index.php?action=contacts&edit=" . $c['ID'] . "'>" . $c['lname'] . ", " . $c['fname'] . "</a></td>

							<td>" . $c['city'] . "</td>

							<td align='center'>" . $c['state'] . "</td>

							<td>" . $c['pFname'] . " " . $c['pLname'] . "</td>

							<td align='center'>N/A</td>

						</tr>

					";
} else {
	echo "

						<tr class='$classAlternate'>

							<td align='left'><a href='index.php?action=contacts&edit=" . $c['ID'] . "'>" . $c['lname'] . ", " . $c['fname'] . "</a></td>

							<td>" . $c['city'] . "</td>

							<td align='center'>" . $c['state'] . "</td>

							<td>" . $c['pFname'] . " " . $c['pLname'] . "</td>

							<td align='center'><a href='index.php?action=contacts&delete=".$c['ID']."' class='delCheck'>X</a></td>

						</tr>

					";
}
	

					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";

				}

	

			?>

                    </table>

                    	

            <?php

                echo "<div class='pagination'><br/>";

				$reload = $_SERVER['PHP_SELF'] . "?action=" . $_SESSION["nav"];

            	include_once($_SESSION['relative_path'] . 'inc/team/layout/pagination.php');

				echo paginate($reload, $page, $tpages, 3);

				echo "</div>";

			?>

                    

                </div>

                <!-- /contentLeftData -->

            

        	<div class='clear'></div> 

      		</div>

        	<!-- /contentLeft -->

            

			<script type='text/javascript' src='<?php echo $_SESSION['relative_path']; ?>inc/common/scripts/jquery-1.5.1.min.js'></script>

			<script language="javascript" type="text/javascript">

			/* <![CDATA[ */

				$('.delCheck').click(function(event) {

					var r=confirm("Are you sure you want to delete this contact? \nThis action is irreversible.");

					if (r==false) {

						event.preventDefault();

					}		

				});

			/* ]]> */

			</script>

            

        	<div class='contentRight'>

				<?php 

					if ( $status == 1 ) { // Phase 1

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');

                    } 

                    else {

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');

                    }

                ?>

            <div class='clear'></div>

           	</div>

			<!-- /contentRight -->


			<div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
        	<div class='clear'></div>

            </div>

            <!-- /pageContent -->

        

        <div class='clear'></div>

        </div>

        <!-- /innerPageContentWrap -->

    

    <div class='clear'></div>

    </div>

	<!-- /pageContentWrap -->





<?php

	closePageWrapToEnd();

?>