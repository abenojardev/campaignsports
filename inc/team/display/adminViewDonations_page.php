<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);
	$ID = array();
	$ID['ID'] = $_GET['view'];
	$ID['table'] = 'donations';
	$donationData = TeamUser::getDonationData($_GET['view']);
?>    

    <div class='pageContentWrap teamSecondaryBGColor'>
    
    	<div class='innerPageContentWrap teamPrimaryBGColor'>
        
        	<div class='pageContent'>
            
            <?php showteamHeader(); ?>

            <div class='topContent'>
            	<div class='welcomeBar'>
                    <div class="welcomeBarCol1"><h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $contact['fname']." " .$contact['lname']; ?></span></h1></div>
                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>
                </div>
                
                <div class='clear'></div>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed lacinia ultricies nunc, vel aliquet nunc congue in. Nulla rutrum tortor et neque commodo luctus. Sed ante nisi, aliquam scelerisque aliquam eu, vulputate sed ligula. Integer at dolor quis felis faucibus convallis nec sit amet nulla. Mauris dapibus sodales erat non sollicitudin. Mauris id mi at justo fermentum mattis. </p>
                
            </div>
            
            <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
                
                    <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminCampaignProgress.php'); ?>
                    
                    <h2 class="teamPrimaryTxtColor">Donor Information</h2>

					<div class="registerWrap">
                        <p><strong>Sponsored Athlete:</strong><strong> <a href='index.php?action=players&edit=<?php echo $donationData['pID']; ?>'><?php echo $donationData['pFname']." ".$donationData['pLname']; ?></a></strong></p>
                        <br />
                        <p><strong>Payment method:</strong> <?php echo ucfirst($donationData['paymentMethod']); ?> Donation</p>
                        <p><strong>Date:</strong> <?php echo date("F j, Y", strtotime($donationData['donation_date'])); ?> </p>
                        <p><strong>Amount:</strong> <strong class="teamPrimaryTxtColor">$ <?php echo $donationData['donationValue']; ?></strong></p>
                        <p><strong>Card type:</strong> <?php echo $donationData['cardType']; ?></p>
                        <p><strong>Status:</strong> <?php echo $donationData['r_approved']; ?></p>
                        <p><strong>Reference code:</strong> <?php echo $donationData['r_ref']; ?></p>
                        <br />
                        <p><strong>Name:</strong> <?php echo $donationData['fname']." ".$donationData['lname']; ?></p>
                        <p><strong>Address:</strong> <?php echo $donationData['address']; ?></p>
                        <p><strong>Address 2:</strong> <?php echo $donationData['address2']; ?></p>
                        <p><strong>City:</strong> <?php echo $donationData['city']; ?></p>
                        <p><strong>State:</strong> <?php echo $donationData['state']; ?></p>
                        <p><strong>Zip:</strong> <?php echo $donationData['zip']; ?></p>
                        <p><strong>Country:</strong> <?php echo $donationData['country']; ?></p>
                        <p><strong>Phone:</strong> <?php echo $donationData['phone']; ?></p>
                        <p><strong>Email:</strong> <?php echo $donationData['email']; ?></p>

                        <div class="registerButton"><br />
                            <p><a href="javascript:history.back()" class="teamButton teamPrimaryBGColor">Return to Previous Page</a></p>
                        </div>
                    </div>

                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->
			<div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
            
        	<div class='clear'></div>
            </div>
            <!-- /pageContent -->
        
        <div class='clear'></div>
        </div>
        <!-- /innerPageContentWrap -->
    
    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->


<?php
	closePageWrapToEnd();
?>