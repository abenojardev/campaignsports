<?php

	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateAdminAcct.js"></script>
		');

    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	$password = $TeamMain->getTeamAdminPassword();

	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	$countries = Common::getCountries();

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);

	$contactDisplay = array();

	foreach($contact AS $f => $d) {
		if( isset($d) ) {
			$contactDisplay[$f] = "value='".$d."'";
		} else {
			$contactDisplay[$f] = "";
		}

		if($f == 'state') $contactDisplay[$f] = $d;
		if($f == 'country') $contactDisplay[$f] = $d;
		if($f == 'ID') $contactDisplay[$f] = $d;

		if($f == 'intl') {
			if($d) {
				$contactDisplay[$f] = "checked='checked'";
				$domStyle = "style='display:none;'";
				$intlStyle = "";
			} 

			else {
				$contactDisplay[$f] = "";
				$intlStyle = "style='display:none;'";
				$domStyle = "";
			}
		}
		//echo "f: $f, d: $d<br>";
	}

?>

<div class='pageContentWrap teamSecondaryBGColor'>
  <div class='innerPageContentWrap teamPrimaryBGColor'>
    <div class='pageContent'>
      <?php showteamHeader(); ?>
      <div class='topContent'>
        <div class='welcomeBar'>
          <div class="welcomeBarCol1">
            <h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $contact['fname']." " .$contact['lname']; ?></span></h1>
          </div>
          <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br />
            <div class="logout"><a href="index.php?action=logout">[logout]</a></div>
          </div>
        </div>
        <div class='clear'></div>
        <p>On this page, you are able to manage and update your team’s key contact information. Please make sure all of your information is correct. </p>
      </div>
      <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php'); ?>
      <div class='contentLeft'>
        <div class='contentLeftData'>
          <div class='registerWrap'>
            <h2 class='teamPrimaryTxtColor'>Contact Info</h2>
            <form method='post' name='frm' id='frm' action='index.php?action=account'>
              <!--
              <div>
                <input id="intl" class="intl" name="intl" type="checkbox" value=1 <?php echo $contactDisplay['intl']; ?> />
                Outside of the United States or Canada. <br />
                <br />
              </div>
              -->
              <div class='formElement'>
                <div class='formElementCol1'><span class='alert'>*</span><strong>First Name:</strong></div>
                <div class='formElementCol2'>
                  <input name='fname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['fname']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><span class='alert'>*</span><strong>Last Name:</strong></div>
                <div class='formElementCol2'>
                  <input name='lname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['lname']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Title:</strong></div>
                <div class='formElementCol2'>
                  <input name='title' type='text' class='textFieldSWR' <?php echo $contactDisplay['title']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Address:</strong></div>
                <div class='formElementCol2'>
                  <input name='address' type='text' class='textFieldSWR' <?php echo $contactDisplay['address']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'>&nbsp;<strong>Address 2:</strong></div>
                <div class='formElementCol2'>
                  <input name='address2' type='text' class='textFieldSWR' <?php echo $contactDisplay['address2']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>City:</strong></div>
                <div class='formElementCol2'>
                  <input name='city' type='text' class='textFieldSWR' <?php echo $contactDisplay['city']; ?> />
                </div>
              </div>
              <div id="domesticAddy" <?php echo $domStyle ?> >
                <div class='formElement'>
                  <div class='formElementCol1'><strong>State/Region:</strong></div>
                  <div class='formElementCol2'>
                    <select id='state' name='state' class='selectFieldSWR' size='1'>
                      <option value=''>Please select...</option>
                                <option value=''>&nbsp;</option>
                                <option value=''>---- United States ----</option>
                      <?php
                                    foreach($states as $state)
                                    {
										$selected = "";
										if ($state['abbrev'] == $contactDisplay['state']) $selected = "selected='selected'";
                                        echo "<option value='".$state['abbrev']."'";
                                        echo "$selected>".$state['name']."</option>";
                                    }
                                ?>
                    </select>
                  </div>
                </div>
                <div class='formElement'>
                  <div class='formElementCol1'><strong>Zip/Postal Code:</strong></div>
                  <div class='formElementCol2'>
                    <input name='zip' type='text' class='textFieldSWR' <?php echo $contactDisplay['zip']; ?> />
                  </div>
                </div>
                <div class='formElement'>
                  <div class='formElementCol1'><span class='alert'>*</span><strong>Email:</strong></div>
                  <div class='formElementCol2'>
                    <input name='email' type='text' class='textFieldSWR pemail validate' <?php echo $contactDisplay['email']; ?> />
                  </div>
                </div>
              </div>
              <div id="intlAddy" <?php echo $intlStyle ?> >
                <div class='formElement'>
                  <div class='formElementCol1'><strong>Province/Region:</strong></div>
                  <div class='formElementCol2'>
                    <input name='stateI' type='text' class='textFieldSWR' <?php if ($contactDisplay['state']) echo "value='".$contactDisplay['state']."'"; ?> />
                  </div>
                </div>
                <div class='formElement'>
                  <div class='formElementCol1'><strong>Postal Code:</strong></div>
                  <div class='formElementCol2'>
                    <input name='zipI' type='text' class='textFieldSWR' <?php echo $contactDisplay['zip']; ?> />
                  </div>
                </div>
                <div class='formElement'>
                  <div class='formElementCol1'><strong>Country:</strong></div>
                  <div class='formElementCol2'>
                    <select id='country' name='country' class='selectFieldSWR' size='1'>
                      <option value=''>Please select...</option>
                      <?php
                                    foreach($countries as $country)
                                    {
										$selected = "";
										if ($country['abbrev'] == $contactDisplay['country']) $selected = "selected=\"selected\"";
                                        echo "<option value='".$country['abbrev']."'";
                                        echo "$selected>".$country['name']."</option>";
                                    }
                                ?>
                    </select>
                  </div>
                </div>
                <div class='formElement'>
                  <div class='formElementCol1'><strong>Email:</strong></div>
                  <div class='formElementCol2'>
                    <input name='emailI' type='text' class='textFieldSWR' <?php echo $contactDisplay['email']; ?> />
                  </div>
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Daytime Phone:</strong></div>
                <div class='formElementCol2'>
                  <input name='phoneDay' type='text' class='textFieldSWR' <?php echo $contactDisplay['phoneDay']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Evening Phone:</strong></div>
                <div class='formElementCol2'>
                  <input name='phoneEve' type='text' class='textFieldSWR' <?php echo $contactDisplay['phoneEve']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Cell Phone:</strong></div>
                <div class='formElementCol2'>
                  <input name='phoneCell' type='text' class='textFieldSWR' <?php echo $contactDisplay['phoneCell']; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Password:</strong></div>
                <div class='formElementCol2'>
                  <input name='coachPassword' type='text' class='textFieldSWR validate validateP1' value='<?php echo $password; ?>' />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Password (validate):</strong></div>
                <div class='formElementCol2'>
                  <input name='coachPassword2' type='text' class='textFieldSWR validate validateP2' value='<?php echo $password; ?>' />
                </div>
              </div>
              <p>
                <input type="hidden" name="cID" value='<?php echo $contactDisplay['ID']; ?>' />
                <input type="hidden" name="submitAction" value='adminUpdateAccount' />
                <a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update My Team Account</a> </p>
            </form>
          </div>
        </div>
        <!-- /contentLeftData -->
        
        <div class='clear'></div>
      </div>
      <!-- /contentLeft -->
      
      <div class='contentRight'>
        <?php 
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
        <div class='clear'></div>
      </div>
      <!-- /contentRight -->
      
      <div class='clear'></div>
      <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
      <div class='clear'></div>
    </div>
    <!-- /pageContent -->
    
    <div class='clear'></div>
  </div>
  <!-- /innerPageContentWrap -->
  
  <div class='clear'></div>
</div>
<!-- /pageContentWrap -->

<?php
	closePageWrapToEnd();
?>
