<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateRegister.js"></script>
		');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
?>    

    <div class='pageContentWrap teamSecondaryBGColor'>
    
    	<div class='innerPageContentWrap teamPrimaryBGColor'>
        
        	<div class='pageContent'>
            
            <?php showteamHeader(); ?>
			
            <div class='topContent'>
            	<div class='welcomeBar'>
                    <div class='welcomeBarCol1'>&nbsp;</div>
                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>
                </div>
                
                <div class='clear'></div>
                
                <div class='welcomeBarLogin'>
                    <h1>Welcome to the <span class='teamPrimaryTxtColor'><?php echo $team; ?></span> Registration Page</h1>
                </div>
                
                <div class='clear'></div>
                
                <div align="center">
                <p>Please register as an athlete for your team by filling out your information below. Thank you! </p>
            	</div>
            </div>

        	<div class='contentFull'>

                <div class='registerWrap'>
                <div id='loginDiv'><?php if (isset($postDupe['error'])) echo $postDupe['error']; ?></div>
                <form method='post' name='frm' id='frm' action='index.php'>

                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>First Name:</strong></div>
                        <div class='formElementCol2'><input name='fname' type='text' class='textFieldSWR validate' /></div>
                  </div>
                  
                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>Last Name:</strong></div>
                        <div class='formElementCol2'><input name='lname' type='text' class='textFieldSWR validate' /></div>
                  </div>

                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>Address:</strong></div>
                        <div class='formElementCol2'><input name='address' type='text' class='textFieldSWR validate' /></div>
                  </div>
                  
                  <div class='formElement'>
                        <div class='formElementCol1'>&nbsp;<strong>Address 2:</strong></div>
                        <div class='formElementCol2'><input name='address2' type='text' class='textFieldSWR' /></div>
                  </div>
                  
                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>City:</strong></div>
                        <div class='formElementCol2'><input name='city' type='text' class='textFieldSWR validate' /></div>
                  </div>
                  
                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>State:</strong></div>
                        <div class='formElementCol2'>
                        <select id='state' name='state' class='selectFieldSWR validate' size='1'>
                           <option value=''>Please select...</option>
                                <option value=''>&nbsp;</option>
                                <option value=''>---- United States ----</option>
							<?php
                                foreach($states as $state)
                                {
                                    echo '<option value="'.$state['abbrev'].'"';
                                    echo '>'.$state['name'].'</option>';
                                }
                            ?>
                        </select>
                        </div>
                  </div>
                  
                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>Zip:</strong></div>
                        <div class='formElementCol2'><input name='zip' type='text' class='textFieldSWR validate' /></div>
                  </div>
                  
                  <div class='formElement'>
                        <div class='formElementCol1'><span class='alert'>*</span><strong>Email:</strong></div>
                        <div class='formElementCol2'><input name='email' type='text' class='textFieldSWR pemail validate' /></div>
                  </div>

				<?php
					if ($_SESSION['current_folder'] > $cfg_playerPassCutoffTeam) {
						echo "
						  <div class='formElement'>
								<div class='formElementCol1'><span class='alert'>*</span><strong>Personal Password:</strong></div>
								<div class='formElementCol2'><input name='password' type='text' class='textFieldSWR validate validateP1' /></div>
						  </div>
						  <div class='formElement'>
								<div class='formElementCol1'><span class='alert'>*</span><strong>Personal Password (verification):</strong></div>
								<div class='formElementCol2'><input name='password2' type='text' class='textFieldSWR validate validateP2' /></div>
						  </div>
						";
					}
				?>

				<div class='registerButton'>
				<p>
                	<input type="hidden" name="action" value='playerRegister' />
                	<input type="hidden" name="cfg_playerPassCutoffTeam" value=<?php echo $cfg_playerPassCutoffTeam ?> />
                    <a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Register and Continue</a>
                </p>
				</div>
 
                </div>

            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->


			<div class='clear'></div>
        	<div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
        	<div class='clear'></div>
            </div>
            <!-- /pageContent -->
        	
            
        <div class='clear'></div>
        </div>
        <!-- /innerPageContentWrap -->
    
    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->


<?php
	closePageWrapToEnd();
?>