<?php

	$data = array(

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '',

		'js' => '');

	

    startToMainHeader($data);

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');

	$Player = new Player($_SESSION['player_id']);

	$name = $Player->getName();
	
	$campStatus = Player::getCampaignStatus11($_SESSION['current_folder']);
	
	$contactsList = Player::getAllContacts();
	
	
	
	//$campBMStatus = Player::getCampaignBMStatus($_SESSION["current_folder"]);
	

	

	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');

	$states = Common::getStates();


	
?>    
<script type="text/javascript">
function campClosed() {
	alert('The contact collection phase of this campaign has ended. You can no longer add or edit contacts. If you need further assistance, please contact your coach.');
}
</script>


    <div class='pageContentWrap teamSecondaryBGColor'>

    

    	<div class='innerPageContentWrap teamPrimaryBGColor'>

        

        	<div class='pageContent'>

            

            <?php showteamHeader(); ?>

			

            <div class='topContent'>

            	<div class='welcomeBar'>

                    <div class="welcomeBarCol1"><h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $name; ?></span></h1></div>

                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span>

                    <br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div>

                </div>

                

                <div class='clear'></div>

                

                <div align="center">

                <p>Welcome to your athlete dashboard. Here you can contribute to your team's Campaign by entering the contact information of potential team supporters. Please enter at least 15 contacts. Thank you for your support!</p>

            	</div>

            </div>

            

            <div class='actionBar'>

            
			<?php
			//if($campStatus==11) {
			?>
            	<!--
                <a href='javascript:void(0);' class='addContactButton' onclick="javascript:campClosed();">Add a New Contact</a> -->
			<?php
			//} else {
			?>
            <!--
            <a href='index.php?action=addContact' class='addContactButton'>Add a New Contact</a>
            -->
			<?php
			//}
			?>
            <a href='index.php?action=addContact' class='addContactButton'>Add a New Contact</a>
            </div>

            

			<div class='contentLeft'>

            

                <div class='contentLeftData'>



					<h2 class='teamPrimaryTxtColor'>My Contacts</h2><br />
<br />



					<table width='100%' border='0' cellspacing='0' cellpadding='7'>

                      <tr>

                        <td><strong>Name</strong></td>

                        <td><strong>Address</strong></td>

                        <td><strong>City</strong></td>

                        <td><strong>State</strong></td>

                        <td><strong>Zip</strong></td>

                        <td><strong>DEL</strong></td>

                      </tr>

                      

					<?php

                        $classAlternate = "bg1";

						//print_r($contactsList);

						foreach($contactsList AS $contact) {

							//print_r($contact);
							
			if($campStatus==11) {
				echo "

								<tr class='$classAlternate'>

									<td><a href='index.php?action=editContact&c=" . $contact['ID'] . "'>" . $contact['lname'] . ", " . $contact['fname'] . "</a></td>

									<td>" . $contact['address'] . "</td>

									<td>" . $contact['city'] . "</td>

									<td align='center'>" . $contact['state'] . "</td>

									<td align='center'>" . $contact['zip'] . "</td>

									<td align='center'>N/A</td>

								</tr>

							";
			} else {
			
			
		
								
							echo "

								<tr class='$classAlternate'>

									<td><a href='index.php?action=editContact&c=" . $contact['ID'] . "'>" . $contact['lname'] . ", " . $contact['fname'] . "</a></td>

									<td>" . $contact['address'] . "</td>

									<td>" . $contact['city'] . "</td>

									<td align='center'>" . $contact['state'] . "</td>

									<td align='center'>" . $contact['zip'] . "</td>

									<td align='center'><a href='index.php?action=deleteContact&delete=".$contact['ID']."' class='delCheck'>X</a></td>

								</tr>

							";
						
}
							$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";

						}

                    ?>   

                    

                    </table>



            </div>

            <!-- /contentLeftData -->

            

			<script type='text/javascript' src='<?php echo $_SESSION['relative_path']; ?>inc/common/scripts/jquery-1.5.1.min.js'></script>

			<script language="javascript" type="text/javascript">

			/* <![CDATA[ */

				$('.delCheck').click(function(event) {

					var r=confirm("Are you sure you want to delete this contact? \nThis action is irreversible.");

					if (r==false) {

						event.preventDefault();

					}		

				});

			/* ]]> */

			</script>

            

        	<div class='clear'></div> 

      		</div>

        	<!-- /contentLeft -->

            

        	<div class='contentRight'>



                <?php suggestionsWrap(); ?>



            <div class='clear'></div>

           	</div>

			<!-- /contentRight -->


			<div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
    
        	<div class='clear'></div>

            </div>

            <!-- /pageContent -->

        

        <div class='clear'></div>

        </div>

        <!-- /innerPageContentWrap -->

    

    <div class='clear'></div>

    </div>

	<!-- /pageContentWrap -->





<?php

	closePageWrapToEnd();

?>