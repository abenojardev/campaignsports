<?php

	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateAdminAcct.js"></script>
		');

    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	$password = $TeamMain->getTeamAdminPassword();

	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	$countries = Common::getCountries();

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);

	$contactDisplay = array();

	foreach($contact AS $f => $d) {
		if( isset($d) ) {
			$contactDisplay[$f] = "value='".$d."'";
		} else {
			$contactDisplay[$f] = "";
		}

		if($f == 'state') $contactDisplay[$f] = $d;
		if($f == 'country') $contactDisplay[$f] = $d;
		if($f == 'ID') $contactDisplay[$f] = $d;

		if($f == 'intl') {
			if($d) {
				$contactDisplay[$f] = "checked='checked'";
				$domStyle = "style='display:none;'";
				$intlStyle = "";
			} 

			else {
				$contactDisplay[$f] = "";
				$intlStyle = "style='display:none;'";
				$domStyle = "";
			}
		}
		//echo "f: $f, d: $d<br>";
	}

?>


        	<div class='fullwidth'>
        	    
                <div class='conleft'>
        			<div class='pbar-wrapper'>
        			    <?php showteamLogo2(); ?>
                        	<?php 
        						if ( $status == 1 ) { // Phase 1
        							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionProgressBar.php');
 
        						} 
        						else {
        							include_once($_SESSION['relative_path'] . 'inc/common/widgets/CampaignProgressBar.php');
        						}
        					?>	 

        			</div>                
                </div>		
                
                <div class='conright'>
                    <?php showteamHeaderCoach(); ?>                           
                    <div class='conrightcon'>
            <form method='post' name='frm' id='frm' action='index.php?action=account'>
              <!--
              <div>
                <input id="intl" class="intl" name="intl" type="checkbox" value=1 <?php echo $contactDisplay['intl']; ?> />
                Outside of the United States or Canada. <br />
                <br />
              </div>
              -->
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>First Name</strong></div>
                    <div class="col-9"><input name='fname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['fname']; ?> /></div>
                  </div>
              </div>
              
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Last Name</strong></div>
                    <div class="col-9"><input name='lname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['lname']; ?> /></div>
                  </div>
              </div>

              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Email</strong></div>
                    <div class="col-9"><input name='emailI' type='text' class='textFieldSWR' <?php echo $contactDisplay['email']; ?> /></div>
                  </div>
              </div>

              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Mobile</strong></div>
                    <div class="col-9"><input name='phoneCell' type='text' class='textFieldSWR' <?php echo $contactDisplay['phoneCell']; ?> /></div>
                  </div>
              </div>
              
              <div class='formElement'>
                  <div class="row">
                <div class="col-3"><strong>Title</strong></div>
                <div class="col-9"><input name='title' type='text' class='textFieldSWR' <?php echo $contactDisplay['title']; ?> /></div>
                  </div>
              </div>
              
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Address</strong></div>
                    <div class="col-9"><input name='address' type='text' class='textFieldSWR' <?php echo $contactDisplay['address']; ?> /></div>
                  </div>
              </div>
              
              
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3">&nbsp;<strong>Address 2</strong></div>
                    <div class="col-9"><input name='address2' type='text' class='textFieldSWR' <?php echo $contactDisplay['address2']; ?> /></div>
                  </div>
              </div>
              
              
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>City</strong></div>
                    <div class="col-9"><input name='city' type='text' class='textFieldSWR' <?php echo $contactDisplay['city']; ?> /></div>
                  </div>
              </div>
              
              <div id="domesticAddy" <?php echo $domStyle ?> >
                <div class='formElement'>
                  <div class="row">                    
                    <div class="col-3"><strong>State/Region:</strong></div>
                    <div class="col-9">
                        <select id='state' name='state' class='selectFieldSWR' size='1'>
                          <option value=''>Please select...</option>
                                    <option value=''>&nbsp;</option>
                                    <option value=''>---- United States ----</option>
                          <?php
                                        foreach($states as $state)
                                        {
    										$selected = "";
    										if ($state['abbrev'] == $contactDisplay['state']) $selected = "selected='selected'";
                                            echo "<option value='".$state['abbrev']."'";
                                            echo "$selected>".$state['name']."</option>";
                                        }
                                    ?>
                        </select>
                      </div>
                  </div>
                </div>
              </div>    
                
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Zip/Postal Code</strong></div>
                    <div class="col-9"><input name='zip' type='text' class='textFieldSWR' <?php echo $contactDisplay['zip']; ?> /></div>
                  </div>
              </div>
              

              
              <div id="intlAddy" <?php echo $intlStyle ?> >
                  <div class='formElement'>
                      <div class="row">
                          <div class="col-3"><strong>Province/Region</strong></div>
                          <div class="col-9"><input name='stateI' type='text' class='textFieldSWR' <?php if ($contactDisplay['state']) echo "value='".$contactDisplay['state']."'"; ?> /></div>
                      </div>
                  </div>
              </div>
              
              <div class='formElement'>
                  <div class="row">
                      <div class="col-3"><strong>Postal Code</strong></div>
                      <div class="col-9"><input name='zipI' type='text' class='textFieldSWR' <?php echo $contactDisplay['zip']; ?> /></div>
                  </div>
              </div>
              
              <div class='formElement'>
                  <div class="row">
                      <div class="col-3"><strong>Country</strong></div>
                      <div class="col-9"><select id='country' name='country' class='selectFieldSWR' size='1'>
                          <option value=''>Please select...</option>
                          <?php
                                        foreach($countries as $country)
                                        {
    										$selected = "";
    										if ($country['abbrev'] == $contactDisplay['country']) $selected = "selected=\"selected\"";
                                            echo "<option value='".$country['abbrev']."'";
                                            echo "$selected>".$country['name']."</option>";
                                        }
                                    ?>
                        </select>
                      </div>
                   </div>
              </div>    
                
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Daytime Phone</strong></div>
                    <div class="col-9"><input name='phoneDay' type='text' class='textFieldSWR' <?php echo $contactDisplay['phoneDay']; ?> /></div>
                  </div>
              </div>
              
               <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Evening Phone</strong></div>
                    <div class="col-9"><input name='phoneEve' type='text' class='textFieldSWR' <?php echo $contactDisplay['phoneEve']; ?> /></div>
                  </div>
              </div>

              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Password</strong></div>
                    <div class="col-9"><input name='coachPassword' type='text' class='textFieldSWR validate validateP1' value='<?php echo $password; ?>' /></div>
                  </div>
              </div>
              
              <div class='formElement'>
                  <div class="row">
                    <div class="col-3"><strong>Password (validate)</strong></div>
                    <div class="col-9"><input name='coachPassword2' type='text' class='textFieldSWR validate validateP2' value='<?php echo $password; ?>' /></div>
                  </div>
              </div>
              
              <p>
                <input type="hidden" name="cID" value='<?php echo $contactDisplay['ID']; ?>' />
                <input type="hidden" name="submitAction" value='adminUpdateAccount' />
                <a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update My Team Account</a> </p>
            </form>
                    </div>                    
                </div>
                
            </div>   




    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->




<?php
	closePageWrapToEnd();
?>
