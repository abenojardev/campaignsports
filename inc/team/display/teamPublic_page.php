<?php

	if ( $_SESSION["team_user"] == "player" ) {
		$loginJS = "login";
	} else if ( $_SESSION["team_user"] == "admin" ) {
		$loginJS = "loginAdmin";
	}
	
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	require_once($relPath . 'inc/team/class/TeamMain.php');
	$name = $Team->getTeamName();
	$team = $Team->getTeamType();
	
	$data = array(
		'title' => 'Support '.$name.': '.$team,
		'desc' => 'Welcome to the fundraising campaign for '.$name.': '.$team.' please share this page and help support our team!',
		'pgname' => $name.': '.$team.' Fundraising Page',
		'team' => $_SESSION["current_folder"],
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/login.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.form.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/'.$loginJS.'.js"></script>
		');
	
    startToMainPublicHeader($data);
	
	$Team2 = new TeamMain($_SESSION["current_folder"]);
	
	$public = $Team2->getTeamPublic();
	$tpwd = $Team2->getTeamPassword();
	$pageTypeDisplay = ( $_SESSION["team_user"] == "player" ) ? "Login" : "Admin Login";
	
	$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
	

?>    

    <div class='pageContentWrap teamSecondaryBGColor'>
    
        <div class='innerPageContentWrap teamPrimaryBGColor'>
        <style>
.pageContent {
	width:808px;
	padding-left:50px;
	padding-right:50px;
	padding-top:30px;
	padding-bottom:30px;
	background-color:#FFF;
		background-image: url(<?php echo $path; ?>/share/bkg-team.png);
	background-repeat: no-repeat;
	background-position: left bottom;	
}
.pFix p {
	padding-top:0px;
	padding-bottom:0px;
	margin-top:0px;
	margin-bottom:0px;
}
</style>
            <div class='pageContent'>
            
            
            
            <div class='topContent'>
<?php showteamPublicHeader(); ?>
            </div>

            <div class='contentFull'>

            <a href="https://www.joinourcampaign.com/v2/index.php?tid=<?php echo $_SESSION['current_folder']; ?>&pid=<?php echo $tpwd; ?>" class="teamSponsor teamPrimaryBGColor"><img src="<?php echo $path; ?>/share/sponsor_button.png" width="808" height="105" border="0" /></a>
              <div class="shareBar">
                    <div class="shareCol1">
                <strong> HELP US SPREAD THE WORD, SHARE OUR TEAM PAGE</strong> </div>
                    <div class="shareCol2">
                    <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_pinterest_share"></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e80bd9231c72661"></script>
<!-- AddThis Button END -->
                    </div>
              <div class="clear"></div>
              </div>
              <?php echo $public['publicCopy']; ?>
<p>&nbsp;</p>


              <a href="https://www.joinourcampaign.com/v2/index.php?tid=<?php echo $_SESSION['current_folder']; ?>&pid=<?php echo $tpwd; ?>" class="teamSponsor teamPrimaryBGColor"><img src="<?php echo $path; ?>/share/sponsor_button.png" width="808" height="105" border="0" /></a>
                    
                    <div class="donateAlso">
<table width="798" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td class="teamPrimaryTxtColor"><strong>You can also donate by:</strong></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><strong>PHONE:</strong></td>
        <td valign="top">877.524.4795<br />
               Monday - Friday<br />
               8am - 6pm EST</td>
      </tr>
    </table>
</td>
    <td valign="top">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td valign="top"><strong>MAIL:</strong></td>
        <td valign="top" class="pFix">
        		<?php echo $public['publicAddress']; ?>
               <br />
               <!--<span style="color:#F00000">Please include Athletes Name on memo line</span>--></td>
      </tr>
    </table>
    </td>
  </tr>
</table>

                    
                    </div><!-- /donateAlso -->
              
              <div class="shareBar">
                    <div class="shareCol1">
                <strong> HELP US SPREAD THE WORD, SHARE OUR TEAM PAGE</strong> </div>
                    <div class="shareCol2">
                    <!-- AddThis Button BEGIN -->
<div class="addthis_toolbox addthis_default_style addthis_16x16_style">
<a class="addthis_button_facebook"></a>
<a class="addthis_button_twitter"></a>
<a class="addthis_button_pinterest_share"></a>
<a class="addthis_button_google_plusone_share"></a>
<a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
</div>
<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4e80bd9231c72661"></script>
<!-- AddThis Button END -->
                    </div>
                    <div class="clear"></div>
                    </div>

                    
			<div class="imageBuffer">&nbsp;</div>

            <div class='clear'></div>
            </div>
            <!-- /contentRight -->

            <div class='clear'></div>
            </div>
            <!-- /pageContent -->
        
        <div class='clear'></div>
        </div>
        <!-- /innerPageContentWrap -->
    
    <div class='clear'></div>
    </div>
    <!-- /pageContentWrap -->
    
    

<?php
	closePageWrapToEnd();
?>