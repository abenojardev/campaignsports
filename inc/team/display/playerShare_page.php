<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateIntl.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/multiContacts.js"></script>
		');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');
	$Player = new Player($_SESSION['player_id']);
	$name = $Player->getName();
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	//$countries = Common::getCountries();
	$relationships = Common::getRelationships();
	$prefixes = Common::getPrefixes();
?>    


            <div class="fullwidth" style="flex-direction: column; padding: 0 20% 5% 20%; ">
                <?php showteamHeaderAthlete(); ?>       
            </div>   
            <div class="contentWrap">
              <div class="row">
                  <div class="col">
                      <h1 style="color: #275a7e; ">Share your fundraising status!</h1>
                      <br>
                  </div>
              </div>    
                
              <div class="row">
                <div class="col sharemedia">
                    <div class="row">
                        <div class="col">
                            <center><i class="fab fa-twitter fa-5x"></i></center>                      
                        </div>
                        <div class="col">
                            <center><i class="fab fa-facebook-f fa-5x"></i></center>
                        </div>
                        <div class="col">
                            <center><i class="fab fa-instagram fa-5x"></i></center>
                        </div>                     
                    </div>
                    <div class="row">
                        <div class="col">
                            <center><i class="far fa-envelope fa-5x"></i></center>
                        </div>
                        <div class="col">
                            <center><i class="fas fa-mobile-alt fa-5x"></i></center>
                        </div>
                    </div>                    

                </div>
                <div class="col" style="display: flex; justify-content: center; align-items: center; ">
                    <button class="shareMediaButton">ADD A MESSAGE</button>
                </div>
              </div>
              
              <hr>

              <div class="row">
                  <div class="col">
                      <h1 style="color: #275a7e; ">Email your fundraising status!<br>Send some encouragement</h1>
                      <br>
                  </div>
              </div>        
              
              <div class="row">
                <div class="col sharemedia">
                    <div class="row">
                        <div class="col-4">
                            <center><i class="far fa-envelope fa-5x"></i></center>
                        </div>
                        <div class="col" style="display: flex; align-items: center; ">
                            <center><h1 style="color: #275a7e; ">CONFIRM EMAILS</h1></center>
                        </div>
                    </div>                    

                </div>
                <div class="col" style="display: flex; justify-content: center; align-items: center; ">
                    <button class="shareMediaButton">ADD A MESSAGE</button>
                </div>
              </div>              
              
            </div>            

<?php
	closePageWrapToEndAthlete();
?>