<?php

	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateIntl.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/multiContacts.js"></script>
		');

    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');
	$Player = new Player($_SESSION['player_id']);
	$name = $Player->getName();
	$contact = $Player->getContact($get['c']);
	$campStatus = Player::getCampaignStatus11($_SESSION['current_folder']);
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	//$countries = Common::getCountries();
	$relationships = Common::getRelationships();
	$prefixes = Common::getPrefixes();

	$contactDisplay = array();

	foreach($contact AS $f => $d) {
		if( isset($d) ) {
			$contactDisplay[$f] = "value='".$d."'";
		} else {
			$contactDisplay[$f] = "";
		}

		if($f == 'state') $contactDisplay[$f] = $d;
		if($f == 'country') $contactDisplay[$f] = $d;
		if($f == 'ID') $contactDisplay[$f] = $d;

		if($f == 'relationship') $contactDisplay[$f] = $d;
		if($f == 'relationship2') $contactDisplay[$f] = $d;
		
		if($f == 'prefix') $contactDisplay[$f] = $d;
		if($f == 'prefix2') $contactDisplay[$f] = $d;

		if($f == 'intl') {
			if($d) {
				$contactDisplay[$f] = "checked='checked'";
				$domStyle = "style='display:none;'";
				$intlStyle = "";
			} 
			else {
				$contactDisplay[$f] = "";
				$intlStyle = "style='display:none;'";
				$domStyle = "";
			}
		}

		if($f == 'multi') {
			if($d) {
				$contactDisplay[$f] = "checked='checked'";
				$multiStyle = "";
			} 
			else {
				$contactDisplay[$f] = "";
				$multiStyle = "style='display:none;'";
			}
		}
		//echo "f: $f, d: $d<br>";

	}

?>
<?php
			if($campStatus==11) {
				$dis = "disabled";
			} else {
				$dis = "";
			}
			?>
<div class='pageContentWrap teamSecondaryBGColor'>
  <div class='innerPageContentWrap teamPrimaryBGColor'>
    <div class='pageContent'>
      <?php showteamHeader(); ?>
      <div class='topContent'>
        <div class='welcomeBar'>
          <div class="welcomeBarCol1">
            <h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $name; ?></span></h1>
          </div>
          <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>
        </div>
        <div class='clear'></div>
        <div align="center">
          <p>Review and edit your contact's information as necessary.</p>
        </div>
      </div>
      <div class='contentLeft'>
        <div class='contentLeftData'>
          <div class="logout"><a href="index.php">[back]</a></div>
          <h2 class='teamPrimaryTxtColor'>Update Contact</h2>
          <form method='post' name='frm' id='frm' action='index.php'>
            <div>
              <input id="multiple" class="multiple" name="multiple" type="checkbox" value=1 <?php echo $contactDisplay['multi']; ?> <?php echo $dis; ?> />
              Check this box if there are multiple persons living at this address <br />
              <br />
            </div>
            <div class='relationRow'>
              <div class='relationPerson' id="multipleInfo1" <?php echo $multiStyle ?>><strong>Person #1:</strong></div>
              <div class="relationshipElement">
                <div class='relationshipElementCol1'><strong>Prefix:</strong> (if applicable) <br />
                  <select id='prefix' name='prefix' class='selectFieldSWR' size='1' <?php echo $dis; ?>>
                    <option value=''>Please select...</option>
					<?php
						foreach($prefixes as $prefix)
						{
							$selected = "";
							if ($prefix['value'] == $contactDisplay['prefix']) $selected = "selected='selected'";
							echo "<option value='".$prefix['value']."'";
							echo "$selected>".$prefix['name']."</option>";
						}
					?>
                  </select>
                </div>
              </div>
              <div class="relationshipElement">
                <div class='relationshipElementCol1'><span class='alert'>*</span><strong>First Name:</strong> <br />
                  <input name='fname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['fname']; ?> <?php echo $dis; ?> />
                </div>
                <div class='relationshipElementCol2'><strong>Relationship:</strong> (if applicable) <br />
                  <select id='relationship' name='relationship' class='selectFieldSWR' size='1' <?php echo $dis; ?>>
                    <option value=''>Please select...</option>
                    <?php
						foreach($relationships as $relationship)
						{
							$selected = "";
							if ($relationship['value'] == $contactDisplay['relationship']) $selected = "selected='selected'";
							echo "<option value='".$relationship['value']."'";
							echo "$selected>".$relationship['name']."</option>";
						}
					?>
                  </select>
                </div>
              </div>
              <div class='relationshipElement'>
                <div class='relationshipElementCol1'><span class='alert'>*</span><strong>Last Name:</strong> <br />
                  <input name='lname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['lname']; ?> <?php echo $dis; ?> />
                </div>
              </div>
              <div id="multipleInfo2" <?php echo $multiStyle ?>>
                <div class='relationPerson'><strong>Person #2:</strong></div>
              <div class="relationshipElement">
                <div class='relationshipElementCol1'><strong>Prefix:</strong> (if applicable) <br />
                  <select id='prefix2' name='prefix2' class='selectFieldSWR' size='1' <?php echo $dis; ?>>
                    <option value=''>Please select...</option>
					<?php
						foreach($prefixes as $prefix)
						{
							$selected = "";
							if ($prefix['value'] == $contactDisplay['prefix2']) $selected = "selected='selected'";
							echo "<option value='".$prefix['value']."'";
							echo "$selected>".$prefix['name']."</option>";
						}
					?>
                  </select>
                </div>
              </div>
                <div class='relationshipElement'>
                  <div class='relationshipElementCol1'><span class='alert'>*</span><strong>First Name:</strong> <br/ >
                    <input name='fname2' type='text' class='textFieldSWR' <?php echo $contactDisplay['fname2']; ?> <?php echo $dis; ?> />
                  </div>
                  <div class='relationshipElementCol2'><strong>Relationship:</strong> (if applicable) <br />
                    <select id='relationship2' name='relationship2' class='selectFieldSWR' size='1' <?php echo $dis; ?>>
                      <option value=''>Please select...</option>
						<?php
							foreach($relationships as $relationship)
							{
								$selected = "";
								if ($relationship['value'] == $contactDisplay['relationship2']) $selected = "selected='selected'";
								echo "<option value='".$relationship['value']."'";
								echo "$selected>".$relationship['name']."</option>";
							}
						?>
                    </select>
                  </div>
                </div>
                <div class='relationshipElement'>
                  <div class='relationshipElementCol1'><span class='alert'>*</span><strong>Last Name:</strong> <br />
                    <input name='lname2' type='text' class='textFieldSWR' <?php echo $contactDisplay['lname2']; ?> <?php echo $dis; ?> />
                  </div>
                </div>
              </div>
              <div class='relationshipElement'>
                <div class='relationshipElementCol1'><strong>Company Name:</strong> <br />
                  <input name='company' type='text' class='textFieldSWR' <?php echo $contactDisplay['company']; ?> <?php echo $dis; ?> />
                </div>
              </div>
              <div class="clear"></div>
            </div>
            <div>
              <p><input id="intl" class="intl" name="intl" type="checkbox" value=1 <?php echo $contactDisplay['intl']; ?> <?php echo $dis; ?> /> Check this box if this is an address <strong>outside</strong> the United States or Canada.</p>
             
            </div>
            <div id="domesticAddy" <?php echo $domStyle ?> >
            <div class='formElement'>
              <div class='formElementCol1'><span class='alert'>*</span><strong>Address:</strong></div>
              <div class='formElementCol2'>
                <input name='address' type='text' class='textFieldSWR validate4' <?php echo $contactDisplay['address']; ?> <?php echo $dis; ?> />
              </div>
            </div>
            <div class='formElement'>
              <div class='formElementCol1'>&nbsp;<strong>Address 2:</strong></div>
              <div class='formElementCol2'>
                <input name='address2' type='text' class='textFieldSWR' <?php echo $contactDisplay['address2']; ?> <?php echo $dis; ?> />
              </div>
            </div>
            <div class='formElement'>
              <div class='formElementCol1'><span class='alert'>*</span><strong>City:</strong></div>
              <div class='formElementCol2'>
                <input name='city' type='text' class='textFieldSWR validate4' <?php echo $contactDisplay['city']; ?> <?php echo $dis; ?> />
              </div>
            </div>
            
              <div class='formElement'>
                <div class='formElementCol1'><span class='alert'>*</span><strong>State/Region:</strong></div>
                <div class='formElementCol2'>
                  	<select id='state' name='state' class='selectFieldSWR validate4' size='1' <?php echo $dis; ?>>
                    <option value=''>Please select...</option>
                                <option value=''>&nbsp;</option>
                                <option value=''>---- United States ----</option>
					<?php
						foreach($states as $state)
						{
							$selected = "";
							if ($state['abbrev'] == $contactDisplay['state']) $selected = "selected='selected'";
							echo "<option value='".$state['abbrev']."'";
							echo "$selected>".$state['name']."</option>";
						}
					?>
                	</select>
                   
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><span class='alert'>*</span><strong>Zip/Postal Code:</strong></div>
                <div class='formElementCol2'>
                  <input name='zip' type='text' class='textFieldSWR validate4' <?php echo $contactDisplay['zip']; ?>  <?php echo $dis; ?> />
                </div>
              </div>
              <div class='formElement'>
                <div class='formElementCol1'><strong>Email:</strong></div>
                <div class='formElementCol2'>
                  <input name='email' type='text' class='textFieldSWR' <?php echo $contactDisplay['email']; ?> <?php echo $dis; ?> />
                </div>
              </div>
            </div>
            
            
            <div id="intlAddy" <?php echo $intlStyle ?> >
            <p>For international contacts, we only require an email address so we can email them your personalized campaign brochure.</p>
              <div class='formElement'>
                <div class='formElementCol1'><span class='alert'>*</span><strong>Email:</strong></div>
                <div class='formElementCol2'>
                  <input name='emailI' type='text' class='textFieldSWR validate3' <?php echo $contactDisplay['email']; ?> <?php echo $dis; ?> />
                </div>
              </div>
            </div>
            <p>
              <input type="hidden" name="cID" value='<?php echo $contactDisplay['ID']; ?>' />
              <input type="hidden" name="submitAction" value='playerUpdateContact' />
              
              <?php
            if($campStatus==11) {
			?>
            
			<?php
			} else {
			?> 
            <a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update Contact</a>
            <?php
			}
			?>
            
               </p>
          </form>
          <div class='clear'></div>
        </div>
        <!-- /contentLeftData -->
        
        <div class='clear'></div>
      </div>
      <!-- /contentLeft -->
      
      <div class='contentRight'>
        <?php suggestionsWrap(); ?>
        <div class='clear'></div>
      </div>
      <!-- /contentRight -->
      <div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
      <div class='clear'></div>
    </div>
    <!-- /pageContent -->
    
    <div class='clear'></div>
  </div>
  <!-- /innerPageContentWrap -->
  
  <div class='clear'></div>
</div>

<!-- /pageContentWrap -->

<?php

	closePageWrapToEnd();

?>
