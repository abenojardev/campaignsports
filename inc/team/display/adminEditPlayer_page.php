<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateIntl.js"></script>
		');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	$countries = Common::getCountries();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);
	$ID = array();
	$ID['ID'] = $_GET['edit'];
	$ID['table'] = 'players';
	$contacts = TeamUser::getXyzFromID($ID);
	
	$contactDisplay = array();
	foreach($contacts AS $f => $d) {
		if( isset($d) ) {
			$contactDisplay[$f] = "value='".$d."'";
		} else {
			$contactDisplay[$f] = "";
		}
		if($f == 'state') $contactDisplay[$f] = $d;
		if($f == 'ID') $contactDisplay[$f] = $d;
		
		//echo "f: $f, d: $d<br>";
	}
	
?>    

    <div class='pageContentWrap teamSecondaryBGColor'>
    
    	<div class='innerPageContentWrap teamPrimaryBGColor'>
        
        	<div class='pageContent'>
            
            <?php showteamHeader(); ?>
			
            <div class='topContent'>
            	<div class='welcomeBar'>
                    <div class="welcomeBarCol1"><h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $contact['fname']." " .$contact['lname']; ?></span></h1></div>
                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>
                </div>
                
                <div class='clear'></div>
                
                <p>Review and edit the athlete's information as necessary. </p>
                
            </div>
            
            <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php'); ?>
            
			<div class='contentLeft'>
            
                <div class='contentLeftData'>
					<div class='registerWrap'>
                    <div class="logout"><a href="index.php?action=players">[back]</a></div>
                    <h2 class='teamPrimaryTxtColor'>Athlete Info</h2>
                      <form method='post' name='frm' id='frm' action='index.php?action=players'>

                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>First Name:</strong></div>
                                <div class='formElementCol2'><input name='fname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['fname']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Last Name:</strong></div>
                                <div class='formElementCol2'><input name='lname' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['lname']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Address 1:</strong></div>
                                <div class='formElementCol2'><input name='address' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['address']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'>&nbsp;<strong>Address 2:</strong></div>
                                <div class='formElementCol2'><input name='address2' type='text' class='textFieldSWR' <?php echo $contactDisplay['address2']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>City:</strong></div>
                                <div class='formElementCol2'><input name='city' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['city']; ?> /></div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>State:</strong></div>
                                <div class='formElementCol2'>
                                    <select id='state' name='state' class='selectFieldSWR validate' size='1'>
                                   <option value=''>Please select...</option>
                                <option value=''>&nbsp;</option>
                                <option value=''>---- United States ----</option>
                                        <?php
                                            foreach($states as $state)
                                            {
												$selected = "";
												if ($state['abbrev'] == $contactDisplay['state']) $selected = "selected='selected'";
												echo "<option value='".$state['abbrev']."'";
												echo "$selected>".$state['name']."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                          
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Zip:</strong></div>
                                <div class='formElementCol2'><input name='zip' type='text' class='textFieldSWR validate' <?php echo $contactDisplay['zip']; ?> /></div>
                            </div>
                            
                            <div class='formElement'>
                                <div class='formElementCol1'><span class='alert'>*</span><strong>Email:</strong></div>
                                <div class='formElementCol2'><input name='email' type='text' class='textFieldSWR pemail validate' <?php echo $contactDisplay['email']; ?> /></div>
                            </div>
                            
                            <div class='registerButton'>
                                  <input type="hidden" name="pID" value='<?php echo $contactDisplay['ID']; ?>' />
                                  <input type="hidden" name="submitAction" value='adminUpdatePlayer' />
                                <p><a href='#' class='teamButton teamPrimaryBGColor' id='frmSubmit'>Update / Continue</a></p>
                            </div>

                      </form>
                    </div>
                    
                </div>
                <!-- /contentLeftData -->
            
        	<div class='clear'></div> 
      		</div>
        	<!-- /contentLeft -->
            
        	<div class='contentRight'>
				<?php 
					if ( $status == 1 ) { // Phase 1
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    } 
                    else {
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');
                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');
                    }
                ?>
            <div class='clear'></div>
           	</div>
			<!-- /contentRight -->
			
            <div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
            
            
        	<div class='clear'></div>
            </div>
            <!-- /pageContent -->
        
        <div class='clear'></div>
        </div>
        <!-- /innerPageContentWrap -->
    
    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->


<?php
	closePageWrapToEnd();
?>