<?php
	// Add sorting feature to donations 
	$orderDisplayNew = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=DESC" : "&order=ASC";
	$orderDisplayOld = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=ASC" : "&order=DESC";
	
	$order = (isset($get['order'])) ? $get['order'] : 'ASC';
	
	$sort = (isset($get['sort'])) ? $get['sort'] : 'date';
	$pageSort = "&sort=$sort";
	switch($sort) {
		case 'date':
		$sortPass['s'] = 'd.donation_date';
		break;
		case 'amount':
		$sortPass['s'] = 'd.donationValue';
		break;
		case 'donor':
		$sortPass['s'] = 'd.lname';
		break;
		case 'player':
		$sortPass['s'] = 'p.lname';
		break;
		case 'type':
		$sortPass['s'] = 'd.paymentMethod';
		break;

		case 'email':
		$sortPass['s'] = 'd.email';
		break;
		
		default:
		$sortPass['s'] = 'd.donation_date';
		break;
	}
	$sortPass['o'] = $order;
	$linkSort = "index.php?action=donations".$orderDisplayNew."&sort=";
	
	
	$data = array(

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '',

		'js' => '');

	

    startToMainHeader($data);

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

	$TeamMain = new TeamMain($_SESSION['current_folder']);

	$status = $TeamMain->getTeamStatus();

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');

	$contact = TeamUser::getAdminData($_SESSION['current_folder']);

	$donationCount = TeamUser::countDonations($_SESSION['current_folder']);

	//$donations = TeamUser::getAllDonationsWithPlayer($_SESSION['current_folder']);
	$donations = TeamUser::getAllDonationsWithPlayerSort($_SESSION['current_folder'],$sortPass);

?>    



    <div class='pageContentWrap teamSecondaryBGColor'>

    

    	<div class='innerPageContentWrap teamPrimaryBGColor'>

        

        	<div class='pageContent'>

            

            <?php showteamHeader(); ?>

			

            <div class='topContent'>

            	<div class='welcomeBar'>

                    <div class="welcomeBarCol1"><h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $contact['fname']." " .$contact['lname']; ?></span></h1></div>

                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>

                </div>

                

                <div class='clear'></div>

                

                <p>On this page, you are able to review and monitor all of the donations coming into the Donations Collection system for your campaign. </p>

                

            </div>

            

            <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php'); ?>

            

			<div class='contentLeft'>

            

                <div class='contentLeftData'>

                

                    <?php include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCampaignProgress.php'); ?>

                    <?php include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachDonationUpdate.php'); ?>

                    

                    <h2 class='teamPrimaryTxtColor'>My Team Donations</h2>



					<table width='100%' border='0' cellspacing='0' cellpadding='7'>

                      <tr>

                        <td><strong><a href='<?php echo $linkSort; ?>date'>Date</a></strong></td>

                        <td><strong><a href='<?php echo $linkSort; ?>amount'>Amount</a></strong></td>

                        <td><strong><a href='<?php echo $linkSort; ?>donor'>Donor</a></strong></td>

                        <td><strong><a href='<?php echo $linkSort; ?>player'>Athlete Name</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>type'>Type</a></strong></td>

                      </tr>

                      

             <?php

			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;

				$tpages = ceil($donationCount/15);

				$min = ($page - 1) * 15 + 1;

				$max = $page * 15;

				

				$classAlternate = "bg1";

				$count = 0;

				foreach($donations AS $d) {

					$count++;

					if ($count < $min || $count > $max) continue;
					$date = date("M j, Y", strtotime($d['donation_date']));
					

					echo "

						<tr class='$classAlternate'>

							<td align='left'>" . $date . "</td>

							<td align='left'><a href='index.php?action=donations&view=" . $d['ID'] . "'>$" . $d['donationValue'] . "</a></td>

							<td>" . $d['lname'] . ", " . $d['fname'] . " </td>

							<td>" . $d['pLname'] . ", " . $d['pFname'] . " </td>
							<td>" . $d['paymentMethod'] . "</td>

						</tr>

					";

	

					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";

				}

	

			?>

                    </table>

                    	

            <?php

                echo "<div class='pagination'><br/>";

				//$reload = $_SERVER['PHP_SELF'] . "?action=" . $_SESSION["nav"];
				$reload = $_SERVER['PHP_SELF'] . "?action=" . $_SESSION["nav"].$pageSort.$orderDisplayOld;
            	include_once($_SESSION['relative_path'] . 'inc/team/layout/pagination.php');

				echo paginate($reload, $page, $tpages, 3);

				echo "</div>";

			?>

                    

                </div>

                <!-- /contentLeftData -->

            

        	<div class='clear'></div> 

      		</div>

        	<!-- /contentLeft -->

            

        	<div class='contentRight'>

				<?php 

					if ( $status == 1 ) { // Phase 1

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/manualCheckDonation.php');

                    } 

                    else {

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/manualCheckDonation.php');

                    }

                ?>

            <div class='clear'></div>

           	</div>

			<!-- /contentRight -->


			<div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
        	<div class='clear'></div>

            </div>

            <!-- /pageContent -->

        

        <div class='clear'></div>

        </div>

        <!-- /innerPageContentWrap -->

    

    <div class='clear'></div>

    </div>

	<!-- /pageContentWrap -->





<?php

	closePageWrapToEnd();

?>