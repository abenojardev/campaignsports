<?php

	$data = array(

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '',

		'js' => '');

	

    startToMainHeader($data);

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

	$TeamMain = new TeamMain($_SESSION['current_folder']);

	$status = $TeamMain->getTeamStatus();

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');

	$contact = TeamUser::getAdminData($_SESSION['current_folder']);

	$playerCount = TeamUser::countPlayers($_SESSION['current_folder']);

	

	if (isset($get['view']) && $get['view'] == 'contacts') {  

		$players = TeamUser::getAllPlayersNameSortContactCount($_SESSION['current_folder']);

	}

	else if (isset($get['view']) && $get['view'] == 'donations') {            

		$players = TeamUser::getAllPlayersNameSortDonationTotal($_SESSION['current_folder']);

	}

	else {

		$players = TeamUser::getAllPlayersNameSort($_SESSION['current_folder']);

	}

	

?>    

    <div class='pageContentWrap teamSecondaryBGColor'>

    

    	<div class='innerPageContentWrap teamPrimaryBGColor'>

        

        	<div class='pageContent'>

            

            <?php showteamHeader(); ?>

			

            <div class='topContent'>

            	<div class='welcomeBar'>

                    <div class="welcomeBarCol1"><h1>Welcome <span class="teamPrimaryTxtColor"><?php echo $contact['fname']." " .$contact['lname']; ?></span></h1></div>

                    <div class='welcomeBarCol2'>Today's date is: <span class='teamPrimaryTxtColor'><?php echo date('F d, Y'); ?></span><br /><div class="logout"><a href="index.php?action=logout">[logout]</a></div></div>

                </div>

                

                <div class='clear'></div>

                

                <p>On this page, you are able to manage and update all of your team�s athlete information. This also provides you the ability to monitor how many of your athletes have registered in the system, and have begun the Contact Collection process of the campaign. </p>

                

            </div>

            

            <?php include_once($_SESSION['relative_path'] . 'inc/team/layout/adminNav.php'); ?>

            

			<div class='contentLeft'>

            

                <div class='contentLeftData'>

                <h2 class='teamPrimaryTxtColor'>My Team Athletes</h2>



					<table width='100%' border='0' cellspacing='0' cellpadding='7'>

                      <tr>

                        <td><strong>Name</strong></td>

                        

            <?php 

				$viewDisplay = ( isset($get['view']) ) ? "&view=".$get['view'] : "";

				

            	if (isset($get['view']) && $get['view'] == 'contacts') {  

					echo "

                        <td><strong>City</strong></td>

                        <td align='center'><strong>State</strong></td>

                        <td align='center'><strong>Zip</strong></td>

                        <td align='center'><strong>Total<br />Contacts</strong></td>

                      </tr>

					";

				}

				else if (isset($get['view']) && $get['view'] == 'donations') {            

					echo "

                        <td><strong>City</strong></td>

                        <td align='center'><strong>State</strong></td>

                        <td align='center'><strong>Zip</strong></td>

                        <td align='center'><strong>Total<br />Donations</strong></td>

                      </tr>

					";

				}

				else {

					echo "

                        <td><strong>Address</strong></td>

                        <td><strong>City</strong></td>

                        <td align='center'><strong>State</strong></td>

                        <td align='center'><strong>Zip</strong></td>

                        <td align='center'><strong>DEL</strong></td>

                      </tr>

					";

				}

				

			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;

				$tpages = ceil($playerCount/15);

				$min = ($page - 1) * 15 + 1;

				$max = $page * 15;

				

				$classAlternate = "bg1";

				$count = 0;

				foreach($players AS $p) {

					$count++;

					if ($count < $min || $count > $max) continue;

					

					echo "

						<tr class='$classAlternate'>

							<td align='left'><a href='index.php?action=players&edit=" . $p['ID'] . "'>" . $p['lname'] . ", " . $p['fname'] . "</a></td>

					";

						

					if (isset($get['view']) && $get['view'] == 'contacts') {  

						echo "

							<td>" . $p['city'] . "</td>

							<td align='center'>" . $p['state'] . "</td>

							<td align='center'>" . $p['zip'] . "</td>

							<td align='center'>" . $p['countTotal'] . "</td>

						</tr>

						";

					}

					else if (isset($get['view']) && $get['view'] == 'donations') {            

						echo "

							<td>" . $p['city'] . "</td>

							<td align='center'>" . $p['state'] . "</td>

							<td align='center'>" . $p['zip'] . "</td>

							<td align='center'>$" . $p['donationTotal'] . "</td>

						</tr>

						";

					}

					else {

						echo "

							<td>" . $p['address'] . "</td>

							<td>" . $p['city'] . "</td>

							<td align='center'>" . $p['state'] . "</td>

							<td align='center'>" . $p['zip'] . "</td>

							<td align='center'><a href='index.php?action=players&delete=".$p['ID']."' class='delCheck'>X</a></td>

						</tr>

						";

					}

					

					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";

				}

	

	

                echo "</table><div class='pagination'><br/>";

				$reload = $_SERVER['PHP_SELF'] . "?action=" . $_SESSION["nav"] . $viewDisplay;

            	include_once($_SESSION['relative_path'] . 'inc/team/layout/pagination.php');

				echo paginate($reload, $page, $tpages, 3);

				echo "</div>";

			?>

                    

                </div>

                <!-- /contentLeftData -->

            

        	<div class='clear'></div> 

      		</div>

        	<!-- /contentLeft -->

            

			<script type='text/javascript' src='<?php echo $_SESSION['relative_path']; ?>inc/common/scripts/jquery-1.5.1.min.js'></script>

			<script language="javascript" type="text/javascript">

			/* <![CDATA[ */

				$('.delCheck').click(function(event) {

					var r=confirm("Are you sure you want to delete this athlete and ALL matching contacts? \nThis action is irreversible.");

					if (r==false) {

						event.preventDefault();

					}		

				});

			/* ]]> */

			</script>

                        

        	<div class='contentRight'>

				<?php 

					if ( $status == 1 ) { // Phase 1

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContacts.php');

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');

                    } 

                    else {

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachRecentContributions.php');

                        include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachOverview.php');

                    }

                ?>

            <div class='clear'></div>

           	</div>

			<!-- /contentRight -->


			<div class='clear'></div>
    <div class="playerSSLSeal"><span id="siteseal"><a href="https://seal.starfieldtech.com/verifySeal?sealID=qnwwiZFlmYGgxd6kqwM084NyYRf6KnaxAJyMZrmbiQzfOUErzbi2R" target="_blank"><img src='https://www.joinourcampaign.com/images/SSLBadge.jpg' width='180' height='79' border='0' alt='256bit Secure Socket Layer' class='sslBadge' /></a></span></div>
    
        	<div class='clear'></div>

            </div>

            <!-- /pageContent -->

        

        <div class='clear'></div>

        </div>

        <!-- /innerPageContentWrap -->

    

    <div class='clear'></div>

    </div>

	<!-- /pageContentWrap -->





<?php

	closePageWrapToEnd();

?>