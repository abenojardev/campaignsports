<?php
	/*$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/validate.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/formValidateIntl.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/multiContacts.js"></script>
		');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/Player.php');
	$Player = new Player($_SESSION['player_id']);
	$name = $Player->getName();
	
	require_once($_SESSION['relative_path'] . 'inc/common/class/Common.php');
	$states = Common::getStates();
	//$countries = Common::getCountries();
	$relationships = Common::getRelationships();
	$prefixes = Common::getPrefixes();*/

  require_once($_SESSION['relative_path'] . 'inc/team/inclides/playerDonorlist_page.php');
?>    


            <div class="fullwidth" style="flex-direction: column; padding: 0 20% 5% 20%; ">
                <?php showteamHeaderAthlete(); ?>       
            </div>   
            <div class="container">
              <div class="row">
                <div class="conleft p-3">
                    <center><h2 style="font-size: x-large;color: #31597c;">Please enter your email and/or mobile donor contact list <br> <i>We suggest a minimum of 15 contacts.  Quality over quantity </i></h2></center>
                    <br>
                    <br>
                    <table class="table">
                      <thead>
                        <tr style="background: #31597c; ">
                          <th scope="col" style=" font-size: large; color: #fff;">Name</th>
                          <th scope="col" style=" font-size: large; color: #fff;">Email</th>
                          <th scope="col" style=" font-size: large; color: #fff;">Mobile</th>
                          <th scope="col" style=" font-size: large; color: #fff;">Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td style=" font-size: large;">John Smith</td>
                          <td style=" font-size: large;">>abc@gmail.com</td>
                          <td style=" font-size: large;">111-222-333</td>
                          <td><button class="athlete-del">DELETE</button></td>
                        </tr>
                      </tbody>
                    </table>                    
                </div>
                <div class="conright p-3">
                    <table class="table table-bordered table-striped ">
                      <thead>
                        <tr>
                          <th scope="col" style=" font-size: x-large; color: #fda527;">Donor Suggestions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td><li>Former Coaches/Teachers</li></td>
                        </tr>
                        <tr>
                          <td><li>Family Doctors/Dentist</li></td>
                        </tr>
                        <tr>
                          <td><li>Attorneys</li></td>
                        </tr>
                        <tr>
                          <td><li>Local Religious Leaders/Teachers</li></td>
                        </tr>
                        <tr>
                          <td><li>Grandparents</li></td>
                        </tr>     
                        <tr>
                          <td><li>Aunts/Uncles/Cousins</li></td>
                        </tr>
                        <tr>
                          <td><li>Close Family/Friends in the Business World</li></td>
                        </tr>
                        <tr>
                          <td><li>Neighbors</li></td>
                        </tr>    
                        <tr>
                          <td><li>Work Associates </li></td>
                        </tr>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>            

<?php
	closePageWrapToEndAthlete();
?>