<?php
	// Add sorting feature to donations 
	$orderDisplayNew = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=DESC" : "&order=ASC";
	$orderDisplayOld = ( isset($get['order']) && $get['order'] == 'ASC' ) ? "&order=ASC" : "&order=DESC";
	
	$order = (isset($get['order'])) ? $get['order'] : 'ASC';
	
	$sort = (isset($get['sort'])) ? $get['sort'] : 'date';
	$pageSort = "&sort=$sort";
	switch($sort) {
		case 'date':
		$sortPass['s'] = 'd.donation_date';
		break;
		case 'amount':
		$sortPass['s'] = 'd.donationValue';
		break;
		case 'donor':
		$sortPass['s'] = 'd.lname';
		break;
		case 'player':
		$sortPass['s'] = 'p.lname';
		break;
		case 'type':
		$sortPass['s'] = 'd.paymentMethod';
		break;

		case 'email':
		$sortPass['s'] = 'd.email';
		break;

		case 'phone':
		$sortPass['s'] = 'd.phone';
		break;
		
		
		default:
		$sortPass['s'] = 'd.donation_date';
		break;
	}
	$sortPass['o'] = $order;
	$linkSort = "index.php?action=donations".$orderDisplayNew."&sort=";
	
	
	$data = array(

		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',

		'css' => '',

		'js' => '');

	

 

	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');

	$TeamMain = new TeamMain($_SESSION['current_folder']);

	$status = $TeamMain->getTeamStatus();

	

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');

	$contact = TeamUser::getAdminData($_SESSION['current_folder']);

	$donationCount = TeamUser::countDonations($_SESSION['current_folder']);

	//$donations = TeamUser::getAllDonationsWithPlayer($_SESSION['current_folder']);
	$donations = TeamUser::getAllDonationsWithPlayerSort($_SESSION['current_folder'],$sortPass);

?>    



					<table width='100%' border='0' cellspacing='0' cellpadding='7' class='athletestable'>

                      <tr>

                        <td><strong><a href='<?php echo $linkSort; ?>date'>First Name</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>amount'>Last Name</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>donor'>Email</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>player'>Mobile #</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>type'>Donations</a></strong></td>
                        <td><strong><a href='<?php echo $linkSort; ?>type'>Delete</a></strong></td>

                      </tr>

                      

             <?php

			 	$page = ( isset($_GET['page']) ) ? intval($_GET['page']) : 1;

				$tpages = ceil($donationCount/5);

				$min = ($page - 1) * 5 + 1;

				$max = $page * 5;

				

				$classAlternate = "bg1";

				$count = 0;

				foreach($donations AS $d) {

					$count++;

					if ($count < $min || $count > $max) continue;
					$date = date("M j, Y", strtotime($d['donation_date']));
					

					echo "

						<tr class='$classAlternate'>

							<td>" . $d['fname'] . "</td>
							<td>" . $d['lname'] . "</td>
							<td>" . $d['email'] . "</td>
							<td>" . $d['phone'] . "</td>

							<td align='left'><a href='index.php?action=donations&view=" . $d['ID'] . "'>$" . $d['donationValue'] . "</a></td>
							<td align='center'><button class='athlete-del'>DELETE</button></td>

		

						</tr>

					";

	

					$classAlternate = ($classAlternate == "bg2") ? "bg1" : "bg2";

				}

	

			?>

                    </table>

 

