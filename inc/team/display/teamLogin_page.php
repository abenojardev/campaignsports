<?php

	if ( $_SESSION["team_user"] == "player" ) {
		$loginJS = "login";
	} else if ( $_SESSION["team_user"] == "admin" ) {
		$loginJS = "loginAdmin";
	}

	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '
			<link rel="stylesheet" type="text/css" href="' . $_SESSION['relative_path'] . 'css/login.css" />
		',
		'js' => '
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery-1.5.1.min.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/common/scripts/jquery.form.js"></script>
			<script type="text/javascript" src="' . $_SESSION['relative_path'] . 'inc/team/scripts/'.$loginJS.'.js"></script>
		');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	$pageTypeDisplay = ( $_SESSION["team_user"] == "player" ) ? "Login" : "Admin Login";




?>    


<script type="text/javascript">
window.onload = function() {
  if (window.location == "https://csjoc.com/v6/team/2722/admin/"){
    document.querySelectorAll('#login-athlete')[0].style.display = 'none';
  } else {
    document.querySelectorAll('#login-coach')[0].style.display = 'none';
  }

  if (window.location == "https://csjoc.com/v6/team/2722/"){
    document.querySelectorAll('#login-coach')[0].style.display = 'none';
  } else {
    document.querySelectorAll('#login-athelete')[0].style.display = 'none';
  }
};
</script>


<div class="container-fluid">
    <div class='loginWrap' id='loginWrap'>
                    
        <div class='welcomeBarLogin'>
            <h1 class='logintitle' id='login-coach'>COACH</h1>
            <h1 class='logintitle' id='login-athlete'>ATHLETE</h1>
        </div>   

        <div id='loginDiv'><?php if (isset($error)) echo $error; ?></div>
            <div id='loginForm'>
                <form id='frmLogin' name='frmLogin' method='post' action='index.php?action=login'>

                    <p><input name='username' type='text' class='textFieldLogin' placeholder='Team ID#' /></p>
                            

                    <p><input name='password' type='text' class='textFieldLogin' placeholder='Password' /></p>
                            
                <button><a href='#' id='btnLogin' class='teamButton'>LOG IN</a></button>
                </form>
            <div class='mobi-help'>
                <br>
                <br>

                <hr>
                <p>Need help? <a href="tel:8775111555"><i class='fas fa-phone'></i></a> <a href="mailto:info@campaignsports.com"><i class='fas fa-envelope'></i></a> <a href="tel:8775111555"><i class='fas fa-mobile-alt'></i></a></p>
            </div>                
            </div>
        </div>
    </div>    
</div>


<?php
	closePageWrapToEnd();
?>