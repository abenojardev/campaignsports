<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);
	
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
	} 
	else {
		$team_id = $_SESSION['current_folder'];
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$playerCount = TeamUser::countPlayers($team_id);
	$contactCount = TeamUser::countContacts($team_id);
	$average = @round($contactCount / $playerCount, 1);

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$target = $Team->getTeamTarget();

	$targetSuccess = $playerCount * $target;
	
?>    


        	<div class='fullwidth'>
        	    
                <div class='conleft'>
        			<div class='pbar-wrapper'>
        			    <?php showteamLogo2(); ?>
                        	<?php 
        						if ( $status == 1 ) { // Phase 1
        							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionProgressBar.php');
 
        						} 
        						else {
        							include_once($_SESSION['relative_path'] . 'inc/common/widgets/CampaignProgressBar.php');
        						}
        					?>	 

        			</div>                
                </div>		
                
                <div class='conright'>
                    <?php showteamHeaderCoach(); ?>                           
                    <div class='container'>
                        <br>
                    	<div class='col'>
                            <h1 class='ptitle'>Brochure Preview</h1>
                            <h3 class='ptitle'>Front</h3>
                            <center><img src="https://csjoc.com/v6/images/brochuresample.jpg" style="width: 300px;"></center>
                            <br>
                            <br>
                            
                            <div class='brochure-btn'>
                                <button class='approved'>APPROVED</button> <button class='napproved'>NOT APPROVED</button>
                            </div>
                        </div>
                        
                        <div class='mobi-help show'>
                            <br>
                            <br>
            
                            <hr>
                                <center><p>Need Edits? <a href="tel:8775111555"><i class='fas fa-phone'></i></a> <a href="mailto:info@campaignsports.com"><i class='fas fa-envelope'></i></a> <a href="tel:8775111555"><i class='fas fa-mobile-alt'></i></a></p></center>
                        </div>                                     
                    </div>                    
                </div>
                
            </div>   





    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->


<?php
	closePageWrapToEnd();
?>