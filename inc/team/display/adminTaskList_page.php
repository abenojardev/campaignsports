<?php
	$data = array(
		'title' => 'Campaign Sports: Sports Team Fundraising, Sports Team Sponsorship, Fundraising Program',
		'css' => '',
		'js' => '');
	
    startToMainHeader($data);
	include_once($_SESSION['relative_path'] . 'inc/team/layout/teamStyles.php');
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$TeamMain = new TeamMain($_SESSION['current_folder']);
	$status = $TeamMain->getTeamStatus();
	
	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$contact = TeamUser::getAdminData($_SESSION['current_folder']);
	
	if ( ( isset($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"]) && ($_SESSION["masterAdmin_id"] != "") ) && 
		 ( isset($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"]) && ($_SESSION["campaign_team"] != "") ) ) {
		$team_id = $_SESSION['campaign_team'];
	} 
	else {
		$team_id = $_SESSION['current_folder'];
	}

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');
	$playerCount = TeamUser::countPlayers($team_id);
	$contactCount = TeamUser::countContacts($team_id);
	$average = @round($contactCount / $playerCount, 1);

	require_once($_SESSION['relative_path'] . 'inc/team/class/TeamMain.php');
	$target = $Team->getTeamTarget();

	$targetSuccess = $playerCount * $target;
	
?>    


        	<div class='fullwidth'>
        	    
                <div class='conleft'>
        			<div class='pbar-wrapper'>
        			    <?php showteamLogo2(); ?>
                        	<?php 
        						if ( $status == 1 ) { // Phase 1
        							include_once($_SESSION['relative_path'] . 'inc/common/widgets/coachCollectionProgressBar.php');
 
        						} 
        						else {
        							include_once($_SESSION['relative_path'] . 'inc/common/widgets/CampaignProgressBar.php');
        						}
        					?>	 

        			</div>                
                </div>		
                
                <div class='conright'>
                    <?php showteamHeaderCoach(); ?>                           
                    <div class='container'>
                    	<div class='col'>
                            <?php include_once($_SESSION['relative_path'] . 'inc/common/display/coachPages/campaignActivity_page.php'); ?>
                        </div>
                    </div>                    
                </div>
                
            </div>   





    <div class='clear'></div>
    </div>
	<!-- /pageContentWrap -->


<?php
	closePageWrapToEnd();
?>