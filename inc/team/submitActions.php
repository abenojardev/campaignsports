<?php

//	if ($post['submitAction'] == 'playerRegister') {
//		require_once($relPath . 'inc/team/class/Player.php');
//		$Player = new Player('empty');
//		$postDupe = $Player->register($post);
//	} 
	if ($post['submitAction'] == 'playerSelect') {
		$pID = $post['player'];
		if ($pID != '' || $pID == NULL) 
			$_SESSION['player_id'] = $pID;
	}
	else if ($post['submitAction'] == 'playerAddContact') {
		require_once($relPath . 'inc/team/class/Player.php');
		$Player = new Player('empty');
		$Player->addContact($post);
		$Player->benchmarkCheck();
	}
	else if ($post['submitAction'] == 'playerUpdateContact') {
		require_once($relPath . 'inc/team/class/Player.php');
		$Player = new Player('empty');
		$Player->updateContact($post);
	}

	else if ($post['submitAction'] == 'adminUpdateColors') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamColors($post);
	}
	else if ($post['submitAction'] == 'adminUpdateLogo') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$files = $_FILES;
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$result = $TeamMain->setTeamLogo();
		$_SESSION['upload_result'] = $result;
	}
	else if ($post['submitAction'] == 'adminUpdatePublic') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamPublic($post);
		
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		$dataS["ID"] = $_SESSION['current_folder'];
		$dataS["sID"] = 27;
		$dataS["pending"] = 1;
		Campaign::setCampaignStatus($dataS);
	}
	else if ($post['submitAction'] == 'adminUpdateGoal') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamGoal($post);
	}
	else if ($post['submitAction'] == 'adminUpdateTarget') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamTarget($post);
	}
	else if ($post['submitAction'] == 'adminUpdateStatus') {
		if ($post['status'] == 'a') {
			require_once($relPath . 'inc/admin/class/Admin.php');
			$Admin = new Admin($_SESSION['masterAdmin_id']);
			$Admin->archiveCampaign($_SESSION['campaign_team']);
			$Admin->deleteCampaignAfterArchive($_SESSION['campaign_team']);
		} else {
			require_once($relPath . 'inc/team/class/TeamMain.php');
			$TeamMain = new TeamMain($_SESSION['current_folder']);
			$TeamMain->setTeamStatus($post);
		}
	}
	else if ($post['submitAction'] == 'adminUpdateAccount') {
		require_once($relPath . 'inc/team/class/TeamUser.php');
		$TeamUser = new TeamUser($_SESSION['current_folder']);
		$TeamUser->updateAdminData($post);
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamAdminPassword($post);
	}
	else if ($post['submitAction'] == 'adminUpdatePlayer') {
		require_once($relPath . 'inc/team/class/TeamUser.php');
		$TeamUser = new TeamUser($_SESSION['current_folder']);
		$TeamUser->updateAdminPlayer($post);
	}
	else if ($post['submitAction'] == 'adminUpdateContact') {
		require_once($relPath . 'inc/team/class/Player.php');
		Player::updateContact($post);
	}
	else if ($post['submitAction'] == 'adminUpdateSchool') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamName($post);
	}
	else if ($post['submitAction'] == 'adminUpdateTeam') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamType($post);
	}
	else if ($post['submitAction'] == 'adminUpdatePassword') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamPassword($post);
	}
	
	
	else if ($post['submitAction'] == 'manualQuickCheckDonation') {
		require_once($relPath . 'inc/donation/class/DonationUser.php');
		DonationUser::manualQuickCheckDonation($post);
	}
	



	else if ($post['submitAction'] == 'adminUpdateBrochureInfo') {
		require_once($relPath . 'inc/team/class/TeamMain.php');
		$TeamMain = new TeamMain($_SESSION['current_folder']);
		$TeamMain->setTeamBrochureInfo($post);
	}




	else if ($post['submitAction'] == 'coachReqInfoInsert') {
		require_once($relPath . 'inc/common/class/campaign/Coach.php');
		Coach::insertInitialInfo($post);
		require_once($relPath . 'inc/common/class/files/Upload.php');
		Upload::multiUpload($post);
		$data["ID"] = $post['tID'];
		$data["sID"] = 3;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($data);
		
		// Send message to admin about info submission
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Team Info";
		$data['benchmarkID'] = 0;
		$data['message'] = "Coach has submmitted the Team Information.";
		Messaging::sendGenericMessageToAdmin($data);
	}
	else if ($post['submitAction'] == 'coachReqInfoUpdate') {
		require_once($relPath . 'inc/common/class/campaign/Coach.php');
		Coach::updateInitialInfo($post);
		require_once($relPath . 'inc/common/class/files/Upload.php');
		Upload::multiUpload($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 3;
		$data["pending"] = 0;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($data);
		
		// Send message to admin about info submission update
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Team Info";
		$data['benchmarkID'] = 0;
		$data['message'] = "Coach has resubmmitted the Team Information.";
		Messaging::sendGenericMessageToAdmin($data);
	}


	else if ($post['submitAction'] == 'coachRejectBrochureCopy') {
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 5;
		$dataS["pending"] = 1;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = "Admin";
		$data['recipID'] = 3;
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Brochure Copy";
		$data['benchmarkID'] = 0;
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}

	else if ($post['submitAction'] == 'coachApproveBrochureCopy') {
		require_once($relPath . 'inc/common/class/campaign/Coach.php');
		Coach::approveBrochureCopy($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 6;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($data);

		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = "Admin";
		$data['recipID'] = 3;
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Brochure Copy";
		$data['benchmarkID'] = 0;
		$data['subject'] = "Final Brochure Copy Approved";
		$data['message'] = "The Coach has approved the Brochure Copy!";
		Messaging::addMessage($data);
		
		// Email to designer
		require_once($relPath . 'inc/common/display/mailText.php');		
		$designer = Campaign::getDesignerInfo($post['designerID']);
		$designerName = $designer['fname']." ".$designer['lname'];
		
		require_once($relPath . 'inc/team/class/TeamMain.php');		
		$teamData = new TeamMain($post['tID']);
		$school = $teamData->getTeamName();
		$team = $teamData->getTeamType();
		$teamName = $school . ", " . $team;
		$mailVars=array("designerName"=>$designerName,"school"=>$school,"team"=>$team,"teamID"=>$post['tID']);
		$toDesignerBrochureApprovedDataReturn = toDesignerBrochureApproved($mailVars);
		
		$mailData['to'] = $designer['email'];
		$mailData['from'] = $cfg_adminEmail;
		$mailData['subject'] = $toDesignerBrochureApprovedDataReturn['toDesignerBrochureApprovedSubject'];
		$mailData['messageHTML'] = $toDesignerBrochureApprovedDataReturn['toDesignerBrochureApproved'];
		Mail::send_email($mailData);
	}


	else if ($post['submitAction'] == 'coachRejectBrochure') {
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 9;
		$dataS["pending"] = 3;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = "Admin";
		$data['recipID'] = 3;
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Brochure Design";
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}

	else if ($post['submitAction'] == 'coachApproveBrochure') {
		require_once($relPath . 'inc/common/class/campaign/Coach.php');
		Coach::approveBrochure($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 10;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($data);

		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = "Admin";
		$data['recipID'] = 3;
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Brochure Design";
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = "Final Design Approved";
		$data['message'] = "The Coach has approved the latest Brochure Design!";
		Messaging::addMessage($data);
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Designer";
		$dataM['recipID'] = $post['designerID'];
		$dataM['sender'] = "Admin";
		$dataM['senderID'] = 3;
		$dataM['benchmark'] = "Brochure Design";
		$dataM['benchmarkID'] = $post['benchmarkID'];
		$dataM['subject'] = "Final Design Approved";
		$dataM['message'] = "The Coach has approved your latest Brochure Design!";
		Messaging::addMessage($dataM);
	}


	else if ($post['submitAction'] == 'coachRejectProof') {
		$dataS["ID"] = $post['tID'];
		$dataS["sID"] = 14;
		$dataS["pending"] = 3;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($dataS);
		
		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = "Admin";
		$data['recipID'] = 3;
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Printer Proof";
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}

	else if ($post['submitAction'] == 'coachApproveProof') {
		require_once($relPath . 'inc/common/class/campaign/Coach.php');
		Coach::approveProof($post);
		
		$data["ID"] = $post['tID'];
		$data["sID"] = 15;
		require_once($relPath . 'inc/common/class/campaign/Campaign.php');
		Campaign::setCampaignStatus($data);

		// Send message
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = "Admin";
		$data['recipID'] = 3;
		$data['sender'] = "Coach";
		$data['senderID'] = $post['coachID'];
		$data['benchmark'] = "Printer Proof";
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = "Final Proof Approved";
		$data['message'] = "The Coach has approved the latest Printer Proof!";
		Messaging::addMessage($data);
		
		$dataM['tID'] = $post['tID'];
		$dataM['recip'] = "Printer";
		$dataM['recipID'] = $post['printerID'];
		$dataM['sender'] = "Admin";
		$dataM['senderID'] = 3;
		$dataM['benchmark'] = "Printer Proof";
		$dataM['benchmarkID'] = $post['benchmarkID'];
		$dataM['subject'] = "Final Proof Approved";
		$dataM['message'] = "The Coach has approved your latest Printer Proof!";
		Messaging::addMessage($dataM);
	}









	else if ($post['submitAction'] == 'messageReply') {
		require_once($relPath . 'inc/common/class/campaign/Messaging.php');
		
		$data['tID'] = $post['tID'];
		$data['recip'] = $post['recip'];
		$data['recipID'] = $post['recipID'];
		$data['sender'] = $post['sender'];
		$data['senderID'] = $post['senderID'];
		$data['benchmark'] = $post['benchmark'];
		$data['benchmarkID'] = $post['benchmarkID'];
		$data['subject'] = $post['subject'];
		$data['message'] = $post['message'];
		Messaging::addMessage($data);
	}
?>

