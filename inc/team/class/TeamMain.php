<?php
/****************************************************************************************
TeamMain.php
Defines the TeamMain class.

Application: Campaign Sports
Trail Associates, May 2011
Artisan Digital Studios, July 2017
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class TeamMain {
	
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				$query = array('select' => "*", 
							   'tbl' => "teams", 
							   'where' => "ID=$ID");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				foreach($result as $field => $data) {
					$this->$field = $data;
				}
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating TeamMain: ' . $e->getMessage() );
			}
		}
	} // __construct()


	public function getTeamName() { 
		return str_replace("�","'",$this->name);
	}

	public function getTeamType() {
		return $this->team;
	}

	public function setTeamName($post) {
		try {
			$set = "name='" . $post['name'] ."'";
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamName(): ' . $e->getMessage() );
		}
//		echo "name";
	} // setTeamName()

	public function setTeamType($post) {
		try {
			$set = "team='" . $post['team'] ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamType(): ' . $e->getMessage() );
		}
	} // setTeamType()


	public function getTeamStatus() {
		return $this->status;
	}
	
	public function setTeamStatus($post) {
		try {
			$set = "status='" . $post['status'] ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamStatus(): ' . $e->getMessage() );
		}
	} // setTeamStatus()
	

	public function getTeamPassword() {
		return $this->password;
	}

	public function getTeamAdminPassword() {
		return $this->adminPassword;
	}


	public function getTeamColors() {
		$color['bgColor1'] = $this->bgColor1;
		$color['bgColor2'] = $this->bgColor2;
		$color['txtColor'] = $this->txtColor;
		
		return $color;
	}

	public function setTeamColors($post) {
		try {
			$i = 0;
			$set = "";
			$updateArray = array('bgColor1', 'bgColor2', 'txtColor'); 
			
			foreach($updateArray AS $data) {
				if ($i == 0) { $set .= $data . "='" . substr($post[$data], 1)."'"; } 
				else { $set .= "," . $data . "='" . substr($post[$data], 1)."'"; }
				$i = 1;
			}
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamColors(): ' . $e->getMessage() );
		}
		
		return $result;
	} // setTeamColors()
	
	public function getTeamPublic() {
		$public['publicCopy'] = $this->pageContent;
		$public['publicAddress'] = $this->pageMailAddress;
		
		return $public;
	}
	
	public function setTeamPublic($post) {
		try {
 
			$pCopy=$post['publicCopy'];
			$pAddress=$post['publicAddress'];

			$set = "pageContent='" . $pCopy ."'";
			$set2 = "pageMailAddress='" . $pAddress ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
			$query2 = array('tbl' => "teams", 
						   'set' => $set2,
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB2 = new DB();
			$result2 = $DB2->update_single($query2);
			
			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			$dataS["ID"] = $_SESSION['current_folder'];
			$dataS["sID"] = 26;
			$dataS["pending"] = 0;
			Campaign::setCampaignStatus($dataS);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamPublic(): ' . $e->getMessage() );
		}
		
		return $result;
	} // setTeamPublic()
	

	public function getTeamGoal() {
		return $this->goal;
	}

	public function setTeamGoal($post) {
		try {
			$set = "goal='" . $post['goal'] ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamGoal(): ' . $e->getMessage() );
		}
	} // setTeamGoal()


	public function getTeamTarget() {
		return $this->target;
	}

	public function setTeamTarget($post) {
		try {
			$set = "target='" . $post['target'] ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamTarget(): ' . $e->getMessage() );
		}
	} // setTeamTarget()


	public function setTeamLogo() {
		try {
			$DB = new DB();
			$name = $DB->clean_string($_FILES["logoUpload"]['name']);
			$name = str_replace(" ", "", $name);
			$name = strtolower($name);
			list($file, $extension) = explode('.', $name);
		
			include_once($_SESSION['relative_path'] . 'inc/common/path/path.php');
			
			$path = $_SESSION['relative_path'] . "team/" . $_SESSION["current_folder"];
			$folder = $_SESSION['current_folder'];
			$fullPath =  $_SESSION['siteRoot'] . "/team/$folder";
			
			if ( $_FILES["logoUpload"]["error"] == UPLOAD_ERR_OK ) {
				if ( $extension != "jpeg"
					&& $extension != "jpg"
					&& $extension != "gif"
					&& $extension != "png" ) throw new Exception("Invalid Image Type. Must be jpg, gif or png file.");
				
				if ($_FILES["logoUpload"]["size"] > 512000) throw new Exception("Image too large. Max avatar size of 500kb.");
				
				if ( is_file($path . "/logo.gif") ) unlink($path . "/logo.gif");
				if ( is_file($path . "/logo.jpg") ) unlink($path . "/logo.jpg");
				if ( is_file($path . "/logo.jpeg") ) unlink($path . "/logo.jpeg");
				if ( is_file($path . "/logo.png") ) unlink($path . "/logo.png");
				
				$file_name = "logo.$extension";
				$tmp_name = $_FILES["logoUpload"]["tmp_name"];
				move_uploaded_file($tmp_name, "$fullPath/$file_name");
				
				return "Upload successful.";
			}
			else {
				return $_FILES["logoUpload"]["error"];
			}
			
		} catch ( Exception $e ) {
			return $e->getMessage();
		}
		
	} // setTeamLogo()

	public function setTeamPassword($post) {
		try {
			$set = "password='" . $post['password'] ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamPassword(): ' . $e->getMessage() );
		}
	} // setTeamPassword()

	public function setTeamAdminPassword($post) {
		try {
			$set = "adminPassword='" . $post['coachPassword'] ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamAdminPassword(): ' . $e->getMessage() );
		}
	} // setTeamAdminPassword()


	// NEW STUFF
	//
	public function getTeamMailDate() {
		return $this->brochure_mailing;
	}

	public function setTeamMailDate($post) {
		try {
			$date = date("Y-m-d", strtotime($post['brochure_mailing'])); 
		
			$set = "brochure_mailing='" . $date ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamMailDate(): ' . $e->getMessage() );
		}
	} // setTeamMailDate()
	
	public function setPrinterTeamMailDate($post) {
		try {
			$date = date("Y-m-d", strtotime($post['brochure_mailing'])); 
		
			$set = "brochure_mailing='" . $date ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $post['tID']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamMailDate(): ' . $e->getMessage() );
		}
	} // setTeamMailDate()
	
	public function getTeamBrochureInfo() {
		$brochureInfo{'brochure_count'} = $this->brochure_count;
		$brochureInfo{'brochure_cost'} = $this->brochure_cost;
		$brochureInfo{'brochure_charges'} = $this->brochure_charges;
		
		return $brochureInfo;
	}

	public function setTeamBrochureInfo($post) {
		try {
			$brochure_count = $post['brochure_count']; 
			$brochure_cost = $post['brochure_cost']; 
			$brochure_charges = $post['brochure_charges']; 
		
			$set = "brochure_count='" . $brochure_count ."', brochure_cost='" . $brochure_cost ."', brochure_charges='" . $brochure_charges ."'";
			
			$query = array('tbl' => "teams", 
						   'set' => $set, 
						   'where' => "ID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; setTeamBrochureInfo(): ' . $e->getMessage() );
		}
	} // setTeamBrochureInfo()

	public function getTeamDesigner() {
		return $this->designerID;
	}

	public function getTeamPrinter() {
		return $this->printerID;
	}

	public function getTeamBaseCost() {
		return $this->v2_base_cost_per_brochure;
	}
	public function getTeamBaseCostReason() {
		return $this->v2_base_cost_edit_reason;
	}

}