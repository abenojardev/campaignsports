<?php
/****************************************************************************************
TeamUser.php
Defines the UserTeam class.

Application: Campaign Sports
Trail Associates, May 2011
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class TeamUser {
	
	protected $xxx;
	private $zzz;

	public function __construct() {
		try {
			$ID = $_SESSION["current_folder"];
			if ($ID == NULL)
				return NULL;
				
			//echo "TEST";
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error instantiating Intake: ' . $e->getMessage() );
		}
	} // __construct()
	

	public function playerLogin($post) {
		try {
			if ( $post['username'] != $_SESSION["current_folder"] ) {
				$loginFailure = 'The username does not match the url, please check your records and try again.';
			}
			else if($post['username']!='' && $post['password']!='') {
				$query = array('select' => "*", 
							   'tbl' => "teams", 
							   'where' => "ID = '".$post['username']."' AND password = '".$post['password']."' AND active = 1");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result) {
					$_SESSION["team_id"] = $result['ID'];
					$loginFailure = NULL;
				}
				else  {
					$loginFailure = 'No matching records were found, please try again.';
				}		
			}
			else {
				$loginFailure = 'Please enter your username and password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; playerLogin(): ' . $e->getMessage() );
		}
	} // playerLogin()


	public function playerLogout() {
		unset($_SESSION["team_id"]);
		unset($_SESSION["player_id"]);
	} // playerLogout()
	
	
	public function adminLogin($post) {
		try {
			if ( $post['username'] != $_SESSION["current_folder"] ) {
				$loginFailure = 'The username does not match the url, please check your records and try again.';
			}
			else if($post['username']!='' && $post['password']!='') {
				$query = array('select' => "*", 
							   'tbl' => "teams", 
							   'where' => "ID = '".$post['username']."' AND adminPassword = '".$post['password']."' AND active = 1");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result) {
					$query2 = array('select' => "*", 
								   'tbl' => "team_contacts", 
								   'where' => "tID=".$result['ID']);
					$DB2 = new DB();
					$result2 = $DB2->select_single($query2);
					
					
					$_SESSION["team_id"] = $_SESSION["current_folder"];
					$_SESSION["admin_id"] = $result2['ID'];
					$loginFailure = NULL;
				}
				else  {
					$loginFailure = 'No matching records were found, please try again.';
				}		
			} 
			else {
				$loginFailure = 'Please enter your username and password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; adminLogin(): ' . $e->getMessage() );
		}
	} // adminLogin()


	public function adminLogout() {
		unset($_SESSION["team_id"]);
		unset($_SESSION["admin_id"]);
	} // adminLogout()
	
	
	public function getAdminData($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "team_contacts", 
						   'where' => "tID=$ID");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAdminData(): ' . $e->getMessage() );
		}
	} // getAdminData()


	public function updateAdminData($post) {
		try {
			$i = 0;
			$set = "";
			
			$contactArray = array('fname', 'lname', 'title', 'address', 'address2', 'city', 'phoneDay', 'phoneEve', 'phoneCell'); 
			
			foreach($contactArray AS $data) {
				if ($i == 0) {
					$set .= $data . "='" . $post[$data]."'";
				} else {
					$set .= "," . $data . "='" . $post[$data]."'";
				}
				$i = 1;
			}
			
			if (isset($post['intl']) && $post['intl']) {
				$set .= ",state='" . $post['stateI']."'";
				$set .= ",zip='" . $post['zipI']."'";
				$set .= ",email='" . $post['emailI']."'";
				$set .= ",country='" . $post['country']."'";
				$set .= ",intl='1'";
			} else {
				$set .= ",state='" . $post['state']."'";
				$set .= ",zip='" . $post['zip']."'";
				$set .= ",email='" . $post['email']."'";
				$set .= ",intl='0'";
			}
			
			$query = array('tbl' => "team_contacts", 
						   'set' => $set, 
						   'where' => "tID=" . $_SESSION['current_folder']);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateAdminData(): ' . $e->getMessage() );
		}
	} // updateAdminData()
	
	public function updateAdminPlayer($post) {
		try {
			$i = 0;
			$set = "";
			
			$contactArray = array('fname', 'lname', 'address', 'address2', 'city', 'state', 'zip', 'email'); 
			
			foreach($contactArray AS $data) {
				if ($i == 0) {
					$set .= $data . "='" . $post[$data]."'";
				} else {
					$set .= "," . $data . "='" . $post[$data]."'";
				}
				$i = 1;
			}
			
			$query = array('tbl' => "players", 
						   'set' => $set, 
						   'where' => "ID=" . $post["pID"]);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateAdminPlayer(): ' . $e->getMessage() );
		}
	} // updateAdminPlayer()
	
	
	public function deleteAdminPlayer($id) {
		try {
			$query = array('tbl' => "players",
						   'where' => "ID=$id");
			$DB = new DB();
			$result[0] = $DB->delete_records($query);
			
			$query2 = array('tbl' => "contacts",
						   'where' => "pID=$id");
			$DB = new DB();
			$result[1] = $DB->delete_records($query2);
			
			return $result[0];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; deleteAdminPlayer(): ' . $e->getMessage() );
		}
	} // deleteAdminPlayer()
	

	public function getXyzFromID($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => $ID['table'], 
						   'where' => "ID = ".$ID['ID']);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getXyzFromID(): ' . $e->getMessage() );
		}
	} // getXyzFromID()

	
	public function getAllDonations($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "donations", 
						   'sort' => "donation_date DESC", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllDonations(): ' . $e->getMessage() );
		}
	} // getAllDonations()
	
	public function getAllFailedDonations($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "donations_failed", 
						   'sort' => "donation_date DESC", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllFailedDonations(): ' . $e->getMessage() );
		}
	} // getAllFailedDonations()

	public function getAllDonationsWithPlayer($ID) {
		try {
			$query = array('select' => "d.*, p.fname AS pFname, p.lname AS pLname", 
							'tbl' => "donations AS d INNER JOIN players AS p ON d.pID = p.ID",
							'sort' => "d.donation_date DESC", 
							'where' => "d.tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllDonationsWithPlayer(): ' . $e->getMessage() );
		}
	} // getAllDonationsWithPlayer()

	public function getAllDonationsWithPlayerSort($ID,$sortPass) {
		try {
			
			$sort = $sortPass['s'] . " " . $sortPass['o'];
			$query = array('select' => "d.*, p.fname AS pFname, p.lname AS pLname", 
							'tbl' => "donations AS d INNER JOIN players AS p ON d.pID = p.ID",
							//'sort' => "d.donation_date DESC", 
							'sort' => $sort,
							'where' => "d.tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllDonationsWithPlayer(): ' . $e->getMessage() );
		}
	} // getAllDonationsWithPlayer()
	public function getAllPlayers($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "players", 
						   'sort' => "register_date", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllPlayers(): ' . $e->getMessage() );
		}
	} // getAllPlayers()

	public function getAllPlayersNameSort($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "players", 
						   'sort' => "lname ASC", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllPlayersNameSort(): ' . $e->getMessage() );
		}
	} // getAllPlayersNameSort()

	public function getAllPlayersNameSortContactCount($ID) {
		try {
			$query = array('select' => "p.*, (SELECT COUNT(c.ID) FROM contacts c WHERE c.pID = p.ID) AS countTotal", 
						   'tbl' => "players p WHERE p.tID = ".$ID, 
						   'sort' => "p.lname, p.fname");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllPlayersNameSortContactCount(): ' . $e->getMessage() );
		}
	} // getAllPlayersNameSortContactCount()

	public function getAllPlayersNameSortDonationTotal($ID) {
		try {
			$query = array('select' => "p.*, (SELECT ROUND(SUM(d.donationValue),2) FROM donations d WHERE d.pID = p.ID) AS donationTotal", 
						   'tbl' => "players p WHERE p.tID = ".$ID, 
						   'sort' => "p.lname, p.fname");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllPlayersNameSortDonationTotal(): ' . $e->getMessage() );
		}
	} // getAllPlayersNameSortDonationTotal()

	public function getAllContacts($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "contacts", 
						   'sort' => "register_date", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllContacts(): ' . $e->getMessage() );
		}
	} // getAllContacts()
	
	public function getAllContactsByDate($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "contacts", 
						   'sort' => "register_date DESC", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllContacts(): ' . $e->getMessage() );
		}
	} // getAllContacts()
	
	public function getAllContactsNameSort($ID) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "contacts", 
						   'sort' => "lname, fname", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllContactsNameSort(): ' . $e->getMessage() );
		}
	} // getAllContactsNameSort()

	public function getAllContactsWithPlayerNameSort($ID) {
		try {
			$query = array('select' => "c.ID, c.fname, c.lname, c.city, c.state, p.fname AS pFname, p.lname AS pLname", 
							'tbl' => "contacts AS c INNER JOIN players AS p ON c.pID = p.ID",
							'sort' => "pLname, pFname, c.lname, c.fname", 
							'where' => "c.tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllContactsWithPlayerNameSort(): ' . $e->getMessage() );
		}
	} // getAllContactsWithPlayerNameSort()

	
	public function countPlayers($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "players", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countPlayers(): ' . $e->getMessage() );
		}
	} // countPlayers()

	public function countContacts($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "contacts", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContacts(): ' . $e->getMessage() );
		}
	} // countContacts()

	public function countContactsCanada($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "contacts", 
						   'where' => "tID = ".$ID." AND canada = 1");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContactsCanada(): ' . $e->getMessage() );
		}
	} // countContactsCanada()
	
	public function countContactsIntl($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "contacts", 
						   'where' => "tID = ".$ID." AND intl=1");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContactsIntl(): ' . $e->getMessage() );
		}
	} // countContactsIntl()
	
	public function countContactsDomestic($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "contacts", 
						   'where' => "tID = ".$ID." AND canada IS NULL AND intl IS NULL");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContactsDomestic(): ' . $e->getMessage() );
		}
	} // countContactsDomestic()

	public function countContactsExactDupes($ID) {
		try {
			$query = array('select' => "prefix, fname, lname, relationship, multi, prefix2, fname2, lname2, relationship2, company, address, address2, city, state, zip, email, canada, intl, country, COUNT(*) AS count", 
						   'tbl' => "contacts",
						   'where' => "tID = ".$ID." GROUP BY prefix, fname, lname, relationship, multi, prefix2, fname2, lname2, relationship2, company, address, address2, city, state, zip, email, canada, intl, country HAVING COUNT(*) > 1");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContacts(): ' . $e->getMessage() );
		}
	} // countContacts()

	public function countContactsBadNames($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "contacts", 
						   'where' => "fname LIKE '' OR lname LIKE '' AND tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContactsDomestic(): ' . $e->getMessage() );
		}
	} // countContactsDomestic()
	
	public function countContactsBadAddresses($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "contacts", 
						   'where' => "fname LIKE '' OR lname LIKE '' AND tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countContactsDomestic(): ' . $e->getMessage() );
		}
	} // countContactsDomestic()
	

	public function countDonations($ID) {
		try {
			$query = array('select' => "COUNT(*) AS count", 
						   'tbl' => "donations", 
						   'where' => "tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result['count'];
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; countDonations(): ' . $e->getMessage() );
		}
	} // countDonations()

	
	public function getTopPlayers($ID) {
		try {
			$query = array('select' => "c.ID, c.tID, c.pID, COUNT(c.ID) AS countField, p.fname, p.lname, p.register_date", 
							'sort' => "countField DESC",
							'tbl' => "contacts AS c INNER JOIN players AS p ON c.pID = p.ID GROUP BY c.pID HAVING c.tID = ".$ID);
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getTopPlayers(): ' . $e->getMessage() );
		}
	} // getTopPlayers()

	public function getTopAchievers($ID) {
		try {
			$query = array('select' => "d.ID, d.tID, d.pID, ROUND(SUM(d.donationValue),2) AS donationTotal, p.fname, p.lname, p.ID AS pID", 
							'sort' => "donationTotal DESC",
							'tbl' => "donations AS d LEFT JOIN players AS p ON d.pID = p.ID GROUP BY d.pID HAVING d.tID = ".$ID);
			
			
			
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getTopAchievers(): ' . $e->getMessage() );
		}
	} // getTopAchievers()

	public function getDonationTotal($ID) {
		try {
			$query = array('select' => "ID, tID, ROUND(SUM(donationValue),2) AS donationTotal", 
							'tbl' => "donations",
						   'where' => "tID = ".$ID);
			
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getDonationTotal(): ' . $e->getMessage() );
		}
	} // getDonationTotal()

	public function getDonationData($ID) {
		try {
			$query = array('select' => "d.*, p.fname AS pFname, p.lname AS pLname", 
							'tbl' => "donations AS d LEFT JOIN players AS p ON d.pID = p.ID",
						   'where' => "d.ID = ".$ID);
			
			
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getDonationData(): ' . $e->getMessage() );
		}
	} // getDonationData()


	public function getCampaignStatus11($c) {
		try {
			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			$campaignStatuses = Campaign::getCampaignStatuses($c);
			//$data["sID"] = 'HI';
			if ($campaignStatuses[11]) {
				//$data["ID"] = $_SESSION["current_folder"];
				$data["sID"] = 11;
				//Campaign::setCampaignStatus($data);
			}
			return $data["sID"];
		}  catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCampaignStatus11(): ' . $e->getMessage() );
		}
	} // getCampaignStatus()
}

?>