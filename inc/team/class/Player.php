<?php
/****************************************************************************************
Player.php
Defines the Player class.

Application: Campaign Sports
Trail Associates, May 2011
****************************************************************************************/
include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
include_once($_SESSION['relative_path'] . 'inc/common/class/Mail.php');

class Player {
	
	protected $xxx;
	private $zzz;

	public function __construct( $ID ) {
		if($ID == 'empty') {
			
		} else {
			try {
				$query = array('select' => "*", 
							   'tbl' => "players", 
							   'where' => "ID=$ID");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				foreach($result as $field => $data) {
					$this->$field = $data;
				}
			} catch ( Exception $e ) {
				throw new Exception( 'Error instantiating Player: ' . $e->getMessage() );
			}
		}
	} // __construct()
	
	
	public function register($post) {
		try {
			// Check for Player in System
			$query = array('select' => "*", 
						   'tbl' => "players", 
						   'where' => "fname='".$post['fname']."' AND lname='".$post['lname']."' AND tID=".$_SESSION["current_folder"]);
			$DB = new DB();
			$result = $DB->select_single($query);
			
			// Duplicate player
			if ($result) {
				unset($_SESSION["team_id"]);
				$error = "That name is already in the system.<br />Please login again and look for your name in the drop-down list.";
				return $error;
			}
			
			// Insert record
			$i = 0;
			$fields = $values = "";
			
			if ( $_SESSION['current_folder'] > $post['cfg_playerPassCutoffTeam'] )
				$registerArray = array('fname', 'lname', 'address', 'address2', 'city', 'state', 'zip', 'email', 'password'); 
			else
				$registerArray = array('fname', 'lname', 'address', 'address2', 'city', 'state', 'zip', 'email'); 
			
			foreach($registerArray as $registerData) {
				if ($i == 0) {
					$fields .= $registerData;
					$values .= "'".$post[$registerData]."'";
				} else {
					$fields .= "," . $registerData;
					$values .= ",'" . $post[$registerData]."'";
				}
				$i = 1;
				
			}
			$fields .= ",tID";
			$values .= "," . $_SESSION["current_folder"];
			
			$query = array('tbl' => "players", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			// Successful insert
			if ($result)
			{
				$_SESSION["player_id"] = $result;
			}
			else
			{
				throw new Exception('Could not complete your request, please contact the administrator.');
			}		
			
		} catch ( Exception $e ) {
			//throw new Exception( 'Error with Method; register(): ' . $e->getMessage() );
			return $e->getMessage();
		}
	} // register()
	
	
	public function loginLevel2($post) {
		try {
			if($post['player']!='' && $post['password']!='') {
				$query = array('select' => "*", 
							   'tbl' => "players", 
							   'where' => "ID = '".$post['player']."' AND password = '".$post['password']."'");
				$DB = new DB();
				$result = $DB->select_single($query);
				
				if ($result) {
					$_SESSION["player_id"] = $result['ID'];
					$loginFailure = NULL;
				}
				else  {
					$loginFailure = 'No matching records were found, please try again.';
				}		
			}
			else {
				$loginFailure = 'Please select your name and enter your password';
			} 
			
			return $loginFailure;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; loginLevel2(): ' . $e->getMessage() );
		}
	} // loginLevel2()
	
	
	public function forgotPassword() {
		GLOBAL $cfg_adminEmail;
		$password = $this->password;
		$email = $this->email;
		
		$mailData['to'] 		= $email;
		$mailData['from']		= $cfg_adminEmail;
		$mailData['subject']	= "Campaign Sports Password Reminder";
		$mailData['messageHTML']	= 
			'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Untitled Document</title>
</head>

<body>
<table style="border: 1px solid #cccccc; width: 550px;" _mce_style="border: 1px solid #cccccc; width: 550px;" border="0" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td align="left" valign="top"><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif" alt="" border="0" height="25" width="1"><br>
        <table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
          <tbody>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td><img src="http://www.joinourcampaign.com/system_emails/images/campaign-sports.jpg" alt="" height="108" width="550"></td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"><br>

                <p>Greetings,</p>

                <p><strong>Thank you for your participation in your team\'s Campaign Sports fundraiser!</strong></p>
                <p>Here is your password as requested:<br>
                <strong>'.$password.'</strong></p>
                
                <p><strong>We wish you and your team great success!</strong></p>
                <p>Sincerely,</p>
<p>The Campaign Sports Staff<br>
                <a style="color: #115a7f;"  href="mailto:support@campaignsports.com" target="_blank">support@campaignsports.com</a></p></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
            <tr>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
              <td><table  style="width: 550px;" _mce_style="width: 550px;" border="0" cellpadding="0" cellspacing="0">
                  <tbody>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"   align="center"><strong style="color: #f88e10;">Follow Us Online</strong> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a style="color: #115a7f;"  href="http://www.CampaignSports.com" target="_blank">www.CampaignSports.com</a> <br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="10" width="550"><br>
                        <a href="https://www.facebook.com/CampaignSports"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_facebook.gif" width="25" height="26" border="0" alt="Campaign Sports on Facebook"></a>&nbsp;&nbsp; <a href="https://www.twitter.com/CampaignSports1"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_twitter.gif" width="25" height="26" border="0" alt="Campaign Sports on Twitter"></a>&nbsp;&nbsp; <a href="https://www.linkedin.com/company/campaign-sports-llc"><img src="https://www.joinourcampaign.com/system_emails/donor/images/icon_linkedin.gif" width="25" height="26" border="0" alt="Campaign Sports on LinkedIn"></a><br>
                        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="40" width="550"><br></td>
                    </tr>
                    <tr>
                      <td style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #666; line-height: 1.25em;"  >&copy; '.date("Y").' Campaign Sports,&nbsp; Manalapan, New Jersey - Delray Beach, Florida - All rights reserved. </td>
                    </tr>
                  </tbody>
                </table></td>
              <td><img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="1" width="30"></td>
            </tr>
          </tbody>
        </table>
        <img src="http://www.joinourcampaign.com/system_emails/images/trans.gif"  alt="" border="0" height="30" width="550"><br></td>
    </tr>
  </tbody>
</table>
</body>
</html>
';
		$mail = Mail::send_email($mailData);
	} // forgotPassword()



	public function getName() {
		return $this->fname . " " . $this->lname;
	}

	public function getAllPlayers() {
		try {
			$ID = $_SESSION['current_folder'];
			
			$query = array('select' => "*", 
						   'tbl' => "players", 
						   'sort' => "fname, lname",
						   'where' => "tID=$ID");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			foreach($result as $field => $data) {
				$this->$field = $data;
			}
			
			return $this;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllPlayers(): ' . $e->getMessage() );
		}
	} // getAllPlayers()

	public function benchmarkCheck() {
		try {
			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			$campaignStatuses = Campaign::getCampaignStatuses($_SESSION["current_folder"]);
			
			if (!$campaignStatuses[8]) {
				$data["ID"] = $_SESSION["current_folder"];
				$data["sID"] = 8;
				Campaign::setCampaignStatus($data);
				
				// email to coach that contact collection has started
				require_once($_SESSION['relative_path'] . 'inc/common/display/mailText.php');		
				require_once($_SESSION['relative_path'] . 'inc/team/class/TeamUser.php');		
				$coach = TeamUser::getAdminData($_SESSION['current_folder']);
				$coachName = $coach['fname']." ".$coach['lname'];
				
				$mailVars=array("coachName"=>$coachName);
				$contactCollectionStartedDataReturn = contactCollectionStarted($mailVars);
				
				$mailData['to'] = $coach['email'];
				$mailData['from'] = $cfg_adminEmail;
				$mailData['subject'] = $contactCollectionStartedDataReturn['contactCollectionStartedSubject'];
				$mailData['messageHTML'] = $contactCollectionStartedDataReturn['contactCollectionStarted'];
				Mail::send_email($mailData);
			}
		} catch ( Exception $e ) {
			//throw new Exception( 'Error with Method; benchmarkCheck(): ' . $e->getMessage() );
			return $e->getMessage();
		}
	} // benchmarkCheck()

	public function addContact($post) {
		try {
			// Insert record
			$i = 0;
			$fields = $values = "";
			
			$registerArray = array('prefix', 'fname', 'lname', 'relationship', 'company', 'address', 'address2', 'city'); 
			
			foreach($registerArray as $registerData) {
				if ($i == 0) {
					$fields .= $registerData;
					$values .= "'".$post[$registerData]."'";
				} else {
					$fields .= "," . $registerData;
					$values .= ",'" . $post[$registerData]."'";
				}
				$i = 1;
				
			}
			$fields .= ",pID";
			$values .= "," . $_SESSION["player_id"];
			
			$fields .= ",tID";
			$values .= "," . $_SESSION["current_folder"];
			
			if (isset($post['multiple']) && $post['multiple']) {
				$fields .= ",prefix2";
				$values .= ",'".$post['prefix2']."'";
				$fields .= ",fname2";
				$values .= ",'".$post['fname2']."'";
				$fields .= ",lname2";
				$values .= ",'".$post['lname2']."'";
				$fields .= ",relationship2";
				$values .= ",'".$post['relationship2']."'";
				$fields .= ",multi";
				$values .= ",'1'";
			} else {
				$fields .= ",prefix2";
				$values .= ",''";
				$fields .= ",fname2";
				$values .= ",''";
				$fields .= ",lname2";
				$values .= ",''";
				$fields .= ",relationship2";
				$values .= ",''";
			}
			
			if (isset($post['intl']) && $post['intl']) {
				$fields .= ",state";
				$values .= ",'".$post['stateI']."'";
				$fields .= ",zip";
				$values .= ",'".$post['zipI']."'";
				$fields .= ",email";
				$values .= ",'".$post['emailI']."'";
				$fields .= ",country";
				$values .= ",'".$post['country']."'";
				$fields .= ",intl";
				$values .= ",'1'";
			} else {
				$fields .= ",state";
				$values .= ",'".$post['state']."'";
				$fields .= ",zip";
				$values .= ",'".$post['zip']."'";
				$fields .= ",email";
				$values .= ",'".$post['email']."'";
			}
			if (($post['state']=='AB') || ($post['state']=='BC') || ($post['state']=='MB') || ($post['state']=='NB') || ($post['state']=='NL') || ($post['state']=='NT') || ($post['state']=='NS') || ($post['state']=='NU') || ($post['state']=='ON') || ($post['state']=='PE') || ($post['state']=='QC') || ($post['state']=='SK') || ($post['state']=='YT')) {
				$fields .= ",canada";
				$values .= ",'1'";

			}
			$query = array('tbl' => "contacts", 
						   'fields' => $fields, 
						   'values' => $values);
			$DB = new DB();
			$result = $DB->insert_single($query);
			
			// Successful insert
			if ($result)
			{
				
			}
			else
			{
				throw new Exception('Could not complete your request, please contact the administrator.');
			}		
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; addContact(): ' . $e->getMessage() );
		}
	} // addContact()
	
	public function getAllContacts2($id) {
		try {
			$ID = $id;
			
			$query = array('select' => "*",
						   'tbl' => "contacts",
						   'sort' => "fname, lname",
						   'where' => "pID=$ID");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			foreach($result as $field => $data) {
				$contacts->$field = $data;
			}
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllContacts(): ' . $e->getMessage() );
		}
	} // getAllContacts()
	
	public function getAllContacts() {
		try {
			$ID = $_SESSION['player_id'];
			
			$query = array('select' => "*",
						   'tbl' => "contacts",
						   'sort' => "fname, lname",
						   'where' => "pID=$ID");
			$DB = new DB();
			$result = $DB->select_multi($query);
			
			foreach($result as $field => $data) {
				$contacts->$field = $data;
			}
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getAllContacts(): ' . $e->getMessage() );
		}
	} // getAllContacts()
	
	public function getContact($c) {
		try {
			$query = array('select' => "*", 
						   'tbl' => "contacts", 
						   'where' => "ID=$c");
			$DB = new DB();
			$result = $DB->select_single($query);
			
			return $result;
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getContact(): ' . $e->getMessage() );
		}
	} // getContact()
	
	public function updateContact($post) {
		try {
			// Insert record
			$i = 0;
			$set = "";
			
			$contactArray = array('prefix', 'fname', 'lname', 'relationship', 'company', 'address', 'address2', 'city'); 
			
			foreach($contactArray AS $data) {
				if ($i == 0) {
					$set .= $data . "='" . $post[$data]."'";
				} else {
					$set .= "," . $data . "='" . $post[$data]."'";
				}
				$i = 1;
			}
			
			if (isset($post['multiple']) && $post['multiple']) {
				$set .= ",prefix2='" . $post['prefix2']."'";
				$set .= ",fname2='" . $post['fname2']."'";
				$set .= ",lname2='" . $post['lname2']."'";
				$set .= ",relationship2='" . $post['relationship2']."'";
				$set .= ",multi='1'";
			} else {
				$set .= ",prefix2=''";
				$set .= ",fname2=''";
				$set .= ",lname2=''";
				$set .= ",relationship2=''";
				$set .= ",multi='0'";
			}

			if (isset($post['intl']) && $post['intl']) {
				$set .= ",state='" . $post['stateI']."'";
				$set .= ",zip='" . $post['zipI']."'";
				$set .= ",email='" . $post['emailI']."'";
				$set .= ",country='" . $post['country']."'";
				$set .= ",canada=NULL";
				$set .= ",intl='1'";
			} else {
				$set .= ",state='" . $post['state']."'";
				$set .= ",zip='" . $post['zip']."'";
				$set .= ",email='" . $post['email']."'";
				$set .= ",intl=NULL";
			}
			if (($post['state']=='AB') || ($post['state']=='BC') || ($post['state']=='MB') || ($post['state']=='NB') || ($post['state']=='NL') || ($post['state']=='NT') || ($post['state']=='NS') || ($post['state']=='NU') || ($post['state']=='ON') || ($post['state']=='PE') || ($post['state']=='QC') || ($post['state']=='SK') || ($post['state']=='YT')) {
				$set .= ",canada='1'";

			}
			$query = array('tbl' => "contacts", 
						   'set' => $set, 
						   'where' => "ID=" . $post["cID"]);
			$DB = new DB();
			$result = $DB->update_single($query);
			
		} catch ( Exception $e ) {
			throw new Exception( 'Error with Method; updateContact(): ' . $e->getMessage() );
		}
	} // updateContact()
	
	public function removeContact($id) {
		try {
			$query = array('tbl' => "contacts",
						   'where' => "ID=$id");
			$DB = new DB();
			$result = $DB->delete_records($query);

		} catch ( Exception $e ) {
			//throw new Exception( 'Error with Method; removeContact(): ' . $e->getMessage() );
			return $e->getMessage();
		}
	} // removeContact()
	
	
	public function getCampaignStatus11($c) {
		try {
			require_once($_SESSION['relative_path'] . 'inc/common/class/campaign/Campaign.php');
			$campaignStatuses = Campaign::getCampaignStatuses($c);
			//$data["sID"] = 'HI';
			if ($campaignStatuses[11]) {
				//$data["ID"] = $_SESSION["current_folder"];
				$data["sID"] = 11;
				//Campaign::setCampaignStatus($data);
			}
			return $data["sID"];
		}  catch ( Exception $e ) {
			throw new Exception( 'Error with Method; getCampaignStatus11(): ' . $e->getMessage() );
		}
	} // getCampaignStatus()

	
}