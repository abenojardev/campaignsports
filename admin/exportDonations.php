<?php
    session_start(); /* if not already done */
	
 	$get = $_GET;
	$team = $get['id'];
	
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', d.tID AS 'teamCode',";
	$query .= " d.fname, d.lname, d.address, d.address2, d.city, d.state, CONCAT('=\"', d.zip, '\"') as zip, d.email, d.ID,";
	$query .= " d.donationValue, d.donation_date, d.paymentMethod";
	$query .= " FROM donations d";
	$query .= " INNER JOIN players p ON d.pID = p.ID";
	$query .= " WHERE d.tID = $team";
	$query .= " ORDER BY d.donation_date";
//	print $query;
	$DB = new DB();
	$donations = $DB->select_custom($query);
 
	$contents = getExcelData($donations);
	
	//print_r($contents);
	//break;
	
	$filename = "campaignDonations".$team.".xls";

	//prepare to give the user a Save/Open dialog...
	header("Cache-Control: cache, must-revalidate");
	header("Pragma: public");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Transfer-Encoding: binary");
	
	// Setting the cache expiration to 30 seconds ahead of current time.
	// An IE 8 issue when opening the data directly in the browser without first saving it to a file
	$expiredate = time() + 30;
	$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
	header ($expireheader);

	//output the contents
	echo $contents;
	exit;
?>

<?php
	function getExcelData($data){
		$retval = "";
		if (is_array($data)  && !empty($data))
		{
			$row = 0;
			foreach($data as $_data){
			if (is_array($_data) && !empty($_data))
			{
				if ($row == 0)
				{
					// write the column headers
					$retval = implode("\t",array_keys($_data));
					$retval .= "\n";
					}
					//create a line of values for this row...
					$retval .= implode("\t",array_values($_data));
					$retval .= "\n";
					//increment the row so we don't create headers all over again
					$row++;
				}
			}
		}
		return $retval;
	}
?>
