<?php
    session_start(); /* if not already done */
	
 	$post = $_POST;
	$date1 = $post['qbDate1'];
	$date2 = $post['qbDate2'];
	
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
	$query = "SELECT CONCAT(t.name,':',t.team) AS 'Customer', d.donation_date AS 'Date',";
	$query .= " d.paymentMethod AS 'Method', d.donationValue AS 'Amount',";
	$query .= " 'Undeposited Funds' AS 'Deposit To', 'Accounts Receivable' AS 'AR Account', d.ID";
	$query .= " FROM donations d";
	$query .= " INNER JOIN teams t ON d.tID = t.ID";
	$query .= " WHERE (d.paymentMethod = 'online' OR d.paymentMethod = 'PayPal') AND d.donation_date >= '$date1' AND d.donation_date <= '$date2'";
	$query .= " ORDER BY d.donation_date";
	//print $query;
	
	$DB = new DB();
	
	$donations = $DB->select_custom($query);
	//print_r($donations);
	
	$contents = getExcelData($donations);
	//print_r($contents);
	//break;
	
	$filename = "qb_export.xls";

	//prepare to give the user a Save/Open dialog...
	header("Cache-Control: cache, must-revalidate");
	header("Pragma: public");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Transfer-Encoding: binary");
	
	// Setting the cache expiration to 30 seconds ahead of current time.
	// An IE 8 issue when opening the data directly in the browser without first saving it to a file
	$expiredate = time() + 30;
	$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
	header ($expireheader);

	//output the contents
	echo $contents;
	exit;
?>

<?php
	function getExcelData($data){
		$retval = "";
		if (is_array($data)  && !empty($data))
		{
			$row = 0;
			foreach($data as $_data){
				if (is_array($_data) && !empty($_data))
				{
					if ($row == 0)
					{
						// write the column headers
						$retval = implode("\t",array_keys($_data));
						$retval .= "\n";
					}
					
					//create a line of values for this row...
					$retval .= implode("\t",array_values($_data));
					$retval .= "\n";
					//increment the row so we don't create headers all over again
					$row++;
				}
			}
		}
		return $retval;
	}
?>
