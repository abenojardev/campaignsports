<?php
    session_start(); /* if not already done */
	
 	$get = $_GET;
	$team = $get['id'];
	
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
// Original	
//	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', CONCAT('team',c.tID) AS 'teamCode', CONCAT('player',c.pID) AS 'playerCode',";
//	$query .= " c.fname, c.lname, c.address, c.address2, c.city, c.state, CONCAT('=\"', c.zip, '\"') as zip, c.email, c.ID";
//	$query .= " FROM contacts c";
//	$query .= " INNER JOIN players p ON c.pID = p.ID";
//	$query .= " WHERE c.tID = $team";

// Before changes for relationships/multiple persons
//	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', c.tID AS 'teamCode', t.password AS 'password',";
//	$query .= " c.fname, c.lname, c.address, c.address2, c.city, c.state, CONCAT('=\"', c.zip, '\"') as zip, c.email, c.ID";
//	$query .= " FROM contacts c";
//	$query .= " INNER JOIN players p ON c.pID = p.ID";
//	$query .= " INNER JOIN teams t ON c.tID = t.ID";
//	$query .= " WHERE c.tID = $team";
	
	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', c.tID AS 'teamCode', t.password AS 'password',";
	$query .= " c.prefix, c.fname, c.lname, c.relationship, c.prefix2, c.fname2, c.lname2, c.relationship2, c.address, c.address2, c.city, c.state, CONCAT('=\"', c.zip, '\"') as zip, c.email, c.ID";
	$query .= " FROM contacts c";
	$query .= " INNER JOIN players p ON c.pID = p.ID";
	$query .= " INNER JOIN teams t ON c.tID = t.ID";
	$query .= " WHERE c.tID = $team";
	
	$DB = new DB();
	$contacts = $DB->select_custom($query);
 
	$contents = getExcelData($contacts);
	
	//print_r($contents);
	//break;
	
	$filename = "campaign".$team.".xls";

	//prepare to give the user a Save/Open dialog...
	header("Cache-Control: cache, must-revalidate");
	header("Pragma: public");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Transfer-Encoding: binary");
	
	// Setting the cache expiration to 30 seconds ahead of current time.
	// An IE 8 issue when opening the data directly in the browser without first saving it to a file
	$expiredate = time() + 30;
	$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
	header ($expireheader);

	//output the contents
	echo $contents;
	exit;
?>

<?php
	function getExcelData($data){
		$retval = "";
		if (is_array($data)  && !empty($data))
		{
			$row = 0;
			foreach($data as $_data){
			if (is_array($_data) && !empty($_data))
			{
				if ($row == 0)
				{
					// write the column headers
					$retval = implode("\t",array_keys($_data));
					$retval .= "\n";
					}
					//create a line of values for this row...
					$retval .= implode("\t",array_values($_data));
					$retval .= "\n";
					//increment the row so we don't create headers all over again
					$row++;
				}
			}
		}
		return $retval;
	}
?>
