<?php
    session_start(); /* if not already done */
	
 	// Code for deleting entire entry
	//
	$post = $_POST;
	if (isset($post['delCheck'])) {
		include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
		$query = array('tbl' => "contacts", 
					   'where' => "ID=" . $post['ID']);
		$DB = new DB();
		$result = $DB->delete_records($query);
	}
	
 	$get = $_GET;
	$team = $get['id'];
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
	$query = "SELECT CONCAT(p.fname,' ',p.lname) AS 'playerName', c.tID AS 'teamCode',";
	$query .= " c.prefix, c.fname, c.lname, c.relationship, c.prefix2, c.fname2, c.lname2, c.relationship2, c.address, c.address2, c.city, c.state, c.zip, c.country, c.email, c.ID";
	$query .= " FROM contacts c";
	$query .= " INNER JOIN players p ON c.pID = p.ID";
	$query .= " INNER JOIN teams t ON c.tID = t.ID";
	$query .= " WHERE c.tID = $team";
	
	$DB = new DB();
	$contacts = $DB->select_custom($query);
?>

<html>
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="../css/init.css" />    
    <link rel="stylesheet" type="text/css" media="screen" href="xEdit/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.css" />    
    <link rel="stylesheet" type="text/css" media="screen" href="xEdit/css/jqueryui-editable.css" />    

	<script src="xEdit/jquery-ui-1.9.2.custom/js/jquery-1.8.3.js" type="text/javascript"></script>
    <script src="xEdit/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.js" type="text/javascript"></script>    
    <script src="xEdit/js/jqueryui-editable-inline.js" type="text/javascript"></script>
    <script type="text/javascript">
		function validateDelete() {
			if (confirm('Are you sure you want to delete? This action is permanent.')) {
				// Save it!
			} else {
				return false;
			}			
		}	
	</script>
<style>
 body {
	 font-size:12px;
	 font-family:Arial, Helvetica, sans-serif;
 }
 .bg1 {
	 background-color:#ffffff;
 }
 .bg2 {
	 background-color:#ebeaea;
 }
 .scrubTable {
	 border:1px solid #666;
 }
 input[type=submit] {
    width:20px;
    height:20px;
	padding:0px;
	margin:0px;
	font-size:12px;
}
</style>   
</head>
<body>


<?php	
	$output = "<table width='100%' border='1' cellspacing='0' cellpadding='3'>";
	$b = true;
	$row = 0;
    foreach($contacts as $contact) {
			$class = ($b) ? "bg1" : "bg2";
			$b = !$b;
		if ($row == 0) {
			$keyArray = preg_grep("/ID/i",array_keys($contact),PREG_GREP_INVERT);
			
			//$output .= "<tr><th>Delete</th><th>";
			//$output .= implode("</th><th>",$keyArray);
			//$output .= "</th></tr><tbody>";
			$output .= "<tr>
			<th>Delete</th>
			<th>Athlete</th>
			<th>Team</th>
			<th>Prefix 1</th>
			<th>Fname 1</th>
			<th>Lname 1</th>
			<th>Relation 1</th>
			<th>Prefix 2</th>
			<th>Fname 2</th>
			<th>Lname 2</th>
			<th>Relation 2</th>
			<th>Address</th>
			<th>Address 2</th>
			<th>City</th>
			<th>State</th>
			<th>Zip</th>
			<th>Ctry</th>
			<th>Email</th>
			</tr><tbody>";
		}
		
		$output .= "<tr class='".$class."'>";

		$output .= "<td align='center' class='".$class."'>
						<form name='frm_".$contact['ID']."' method='post' action='' onsubmit='return validateDelete()'>
						<input type='hidden' name='delCheck' value=1>
						<input type='hidden' name='ID' value='".$contact['ID']."'>
						<input type='submit' value='X'>
						</form>
					</td>";

		foreach($contact as $key => $value) {
			if ($key != 'ID') {
				if ($key == 'playerName' || $key == 'teamCode' || $key == 'password')
					$output .= "<td class=".$class.">$value</td>";
				else 
					$output .= "<td class=".$class."><a id='".$key."' data-type='text' data-pk='".$contact['ID']."' data-url='scrubberUpdate.php' data-original-title='".$key."'>$value</a></td>";
			}
		}
		$output .= "</tr>";
			
		$row++;
    }
	
	$output .= "</tbody></table>";
	
	echo $output;

?>


<script>
	$('a').each(function() {
		$(this).editable();
	});
</script>


</body>
</html>	
