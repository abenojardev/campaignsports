<?php
    session_start(); /* if not already done */
	
	include_once($_SESSION['relative_path'] . 'inc/common/class/DB.php');
	
	$type = $_GET['type'];
	
	if ($type == 'date') {
    	$query = "SELECT d.ID, d.pID, p.ID, d.donationValue, d.donation_date, d.paymentMethod,";
    	$query .= " CONCAT(p.fname,' ',p.lname) AS 'playerName', d.tID AS 'teamCode',";
    	$query .= " d.fname, d.lname, d.address, d.address2, d.city, d.state, CONCAT('=\"', d.zip, '\"') as zip, d.email";
    	$query .= " FROM donations d";
    	$query .= " INNER JOIN players p ON d.pID = p.ID";
    	$query .= " WHERE d.donation_date BETWEEN '".$_GET['date_from']." 00:00:00' and '".$_GET['date_to']." 00:00:00'";
    	$query .= " ORDER BY d.donation_date";
	    $filename = "Donations_".$_GET['date_from']."to".$_GET['date_to'].".xls";
	} else if ($type == 'campaign') {
    	$query = "SELECT d.ID, d.pID, p.ID, d.donationValue, d.donation_date, d.paymentMethod,";
    	$query .= " CONCAT(p.fname,' ',p.lname) AS 'playerName', d.tID AS 'teamCode',";
    	$query .= " d.fname, d.lname, d.address, d.address2, d.city, d.state, CONCAT('=\"', d.zip, '\"') as zip, d.email";
    	$query .= " FROM donations d";
    	$query .= " INNER JOIN players p ON d.pID = p.ID";
    	$query .= " WHERE d.tID='".$_GET['cID']."'";
    	$query .= " ORDER BY d.donation_date";
    	$filename = "CampaignDonation_".$_GET['cID'].".xls";
	} else {
	    if($_GET['state'] == 'all'){
        	$query = "SELECT * FROM team_contacts"; 
	    } else {
        	$query = "SELECT * FROM team_contacts where state='".$_GET['state']."'"; 
	    }
    	$filename = "CoacheEmailList.xls";
	}
	 
	$DB = new DB();
	$donations = $DB->select_custom($query);
 
	$contents = getExcelData($donations);
	
// 	print_r($contents);
// 	break;
	$date = date("m-d-y");

	//prepare to give the user a Save/Open dialog...
	header("Cache-Control: cache, must-revalidate");
	header("Pragma: public");
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=".$filename);
	header("Content-Transfer-Encoding: binary");
	
	// Setting the cache expiration to 30 seconds ahead of current time.
	// An IE 8 issue when opening the data directly in the browser without first saving it to a file
	$expiredate = time() + 30;
	$expireheader = "Expires: ".gmdate("D, d M Y G:i:s",$expiredate)." GMT";
	header ($expireheader);

	//output the contents
	echo $contents;
	exit;
?> 


<?php
	function getExcelData($data){
		$retval = "";
		if (is_array($data)  && !empty($data))
		{
			$row = 0;
			foreach($data as $_data){
			if (is_array($_data) && !empty($_data))
			{
				if ($row == 0)
				{
					// write the column headers
					$retval = implode("\t",array_keys($_data));
					$retval .= "\n";
					}
					//create a line of values for this row...
					$retval .= implode("\t",array_values($_data));
					$retval .= "\n";
					//increment the row so we don't create headers all over again
					$row++;
				}
			}
		}
		return $retval;
	}
?>
